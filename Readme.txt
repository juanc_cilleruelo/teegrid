Welcome to TeeGrid
www.steema.com
------------------

Installing:

Use the included TeeGridRecompile.exe to compile and install
packages for design-time support (RAD Studio 2009 and up) and
Lazarus / FPC 3.0.

TeeGridRecompile adapts and modifies package sources depending
on the IDE version, including or excluding features that are 
not available for each IDE.

Documentation:

See the "Docs\Starting Guide" document for a technical
introduction and basic examples.

Support:

Please visit our website for technical help:

http://www.steema.com/support_options



