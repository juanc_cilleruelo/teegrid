{*********************************************}
{  TeeGrid Software Library                   }
{  TRows class                                }
{  Copyright (c) 2016 by Steema Software      }
{  All Rights Reserved                        }
{*********************************************}
unit Tee.Grid.Rows;
{$I Tee.inc}

interface

{
   This unit implements a TRows class, derived from TGridBand.

   TRows class paints multiple rows of column cells.

   It also includes a "Children[]" property, with optional sub-rows for
   each row.

   This enables implementing hierarchical "rows-inside-rows".
}

uses
  {System.}Classes,

  {$IFNDEF FPC}
  {System.}Types,
  {$ENDIF}

  {System.}SysUtils,

  Tee.Format, Tee.Painter, Tee.Renders,
  Tee.Grid.Data, Tee.Grid.Columns, Tee.Grid.Bands,
  Tee.Grid.Selection;

type
  // Formatting properties to paint odd row cells
  TAlternateFormat=class(TVisibleFormat)
  public
    Constructor Create(const AChanged:TNotifyEvent); override;

    function ShouldPaint:Boolean;
  published
    property Visible default False;
  end;

  TCellHover=class(TGridSelection)
  private
    procedure InitFormat;
  end;

  TRowsBands=class(TGridBands)
  private
    function GetRow(const Index:Integer):TGridBand;
    procedure SetRow(const Index:Integer; const ABand:TGridBand);
  protected
    Rows : Array of TGridBand;
  public
    function Height(const ARow: Integer): Single;
    function Hit(const X,Y:Single):TGridBand;
    function RowOf(const ABand:TGridBand):Integer;
    procedure Swap(const A,B:Integer);

    property Row[const Index:Integer]:TGridBand read GetRow write SetRow;
  end;

  // Grid Band to paint multiple rows of cells
  TRows=class(TGridBandLines)
  private
    FAlternate: TAlternateFormat;
    FBack : TFormat;
    FHeight: TCoordinate;
    FHover: TCellHover;
    FRowLines: TStroke;
    FSpacing: TCoordinate;

    IData : TVirtualData;

    IHeights : Array of Single;
    ISpacing : Single;

    FChildren : TRowsBands;
    FSubBands : TRowsBands;

    function CalcColumnHeight(const AColumn:TColumn; const AText:String): Single;
    function GetHeights(const Index: Integer): Single;
    procedure SetAlternate(const Value: TAlternateFormat);
    procedure SetBack(const Value: TFormat);
    procedure SetHeight(const Value: TCoordinate);
    procedure SetHeights(const Index: Integer; const Value: Single);
    procedure SetHover(const Value: TCellHover);
    procedure SetSpacing(const Value: TCoordinate);
    procedure SetRowLines(const Value: TStroke);
  protected
    procedure PaintRow(var AData:TRenderData; const ARender: TRender);
  public
    DefaultHeight : Single;
    Painter : TPainter;
    VisibleColumns : TVisibleColumns;

    // Temporary:
    XOffset : Single;
    XSpacing : Single;
    Scroll : TPointF;

    Constructor Create(ACollection:TCollection); override;

    {$IFNDEF AUTOREFCOUNT}
    Destructor Destroy; override;
    {$ENDIF}

    procedure Assign(Source:TPersistent); override;

    function AllHeightsEqual:Boolean;
    function AutoHeight(const ARow: Integer): Single; overload;
    function BottomOf(const ARow:Integer):Single;
    procedure CalcDefaultHeight;
    procedure CalcYSpacing(const AHeight:Single);
    procedure Clear;
    function Count:Integer;
    function DraggedColumn(const X:Single; const AColumn:TColumn):TColumn;
    function FirstVisible:Integer;
    function FontOf(const AColumn:TColumn):TFont;
    function HeightOf(const ARow:Integer):Single;
    function IsChildrenVisible(const Sender:TRender; const ARow: Integer): Boolean;
    function MaxBottom: Single;
    procedure Paint(var AData:TRenderData; const ARender:TRender); override;
    procedure PaintLines(const AData:TRenderData; const FirstRow:Integer; Y:Single);
    function RowAt(const Y, AvailableHeight: Single): Integer;
    procedure SetColumnsLeft(const ALeft:Single);
    procedure Swap(const A,B:Integer);
    function TopOf(const ARow:Integer):Single;
    function TopOfRow(const ARow:Integer):Single;
    function TotalHeight(const ARow:Integer):Single;
    function UpToRowHeight(const ARow:Integer):Single;

    property Children:TRowsBands read FChildren;
    property Data:TVirtualData read IData write IData;
    property Heights[const Index:Integer]:Single read GetHeights write SetHeights;
    property SubBands:TRowsBands read FSubBands;
    property YSpacing:Single read ISpacing;
  published
    property Alternate:TAlternateFormat read FAlternate write SetAlternate;
    property Back:TFormat read FBack write SetBack;
    property Height:TCoordinate read FHeight write SetHeight;
    property Hover:TCellHover read FHover write SetHover;
    property RowLines:TStroke read FRowLines write SetRowLines;
    property Spacing:TCoordinate read FSpacing write SetSpacing;
  end;

implementation

{$IFDEF FPC}
uses
  LCLType;
{$ELSE}
{$IFNDEF NOUITYPES}
uses
  System.UITypes;
{$ENDIF}
{$ENDIF}


{ TAlternateFormat }

Constructor TAlternateFormat.Create(const AChanged:TNotifyEvent);
begin
  inherited;

  InitVisible(False);

  Stroke.InitVisible(False);
  Brush.InitColor(TColor($F0F0F0) {TColors.Gainsboro});
  Brush.InitVisible(True);
end;

function TAlternateFormat.ShouldPaint: Boolean;
begin
  result:=Visible and (Brush.Visible or Stroke.Visible);
end;

{ TCellHover }

procedure TCellHover.InitFormat;

  procedure Init(const AFormat:TFormat; const AColor:TColor);
  begin
    AFormat.Brush.InitVisible(False);
    AFormat.Stroke.Brush.InitColor(AColor);
    AFormat.Stroke.InitStyle(TStrokeStyle.Solid);
  end;

begin
  PaintText:=False;
  CheckScroll:=False;

  Init(Format,TColors.Aqua);

  Init(UnFocused.Format,TColors.DkGray);

  UnFocused.Format.Stroke.InitVisible(True);
end;

{ TRows }

Constructor TRows.Create(ACollection:TCollection);
begin
  inherited;

  FBack:=TFormat.Create(Changed);
  FBack.Brush.InitVisible(False);
  FBack.Stroke.InitVisible(False);

  FHeight:=TCoordinate.Create(Changed);
  FHeight.InitValue(0); // <-- Set automatic to False

  FAlternate:=TAlternateFormat.Create(Changed);

  FSpacing:=TCoordinate.Create(Changed);

  FRowLines:=TStroke.Create(Changed);
  FRowLines.Brush.InitColor($D0D0D0);

  FHover:=TCellHover.Create(Changed);
  FHover.InitFormat;

  FSubBands:=TRowsBands.Create(Self,Changed);
  FChildren:=TRowsBands.Create(Self,Changed);
end;

{$IFNDEF AUTOREFCOUNT}
Destructor TRows.Destroy;
begin
  FHeight.Free;

  FChildren.Free;
  FSubBands.Free;

  FHover.Free;
  FRowLines.Free;
  FSpacing.Free;
  FBack.Free;
  FAlternate.Free;

  inherited;
end;
{$ENDIF}

procedure TRows.Clear;
begin
  Scroll.X:=0;
  Scroll.Y:=0;

  VisibleColumns.Clear;

  IHeights:=nil;
  FChildren.Clear;
  FSubBands.Clear;
end;

function TRows.Count: Integer;
begin
  if IData=nil then
     result:=0
  else
     result:=IData.Count;
end;

procedure TRows.Assign(Source: TPersistent);
begin
  if Source is TRows then
  begin
    Alternate:=TRows(Source).FAlternate;
    Back:=TRows(Source).FBack;
    Height:=TRows(Source).FHeight;
    Hover:=TRows(Source).FHover;
    RowLines:=TRows(Source).FRowLines;
    Spacing:=TRows(Source).FSpacing;
  end;

  inherited;
end;

function TRows.CalcColumnHeight(const AColumn:TColumn; const AText:String): Single;
var tmpRender : TRender;
    tmpFormat : TFormat;
begin
  Painter.SetFont(FontOf(AColumn));

  result:=Painter.TextHeight(AText);

  if AColumn.HasFormat then
     tmpFormat:=AColumn.Format
  else
     tmpFormat:=Format;

  // if tmpFormat.Brush.Picture.Valid then
  //    if Picture.Height>result then
  //       result:=Picture.Height;

  if AColumn.HasRender then
     tmpRender:=AColumn.Render
  else
     tmpRender:=Render;

  if tmpRender is TTextRender then
     result:=result+TTextRender(tmpRender).Margins.Vertical;

  if tmpFormat.Stroke.Visible then
     result:=result+2*tmpFormat.Stroke.Size;
end;

// Returns the maximum Height of all visible columns Cells, for a given row
function TRows.AutoHeight(const ARow: Integer): Single;

  function Calc(const AColumn: TColumn): Single;
  var tmp : String;
  begin
    tmp:=Data.AsString(AColumn,ARow);

    if tmp='' then
       tmp:='0';

    result:=CalcColumnHeight(AColumn,tmp);
  end;

var tmp : Single;
    t : Integer;
begin
  if FHeight.Automatic then
  begin
    result:=0;

    for t:=0 to VisibleColumns.Count-1 do
    begin
      tmp:=Calc(VisibleColumns[t]);

      if tmp>result then
         result:=tmp;
    end;
  end
  else
    result:=HeightOf(ARow);
end;

procedure TRows.CalcYSpacing(const AHeight:Single);
begin
  if RowLines.Visible then
     ISpacing:=RowLines.Size
  else
     ISpacing:=0;

  Spacing.Prepare(AHeight);
  ISpacing:=ISpacing+Spacing.Pixels;
end;

procedure TRows.CalcDefaultHeight;

  function MaxColumnHeight:Single;
  var t : Integer;
      tmp : Single;
  begin
    result:=0;

    for t:=0 to VisibleColumns.Count-1 do
    begin
      tmp:=CalcColumnHeight(VisibleColumns[t],'0');

      if tmp>result then
         result:=tmp;
    end;
  end;

var tmp : Single;
begin
  if VisibleColumns.Count=0 then
     tmp:=Format.Font.Size*2
  else
  if VisibleColumns.AllSameFormat then
     tmp:=CalcColumnHeight(VisibleColumns[0],'0')
  else
     tmp:=MaxColumnHeight;

  DefaultHeight:=tmp;
end;

function TRows.HeightOf(const ARow: Integer): Single;
begin
  result:=Heights[ARow];

  if result=0 then
  begin
    result:=FHeight.Pixels;

    if result=0 then
       result:=DefaultHeight;
  end;
end;

function TRows.FontOf(const AColumn:TColumn):TFont;
begin
  if AColumn.HasFormat then
     result:=AColumn.Format.Font
  else
     result:=Format.Font;
end;

function TRows.DraggedColumn(const X:Single; const AColumn:TColumn):TColumn;
var tmpPrevious : TColumn;
begin
  result:=nil;

  if AColumn<>nil then
  begin
    if Abs(X-AColumn.Right)<3 then
       result:=AColumn
    else
    begin
      tmpPrevious:=VisibleColumns.Previous(AColumn);

      if tmpPrevious<>nil then
         if Abs(X-AColumn.Left)<3 then
            result:=tmpPrevious;
    end;
  end;
end;

function TRows.FirstVisible:Integer;

  function DifferentHeights:Integer;
  var t : Integer;
      Y : Single;
  begin
    result:=0;

    Y:=0;

    for t:=0 to Count-1 do
    begin
      Y:=Y+TotalHeight(t);

      if Y<Scroll.Y then
         Inc(result)
      else
         break;
    end;
  end;

  function AllEqual:Integer;
  begin
    result:=Trunc(Scroll.Y/(DefaultHeight+ISpacing));
  end;

begin
  if Scroll.Y>0 then
  begin
    if AllHeightsEqual then
       result:=AllEqual
    else
       result:=DifferentHeights;
  end
  else
    result:=0;
end;

function TRows.GetHeights(const Index: Integer): Single;
begin
  if Length(IHeights)>Index then
     result:=IHeights[Index]
  else
     result:=0;
end;

function TRows.BottomOf(const ARow:Integer):Single;
begin
  result:=UpToRowHeight(ARow)+TotalHeight(ARow);
end;

function TRows.MaxBottom: Single;
var tmp : Integer;
begin
  tmp:=Count-1;

  if tmp=-1 then
     result:=0
  else
     result:=Top+BottomOf(tmp);
end;

function TRows.AllHeightsEqual:Boolean;
begin
  result:=(IHeights=nil) and
          (not FHeight.Automatic) and
          (not FChildren.CanDisplay) and
          (not FSubBands.CanDisplay);
end;

function TRows.TotalHeight(const ARow:Integer):Single;
begin
  result:=AutoHeight(ARow)+ISpacing;

  if FChildren.CanDisplay then
     result:=result+Children.Height(ARow);

  if FSubBands.CanDisplay then
     result:=result+SubBands.Height(ARow);
end;

function TRows.UpToRowHeight(const ARow:Integer):Single;
var t : Integer;
begin
  if AllHeightsEqual then
     result:=ARow*(HeightOf(0)+ISpacing)
  else
  begin
    result:=0;

    for t:=0 to ARow-1 do
        result:=result+TotalHeight(t);
  end;
end;

function TRows.TopOf(const ARow:Integer):Single;
begin
  result:=Top-Scroll.Y+UpToRowHeight(ARow);
end;

function TRows.TopOfRow(const ARow:Integer):Single;
begin
  result:=TopOf(ARow);

  if SubBands.CanDisplay then
     result:=result+SubBands.Height(ARow);
end;

procedure TRows.PaintLines(const AData:TRenderData; const FirstRow:Integer; Y:Single);
var t : Integer;
begin
  Y:=Y+((ISpacing-1)*0.5); // center line in between rows

  for t:=FirstRow to Count-1 do
  begin
    Y:=Y+TotalHeight(t)-ISpacing;

    if Y>AData.Rect.Bottom then
       break
    else
    begin
      AData.Painter.HorizontalLine(Y,AData.Rect.Left,AData.Rect.Right);

      Y:=Y+ISpacing;
    end;
  end;
end;

procedure TRows.Paint(var AData:TRenderData; const ARender:TRender);

  procedure PaintColumnLines;

    procedure PaintLine(const X:Single);
    begin
      // Pending to find a better solution (paint several segments, one per up-to-children)
      if FChildren.Count=0 then
         AData.Painter.VerticalLine(X,AData.Rect.Top,AData.Rect.Bottom);
    end;

  var t : Integer;
      tmp : Single;
      tmpHalf : Single;
  begin
    AData.Painter.SetStroke(Lines);

    tmpHalf:=(Lines.Size-1)*0.5;

    for t:=0 to VisibleColumns.Count-1 do
    begin
      tmp:=VisibleColumns[t].Left-tmpHalf-1;

      if tmp>AData.Rect.Right then
         Exit
      else
         PaintLine(tmp);
    end;

    if VisibleColumns.Count>0 then
       PaintLine(VisibleColumns.MaxRight+tmpHalf);
  end;

  procedure FixMaxRight;
  var tmp : Single;
  begin
    tmp:=VisibleColumns.MaxRight;

    if tmp<AData.Rect.Right then
       AData.Rect.Right:=tmp;
  end;

var
  DoAlternate : Boolean;

  procedure PaintBackground(const ARow:Integer);
  var tmpFormat : TFormat;
  begin
    if DoAlternate and ((ARow mod 2)=1) then
       tmpFormat:=Alternate
    else
       tmpFormat:=Format;

    AData.Painter.Paint(tmpFormat,AData.Rect);
  end;

  procedure PaintBand(const ABand:TGridBand);
  var Old,
      tmpR : TRectF;
  begin
    Old:=AData.Rect;

    tmpR:=AData.Rect;

    tmpR.Top:=tmpR.Bottom;
    tmpR.Bottom:=tmpR.Top+ABand.Height.Pixels;

    AData.Rect:=tmpR;

    AData.Text:='';

    ABand.Paint(AData,nil);

    AData.Rect:=Old;
  end;

  procedure PrepareFormat(const AFormat:TTextFormat);
  begin
    AData.Painter.SetBrush(AFormat.Brush);
    AData.Painter.SetFont(AFormat.Font);
  end;

var
  tmpFirst : Integer;
  YFirst : Single;
  tmpBackGround : Boolean;
  tmpBottom : Single;
  tmpChildren,
  tmpSubBands : Boolean;

  procedure PaintRowLines;
  var Old : TRectF;
  begin
    AData.Painter.SetStroke(FRowLines);

    Old:=AData.Rect;

    FixMaxRight;

    PaintLines(AData,tmpFirst,YFirst);
    AData.Rect:=Old;
  end;

  procedure PrepareParameters;
  begin
    tmpFirst:=FirstVisible;

    YFirst:=TopOf(tmpFirst);

    DoAlternate:=Alternate.ShouldPaint;

    tmpBackGround:=Format.Stroke.Visible or Format.Brush.Visible or DoAlternate;

    tmpBottom:=AData.Rect.Bottom;

    tmpSubBands:=SubBands.CanDisplay;
    tmpChildren:=Children.CanDisplay;
  end;

  // Paints all rows of a single column
  procedure PaintColumn(const AColumn:TColumn);
  var Y,
      tmpH : Single;
      t : Integer;
  begin
    Y:=YFirst;

    AData.Rect.Left:=AColumn.Left;
    AData.Rect.Right:=AColumn.Right;

    for t:=tmpFirst to Count-1 do
    begin
      AData.Row:=t;

      if tmpSubBands then
         Y:=Y+SubBands.Height(t);

      tmpH:=AutoHeight(t);

      AData.Rect.Top:=Y;
      AData.Rect.Bottom:=Y+tmpH;

      if tmpBackground then
         PaintBackground(t);

      AData.Text:=Data.AsString(AColumn,t);

      AColumn.Paint(AData,Render);

      Y:=AData.Rect.Bottom+ISpacing;

      if tmpChildren then
         Y:=Y+Children.Height(t);

      if Y>tmpBottom then
         break;
    end;
  end;

  // Paints all visible columns, one by one
  procedure PaintByColumns;
  var t : Integer;
      tmp : TColumn;
  begin
    for t:=0 to VisibleColumns.Count-1 do
    begin
      tmp:=VisibleColumns[t];

      if (not tmp.ParentFormat) and tmp.HasFormat then
         PrepareFormat(tmp.Format)
      else
         PrepareFormat(Format);

      PaintColumn(tmp);
    end;
  end;

  // Paint all rows, one by one
  procedure PaintByRows;
  var t : Integer;
      Y,
      tmpH : Single;
  begin
    PrepareFormat(Format);

    FixMaxRight;

    Y:=YFirst;

    for t:=tmpFirst to Count-1 do
    begin
      tmpH:=AutoHeight(t);

      if tmpSubBands then
         Y:=Y+SubBands.Height(t);

      AData.Rect.Top:=Y;
      AData.Rect.Bottom:=Y+tmpH;

      if tmpBackground then
         PaintBackground(t);

      AData.Row:=t;

      PaintRow(AData,Render);

      Y:=Y+tmpH+ISpacing;

      if tmpChildren then
         Y:=Y+Children.Height(t);

      if Y>tmpBottom then
         break;
    end;
  end;

  procedure PaintAllChildren;
  var t,
      L : Integer;
      tmpH : Single;
      tmp : TGridBand;
      tmpSub : TGridBand;
      Y: Single;
      ExpandRender : TExpanderRender;
      Old : TRectF;
  begin
    Y:=YFirst;

    L:=Length(Children.Rows);

    if VisibleColumns[0].Render is TExpanderRender then
       ExpandRender:=VisibleColumns[0].Render as TExpanderRender
    else
       ExpandRender:=nil;

    for t:=tmpFirst to Count-1 do
    begin
      tmpH:=HeightOf(t);

      if L>t then
      begin
        tmp:=Children.Rows[t];

        if (tmp<>nil) and tmp.Visible then
        begin
          //tmp.CalcHeight(0);

          tmpSub:=SubBands.Row[t];

          if tmpSub=nil then
             AData.Rect.Top:=Y
          else
             AData.Rect.Top:=Y+tmpSub.Height.Pixels;

          AData.Rect.Bottom:=AData.Rect.Top+tmpH;

          PaintBand(tmp);

          if ExpandRender<>nil then
          begin
            Old:=AData.Rect;
            AData.Rect.Top:=Y;
            AData.Rect.Bottom:=Y+tmp.Height.Pixels;

            ExpandRender.PaintLines(AData);
            AData.Rect:=Old;
          end;

          Y:=Y+tmp.Height.Pixels;
        end;
      end;

      Y:=Y+tmpH+ISpacing;

      if Y>tmpBottom then
         break;
    end;
  end;

  procedure PaintAllSubBands;
  var t,
      L : Integer;
      Y : Single;
      tmpChild,
      tmp : TGridBand;
  begin
    Y:=YFirst;

    L:=Length(SubBands.Rows);

    for t:=tmpFirst to Count-1 do
    begin
      if L>t then
         tmp:=SubBands.Rows[t]
      else
         tmp:=nil;

      if (tmp<>nil) and tmp.Visible then
      begin
        AData.Rect.Bottom:=Y;

        PaintBand(tmp);
        Y:=Y+tmp.Height.Pixels;
      end;

      if tmpChildren then
      begin
        tmpChild:=Children.Row[t];

        if (tmpChild<>nil) and tmpChild.Visible then
            Y:=Y+tmpChild.Height.Pixels;
      end;

      Y:=Y+HeightOf(t)+ISpacing;

      if Y>tmpBottom then
         break;
    end;
  end;

  procedure CalcSpacing;
  begin
    FSpacing.Prepare(AData.Rect.Height);

    ISpacing:=FSpacing.Pixels;

    if RowLines.Visible then
       ISpacing:=ISpacing+RowLines.Size;
  end;

var Old : TRectF;
    LastBottom : Single;
begin
  AData.Text:='';

  inherited;

  AData.Painter.Paint(FBack,AData.Rect);

  Old:=AData.Rect;

  CalcSpacing;
  PrepareParameters;

  if VisibleColumns.AllSameFormat then
     PaintByRows
  else
     PaintByColumns;

  LastBottom:=AData.Rect.Bottom;

  AData.Rect:=Old;

  if LastBottom<AData.Rect.Bottom then
     AData.Rect.Bottom:=LastBottom;

  if RowLines.Visible then
     PaintRowLines;

  if Lines.Visible then
     PaintColumnLines;

  AData.Rect.Left:=AData.Rect.Left+XOffset;
  FixMaxRight;

  // Pending: Recalculate all visible sub-band and children-band Height,
  // before painting any of them.

  if FChildren.CanDisplay then
     PaintAllChildren;

  if FSubBands.CanDisplay then
     PaintAllSubBands;

  AData.Rect:=Old;
end;

procedure TRows.SetAlternate(const Value: TAlternateFormat);
begin
  FAlternate.Assign(Value);
end;

procedure TRows.SetBack(const Value: TFormat);
begin
  FBack.Assign(Value);
end;

procedure TRows.SetColumnsLeft(const ALeft:Single);

  procedure TrySetParentLeft(const AColumn:TColumn);
  var tmp : TColumn;
  begin
    tmp:=AColumn.Parent;

    if tmp<>nil then
       if AColumn.ParentColumns.FirstVisible=AColumn then
       begin
         tmp.Left:=AColumn.Left;

         while tmp.Parent<>nil do
         begin
           tmp:=tmp.Parent;
           tmp.Left:=AColumn.Left;
         end;
       end;
  end;

var t : Integer;
    tmpX : Single;
    tmp : TColumn;
    tmpSpacing : Single;
begin
  tmpSpacing:=XSpacing;

  if Lines.Visible then
     tmpSpacing:=tmpSpacing+Lines.Size;

  tmpX:=ALeft+XOffset-Scroll.X;

  if Lines.Visible then
     tmpX:=tmpX+Lines.Size;

  for t:=0 to VisibleColumns.Count-1 do
  begin
    tmp:=VisibleColumns[t];

    tmp.Left:=tmpX;

    TrySetParentLeft(tmp);

    tmpX:=tmpX+tmpSpacing+tmp.Width.Pixels;
  end;
end;

procedure TRows.SetHeight(const Value: TCoordinate);
begin
  FHeight.Assign(Value);
end;

procedure TRows.SetHeights(const Index: Integer; const Value: Single);
begin
  if Heights[Index]<>Value then
  begin
    if Length(IHeights)<=Index then
       SetLength(IHeights,Index+1);

    IHeights[Index]:=Value;

    DoChanged;
  end;
end;

procedure TRows.SetHover(const Value: TCellHover);
begin
  FHover.Assign(Value);
end;

procedure TRows.SetRowLines(const Value: TStroke);
begin
  FRowLines.Assign(Value);
end;

procedure TRows.SetSpacing(const Value: TCoordinate);
begin
  FSpacing.Assign(Value);
end;

procedure TRows.Swap(const A, B: Integer);
var tmp : Single;
begin
  tmp:=IHeights[A];
  IHeights[A]:=IHeights[B];
  IHeights[B]:=tmp;

  FSubBands.Swap(A,B);
  FChildren.Swap(A,B);
end;

procedure TRows.PaintRow(var AData:TRenderData;
                         const ARender: TRender);

  procedure PaintColumns;

    procedure PaintColumn(const AColumn:TColumn);
    var Old : TRectF;
    begin
      AData.Text:=IData.AsString(AColumn,AData.Row);

      Old:=AData.Rect;

      AData.Rect.Left:=AColumn.Left;
      AData.Rect.Right:=AColumn.Right;

      AColumn.Paint(AData,ARender);

      AData.Rect:=Old;
    end;

  var t : Integer;
      tmp : TColumn;
      GridRight : Single;
  begin
    GridRight:=AData.Rect.Right+XOffset;

    for t:=0 to VisibleColumns.Count-1 do
    begin
      tmp:=VisibleColumns[t];

      if tmp.Left>GridRight then
         break
      else
      if tmp.Right>AData.Rect.Left+XOffset then
         PaintColumn(tmp);
    end;
  end;

begin
  AData.Painter.HideBrush;

  PaintColumns;
end;

function TRows.IsChildrenVisible(const Sender: TRender; const ARow: Integer): Boolean;
var tmp : TGridBand;
begin
  tmp:=Children.Row[ARow];
  result:=(tmp<>nil) and tmp.Visible;
end;

function TRows.RowAt(const Y, AvailableHeight: Single): Integer;

  function FindRow:Integer;
  var tmpH,
      tmpY : Single;
      t : Integer;
      tmpFirst : Integer;

      tmpSubBands,
      tmpChildren : Boolean;
  begin
    tmpFirst:=FirstVisible;

    tmpSubBands:=SubBands.CanDisplay;
    tmpChildren:=Children.CanDisplay;

    tmpY:=TopOfRow(tmpFirst);

    for t:=tmpFirst to Count-1 do
    begin
      if tmpSubBands and (t>tmpFirst) then
         tmpY:=tmpY+SubBands.Height(t);

      tmpH:=AutoHeight(t);

      if (Y>=tmpY) and (Y<=tmpY+tmpH) then
         Exit(t)
      else
      begin
        tmpY:=tmpY+tmpH+YSpacing;

        if tmpChildren then
           tmpY:=tmpY+Children.Height(t);

        if tmpY>AvailableHeight then
           break;
      end;
    end;

    result:=-1;
  end;

begin
  if (Y>=-Scroll.Y) and (Y<=AvailableHeight) and (DefaultHeight<>0) then
     result:=FindRow
  else
     result:=-1;
end;

{ TRowsBands }

function TRowsBands.GetRow(const Index: Integer): TGridBand;
begin
  if Length(Rows)>Index then
     result:=Rows[Index]
  else
     result:=nil;
end;

procedure TRowsBands.SetRow(const Index: Integer; const ABand: TGridBand);
begin
  if Length(Rows)<=Index then
     SetLength(Rows,Index+1);

  Rows[Index]:=ABand;
end;

procedure TRowsBands.Swap(const A, B: Integer);
var tmp : Integer;
    tmpBand : TGridBand;
begin
  tmp:=Items[A].Index;
  Items[A].Index:=B;
  Items[B].Index:=tmp;

  tmpBand:=Rows[A];
  Rows[A]:=Rows[B];
  Rows[B]:=tmpBand;
end;

// Returns the row index where ABand is painted above
function TRowsBands.RowOf(const ABand: TGridBand): Integer;
var t : Integer;
begin
  for t:=Low(Rows) to High(Rows) do
      if Rows[t]=ABand then
         Exit(t);

  result:=-1;
end;

// Returns the Band with bounds containing XY
function TRowsBands.Hit(const X, Y: Single): TGridBand;
var t : Integer;
    tmp : TGridBand;
begin
  if Visible then
  for t:=0 to Count-1 do
  begin
    tmp:=Items[t];

    if tmp.Contains(X,Y) then
       Exit(tmp);
  end;

  result:=nil;
end;

function TRowsBands.Height(const ARow: Integer): Single;
var tmp : TGridBand;
begin
  if Length(Rows)>ARow then
  begin
    tmp:=Rows[ARow];

    if (tmp<>nil) and tmp.Visible then
       result:=tmp.Height.Pixels
    else
       result:=0;
  end
  else
    result:=0;
end;

end.
