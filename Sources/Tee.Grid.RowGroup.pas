{*********************************************}
{  TeeGrid Software Library                   }
{  TRowGroup class                            }
{  Copyright (c) 2016 by Steema Software      }
{  All Rights Reserved                        }
{*********************************************}
unit Tee.Grid.RowGroup;
{$I Tee.inc}

interface

{
   This unit implements a TRowGroup class derived from TGridBand.

   TRowGroup class paints Rows, Headers and Footers:

     Rows is a TGridBand with a Data property (TVirtualData).

     Headers and Footers are TGridBands (collection of TGridBand).

     A TGridBand is an abstract class, any kind of band is supported.

}

uses
  {System.}Classes,

  {$IFNDEF FPC}
  {System.}Types,
  {$ENDIF}

  {System.}SysUtils,

  Tee.Format, Tee.Painter, Tee.Renders,
  Tee.Grid.Data, Tee.Grid.Columns, Tee.Grid.Bands, Tee.Grid.Header,
  Tee.Grid.Selection, Tee.Grid.Rows;

type
  TRowGroup=class;

  TNewDetailEvent=procedure(const Sender,NewGroup:TRowGroup) of object;

  // Grid band to paint headers, rows and footers
  TRowGroup=class(TGridBand)
  private
    FFooter: TGridBands;
    FHeader: TColumnHeaderBand;
    FHeaders: TGridBands;
    FIndicator: TIndicator;
    FOnChangedSelected : TNotifyEvent;
    FOnNewDetail : TNewDetailEvent;
    FReadOnly: Boolean;
    FRows : TRows;
    FSelected: TGridSelection;

    OwnsData : Boolean;
    FCurrent: TRowGroup;

    IBands : TGridBands;

    function CalcAutoWidth(const APainter:TPainter; const AColumn:TColumn;
                           const AWidth:Single):Single;
    procedure ChangedCellFormat(Sender: TObject);
    procedure ChangedHeadersFooter(Sender:TObject);
    procedure DoCalcWidth(const APainter:TPainter; const AColumn: TColumn;
                          const AWidth:Single);
    procedure DoHeaderFooter(var AState:TMouseState; const AWidth,AHeight:Single);
    procedure DoMove(var AState:TMouseState; const AWidth,AHeight:Single);
    procedure DoSelectedChanged(Sender:TObject);

    function GetCells: TTextRender;
    function GetColumns: TColumns;
    function GetData: TVirtualData;
    function GetFooter: TGridBands;
    function GetHeaders: TGridBands;

    procedure PaintRest(var AData: TRenderData);
    function PositionOf(const AColumn:TColumn; const ARow:Integer):TPointF;
    procedure RemovedColumn(Sender:TObject; const AColumn:TColumn);
    procedure SetCells(const Value: TTextRender);
    procedure SetCurrent(const Value: TRowGroup);
    procedure SetFooter(const Value: TGridBands);
    procedure SetHeader(const Value: TColumnHeaderBand);
    procedure SetHeaders(const Value: TGridBands);
    procedure SetIndicator(const Value: TIndicator);
    procedure SetReadOnly(const Value: Boolean);
    procedure SetRows(const Value: TRows);
    procedure SetSelected(const Value: TGridSelection);
    procedure TryMouseChildren(var AState:TMouseState; const AWidth,AHeight:Single);
    function WidthOf(const APainter:TPainter; const AColumns: TColumns;
                     const AWidth:Single): Single;
  public
    ParentColumn : TColumn;
    IsFocused,
    RecalcScrollBars : Boolean;

    const
      MinColumnWidth:Single=24;

    Constructor Create(const ACollection:TCollection;
                       const AData:TVirtualData); reintroduce;

    {$IFNDEF AUTOREFCOUNT}
    Destructor Destroy; override;
    {$ENDIF}

    procedure Assign(Source:TPersistent); override;

    procedure CalcHeight(const ATotal:Single); override;
    function CanEditRender(const AColumn:TColumn):Boolean;
    function CanStartEditor:Boolean;

    function CellRect(const ATopLeft:TPointF; const AColumn:TColumn; const ARow:Integer):TRectF; overload;
    function CellRect(const AColumn: TColumn; const ARow: Integer): TRectF; overload; inline;

    procedure CheckColumnsWidth(const APainter:TPainter; const Forced:Boolean;
                                const AWidth:Single);

    procedure Key(const AState:TKeyState);

    function MaxBottom:Single;

    function Mouse(var AState:TMouseState; const AWidth,AHeight:Single): Boolean; override;

    procedure Paint(var AData:TRenderData; const ARender:TRender); override;
    procedure PrepareColumns(const APainter:TPainter; const ALeft,AWidth:Single);

    procedure RefreshData(const AData:TVirtualData);
    function RenderHit(const AColumn:TColumn; const ARow:Integer; const X,Y:Single):Boolean;
    procedure ToggleDetailRows(const ARow:Integer);

    property Columns:TColumns read GetColumns;
    property Current:TRowGroup read FCurrent write SetCurrent;
    property Data:TVirtualData read GetData;
  published
    property Cells:TTextRender read GetCells write SetCells;
    property Footer:TGridBands read GetFooter write SetFooter;
    property Header:TColumnHeaderBand read FHeader write SetHeader;
    property Headers:TGridBands read GetHeaders write SetHeaders stored False;
    property Indicator:TIndicator read FIndicator write SetIndicator;
    property ReadOnly:Boolean read FReadOnly write SetReadOnly default False;
    property Rows:TRows read FRows write SetRows;
    property Selected:TGridSelection read FSelected write SetSelected;

    property OnNewDetail:TNewDetailEvent read FOnNewDetail write FOnNewDetail;
    property OnChangedSelected:TNotifyEvent read FOnChangedSelected write FOnChangedSelected;
  end;

implementation

{$IFDEF FPC}
uses
  LCLType;

const
  vkLeft = VK_LEFT;
  vkUp = VK_UP;
  vkRight = VK_RIGHT;
  vkDown = VK_DOWN;
  vkHome = VK_HOME;
  vkEnd = VK_END;
  vkPrior = VK_PRIOR;
  vkNext = VK_NEXT;
  vkReturn = VK_RETURN;
  vkTab = VK_TAB;
{$ELSE}

{$IFDEF NOUITYPES}
const
  vkTab = 9;
  vkReturn = 13;
  vkPrior = 33;
  vkNext = 34;
  vkEnd = 35;
  vkHome = 36;
  vkLeft = 37;
  vkUp = 38;
  vkRight = 39;
  vkDown = 40;
{$ELSE}

uses
  {System.}UITypes;
{$ENDIF}

{$ENDIF}

{ TRowGroup }

Constructor TRowGroup.Create(const ACollection:TCollection;
                             const AData:TVirtualData);
var tmp : TColumns;
begin
  inherited Create(ACollection);

  IBands:=TGridBands.Create(Self,Changed);

  tmp:=TColumns.Create(Self,TColumn);
  tmp.OnChanged:=ChangedCellFormat;
  tmp.OnRemoved:=RemovedColumn;

  FHeader:=TColumnHeaderBand.Create(Headers,tmp,AData);

  FIndicator:=TIndicator.Create(Changed);

  FRows:=TRows.Create(IBands);
  FRows.Data:=AData;
  Cells.OnChange:=ChangedCellFormat;

  FSelected:=TGridSelection.Create(Changed);
  FSelected.OnChange:=DoSelectedChanged;
end;

{$IFNDEF AUTOREFCOUNT}
Destructor TRowGroup.Destroy;
begin
  Columns.Free;

  FFooter.Free;
  FHeaders.Free;
  FIndicator.Free;

  if OwnsData then
  begin
    FRows.Data.Free;
    FRows.Data:=nil;
  end;

  FSelected.Free;

  IBands.Free;

  inherited;
end;
{$ENDIF}

procedure TRowGroup.ChangedCellFormat(Sender: TObject);
begin
  Rows.DefaultHeight:=0;
  Columns.ValidWidth:=False;

  DoChanged;
end;

procedure TRowGroup.RemovedColumn(Sender:TObject; const AColumn:TColumn);
begin
  if Selected.Column=AColumn then
     Selected.Column:=nil;
end;

type
  TDataAccess=class(TVirtualData);

procedure TRowGroup.DoSelectedChanged(Sender:TObject);
begin
  if Data<>nil then
     TDataAccess(Data).RowChanged(Selected.Row);

  if Assigned(FOnChangedSelected) then
     FOnChangedSelected(Self);
end;

procedure TRowGroup.Assign(Source: TPersistent);
begin
  if Source is TRowGroup then
  begin
    Header.Columns:=TRowGroup(Source).Columns;

    Footer:=TRowGroup(Source).FFooter;
    Headers:=TRowGroup(Source).FHeaders;
    Indicator:=TRowGroup(Source).FIndicator;
    FReadOnly:=TRowGroup(Source).FReadOnly;
    Rows:=TRowGroup(Source).Rows;
    Selected:=TRowGroup(Source).FSelected;
  end;

  inherited;
end;

procedure TRowGroup.SetCells(const Value: TTextRender);
begin
  Rows.Render:=Value;
end;

procedure TRowGroup.SetCurrent(const Value: TRowGroup);

  procedure SetChildrenCurrent(const AChildren:TRowsBands);
  var t : Integer;
      tmp : TGridBand;
  begin
    for t:=0 to AChildren.Count-1 do
    begin
      tmp:=AChildren[t];

      if tmp is TRowGroup then
         TRowGroup(tmp).Current:=Value;
    end;
  end;

begin
  if FCurrent<>Value then
  begin
    FCurrent:=Value;
    SetChildrenCurrent(Rows.Children);
  end;
end;

function TRowGroup.GetCells: TTextRender;
begin
  result:=Rows.Render as TTextRender;
end;

function TRowGroup.GetColumns: TColumns;
begin
  result:=Header.Columns;
end;

function TRowGroup.GetData: TVirtualData;
begin
  result:=Rows.Data;
end;

function TRowGroup.GetFooter: TGridBands;
begin
  if FFooter=nil then
     FFooter:=TGridBands.Create(Self,ChangedHeadersFooter);

  result:=FFooter;
end;

function TRowGroup.GetHeaders: TGridBands;
begin
  if FHeaders=nil then
     FHeaders:=TGridBands.Create(Self,ChangedHeadersFooter);

  result:=FHeaders;
end;

procedure TRowGroup.Key(const AState: TKeyState);

  procedure TrySelect(const AColumn:TColumn; const UseShift:Boolean=True);
  begin
    if AColumn<>nil then
       if UseShift and (ssShift in AState.Shift) then
          Selected.Range.ToColumn:=AColumn
       else
          Selected.Column:=AColumn;
  end;

  procedure ResetXScroll;
  begin
    if Rows.Scroll.X>0 then
    begin
      //ChangeHorizScroll(0);
      DoChanged{(Self)};
    end;
  end;

  function PageUp:Integer;
  begin
    if (ssCtrl in AState.Shift) or (Selected.Row<=0) then
       result:=0
    else
    begin
      result:=Selected.Row-10;

      if result<0 then
         result:=0;
    end;
  end;

  function PageDown:Integer;
  begin
    if ssCtrl in AState.Shift then
       result:=Rows.Count-1
    else
    begin
      result:=Selected.Row+10;

      if result>Rows.Count-1 then
         result:=Rows.Count-1;
    end;
  end;

  procedure SelectDown;
  begin
    if ssShift in AState.Shift then
    begin
      if Selected.Range.ToRow<Rows.Count-1 then
         Selected.Range.ToRow:=Selected.Range.ToRow+1;
    end
    else
    if Selected.Row<Rows.Count-1 then
       Selected.Row:=Selected.Row+1;
  end;

  procedure SelectUp;
  begin
    if ssShift in AState.Shift then
    begin
      if Selected.Range.ToRow>0 then
         Selected.Range.ToRow:=Selected.Range.ToRow-1;
    end
    else
    if Selected.Row>0 then
       Selected.Row:=Selected.Row-1;
  end;

  procedure SelectLeft(const UseShift:Boolean=True);
  var tmpPrevious : TColumn;
  begin
    tmpPrevious:=Rows.VisibleColumns.Previous(Selected.Column);

    if tmpPrevious=nil then
       ResetXScroll
    else
       TrySelect(tmpPrevious,UseShift);
  end;

  procedure SelectRight;
  begin
    TrySelect(Rows.VisibleColumns.Next(Selected.Range.ToColumn));
  end;

begin

  case AState.Key of
    vkDown: SelectDown;

      vkUp: SelectUp;

    vkLeft: SelectLeft;

   vkRight: SelectRight;

     vkTab: if ssShift in AState.Shift then
               SelectLeft(False)
            else
               SelectRight;

  vkReturn: SelectRight;

    vkHome: begin
              TrySelect(Rows.VisibleColumns.First);
              ResetXScroll;
            end;

     vkEnd: TrySelect(Rows.VisibleColumns.Last);

   vkPrior: Selected.Row:=PageUp;
    vkNext: Selected.Row:=PageDown;
  end;
end;

procedure TRowGroup.PrepareColumns(const APainter:TPainter; const ALeft,AWidth:Single);
begin
  if Indicator.Visible then
     Rows.XOffset:=Indicator.Width.Pixels+Rows.XSpacing
  else
     Rows.XOffset:=0;

  Rows.VisibleColumns.Clear;
  Rows.VisibleColumns.Add(Columns);

  if Rows.DefaultHeight=0 then
     Rows.CalcDefaultHeight;

  if not Columns.ValidWidth then
     CheckColumnsWidth(APainter,not Columns.ValidWidth,AWidth);

  Rows.SetColumnsLeft(ALeft);
end;

type
  TRowsAccess=class(TRows);

procedure TRowGroup.Paint(var AData: TRenderData; const ARender:TRender);

  function RectWithOffset:TRectF;
  begin
    result:=AData.Rect;
    result.Left:=result.Left+Rows.XOffset;
  end;

  procedure PaintHeaders;
  var Old : Single;
  begin
    //Header.CalcHeight(AData.Rect.Height);

    Old:=AData.Rect.Bottom;

//    AData.Painter.Clip(RectWithOffset);
    Headers.Paint(AData);
//    AData.Painter.UnClip;

    AData.Rect.Bottom:=Old;
  end;

  // Returns the "Indicator" rectangle of the current row
  function IndicatorBounds(const ARow:Integer):TRectF;
  begin
    result.Left:=(Render as TTextRender).Margins.Left.Pixels;
    result.Right:=result.Left+Indicator.Width.Pixels;

    result.Top:=Rows.TopOfRow(ARow);
    result.Bottom:=result.Top+Rows.HeightOf(ARow);
  end;

  procedure PaintIndicator;
  var tmp : TRectF;
  begin
    if Selected.Row<>-1 then
    begin
      tmp:=IndicatorBounds(Selected.Row);

      if (tmp.Top>=TRowsAccess(Rows).Top) and (tmp.Bottom<=AData.Rect.Bottom) then
      begin
        AData.Painter.Clip(AData.Rect);

        AData.Rect:=tmp;
        Indicator.Paint(AData);

        AData.Painter.UnClip;
      end;
    end;
  end;

  procedure PaintFooter;
  var Old,
      tmpNew : Single;
  begin
    //Footer.CalcHeight(AData.Rect.Height);
    Old:=AData.Rect.Top;

    if Footer.Floating then
    begin
      tmpNew:=Old+Height.Pixels-Footer.Height;

      if Headers.Visible then
         tmpNew:=tmpNew-Headers.Height;

      AData.Rect.Bottom:=tmpNew+Footer.Height;
    end
    else
       tmpNew:=AData.Rect.Bottom-Footer.Height;

    AData.Rect.Top:=tmpNew;

    Footer.Paint(AData);

    AData.Rect.Bottom:=tmpNew;
    AData.Rect.Top:=Old;
  end;

  function CalcXSpacing:Single;
  begin
    Columns.Spacing.Prepare(AData.Rect.Width);
    result:=Columns.Spacing.Pixels;
  end;

  procedure PaintRows;
  var Old : TRectF;
  begin
    // Paints all data cells (rows and columns)
    if Rows.Count>0 then
    begin
      Old:=AData.Rect;

      AData.Painter.Clip(RectWithOffset);

      Rows.Paint(AData,ARender);
      PaintRest(AData);

      AData.Painter.UnClip;

      AData.Rect:=Old;
    end;
  end;

var Old : TRectF;
    tmpH : Single;
begin
  inherited;

  Rows.Painter:=AData.Painter;

  if Selected.Column<>nil then
     if not Selected.Column.CanDisplay then
        Selected.Column:=nil;

  tmpH:=AData.Rect.Height;

  Old:=AData.Rect;

  if ParentColumn<>nil then
     AData.Rect.Left:=ParentColumn.Right;

  Rows.XSpacing:=CalcXSpacing;

  Rows.CalcYSpacing(tmpH);

  PrepareColumns(AData.Painter,AData.Rect.Left,AData.Rect.Width);

  CalcHeight(tmpH);

  if Headers.CanDisplay then
     PaintHeaders;

  if Footer.CanDisplay then
     PaintFooter;

  if Data<>nil then
     PaintRows;

  AData.Rect:=Old;

  if Indicator.Visible then
     PaintIndicator;
end;

procedure TRowGroup.PaintRest(var AData: TRenderData);
var
  MaxRight : Single;

  function PaintSelection(const ASelected:TGridSelection):Boolean;

    procedure TryPaintColumn(const AColumn:TColumn; const ARow:Integer);
    begin
      if AColumn.Visible then
      begin
        AData.Rect:=CellRect(AColumn,ARow);

        if AData.Rect.Left>MaxRight then
           Exit;

        if ASelected.PaintText then
           AData.Text:=Data.AsString(AColumn,ARow)
        else
           AData.Text:='';

        AData.Row:=ARow;

        ASelected.PaintColumn(AData,AColumn,Rows.FontOf(AColumn),(Self=Current) and IsFocused);
      end;
    end;

  var t,
      tt,
      tmpFrom,
      tmpTo,
      tmpFirst : Integer;
  begin
    result:=not ASelected.IsEmpty;

    if result then
    begin
      tmpFirst:=Rows.FirstVisible;

      result:=(tmpFirst<=ASelected.Row);

      if result then
      begin
        ASelected.GetColumns(Rows.VisibleColumns,tmpFrom,tmpTo);

        if tmpFrom>-1 then
        begin
          for t:=ASelected.Range.FromRow to ASelected.Range.ToRow do
              for tt:=tmpFrom to tmpTo do
                  TryPaintColumn(Rows.VisibleColumns[tt],t);
        end;
      end;
    end;
  end;

var Old : TRectF;
begin
  Old:=AData.Rect;

  MaxRight:=Old.Right;

  if PaintSelection(Selected) then
     AData.Painter.SetBrush(Format.Brush);

  if Rows.Hover.Visible and IsFocused then
     PaintSelection(Rows.Hover);

  AData.Rect:=Old;
end;

function TRowGroup.PositionOf(const AColumn: TColumn; const ARow: Integer): TPointF;
begin
  result.X:=AColumn.Left;
  result.Y:=Rows.TopOfRow(ARow);
end;

procedure TRowGroup.RefreshData(const AData: TVirtualData);
begin
  Header.Data:=AData;
  Rows.Data:=AData;

  Selected.Clear;
  Columns.Clear;
  Rows.Clear;

  if AData<>nil then
  begin
    AData.Load;

    AData.AddColumns(Columns);

    if AData.Count>0 then
       Selected.Row:=0
    else
       Selected.Row:=-1;

    Selected.Column:=Rows.VisibleColumns.First;
  end;
end;

procedure TRowGroup.CalcHeight(const ATotal: Single);

  function HeightUpToTotal:Single;
  var t : Integer;
  begin
    if Rows.Count=0 then
       result:=0
    else
    if Rows.AllHeightsEqual then
       result:=Rows.BottomOf(Rows.Count-1)
    else
    begin
      result:=0;

      for t:=0 to Rows.Count-1 do
      begin
        result:=result+Rows.TotalHeight(t);

        if result>ATotal then
           break;
      end;
    end;
  end;

begin
  inherited;

  if Height.Automatic then
  begin
    Height.Pixels:=HeightUpToTotal;

    if Headers.Visible then
    begin
      Headers.CalcHeight(ATotal);
      Height.Pixels:=Height.Pixels+Headers.Height;
    end;

    if Footer.Visible then
    begin
      Footer.CalcHeight(ATotal);
      Height.Pixels:=Height.Pixels+Footer.Height;
    end;
  end;
end;

type
  TColumnHeaderBandAccess=class(TColumnHeaderBand);

procedure TRowGroup.ChangedHeadersFooter(Sender:TObject);
begin
  RecalcScrollBars:=True;

  // JustRepaint is True when Header.Highlight is changed (so, do not invalidate widths)
  if (Header<>nil) and (not TColumnHeaderBandAccess(Header).IJustRepaint) then
     Columns.ValidWidth:=False;

  DoChanged;
end;

procedure TRowGroup.DoCalcWidth(const APainter:TPainter; const AColumn: TColumn;
                                const AWidth:Single);
var tmp : Single;
begin
  if AColumn.Width.Automatic then
     tmp:=CalcAutoWidth(APainter,AColumn,AWidth)
  else
     tmp:=AColumn.Width.Calculate(AWidth);

  AColumn.Width.Pixels:=tmp;

  AColumn.ValidWidth:=True;
end;

function TRowGroup.WidthOf(const APainter:TPainter; const AColumns: TColumns;
                           const AWidth:Single): Single;
var t : Integer;
    tmp : TColumn;
begin
  result:=0;

  for t:=0 to AColumns.Count-1 do
  begin
    tmp:=AColumns[t];

    if tmp.Visible then
    begin
      if not tmp.ValidWidth then
         DoCalcWidth(APainter,tmp,AWidth);

      result:=result+tmp.Width.Pixels;
    end;
  end;
end;

function TRowGroup.CalcAutoWidth(const APainter:TPainter; const AColumn:TColumn;
                                 const AWidth:Single):Single;
var tmp : Single;
    tmpMargins : TMargins;
begin
  if Header.Visible then
     result:=Header.AutoWidth(APainter,AColumn)
  else
     result:=0;

  if AColumn.HasItems then
     tmp:=WidthOf(APainter,AColumn.Items,AWidth)
  else
  if Data=nil then
     tmp:=0
  else
  begin
    if AColumn.ParentFormat then
       APainter.SetFont(Rows.Format.Font)
    else
       APainter.SetFont(AColumn.Format.Font);

    tmp:=Data.AutoWidth(APainter,AColumn);

    if AColumn.HasRender then
       tmpMargins:=AColumn.Margins
    else
       tmpMargins:=Cells.Margins;

    if tmpMargins<>nil then
    begin
      tmpMargins.Prepare(tmp,0);
      tmp:=tmp+tmpMargins.Horizontal;
    end;

    if tmp<MinColumnWidth then
       tmp:=MinColumnWidth;
  end;

  if tmp>result then
     result:=tmp;
end;

procedure TRowGroup.CheckColumnsWidth(const APainter:TPainter; const Forced:Boolean;
                                      const AWidth:Single);

  function Check(const AColumns:TColumns):Single;
  var t : Integer;
      tmp : TColumn;
  begin
    result:=0;

    for t:=0 to AColumns.Count-1 do
    begin
      tmp:=AColumns[t];

      if tmp.Visible then
      begin
        if tmp.Width.Automatic then
        begin
          if tmp.HasItems then
          begin
            tmp.Width.Pixels:=Check(tmp.Items);
            tmp.ValidWidth:=True;
          end
          else
          if Forced or (not tmp.ValidWidth) then
             DoCalcWidth(APainter,tmp,AWidth);
        end;

        result:=result+tmp.Width.Pixels;
      end;
    end;
  end;

begin
  Check(Columns);
  Columns.ValidWidth:=True;
end;

function TRowGroup.MaxBottom: Single;
begin
  result:=Rows.MaxBottom+Footer.Height;
end;

function TRowGroup.CellRect(const ATopLeft: TPointF;
  const AColumn: TColumn; const ARow: Integer): TRectF;
var tmp : Single;
begin
  tmp:=Rows.AutoHeight(ARow);

  result:=TRectF.Create(ATopLeft,AColumn.Width.Pixels,tmp);
end;

function TRowGroup.CellRect(const AColumn: TColumn; const ARow: Integer): TRectF;
begin
  result:=CellRect(PositionOf(AColumn,ARow),AColumn,ARow);
end;

procedure TRowGroup.DoHeaderFooter(var AState:TMouseState; const AWidth,AHeight:Single);
begin
  Headers.Mouse(AState,AWidth,AHeight);
  Footer.Mouse(AState,AWidth,AHeight);
end;

function TRowGroup.Mouse(var AState:TMouseState; const AWidth,AHeight:Single): Boolean;

  procedure Toogle(const AColumn:TColumn; const ARow:Integer);
  var tmp : String;
  begin
    tmp:=Data.AsString(AColumn,ARow);

    if SameText(tmp,'TRUE') then
       tmp:='False'
    else
       tmp:='True';

    Data.SetValue(AColumn,ARow,tmp);

    DoChanged;
  end;

  procedure DoDown;
  var tmpColumn : TColumn;
      tmpRow : Integer;
  begin
    if AState.Button=TGridMouseButton.Left then
    begin
      tmpColumn:=Columns.FindAt(AState.X,AWidth);

      if tmpColumn<>nil then
      begin
        tmpRow:=Rows.RowAt(AState.Y,AHeight);

        if tmpRow=-1 then
        begin
          if Header.Contains(AState.X,AState.Y) then
          begin
            Header.Dragging:=Rows.DraggedColumn(AState.X,tmpColumn);
            Header.MouseColumn:=tmpColumn;
            Header.Mouse(AState,AWidth,AHeight);
          end
          else
            TryMouseChildren(AState,AWidth,AHeight);
        end
        else
        if RenderHit(tmpColumn,tmpRow,AState.X,AState.Y) then
        begin
          if tmpColumn.Render is TExpanderRender then
             ToggleDetailRows(tmpRow)
          else
          if tmpColumn.Render is TBooleanRender then
             if CanEditRender(tmpColumn) then
                Toogle(tmpColumn,tmpRow)
        end
        else
        begin
          Selected.Change(tmpColumn,tmpRow);

          if Selected.Range.Enabled then
             Selected.Dragging:=AState.Y;
        end;
      end
      else
        TryMouseChildren(AState,AWidth,AHeight);
    end;
  end;

  procedure DoUp;
  begin
    Selected.Dragging:=-1;

    TryMouseChildren(AState,AWidth,AHeight);

    DoHeaderFooter(AState,AWidth,AHeight);
  end;

begin
  result:=inherited;

  if not result then
  case AState.Event of
    TGridMouseEvent.Down: DoDown;

      TGridMouseEvent.Up: DoUp;
  else
    DoMove(AState,AWidth,AHeight);
  end;
end;

function TRowGroup.CanEditRender(const AColumn:TColumn):Boolean;
begin
  result:=(not ReadOnly) and (not AColumn.ReadOnly);
end;

function TRowGroup.CanStartEditor: Boolean;
begin
  result:=(not ReadOnly) and
          (Selected.Column<>nil) and
          (not Selected.Column.ReadOnly) and
          (not Data.ReadOnly(Selected.Column));
end;

function TRowGroup.RenderHit(const AColumn:TColumn; const ARow:Integer; const X,Y:Single):Boolean;
begin
  result:=(AColumn<>nil) and AColumn.HasRender and
          AColumn.Render.Hit(CellRect(AColumn,ARow),X,Y);
end;

procedure TRowGroup.TryMouseChildren(var AState:TMouseState; const AWidth,AHeight:Single);
var tmpBand : TGridBand;
begin
  tmpBand:=Rows.Children.Hit(AState.X,AState.Y);

  if tmpBand=nil then
     DoHeaderFooter(AState,AWidth,AHeight)
  else
  if tmpBand is TRowGroup then
     TRowGroup(tmpBand).Mouse(AState,AWidth,AHeight)
  else
     tmpBand.Mouse(AState,AWidth,AHeight);
end;

procedure TRowGroup.DoMove(var AState:TMouseState; const AWidth,AHeight:Single);

  procedure MoveInHeader(const AColumn:TColumn);

    function InColumnResize(const X:Single; const AColumn:TColumn):Boolean;
    begin
      result:=Rows.DraggedColumn(X,AColumn)<>nil;
    end;

    function InIndicatorHeader:Boolean;
    begin
      result:=Indicator.Visible and (AState.X<Indicator.Width.Pixels);
    end;

  begin
    Header.MouseColumn:=AColumn;
    Header.Mouse(AState,AWidth,AHeight);

    if InColumnResize(AState.X,AColumn) then
       AState.Cursor:=TMouseCursor.HorizResize
    else
    if Header.CanSort(AColumn) then
       AState.Cursor:=TMouseCursor.HandPoint
    else
    if Assigned(Header.OnClick) then
       if not InIndicatorHeader then
          AState.Cursor:=TMouseCursor.HandPoint;
  end;

  procedure TrySelectionRange(const AColumn:TColumn; const ARow:Integer);
  begin
    if Selected.Dragging<>-1 then
    begin
      if ARow<>-1 then
         if AState.Y<Selected.Dragging then
            Selected.Range.FromRow:=ARow
         else
            Selected.Range.ToRow:=ARow;

      Selected.Range.ToColumn:=AColumn;
    end;
  end;

var tmpColumn : TColumn;
    tmpRow : Integer;
    tmpContinue : Boolean;
begin
  tmpContinue:=(not Header.Visible) or (not Header.Mouse(AState,AWidth,AHeight));

  if tmpContinue then
  begin
    AState.Cursor:=TMouseCursor.Default;

    tmpColumn:=Columns.FindAt(AState.X,AWidth);

    Header.MouseColumn:=nil;
    //Footer.MouseColumn:=nil;

    if tmpColumn<>nil then
       tmpRow:=Rows.RowAt(AState.Y,AHeight)
    else
       tmpRow:=-1;

    if Rows.Hover.Visible then
       Rows.Hover.Change(tmpColumn,tmpRow);

    if tmpRow=-1 then
    begin
      if Header.Contains(AState.X,AState.Y) and (tmpColumn<>nil) then
         MoveInHeader(tmpColumn)
      else
         TryMouseChildren(AState,AWidth,AHeight);
    end
    else
    if CanEditRender(tmpColumn) and RenderHit(tmpColumn,tmpRow,AState.X,AState.Y) then
       AState.Cursor:=TMouseCursor.HandPoint;

    TrySelectionRange(tmpColumn,tmpRow);
  end;
end;

procedure TRowGroup.ToggleDetailRows(const ARow: Integer);

  function NewGroup(const AData:TVirtualData; const AParent:TColumn):TRowGroup;
  begin
    result:=TRowGroup.Create(Rows.Children,AData);

    AData.AddColumns(result.Columns);

    result.OnChangedSelected:=OnChangedSelected;
    result.ParentColumn:=AParent;
    result.OwnsData:=True;
  end;

var tmp : TVirtualData;
    tmpGroup : TRowGroup;
    tmpBand : TGridBand;
    tmpParent : TColumn;
begin
  tmpBand:=Rows.Children.Row[ARow];

  if tmpBand=nil then
  begin
    tmp:=Data.GetDetail(ARow,Columns,tmpParent);

    if tmp<>nil then
    begin
      tmpGroup:=NewGroup(tmp,tmpParent);

      tmpGroup.Footer.Floating:=True;

      Rows.Children.Row[ARow]:=tmpGroup;

      if Assigned(FOnNewDetail) then
         FOnNewDetail(Self,tmpGroup);

      DoChanged;
    end;
  end
  else
    tmpBand.Visible:=not tmpBand.Visible;
end;

procedure TRowGroup.SetFooter(const Value: TGridBands);
begin
  Footer.Assign(Value);
end;

procedure TRowGroup.SetHeader(const Value: TColumnHeaderBand);
begin
  FHeader.Assign(Value);
end;

procedure TRowGroup.SetHeaders(const Value: TGridBands);
begin
  Headers.Assign(Value);
end;

procedure TRowGroup.SetIndicator(const Value: TIndicator);
begin
  FIndicator.Assign(Value);
end;

procedure TRowGroup.SetReadOnly(const Value: Boolean);
begin
  if FReadOnly<>Value then
  begin
    FReadOnly:=Value;
    DoChanged;
  end;
end;

procedure TRowGroup.SetRows(const Value: TRows);
begin
  FRows.Assign(Value);
end;

procedure TRowGroup.SetSelected(const Value: TGridSelection);
begin
  FSelected.Assign(Value);
end;

end.

