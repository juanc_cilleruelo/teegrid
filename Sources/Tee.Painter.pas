{*********************************************}
{  TeeGrid Software Library                   }
{  Base abstract Painter class                }
{  Copyright (c) 2016 by Steema Software      }
{  All Rights Reserved                        }
{*********************************************}
unit Tee.Painter;
{$I Tee.inc}
{$SCOPEDENUMS ON}

{
   Abstract TPainter class.

   TPainter provides basic methods to draw and fill shapes and text.

   VCLTee.Painter and FMXTee.Painter units inherit from TPainter class to
   implement the actual drawing for VCL and Firemonkey frameworks.

   Needs: Tee.Format
}

interface

uses
  {System.}Classes, {System.}Types,

  {$IFNDEF FPC}
  {$IFNDEF NOUITYPES}
  System.UITypes,
  {$ENDIF}
  {$ENDIF}

  Tee.Format;

type
  {$IFDEF NOUITYPES}
  TPointF=record
  public
    X,Y : Single;

    Constructor Create(const AX,AY:Single);
  end;

  TRectF=record
  public
    Left,
    Top,
    Right,
    Bottom : Single;

    Constructor Create(const ALeft,ATop,ARight,ABottom:Single); overload;
    Constructor Create(const ATopLeft:TPointF; const AWidth,AHeight:Single); overload;
    Constructor Create(const ARect:TRect); overload;

    function CenterPoint:TPointF;
    function Contains(const P:TPointF):Boolean;
    procedure Inflate(const X,Y:Single);
    function Height:Single; inline;
    function Round:TRect;
    function Width:Single; inline;
  end;
  {$ENDIF}

  THorizontalAlign=(Left,Center,Right);
  TVerticalAlign=(Top,Center,Bottom);

  TPointsF=Array of TPointF;

  TPainter=class
  protected
  public
    procedure Clip(const R:TRectF); overload; virtual; abstract;
    procedure UnClip; virtual; abstract;

    procedure HideBrush; virtual; abstract;

    procedure SetBrush(const ABrush:TBrush); virtual; abstract;
    procedure SetFont(const AFont:TFont); virtual; abstract;
    procedure SetHorizontalAlign(const Align:THorizontalAlign); virtual; abstract;
    procedure SetStroke(const AStroke:TStroke); virtual; abstract;
    procedure SetVerticalAlign(const Align:TVerticalAlign); virtual; abstract;

    procedure Draw(const R:TRectF); overload; virtual; abstract;
    procedure Draw(const P:TPointsF); overload; virtual; abstract;
    procedure Draw(const APicture:TPicture; const X,Y:Single); overload; virtual; abstract;
    procedure Draw(const APicture:TPicture; const R:TRectF); overload; virtual; abstract;
    procedure DrawEllipse(const R:TRectF); virtual; abstract;

    procedure Fill(const R:TRectF); overload; virtual; abstract;
    procedure Fill(const R:TRectF; const AColor:TColor); overload; virtual; abstract;
    procedure Fill(const P:TPointsF); overload; virtual; abstract;
    procedure FillEllipse(const R:TRectF); virtual; abstract;

    procedure HorizontalLine(const Y,X0,X1:Single); virtual; abstract;
    procedure Line(const X0,Y0,X1,Y1:Single); virtual; abstract;
    procedure Lines(const P:TPointsF); virtual; abstract;
    procedure Paint(const AFormat:TFormat; const R:TRectF); overload; virtual; abstract;
    procedure Paint(const AFormat:TFormat; const P:TPointsF); overload; virtual; abstract;
    function TextHeight(const AText:String):Single; virtual; abstract;
    procedure TextOut(const X,Y:Single; const AText:String); virtual; abstract;
    function TextWidth(const AText:String):Single; virtual; abstract;
    procedure VerticalLine(const X,Y0,Y1:Single); virtual; abstract;

    function TryClip(const AText:String;
                     const X,Y,AWidth,AHeight,AMinX:Single):Boolean; overload;

    function TryClip(const AText:String;
                     const ARect:TRectF;
                     const AMinX:Single):Boolean; overload;
  end;

implementation

{$IFDEF NOUITYPES}
Constructor TPointF.Create(const AX,AY:Single);
begin
  X:=AX;
  Y:=AY;
end;

Constructor TRectF.Create(const ALeft,ATop,ARight,ABottom:Single);
begin
  Left:=ALeft;
  Top:=ATop;
  Right:=ARight;
  Bottom:=ABottom;
end;

Constructor TRectF.Create(const ATopLeft:TPointF; const AWidth,AHeight:Single);
begin
  Left:=ATopLeft.X;
  Top:=ATopLeft.Y;
  Right:=Left+AWidth;
  Bottom:=Top+AHeight;
end;

Constructor TRectF.Create(const ARect:TRect);
begin
  Create(ARect.Left,ARect.Top,ARect.Right,ARect.Bottom);
end;

function TRectF.Round:TRect;
begin
  result.Left:=System.Round(Left);
  result.Top:=System.Round(Top);
  result.Right:=System.Round(Right);
  result.Bottom:=System.Round(Bottom);
end;

function TRectF.Height:Single;
begin
  result:=Bottom-Top;
end;

function TRectF.Width:Single;
begin
  result:=Right-Left;
end;

function TRectF.CenterPoint:TPointF;
begin
  result.X:=(Left+Right)*0.5;
  result.Y:=(Top+Bottom)*0.5;
end;

function TRectF.Contains(const P:TPointF):Boolean;
begin
  result:=(P.X>=Left) and (P.X<Right) and (P.Y>=Top) and (P.Y<Bottom);
end;

procedure TRectF.Inflate(const X,Y:Single);
begin
  Left:=Left-X;
  Right:=Right+X;
  Top:=Top-Y;
  Bottom:=Bottom+Y;
end;
{$ENDIF}

{ TPainter }

function TPainter.TryClip(const AText:String;
                          const X,Y,AWidth,AHeight,AMinX:Single):Boolean;
var tmpX : Single;
begin
  result:=(X<AMinX) or (TextWidth(AText)>AWidth);

  if result then
  begin
    if X<AMinX then
       tmpX:=AMinX
    else
       tmpX:=X;

    Clip(TRectF.Create(tmpX,Y,X+AWidth,Y+AHeight));
  end;
end;

function TPainter.TryClip(const AText:String;
                          const ARect:TRectF;
                          const AMinX:Single):Boolean;
begin
  result:=TryClip(AText,ARect.Left,ARect.Top,ARect.Width,ARect.Height,AMinX);
end;

end.
