unit FMXTeeGridRegister;

interface

uses
  System.Classes;

procedure Register;

implementation

uses
  DesignIntf, DesignEditors, PropertyCategories,

  FMX.Types,

  Tee.Format,

  FMXTee.Grid,
  FMXTee.Grid.About,

  FMXTee.Editor.Painter.Brush,
  FMXTee.Editor.Painter.Stroke; //, FMXTee.Editor.Grid, FMXTee.Editor...;

type
  TTeeComponentEditor=class(TComponentEditor)
  public
    procedure Edit; override;
    procedure ExecuteVerb(Index:Integer); override;
    function GetVerbCount:Integer; override;
    function GetVerb(Index:Integer):String; override;
  end;

  TTeeGridCompEditor=class(TTeeComponentEditor)
  public
    procedure Edit; override;
  end;

  TGridClassProperty=class(TClassProperty)
  protected
    function GetObject:Integer;
  public
    function GetAttributes : TPropertyAttributes; override;
//    function GetValue: String; override;
  end;

  TGridClassSubProperties=class(TGridClassProperty)
  public
    function GetAttributes : TPropertyAttributes; override;
  end;

  TBrushProperty=class(TGridClassSubProperties)
  public
    procedure Edit; override;
  end;

  TStrokeProperty=class(TGridClassSubProperties)
  public
    procedure Edit; override;
  end;

procedure Register;
begin
  StartClassGroup(TFmxObject);
  ActivateClassGroup(TFmxObject);

  RegisterComponents('TeeGrid',[TTeeGrid]);

  RegisterComponentEditor(TTeeGrid,TTeeGridCompEditor);

// Warning: VCL property editors should take priority for agnostic classes:
//  RegisterPropertyEditor(TypeInfo(TStroke), nil, '',TStrokeProperty);
//  RegisterPropertyEditor(TypeInfo(TBrush), nil, '',TBrushProperty);
end;

{ TTeeComponentEditor }

procedure TTeeComponentEditor.Edit;
begin // default, do nothing
end;

procedure TTeeComponentEditor.ExecuteVerb(Index: Integer);
begin
  if Index=GetVerbCount-1 then
     TFMXGridAbout.Show(nil)
  else
  if Index=GetVerbCount-2 then
     Edit
  else
     inherited;
end;

function TTeeComponentEditor.GetVerb(Index: Integer): String;
begin
  if Index=GetVerbCount-1 then
     result:='About...'
  else
  if Index=GetVerbCount-2 then
     result:='Edit...'
  else
     result:=inherited;
end;

function TTeeComponentEditor.GetVerbCount: Integer;
begin
  result:=inherited+2;
end;

{ TTeeGridCompEditor }

procedure TTeeGridCompEditor.Edit;
begin
//  if TTeeGridEditor.Edit(nil,TTeeGrid(Component)) then
     Designer.Modified;
end;

{ TGridClassProperty }

function TGridClassProperty.GetAttributes: TPropertyAttributes;
begin
  Result := [paDialog];
end;

function TGridClassProperty.GetObject: Integer;
begin
  result:=GetOrdValue;
end;

{ TGridClassSubProperties }

function TGridClassSubProperties.GetAttributes: TPropertyAttributes;
begin
  Result := [paSubProperties,paDialog];
end;

{ TStrokeProperty }

procedure TStrokeProperty.Edit;
begin
  TStrokeEditor.Edit(nil,TStroke(GetObject));
  Designer.Modified;
end;

{ TBrushProperty }

procedure TBrushProperty.Edit;
begin
  TBrushEditor.Edit(nil,TBrush(GetObject));
  Designer.Modified;
end;

end.
