{*********************************************}
{  TeeGrid Software Library                   }
{  FMX About TeeGrid...                       }
{  Copyright (c) 2016 by Steema Software      }
{  All Rights Reserved                        }
{*********************************************}
unit FMXTee.Grid.About;
{$I Tee.inc}

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms,

  {$IF CompilerVersion<=27}
  {$DEFINE HASFMX20}
  {$IFEND}

  {$IFNDEF HASFMX20}
  FMX.Graphics,
  {$ENDIF}

  {$IF CompilerVersion<25}
  {$DEFINE HASFMX21}
  {$IFEND}

  {$IFNDEF HASFMX21}
  FMX.StdCtrls,
  {$ENDIF}

  {$IF CompilerVersion<=27}
  {$DEFINE HASFMX22}
  {$IFEND}

  {$IFNDEF HASFMX22}
  FMX.Controls.Presentation,
  {$ENDIF}

  FMX.Dialogs, FMX.Layouts,

  Tee.Grid.Data.Strings, Tee.Grid.Ticker, FMXTee.Control, FMXTee.Grid;

type
  TFMXGridAbout = class(TForm)
    Layout1: TLayout;
    Layout2: TLayout;
    Button1: TButton;
    TeeGrid1: TTeeGrid;
    Timer1: TTimer;
    procedure Timer1Timer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }

    Data : TStringsData;
    Ticker : TGridTicker;

    procedure ColumnTextAlign;
    procedure CustomFormat;
    procedure FillNames;
    procedure FillRandomValues;
    procedure RandomCell(out ACol,ARow:Integer);
  public
    { Public declarations }

    class procedure Show(const AOwner:TComponent); static;
  end;

var
  FMXGridAbout: TFMXGridAbout;

implementation

{$R *.fmx}

uses
  Tee.Grid.Columns, Tee.Painter, Tee.Grid, Tee.Format, Tee.Grid.RowGroup;

procedure TFMXGridAbout.FormCreate(Sender: TObject);
begin
  Data:=TStringsData.Create(5,8);

  TeeGrid1.Data:=Data;

  FillNames;
  FillRandomValues;
  ColumnTextAlign;
  CustomFormat;

  TeeGrid1.Rows.Back.Brush.Show;
  TeeGrid1.Rows.Back.Brush.Color:=TColors.Cream;

  TeeGrid1.Back.Stroke.Show;

  Ticker:=TGridTicker.Create(TeeGrid1.Grid.Current);
end;

procedure TFMXGridAbout.FormDestroy(Sender: TObject);
begin
  Ticker.Free; // avoid memory leak
end;

procedure TFMXGridAbout.FormShow(Sender: TObject);
begin
  Timer1.Enabled:=True;
end;

procedure TFMXGridAbout.Timer1Timer(Sender: TObject);
var Col, Row : Integer;
    OldValue : Integer;
begin
  RandomCell(Col,Row);
  OldValue:=StrToInt(Data.AsString(TeeGrid1.Columns[Col],Row));

  Data[Col,Row]:=IntToStr(OldValue+Random(100)-50);
  Ticker.Change(Col,Row,OldValue);
end;

class procedure TFMXGridAbout.Show(const AOwner: TComponent);
begin
  with TFMXGridAbout.Create(AOwner) do
  try
    //LVersion.Caption:=TeeGrid_Version;

    ShowModal;
  finally
    Free;
  end;
end;

// Just fill grid cells with random values
procedure TFMXGridAbout.FillRandomValues;
var Col,Row : Integer;
begin
  for Col:=1 to TeeGrid1.Columns.Count-1 do
      for Row:=0 to Data.Count-1 do
          Data[Col,Row]:=IntToStr(Random(Col*2000));
end;

// Change first column font style
procedure TFMXGridAbout.CustomFormat;
var Col : TColumn;
begin
  Col:=TeeGrid1.Columns[0];

  Col.ParentFormat:=False;
  Col.Format.Font.Style:=[TFontStyle.fsBold];
  Col.Format.Font.Color:=TColors.Navy;
end;

// For all numeric columns, set right text alignment
procedure TFMXGridAbout.Button1Click(Sender: TObject);
begin
  Close;
end;

procedure TFMXGridAbout.ColumnTextAlign;
var Col : Integer;
    tmp : TColumn;
begin
  for Col:=1 to TeeGrid1.Columns.Count-1 do
  begin
    tmp:=TeeGrid1.Columns[Col];

    tmp.TextAlignment:=TColumnTextAlign.Custom;
    tmp.TextAlign.Horizontal:=THorizontalAlign.Right;
  end;
end;

// Initialize data headers
procedure TFMXGridAbout.FillNames;
begin
  Data.Headers[0]:='Product';
  Data.Headers[1]:='Sales';
  Data.Headers[2]:='Stock';
  Data.Headers[3]:='Orders';
  Data.Headers[4]:='Returns';

  // First column cells
  Data[0,0]:='Cars';
  Data[0,1]:='Chairs';
  Data[0,2]:='Keyboards';
  Data[0,3]:='Lamps';
  Data[0,4]:='Monitors';
  Data[0,5]:='Tables';
  Data[0,6]:='Umbrellas';
  Data[0,7]:='Windows';
end;

// Return a random cell coordinate (Column and Row)
procedure TFMXGridAbout.RandomCell(out ACol,ARow:Integer);
begin
  ACol:=1+Random(TeeGrid1.Columns.Count-1);
  ARow:=Random(TeeGrid1.Data.Count);
end;

end.
