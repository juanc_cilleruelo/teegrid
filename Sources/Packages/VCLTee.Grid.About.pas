{*********************************************}
{  TeeGrid Software Library                   }
{  VCL About TeeGrid...                       }
{  Copyright (c) 2016 by Steema Software      }
{  All Rights Reserved                        }
{*********************************************}
unit VCLTee.Grid.About;
{$I Tee.inc}

interface

uses
  {Winapi.}Windows, {Winapi.}Messages, {System.}SysUtils, {System.}Variants,
  {System.}Classes, {Vcl.}Graphics,
  {Vcl.}Controls, {Vcl.}Forms, {Vcl.}Dialogs, {Vcl.}StdCtrls, {Vcl.}ExtCtrls,
  VCLTee.Control, VCLTee.Grid,

  Tee.Grid.Data.Strings, Tee.Grid.Ticker;

type
  TVCLGridAbout = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Button1: TButton;
    LVersion: TLabel;
    TeeGrid1: TTeeGrid;
    Timer1: TTimer;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    { Private declarations }

    Data : TStringsData;
    Ticker : TGridTicker;

    procedure ColumnTextAlign;
    procedure CustomFormat;
    procedure FillNames;
    procedure FillRandomValues;
    procedure RandomCell(out ACol,ARow:Integer);
  public
    { Public declarations }

    class procedure Show(const AOwner:TComponent); static;
  end;

const
  TeeGrid_Version='v0.3 beta';
  TeeGrid_Version_Numeric=003;

implementation

{$R *.dfm}

uses
  {$IFNDEF NOUITYPES}
  System.UITypes,
  {$ENDIF}

  Tee.Grid.Columns, Tee.Painter, Tee.Grid, Tee.Format, Tee.Grid.RowGroup;

{ TVCLGridAbout }

procedure TVCLGridAbout.FormCreate(Sender: TObject);
begin
  Data:=TStringsData.Create(5,8);

  TeeGrid1.Data:=Data;

  FillNames;
  FillRandomValues;
  ColumnTextAlign;
  CustomFormat;

  Ticker:=TGridTicker.Create(TeeGrid1.Grid.Current);
end;

procedure TVCLGridAbout.FormDestroy(Sender: TObject);
begin
  Ticker.Free; // avoid memory leak
end;

procedure TVCLGridAbout.FormShow(Sender: TObject);
begin
  Timer1.Enabled:=True;
end;

class procedure TVCLGridAbout.Show(const AOwner: TComponent);
begin
  with TVCLGridAbout.Create(AOwner) do
  try
    LVersion.Caption:=TeeGrid_Version;

    ShowModal;
  finally
    Free;
  end;
end;

procedure TVCLGridAbout.Timer1Timer(Sender: TObject);
var Col, Row : Integer;
    OldValue : Integer;
begin
  RandomCell(Col,Row);
  OldValue:=StrToInt(Data.AsString(TeeGrid1.Columns[Col],Row));

  Data[Col,Row]:=IntToStr(OldValue+Random(100)-50);
  Ticker.Change(Col,Row,OldValue);
end;

// Just fill grid cells with random values
procedure TVCLGridAbout.FillRandomValues;
var Col,Row : Integer;
begin
  for Col:=1 to TeeGrid1.Columns.Count-1 do
      for Row:=0 to Data.Count-1 do
          Data[Col,Row]:=IntToStr(Random(Col*2000));
end;

// Change first column font style
procedure TVCLGridAbout.CustomFormat;
var Col : TColumn;
begin
  Col:=TeeGrid1.Columns[0];

  Col.ParentFormat:=False;
  Col.Format.Font.Style:=[TFontStyle.fsBold];
  Col.Format.Font.Color:=TColors.Navy;
end;

// For all numeric columns, set right text alignment
procedure TVCLGridAbout.ColumnTextAlign;
var Col : Integer;
    tmp : TColumn;
begin
  for Col:=1 to TeeGrid1.Columns.Count-1 do
  begin
    tmp:=TeeGrid1.Columns[Col];

    tmp.TextAlignment:=TColumnTextAlign.Custom;
    tmp.TextAlign.Horizontal:=THorizontalAlign.Right;
  end;
end;

// Initialize data headers
procedure TVCLGridAbout.FillNames;
begin
  Data.Headers[0]:='Product';
  Data.Headers[1]:='Sales';
  Data.Headers[2]:='Stock';
  Data.Headers[3]:='Orders';
  Data.Headers[4]:='Returns';

  // First column cells
  Data[0,0]:='Cars';
  Data[0,1]:='Chairs';
  Data[0,2]:='Keyboards';
  Data[0,3]:='Lamps';
  Data[0,4]:='Monitors';
  Data[0,5]:='Tables';
  Data[0,6]:='Umbrellas';
  Data[0,7]:='Windows';
end;

// Return a random cell coordinate (Column and Row)
procedure TVCLGridAbout.RandomCell(out ACol,ARow:Integer);
begin
  ACol:=1+Random(TeeGrid1.Columns.Count-1);
  ARow:=Random(TeeGrid1.Data.Count);
end;

end.
