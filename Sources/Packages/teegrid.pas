{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit teegrid;

interface

uses
  TeeGrid_Lazarus, LazarusPackageIntf;

implementation

procedure Register;
begin
  RegisterUnit('TeeGrid_Lazarus', @TeeGrid_Lazarus.Register);
end;

initialization
  RegisterPackage('teegrid', @Register);
end.
