unit VCLTeeGridRegister;

interface

uses
  {System.}Classes;

procedure Register;

implementation

uses
  {$IFDEF FPC}
  LCLIntf, PropEdits, ComponentEditors,
  {$ELSE}
  DesignIntf, DesignEditors, PropertyCategories,
  {$ENDIF}

  {Vcl.}Controls,

  Tee.Format, Tee.Grid.Header, Tee.Grid.Columns, Tee.Renders,

  // Force unit usage
  Tee.Grid.Data.DB,

  VCLTee.Grid,
  VCLTee.Grid.About,
  
  VCLTee.Editor.Grid,
  VCLTee.Editor.Gradient,
  VCLTee.Editor.Font,
  VCLTee.Editor.Stroke,
  VCLTee.Editor.Format,
  VCLTee.Editor.Format.Text,
  VCLTee.Editor.Brush,
  VCLTee.Editor.Column,
  VCLTee.Editor.Header,
  VCLTee.Editor.Margins,
  VCLTee.Editor.Borders;

{$R TeeGrid_Images.res}

type
  TTeeComponentEditor=class(TComponentEditor)
  public
    procedure Edit; override;
    procedure ExecuteVerb(Index:Integer); override;
    function GetVerbCount:Integer; override;
    function GetVerb(Index:Integer):String; override;
  end;

  TTeeGridCompEditor=class(TTeeComponentEditor)
  public
    procedure Edit; override;
  end;

  TGridClassProperty=class(TClassProperty)
  protected
    {$IFDEF LCL}
    function Designer:TGridClassProperty;
    {$ENDIF}
    function GetObject:Integer;
  public
    function GetAttributes : TPropertyAttributes; override;
//    function GetValue: String; override;
  end;

  TGridClassSubProperties=class(TGridClassProperty)
  public
    function GetAttributes : TPropertyAttributes; override;
  end;

  TColumnProperty=class(TGridClassSubProperties)
  public
    procedure Edit; override;
  end;

  THeaderProperty=class(TGridClassSubProperties)
  public
    procedure Edit; override;
  end;

  TGradientProperty=class(TGridClassSubProperties)
  public
    procedure Edit; override;
  end;

  TBrushProperty=class(TGridClassSubProperties)
  public
    procedure Edit; override;
  end;

  TStrokeProperty=class(TGridClassSubProperties)
  public
    procedure Edit; override;
  end;

  TFontProperty=class(TGridClassSubProperties)
  public
    procedure Edit; override;
  end;

  TBordersProperty=class(TGridClassSubProperties)
  public
    procedure Edit; override;
  end;

  TFormatProperty=class(TGridClassSubProperties)
  public
    procedure Edit; override;
  end;

  TTextFormatProperty=class(TGridClassSubProperties)
  public
    procedure Edit; override;
  end;

  TMarginsProperty=class(TGridClassSubProperties)
  public
    procedure Edit; override;
  end;

procedure Register;
begin
  StartClassGroup(TControl);
  ActivateClassGroup(TControl);

  RegisterComponents('TeeGrid',[TTeeGrid]);

  RegisterComponentEditor(TTeeGrid,TTeeGridCompEditor);

  RegisterPropertyEditor(TypeInfo(TColumnHeaderBand), nil, '',THeaderProperty);
  RegisterPropertyEditor(TypeInfo(TColumn), nil, '',TColumnProperty);

  RegisterPropertyEditor(TypeInfo(TGradient), nil, '',TGradientProperty);
  RegisterPropertyEditor(TypeInfo(TStroke), nil, '',TStrokeProperty);
  RegisterPropertyEditor(TypeInfo(TBrush), nil, '',TBrushProperty);
  RegisterPropertyEditor(TypeInfo(TFont), nil, '',TFontProperty);
  RegisterPropertyEditor(TypeInfo(TBorders), nil, '',TBordersProperty);

  RegisterPropertyEditor(TypeInfo(TFormat), nil, '',TFormatProperty);
  RegisterPropertyEditor(TypeInfo(TTextFormat), nil, '',TTextFormatProperty);
  RegisterPropertyEditor(TypeInfo(TMargins), nil, '',TMarginsProperty);
end;

{ TTeeComponentEditor }

procedure TTeeComponentEditor.Edit;
begin // default, do nothing
end;

procedure TTeeComponentEditor.ExecuteVerb(Index: Integer);
begin
  if Index=GetVerbCount-1 then
     TVCLGridAbout.Show(nil)
  else
  if Index=GetVerbCount-2 then
     Edit
  else
     inherited;
end;

function TTeeComponentEditor.GetVerb(Index: Integer): String;
begin
  if Index=GetVerbCount-1 then
     result:='About...'
  else
  if Index=GetVerbCount-2 then
     result:='Edit...'
  else
     result:=inherited;
end;

function TTeeComponentEditor.GetVerbCount: Integer;
begin
  result:=inherited+2;
end;

{ TTeeGridCompEditor }

procedure TTeeGridCompEditor.Edit;
begin
  TTeeGridEditor.Edit(nil,TTeeGrid(Component));
  Designer.Modified;
end;

{ TGridClassProperty }

{$IFDEF LCL}
function TGridClassProperty.Designer:TGridClassProperty;
begin
  result:=Self;
end;
{$ENDIF}

function TGridClassProperty.GetAttributes: TPropertyAttributes;
begin
  Result := [paDialog];
end;

function TGridClassProperty.GetObject: Integer;
begin
  result:=GetOrdValue;
end;

{ TGridClassSubProperties }

function TGridClassSubProperties.GetAttributes: TPropertyAttributes;
begin
  Result := [paSubProperties,paDialog];
end;

{ THeaderProperty }

procedure THeaderProperty.Edit;
begin
  THeaderEditor.Edit(nil,TColumnHeaderBand(GetObject));
  Designer.Modified;
end;

{ TColumnProperty }

procedure TColumnProperty.Edit;
begin
  TColumnEditor.Edit(nil,TColumn(GetObject));
  Designer.Modified;
end;

{ TStrokeProperty }

procedure TStrokeProperty.Edit;
begin
  TStrokeEditor.Edit(nil,TStroke(GetObject));
  Designer.Modified;
end;

{ TBordersProperty }

procedure TBordersProperty.Edit;
begin
  TBordersEditor.Edit(nil,TBorders(GetObject));
  Designer.Modified;
end;

{ TBrushProperty }

procedure TBrushProperty.Edit;
begin
  TBrushEditor.Edit(nil,TBrush(GetObject));
  Designer.Modified;
end;

{ TFormatProperty }

procedure TFormatProperty.Edit;
begin
  TFormatEditor.Edit(nil,TFormat(GetObject));
  Designer.Modified;
end;

{ TTextFormatProperty }

procedure TTextFormatProperty.Edit;
begin
  TTextFormatEditor.Edit(nil,TTextFormat(GetObject));
  Designer.Modified;
end;

{ TMarginsProperty }

procedure TMarginsProperty.Edit;
begin
  TMarginsEditor.Edit(nil,TMargins(GetObject));
  Designer.Modified;
end;

{ TGradientProperty }

procedure TGradientProperty.Edit;
begin
  TGradientEditor.Edit(nil,TGradient(GetObject));
  Designer.Modified;
end;

{ TFontProperty }

procedure TFontProperty.Edit;
begin
  TFontEditor.Edit(nil,TFont(GetObject));
  Designer.Modified;
end;

end.
