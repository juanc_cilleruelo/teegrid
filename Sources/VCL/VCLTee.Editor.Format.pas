{*********************************************}
{  TeeGrid Software Library                   }
{  VCL TFormat Editor                         }
{  Copyright (c) 2016 by Steema Software      }
{  All Rights Reserved                        }
{*********************************************}
unit VCLTee.Editor.Format;
{$I Tee.inc}

interface

uses
  {Winapi.}Windows, {Winapi.}Messages, {System.}SysUtils, {System.}Classes, {Vcl.}Graphics,
  {Vcl.}Controls, {Vcl.}Forms, {Vcl.}Dialogs, {Vcl.}ComCtrls, {Vcl.}StdCtrls,
  {Vcl.}ExtCtrls,

  Tee.Format,
  VCLTee.Editor.Stroke, VCLTee.Editor.Brush;

type
  TFormatEditor = class(TForm)
    PageFormat: TPageControl;
    TabStroke: TTabSheet;
    TabBrush: TTabSheet;
    procedure FormCreate(Sender: TObject);
    procedure PageFormatChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }

    IFormat : TFormat;

    IBrush : TBrushEditor;
    IStroke : TStrokeEditor;
  public
    { Public declarations }

    procedure RefreshFormat(const AFormat:TFormat);

    class function Edit(const AOwner:TComponent; const AFormat:TFormat):Boolean; static;

    class function Embedd(const AOwner:TComponent; const AParent:TWinControl;
                          const AFormat:TFormat):TFormatEditor; static;
  end;

implementation

{$R *.dfm}

{$IFNDEF NOUITYPES}
uses
  System.UITypes;
{$ENDIF}

{ TFormatEditor }

class function TFormatEditor.Edit(const AOwner: TComponent;
  const AFormat: TFormat): Boolean;
begin
  with TFormatEditor.Create(AOwner) do
  try
    RefreshFormat(AFormat);
    result:=ShowModal=mrOk;
  finally
    Free;
  end;
end;

class function TFormatEditor.Embedd(const AOwner: TComponent;
  const AParent: TWinControl; const AFormat: TFormat): TFormatEditor;
begin
  result:=TFormatEditor.Create(AOwner);
  result.RefreshFormat(AFormat);
  TStrokeEditor.AddForm(result,AParent);
end;

procedure TFormatEditor.FormCreate(Sender: TObject);
begin
  PageFormat.ActivePage:=TabStroke;
end;

procedure TFormatEditor.FormShow(Sender: TObject);
begin
  PageFormatChange(Self);
end;

procedure TFormatEditor.PageFormatChange(Sender: TObject);
begin
  if IFormat=nil then
     Exit;

  if PageFormat.ActivePage=TabStroke then
  begin
    if IStroke=nil then
       IStroke:=TStrokeEditor.Embedd(Self,TabStroke,IFormat.Stroke);
  end
  else
  if PageFormat.ActivePage=TabBrush then
  begin
    if IBrush=nil then
       IBrush:=TBrushEditor.Embedd(Self,TabBrush,IFormat.Brush);
  end;
end;

procedure TFormatEditor.RefreshFormat(const AFormat: TFormat);
begin
  IFormat:=AFormat;

  if IBrush<>nil then
     IBrush.RefreshBrush(IFormat.Brush);

  if IStroke<>nil then
     IStroke.RefreshStroke(IFormat.Stroke);
end;

end.
