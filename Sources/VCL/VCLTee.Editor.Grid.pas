{*********************************************}
{  TeeGrid Software Library                   }
{  VCL TTeeGrid Editor                        }
{  Copyright (c) 2016 by Steema Software      }
{  All Rights Reserved                        }
{*********************************************}
unit VCLTee.Editor.Grid;

interface

{
   Main VCL TeeGrid editor dialog
}

uses
  {Winapi.}Windows, {Winapi.}Messages, {System.}SysUtils, {System.}Classes, {Vcl.}Graphics,
  {Vcl.}Controls, {Vcl.}Forms, {Vcl.}Dialogs, {Vcl.}ComCtrls, {Vcl.}StdCtrls, {Vcl.}ExtCtrls,
  {Vcl.}Buttons,

  Tee.Renders, Tee.Grid.Columns, Tee.Grid.Selection,

  VCLTee.Grid,

  VCLTee.Editor.Format, VCLTee.Editor.Stroke, VCLTee.Editor.Column,
  VCLTee.Editor.Coordinate, VCLTee.Editor.Margins,
  VCLTee.Editor.Render.Text, VCLTee.Editor.Grid.Bands;

type
  TTeeGridEditor = class(TForm)
    PageGrid: TPageControl;
    TabColumns: TTabSheet;
    TreeColumns: TTreeView;
    Panel5: TPanel;
    TabOptions: TTabSheet;
    SBDeleteColumn: TSpeedButton;
    TabBands: TTabSheet;
    PanelEditor: TPanel;
    TabCells: TTabSheet;
    PageCells: TPageControl;
    TabCellsFormat: TTabSheet;
    TabCellsHover: TTabSheet;
    Splitter1: TSplitter;
    TabRows: TTabSheet;
    PageOptions: TPageControl;
    TabIndicator: TTabSheet;
    PageRows: TPageControl;
    TabRowLines: TTabSheet;
    TabRowAlternate: TTabSheet;
    Panel3: TPanel;
    CBIndicatorVisible: TCheckBox;
    SBColumnUp: TSpeedButton;
    SBColumnDown: TSpeedButton;
    TabRowsGeneral: TTabSheet;
    Label1: TLabel;
    ERowHeight: TEdit;
    UDRowHeight: TUpDown;
    CBRowHeightAuto: TCheckBox;
    PanelButtons: TPanel;
    PanelOk: TPanel;
    BOk: TButton;
    TabBack: TTabSheet;
    TabMargins: TTabSheet;
    TabTheme: TTabSheet;
    LBThemes: TListBox;
    TabSelection: TTabSheet;
    Panel2: TPanel;
    CBFullRow: TCheckBox;
    CBSelectedParentFont: TCheckBox;
    Panel4: TPanel;
    CBAlternateVisible: TCheckBox;
    PageIndicator: TPageControl;
    TabIndicatorFormat: TTabSheet;
    TabIndicatorWidth: TTabSheet;
    TabColumnLines: TTabSheet;
    TabEditing: TTabSheet;
    CBDoubleClick: TCheckBox;
    CBEditingAlways: TCheckBox;
    CBReadOnly: TCheckBox;
    Label4: TLabel;
    TBVertSpacing: TTrackBar;
    LVertSpacing: TLabel;
    CBSelectedRange: TCheckBox;
    PageSelected: TPageControl;
    TabSelectedFocused: TTabSheet;
    TabSelectedUnfocused: TTabSheet;
    Label3: TLabel;
    TBHorizSpacing: TTrackBar;
    LHorizSpacing: TLabel;
    RGPainter: TRadioGroup;
    TabScrollBars: TTabSheet;
    CBScrollBars: TCheckBox;
    PageBands: TPageControl;
    TabHeaders: TTabSheet;
    TabFooter: TTabSheet;
    procedure TreeColumnsChange(Sender: TObject; Node: TTreeNode);
    procedure CBFullRowClick(Sender: TObject);
    procedure SBDeleteColumnClick(Sender: TObject);
    procedure PageGridChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CBIndicatorVisibleClick(Sender: TObject);
    procedure SBColumnUpClick(Sender: TObject);
    procedure SBColumnDownClick(Sender: TObject);
    procedure ERowHeightChange(Sender: TObject);
    procedure CBRowHeightAutoClick(Sender: TObject);
    procedure CBReadOnlyClick(Sender: TObject);
    procedure PageOptionsChange(Sender: TObject);
    procedure TreeColumnsEdited(Sender: TObject; Node: TTreeNode; var S: string);
    procedure PageRowsChange(Sender: TObject);
    procedure PageCellsChange(Sender: TObject);
    procedure LBThemesClick(Sender: TObject);
    procedure CBSelectedParentFontClick(Sender: TObject);
    procedure CBAlternateVisibleClick(Sender: TObject);
    procedure TBHorizSpacingChange(Sender: TObject);
    procedure TBVertSpacingChange(Sender: TObject);
    procedure PageIndicatorChange(Sender: TObject);
    procedure CBDoubleClickClick(Sender: TObject);
    procedure CBEditingAlwaysClick(Sender: TObject);
    procedure CBSelectedRangeClick(Sender: TObject);
    procedure RGPainterClick(Sender: TObject);
    procedure CBScrollBarsClick(Sender: TObject);
    procedure PageBandsChange(Sender: TObject);
  private
    { Private declarations }

    Grid : TTeeGrid;

    ICells,
    ICellsHover,
    ISelectedFocused,
    ISelectedUnfocused : TTextRenderEditor;

    IFooters,
    IHeaders : TGridBandsEditor;

    IRowAlternate,
    IIndicatorFormat : TFormatEditor;

    IIndicatorWidth : TCoordinateEditor;

    IColumnEditor : TColumnEditor;

    IBack : TFormatEditor;

    IMargins : TMarginsEditor;

    IRowLines,
    IColumnLines : TStrokeEditor;

    UpdatingTree : Boolean;

    procedure ChangedHeader(Sender:TObject);
    function Column:TColumn;
    procedure EnableUpDown;
    procedure FillThemes;
    procedure MoveColumn(const Delta:Integer);
    procedure RefreshMargins(const AMargins:TMargins);
    procedure RefreshSelected(const ASelected:TGridSelection);
    procedure SetSpacingSettings;
    procedure ShowColumns(const ATree:TTreeView; const AColumns:TColumns);
  public
    { Public declarations }

    procedure RefreshGrid(const AGrid:TTeeGrid);

    class function Edit(const AOwner:TComponent; const AGrid:TTeeGrid):Boolean; static;

    class function Embedd(const AOwner:TComponent; const AParent:TWinControl;
                          const AGrid:TTeeGrid):TTeeGridEditor; static;
  end;

implementation

{$R *.dfm}

{$IFNDEF FPC}
{$IF CompilerVersion>=23}
{$DEFINE HASGDIPLUS}
{$IFEND}
{$ENDIF}

uses
  Tee.Grid.Themes,

  {$IFDEF HASGDIPLUS} // XE2 and up only (missing GDIPAPI / GDIPOBJ units)
  VCLTee.Painter.GdiPlus,
  {$ENDIF}

  VCLTee.Painter;

function TTeeGridEditor.Column:TColumn;
begin
  if TreeColumns.Selected=nil then
     result:=nil
  else
     result:=TColumn(TreeColumns.Selected.Data);
end;

class function TTeeGridEditor.Edit(const AOwner: TComponent;
  const AGrid: TTeeGrid): Boolean;
begin
  with TTeeGridEditor.Create(AOwner) do
  try
    RefreshGrid(AGrid);
    PanelButtons.Visible:=True;

    result:=ShowModal=mrOk;
  finally
    Free;
  end;
end;

class function TTeeGridEditor.Embedd(const AOwner: TComponent;
  const AParent: TWinControl; const AGrid: TTeeGrid): TTeeGridEditor;
begin
  result:=TTeeGridEditor.Create(AOwner);
  result.RefreshGrid(AGrid);
  TStrokeEditor.AddForm(result,AParent);
end;

procedure TTeeGridEditor.FormCreate(Sender: TObject);
begin
  {$IFNDEF HASGDIPLUS} // XE2 and up only (missing GDIPAPI / GDIPOBJ units)
  RGPainter.Hide;
  {$ENDIF}

  PageGrid.ActivePage:=TabColumns;
  PageOptions.ActivePage:=TabIndicator;
  PageCells.ActivePage:=TabCellsFormat;
  PageRows.ActivePage:=TabRowsGeneral;
  PageIndicator.ActivePage:=TabIndicatorFormat;
end;

procedure TTeeGridEditor.LBThemesClick(Sender: TObject);
begin
  case LBThemes.ItemIndex of
    0: TGridThemes.Default.ApplyTo(Grid.Grid);
    1: TGridThemes.Black.ApplyTo(Grid.Grid);
    2: TGridThemes.iOS.ApplyTo(Grid.Grid);
    3: TGridThemes.Android.ApplyTo(Grid.Grid);
  end;
end;

procedure TTeeGridEditor.PageBandsChange(Sender: TObject);
begin
  if PageBands.ActivePage=TabHeaders then
  begin
    if IHeaders=nil then
       IHeaders:=TGridBandsEditor.Embedd(Self,TabHeaders,Grid.Headers);
  end
  else
  if PageBands.ActivePage=TabFooter then
  begin
    if IFooters=nil then
       IFooters:=TGridBandsEditor.Embedd(Self,TabFooter,Grid.Footer);
  end;
end;

procedure TTeeGridEditor.PageCellsChange(Sender: TObject);
begin
  if Grid=nil then
     Exit;

  if PageCells.ActivePage=TabCellsFormat then
  begin
    if ICells=nil then
       ICells:=TTextRenderEditor.Embedd(Self,TabCellsFormat,Grid.Rows.Render as TTextRender);
  end
  else
  if PageCells.ActivePage=TabCellsHover then
  begin
    if ICellsHover=nil then
       ICellsHover:=TTextRenderEditor.Embedd(Self,TabCellsHover,Grid.Rows.Hover);
  end
  else
  if PageCells.ActivePage=TabColumnLines then
  begin
    if IColumnLines=nil then
       IColumnLines:=TStrokeEditor.Embedd(Self,TabColumnLines,Grid.Rows.Lines);
  end;
end;

procedure TTeeGridEditor.FillThemes;
begin
  LBThemes.Items.BeginUpdate;
  try
    LBThemes.Clear;

    LBThemes.Items.Add('Default');
    LBThemes.Items.Add('Black');
    LBThemes.Items.Add('iOS');
    LBThemes.Items.Add('Android');
  finally
    LBThemes.Items.EndUpdate;
  end;
end;

procedure TTeeGridEditor.PageGridChange(Sender: TObject);
begin
  if PageGrid.ActivePage=TabBands then
     PageBandsChange(Self)
  else
  if PageGrid.ActivePage=TabCells then
     PageCellsChange(Self)
  else
  if PageGrid.ActivePage=TabRows then
     PageRowsChange(Self)
  else
  if PageGrid.ActivePage=TabOptions then
     PageOptionsChange(Self)
  else
  if PageGrid.ActivePage=TabTheme then
     if LBThemes.Count=0 then
        FillThemes;
end;

procedure TTeeGridEditor.PageIndicatorChange(Sender: TObject);
begin
  if Grid=nil then
     Exit;

  if PageIndicator.ActivePage=TabIndicatorFormat then
  begin
    if IIndicatorFormat=nil then
       IIndicatorFormat:=TFormatEditor.Embedd(Self,TabIndicatorFormat,Grid.Indicator.Format);
  end
  else
  if PageIndicator.ActivePage=TabIndicatorWidth then
  begin
    if IIndicatorWidth=nil then
       IIndicatorWidth:=TCoordinateEditor.Embedd(Self,TabIndicatorWidth,Grid.Indicator.Width);
  end;
end;

procedure TTeeGridEditor.PageOptionsChange(Sender: TObject);
begin
  if PageOptions.ActivePage=TabIndicator then
     PageIndicatorChange(Self)
  else
  if PageOptions.ActivePage=TabBack then
  begin
    if IBack=nil then
       IBack:=TFormatEditor.Embedd(Self,TabBack,Grid.Back);
  end
  else
  if PageOptions.ActivePage=TabMargins then
  begin
    if IMargins=nil then
       IMargins:=TMarginsEditor.Embedd(Self,TabMargins,Grid.Grid.Margins);
  end
  else
  if PageOptions.ActivePage=TabSelection then
  begin
    if ISelectedFocused=nil then
       ISelectedFocused:=TTextRenderEditor.Embedd(Self,TabSelectedFocused,Grid.Selected);

    if ISelectedUnFocused=nil then
       ISelectedUnFocused:=TTextRenderEditor.Embedd(Self,TabSelectedUnFocused,Grid.Selected.UnFocused);
  end
end;

procedure TTeeGridEditor.PageRowsChange(Sender: TObject);
begin
  if PageRows.ActivePage=TabRowLines then
  begin
    if IRowLines=nil then
       IRowLines:=TStrokeEditor.Embedd(Self,TabRowLines,Grid.Rows.RowLines);
  end
  else
  if PageRows.ActivePage=TabRowAlternate then
  begin
    if IRowAlternate=nil then
       IRowAlternate:=TFormatEditor.Embedd(Self,TabRowAlternate,Grid.Rows.Alternate);

    // VCL bug, hiding tab does not refresh page control correctly (empty tab)
    // IRowAlternate.TabFont.TabVisible:=False;
  end;
end;

procedure TTeeGridEditor.RefreshMargins(const AMargins:TMargins);
begin
  if IMargins<>nil then
     IMargins.RefreshMargins(AMargins);
end;

procedure TTeeGridEditor.RefreshSelected(const ASelected:TGridSelection);
begin
  CBFullRow.Checked:=Grid.Selected.FullRow;
  CBSelectedParentFont.Checked:=Grid.Selected.ParentFont;
  CBSelectedRange.Checked:=Grid.Selected.Range.Enabled;
end;

procedure TTeeGridEditor.RGPainterClick(Sender: TObject);
begin
  {$IFDEF HASGDIPLUS} // XE2 and up only (missing GDIPAPI / GDIPOBJ units)
  if RGPainter.ItemIndex=0 then
     Grid.Painter:=TGdiPlusPainter.Create
  else
     Grid.Painter:=TGdiPainter.Create(Grid.Canvas);
  {$ENDIF}
end;

procedure TTeeGridEditor.RefreshGrid(const AGrid: TTeeGrid);
begin
  Grid:=AGrid;

  if Grid.Name<>'' then
     Caption:='Editing '+Grid.Name;

  UDRowHeight.Position:=Round(Grid.Rows.Height.Pixels);
  CBRowHeightAuto.Checked:=Grid.Rows.Height.Automatic;

  CBReadOnly.Checked:=Grid.ReadOnly;

  CBDoubleClick.Checked:=Grid.Editing.DoubleClick;
  CBEditingAlways.Checked:=Grid.Editing.AlwaysVisible;

  CBScrollBars.Checked:=Grid.ScrollBars.Visible;

  RefreshSelected(Grid.Selected);

  CBAlternateVisible.Checked:=Grid.Rows.Alternate.Visible;

  RefreshMargins(Grid.Grid.Margins);

  ShowColumns(TreeColumns,Grid.Columns);

  SetSpacingSettings;
end;

procedure TTeeGridEditor.SetSpacingSettings;
begin
  TBHorizSpacing.Position:=Round(Grid.Columns.Spacing.Value);
  LHorizSpacing.Caption:=IntToStr(TBHorizSpacing.Position);

  TBVertSpacing.Position:=Round(Grid.Rows.Spacing.Value);
  LVertSpacing.Caption:=IntToStr(TBVertSpacing.Position);
end;

procedure TTeeGridEditor.CBAlternateVisibleClick(Sender: TObject);
begin
  Grid.Rows.Alternate.Visible:=CBAlternateVisible.Checked;
end;

procedure TTeeGridEditor.CBDoubleClickClick(Sender: TObject);
begin
  Grid.Editing.DoubleClick:=CBDoubleClick.Checked;
end;

procedure TTeeGridEditor.CBEditingAlwaysClick(Sender: TObject);
begin
  Grid.Editing.AlwaysVisible:=CBEditingAlways.Checked;
end;

procedure TTeeGridEditor.CBFullRowClick(Sender: TObject);
begin
  Grid.Selected.FullRow:=CBFullRow.Checked;
end;

procedure TTeeGridEditor.CBIndicatorVisibleClick(Sender: TObject);
begin
  Grid.Indicator.Visible:=CBIndicatorVisible.Checked;
end;

procedure TTeeGridEditor.CBReadOnlyClick(Sender: TObject);
begin
  Grid.ReadOnly:=CBReadOnly.Checked;
end;

procedure TTeeGridEditor.CBRowHeightAutoClick(Sender: TObject);
begin
  Grid.Rows.Height.Automatic:=CBRowHeightAuto.Checked;
end;

procedure TTeeGridEditor.CBScrollBarsClick(Sender: TObject);
begin
  Grid.ScrollBars.Visible:=CBScrollBars.Checked;
end;

procedure TTeeGridEditor.CBSelectedParentFontClick(Sender: TObject);
begin
  Grid.Selected.ParentFont:=CBSelectedParentFont.Checked;
end;

procedure TTeeGridEditor.CBSelectedRangeClick(Sender: TObject);
begin
  Grid.Selected.Range.Enabled:=CBSelectedRange.Checked;
end;

procedure TTeeGridEditor.TBHorizSpacingChange(Sender: TObject);
begin
  Grid.Columns.Spacing.Value:=TBHorizSpacing.Position;
  LHorizSpacing.Caption:=IntToStr(TBHorizSpacing.Position);
end;

procedure TTeeGridEditor.TBVertSpacingChange(Sender: TObject);
begin
  Grid.Rows.Spacing.Value:=TBVertSpacing.Position;
  LVertSpacing.Caption:=IntToStr(TBVertSpacing.Position);
end;

procedure TTeeGridEditor.EnableUpDown;
var tmp : TColumn;
begin
  tmp:=Column;

  SBColumnUp.Enabled:=(tmp<>nil) and (tmp.Index>0);
  SBColumnDown.Enabled:=(tmp<>nil) and (tmp.Index<tmp.Collection.Count-1);
end;

procedure TTeeGridEditor.ERowHeightChange(Sender: TObject);
begin
  Grid.Rows.Height.Automatic:=False;
  Grid.Rows.Height.Pixels:=UDRowHeight.Position;
  CBRowHeightAuto.Checked:=False;
end;

procedure TTeeGridEditor.TreeColumnsChange(Sender: TObject; Node: TTreeNode);
var tmp : TColumn;
begin
  if UpdatingTree then
     Exit;

  tmp:=Column;

  EnableUpDown;

  SBDeleteColumn.Enabled:=tmp<>nil;
  PanelEditor.Visible:=tmp<>nil;
  Splitter1.Visible:=tmp<>nil;

  if PanelEditor.Visible then
  begin
    if IColumnEditor=nil then
    begin
      IColumnEditor:=TColumnEditor.Embedd(Self,PanelEditor,tmp);
      IColumnEditor.OnChangedHeader:=ChangedHeader;
    end
    else
       IColumnEditor.RefreshColumn(tmp);

    Splitter1.Top:=IColumnEditor.Top-1;
  end;
end;

procedure TTeeGridEditor.TreeColumnsEdited(Sender: TObject; Node: TTreeNode;
  var S: string);
begin
  Column.Header.Text:=S;

  IColumnEditor.MemoHeader.Text:=S;
end;

procedure TTeeGridEditor.ChangedHeader(Sender:TObject);
begin
  TreeColumns.Selected.Text:=Column.Header.DisplayText;
end;

procedure TTeeGridEditor.MoveColumn(const Delta:Integer);
var tmp: TTreeNode;
begin
  Column.Index:=Column.Index+Delta;

  tmp:=TreeColumns.Selected;

  if Delta=-1 then
  begin
    if tmp.getPrevSibling<>nil then
       tmp.MoveTo(tmp.getPrevSibling,naInsert)
    else
       tmp.MoveTo(tmp.getPrevSibling,naAddFirst);
  end
  else
  begin
    if tmp.getNextSibling.getNextSibling = nil then
       tmp.MoveTo(tmp.getNextSibling,naAdd)
    else
       tmp.MoveTo(tmp.getNextSibling.getNextSibling,naInsert);
  end;

  EnableUpDown;
end;

procedure TTeeGridEditor.SBColumnDownClick(Sender: TObject);
begin
  MoveColumn(1);
end;

procedure TTeeGridEditor.SBColumnUpClick(Sender: TObject);
begin
  MoveColumn(-1);
end;

procedure TTeeGridEditor.SBDeleteColumnClick(Sender: TObject);
begin
  Column.Free;
  TreeColumns.Selected.Free;
end;

procedure TTeeGridEditor.ShowColumns(const ATree:TTreeView; const AColumns:TColumns);

  procedure Add(const AParent:TTreeNode; const AColumns:TColumns);
  var tmp : TTreeNode;
      tmpCol : TColumn;
      t : Integer;
  begin
    for t:=0 to AColumns.Count-1 do
    begin
      tmpCol:=AColumns[t];

      tmp:=ATree.Items.AddChildObject(AParent,tmpCol.Header.DisplayText,tmpCol);

      if tmpCol.HasItems then
         Add(tmp,tmpCol.Items);
    end;
  end;

begin
  UpdatingTree:=True;
  try
    ATree.Items.BeginUpdate;
    ATree.Items.Clear;

    Add(nil,AColumns);

    ATree.Items.EndUpdate;
  finally
    UpdatingTree:=False;

    TreeColumnsChange(Self,nil);
  end;
end;

end.
