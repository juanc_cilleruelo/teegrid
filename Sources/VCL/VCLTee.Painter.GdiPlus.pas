{*********************************************}
{  TeeGrid Software Library                   }
{  Windows GDI+ Plus Painter class            }
{  Copyright (c) 2016 by Steema Software      }
{  All Rights Reserved                        }
{*********************************************}
unit VCLTee.Painter.GdiPlus;
{$I Tee.inc}

interface

uses
  {System.}Types,
  Windows, {VCL.}Graphics, {Winapi.}GdiPAPI, {Winapi.}GdiPObj,
  Tee.Painter, Tee.Format, VCLTee.Painter;

type
  TGdiPlusPainter=class(TWindowsPainter)
  private
    IPlus : TGPGraphics;

    IBitmap : TBitmap;
    IBrush : TGPSolidBrush;
    IFont : TGPFont;
    IFontBrush,
    ISolidBrush : TGPSolidBrush;
    IPen : TGpPen;

    IHorizAlign : THorizontalAlign;
    IVertAlign : TVerticalAlign;

    IClipped : Array of TGPRectF;

    IFontGradient,
    IStrokeGradient,
    IBrushGradient : TGradient;

    IBrushPicture : TPicture;

    // Maximum 256 colors for Gradient:
    IBlendColors : Array[0..255] of TGPColor;
    IBlendPositions : Array[0..255] of Single;

    function CreateGradient(const AGradient:TGradient; const R:TGPRectF):TGPBrush; overload;
    function CreateGradient(const AGradient:TGradient; const P:TPointsF):TGPBrush; overload;

    procedure FinishPen(const APen:TGPPen; const AStroke: TStroke);
    function GetCanvas:TGPGraphics;
    function ImageFrom(const AGraphic: TGraphic):TGPImage; overload;
    function ImageFrom(const APicture: TPicture):TGPImage; overload;
    function TextSize(const AText:String):TGPRectF;
  public
    Constructor Create;
    Destructor Destroy; override;

    property Canvas:TGPGraphics read GetCanvas;

    procedure Clip(const R:TRectF); override;
    procedure UnClip; override;

    procedure HideBrush; override;

    procedure Init(const DC:HDC); override;

    procedure SetBrush(const ABrush:TBrush); override;
    procedure SetFont(const AFont:TFont); override;
    procedure SetHorizontalAlign(const Align:THorizontalAlign); override;
    procedure SetStroke(const AStroke:TStroke); override;
    procedure SetVerticalAlign(const Align:TVerticalAlign); override;

    procedure Draw(const R:TRectF); override;
    procedure Draw(const P:TPointsF); override;
    procedure Draw(const APicture:TPicture; const X,Y:Single); overload; override;
    procedure Draw(const APicture:TPicture; const R:TRectF); overload; override;
    procedure DrawEllipse(const R:TRectF); override;

    procedure Fill(const R:TRectF); override;
    procedure Fill(const R:TRectF; const AColor:TColor); override;
    procedure Fill(const P:TPointsF); override;
    procedure FillEllipse(const R:TRectF); override;

    procedure HorizontalLine(const Y,X0,X1:Single); override;
    procedure Line(const X0,Y0,X1,Y1:Single); override;
    procedure Lines(const P:TPointsF); override;
    procedure Paint(const AFormat:TFormat; const R:TRectF); override;
    procedure Paint(const AFormat:TFormat; const P:TPointsF); override;
    function TextHeight(const AText:String):Single; override;
    procedure TextOut(const X,Y:Single; const AText:String); override;
    function TextWidth(const AText:String):Single; override;
    procedure VerticalLine(const X,Y0,Y1:Single); override;
  end;

implementation

uses
  {System.}Classes, {System.}SysUtils, {WinAPI.}ActiveX
  {$IFNDEF NOUITYPES}
  ,{System.}UITypes
  {$ENDIF}
  ;

function GDIPColor(const AColor:TColor):TGPColor; inline;
begin
  result:=TColorHelper.SwapCheck(AColor);
end;

{ TGdiPlusPainter }

Constructor TGdiPlusPainter.Create;
begin
  inherited Create(nil);

  IBrush:=TGPSolidBrush.Create(GDIPColor(TColors.White));
  IPen:=TGPPen.Create(GDIPColor(TColors.Black));
  IFont:=TGPFont.Create;
  IFontBrush:=TGPSolidBrush.Create(GDIPColor(TColors.Black));
  ISolidBrush:=TGPSolidBrush.Create(GDIPColor(TColors.White));

  Canvas; // <-- force creating an initial "dummy" TGPGraphics
end;

function TGdiPlusPainter.CreateGradient(const AGradient:TGradient; const P: TPointsF): TGPBrush;

  function CreateRadial:TGPPathGradientBrush;
  begin
    result:=TGPPathGradientBrush.Create(PGPPointF(P),Length(P));
  end;

  function CreateLinear:TGPLinearGradientBrush;
  begin
    result:=TGPLinearGradientBrush.Create; //(PGPPointF(P),Length(P));
  end;

begin
  if AGradient.Direction=TGradientDirection.Radial then
     result:=CreateRadial
  else
     result:=CreateLinear;
end;

Destructor TGdiPlusPainter.Destroy;
begin
  IBitmap.Free;
  ISolidBrush.Free;
  IFontBrush.Free;
  IFont.Free;
  IPen.Free;
  IBrush.Free;
  ICanvas.Free;

  inherited;
end;

function GPRectF(const R:TRectF):TGPRectF;
begin
  result.X:=R.Left;
  result.Y:=R.Top;

  result.Width:=R.Right-R.Left;
  result.Height:=R.Bottom-R.Top;
end;

function RectCenterF(const R:TGPRectF):TGPPointF;
begin
  result.X:=R.X+(R.Width*0.5);
  result.Y:=R.Y+(R.Height*0.5);
end;

function InflateRectF(const R:TGPRectF; const X,Y:Single):TGPRectF;
begin
  result.X:=R.X-X;
  result.Y:=R.Y-Y;
  result.Width:=R.Width+2*X;
  result.Height:=R.Height+2*Y;
end;

procedure TGdiPlusPainter.Draw(const R: TRectF);
var tmp : TGPRectF;
    tmpW : Single;
    tmpBrush : TGPBrush;
begin
  tmp:=GPRectF(R);

  tmpW:=(0.5*IPen.GetWidth)-0.5;

  tmp.X:=tmp.X+tmpW;
  tmp.Y:=tmp.Y+tmpW;

  tmpW:=tmpW+0.5+(0.5*IPen.GetWidth);

  tmp.Width:=tmp.Width-tmpW;
  tmp.Height:=tmp.Height-tmpW;

  if IStrokeGradient=nil then
     IPlus.DrawRectangle(IPen,tmp)
  else
  begin
    tmpBrush:=CreateGradient(IStrokeGradient,GPRectF(R));

    IPen.SetBrush(tmpBrush);
    IPlus.DrawRectangle(IPen,tmp);
    tmpBrush.Free;
  end;
end;

procedure TGdiPlusPainter.Fill(const R:TRectF; const AColor:Tee.Format.TColor);
begin
  ISolidBrush.SetColor(GDIPColor(AColor));
  IPlus.FillRectangle(ISolidBrush,GPRectF(R));
end;

function TGdiPlusPainter.CreateGradient(const AGradient:TGradient; const R:TGPRectF):TGPBrush;
type
  TColorBlend=record
    Colors    : PGPColor;
    Positions : PSingle;
  end;

  function GetColorBlend(out ACount:Integer):TColorBlend;
  var t : Integer;
      tmp : TGradientColor;
  begin
    ACount:=AGradient.Colors.Count;

    if AGradient.Inverted then
      for t:=0 to ACount-1 do
      begin
        tmp:=AGradient.Colors[t];
        IBlendPositions[t]:=tmp.Offset;
        IBlendColors[t]:=GDIPColor(tmp.Color);
      end
    else
      for t:=0 to ACount-1 do
      begin
        tmp:=AGradient.Colors[ACount-t-1];
        IBlendPositions[t]:=1-tmp.Offset;
        IBlendColors[t]:=GDIPColor(tmp.Color);
      end;

    result.Colors:=@IBlendColors;
    result.Positions:=@IBlendPositions;
  end;

  procedure SetGradientColors(const ABrush:TGPPathGradientBrush); overload;
  var tmpBlend : TColorBlend;
      ColorItems : Integer;
  begin
    tmpBlend:=GetColorBlend(ColorItems);
    ABrush.SetInterpolationColors(PARGB(tmpBlend.Colors), tmpBlend.Positions, ColorItems);
  end;

  procedure SetGradientColors(const ABrush:TGPLinearGradientBrush); overload;
  var tmpBlend : TColorBlend;
      ColorItems : Integer;
  begin
    tmpBlend:=GetColorBlend(ColorItems);
    ABrush.SetInterpolationColors(tmpBlend.Colors, tmpBlend.Positions, ColorItems);
  end;

  function ColorFrom(const AIndex:Integer):Cardinal;
  begin
    result:=GDIPColor(AGradient.Colors[AIndex].Color);
  end;

  function Color0:Cardinal;
  begin
    if AGradient.Inverted then
       result:=ColorFrom(1)
    else
       result:=ColorFrom(0);
  end;

  function Color1:Cardinal;
  begin
    if AGradient.Inverted then
       result:=ColorFrom(0)
    else
       result:=ColorFrom(1);
  end;

  function Mode:TLinearGradientMode;
  begin
    case AGradient.Direction of
        TGradientDirection.Vertical: result:=LinearGradientModeVertical;
      TGradientDirection.Horizontal: result:=LinearGradientModeHorizontal;
        TGradientDirection.Diagonal: result:=LinearGradientModeForwardDiagonal;
    else
       result:=LinearGradientModeBackwardDiagonal;
    end;
  end;

  function CreateRadial:TGPPathGradientBrush;
  var tmpPath : TGPGraphicsPath;
      P : TGPPointF;
  begin
    tmpPath:=TGPGraphicsPath.Create;

    //if AGradient.Direction=TGradientDirection.FromCenter then
    //   tmpPath.AddRectangle(R)
    //else
         tmpPath.AddEllipse(InflateRectF(R,R.Width*0.25,R.Height*0.25));

    result:=TGPPathGradientBrush.Create(tmpPath);

    tmpPath.Free;

    P:=RectCenterF(R);
    //P.X:=P.X+AGradient.Offset.X;
    //P.Y:=P.Y+AGradient.Offset.Y;

    result.SetCenterPoint(P);

    SetGradientColors(result);
  end;

  function CreateLinear:TGPLinearGradientBrush;
  begin
    if AGradient.Angle=0 then
       result:=TGPLinearGradientBrush.Create(R,Color0,Color1,Mode)
    else
       result:=TGPLinearGradientBrush.Create(R,Color0,Color1,AGradient.Angle);

    if AGradient.Colors.Count>2 then
       SetGradientColors(result);
  end;

begin
  if AGradient.Direction=TGradientDirection.Radial then
     result:=CreateRadial
  else
     result:=CreateLinear;
end;

procedure TGdiPlusPainter.Fill(const R: TRectF);
var tmp : TGPBrush;
    tmpR : TGPRectF;
    tmpImage : TGPImage;
begin
  tmpR:=GPRectF(R);

  if IBrushPicture<>nil then
  begin
    //if StretchDrawQuality=sqHigh then
    //   IPlus.SetInterpolationMode(InterpolationModeHighQuality)
    //else
    //   IPlus.SetInterpolationMode(InterpolationModeNearestNeighbor);

    //if Opacity>0 then
    //   DrawBlendImage(AImage,MakeRect(ARect))
    //else
    //   IPlus.DrawImage(AImage,MakeRect(ARect));

    tmpImage:=ImageFrom(IBrushPicture);

    if tmpImage<>nil then
       IPlus.DrawImage(tmpImage,tmpR);
  end
  else
  if IBrushGradient=nil then
     IPlus.FillRectangle(IBrush,tmpR)
  else
  begin
    tmp:=CreateGradient(IBrushGradient,tmpR);
    IPlus.FillRectangle(tmp,tmpR);
    tmp.Free;
  end;
end;

procedure TGdiPlusPainter.Fill(const P:TPointsF);
var tmp : TGPBrush;
begin
  if IBrushGradient=nil then
     IPlus.FillPolygon(IBrush,PGPPointF(P),Length(P))
  else
  begin
    tmp:=CreateGradient(IBrushGradient,P);
    IPlus.FillPolygon(tmp,PGPPointF(P),Length(P));
    tmp.Free;
  end;
end;

procedure TGdiPlusPainter.FillEllipse(const R: TRectF);
var tmp : TGPBrush;
    tmpR : TGPRectF;
begin
  if IBrushGradient=nil then
     IPlus.FillEllipse(IBrush,GPRectF(R))
  else
  begin
    tmpR:=GPRectF(R);
    tmp:=CreateGradient(IBrushGradient,tmpR);
    IPlus.FillEllipse(IBrush,tmpR);
    tmp.Free;
  end;
end;

function TGdiPlusPainter.GetCanvas: TGPGraphics;

  procedure DoRaise(const AStatus:TStatus);
  begin
    Raise Exception.Create('Error creating GdiPlus graphics: '+IntToStr(Ord(AStatus)));
  end;

  procedure CheckError;
  var tmp : TStatus;
  begin
    tmp:=IPlus.GetLastStatus;

    if tmp<>Ok then
       DoRaise(tmp);
  end;

  // Setup anti-alias for curves and text
  procedure InitGraphics;
  begin
    IPlus.SetPageUnit(UnitPixel);

    IPlus.SetSmoothingMode(SmoothingModeHighQuality);
    IPlus.SetTextRenderingHint(TextRenderingHintClearTypeGridFit);
    IPlus.SetInterpolationMode(InterpolationModeHighQuality)
  end;

  procedure VerifyHandle;
  begin
    // Use a temporary "dummy" TBitmap to provide a DC handle for our TGPGraphics
    if IDC=0 then
    begin
      if IBitmap=nil then
         IBitmap:=TBitmap.Create;

      IDC:=IBitmap.Canvas.Handle;
    end;
  end;

begin
  if IPlus=nil then
  begin
    VerifyHandle;

    // Create GDI+ graphics
    IPlus:=TGPGraphics.Create(IDC);

    CheckError;
    InitGraphics;
  end;

  result:=IPlus;
end;

procedure TGdiPlusPainter.HideBrush;
begin
//  ICanvas.Brush.Style:=TBrushStyle.bsClear;
end;

procedure TGdiPlusPainter.HorizontalLine(const Y, X0, X1: Single);
begin
  IPlus.DrawLine(IPen,X0,Y,X1,Y);
end;

type
{$IFDEF FPC}
  TVCLPicture=Graphics.TPicture;
  TVCLGraphic=Graphics.TGraphic;
{$ELSE}
  TVCLPicture=Graphics.TPicture;
  TVCLGraphic=Graphics.TGraphic;
{$ENDIF}

type
  TPictureAccess=class(TPicture);

function TGdiPlusPainter.ImageFrom(const APicture: TPicture): TGPImage;
var tmp : TObject;
begin
  if TPictureAccess(APicture).Internal=nil then
  begin
    tmp:=TPictureAccess(APicture).TagObject;

    if tmp is TVCLPicture then
       TPictureAccess(APicture).Internal:=ImageFrom(TVCLPicture(tmp).Graphic)
    else
    if tmp is TVCLGraphic then
       TPictureAccess(APicture).Internal:=ImageFrom(TVCLGraphic(tmp))
  end;

  result:=TGPImage(TPictureAccess(APicture).Internal);
end;

procedure TGdiPlusPainter.Init(const DC: HDC);
var tmp : Boolean;
begin
  tmp:=IDC<>DC;

  inherited;

  if tmp then
  begin
    IPlus.Free;
    IPlus:=nil;

    Canvas;

    IPlus.Clear(GDIPColor(TColors.White));
  end;
end;

procedure TGdiPlusPainter.Line(const X0, Y0, X1, Y1: Single);
begin
  IPlus.DrawLine(IPen,X0,Y0,X1,Y1);
end;

procedure TGdiPlusPainter.Lines(const P: TPointsF);
begin
  IPlus.DrawLines(IPen,PGPPointF(P),Length(P));
end;

procedure TGdiPlusPainter.Paint(const AFormat: TFormat; const R: TRectF);
begin
  if AFormat.Brush.Visible then
  begin
    SetBrush(AFormat.Brush);
    Fill(R);
  end;

  if AFormat.Stroke.Visible then
  begin
    SetStroke(AFormat.Stroke);
    Draw(R);
  end;
end;

procedure TGdiPlusPainter.Paint(const AFormat: TFormat; const P: TPointsF);
begin
  if AFormat.Brush.Visible then
  begin
    SetBrush(AFormat.Brush);
    Fill(P);
  end;

  if AFormat.Stroke.Visible then
  begin
    SetStroke(AFormat.Stroke);
    Draw(P);
  end;
end;

function GraphicFrom(const AObject:TObject):TGraphic;
begin
  if AObject is TVCLPicture then
     result:=TVCLPicture(AObject).Graphic
  else
  if AObject is TGraphic then
     result:=TGraphic(AObject)
  else
     result:=nil;
end;

type
  TBrushAccess=class(TBrush);

procedure TGdiPlusPainter.SetBrush(const ABrush: TBrush);
begin
  if TBrushAccess(ABrush).HasPicture then
     IBrushPicture:=ABrush.Picture
  else
  begin
    IBrushPicture:=nil;

    if TBrushAccess(ABrush).HasGradient then
       IBrushGradient:=ABrush.Gradient
    else
    begin
      IBrush.SetColor(GDIPColor(ABrush.Color));
      IBrushGradient:=nil;
    end;
  end;
end;

procedure TGdiPlusPainter.SetFont(const AFont: TFont);

  function FontStyle:Integer;
  begin
    result:=0;

    if AFont.Style<>[] then // optimization
    begin
      if fsBold in AFont.Style then
         result:=result or FontStyleBold;

      if fsItalic in AFont.Style then
         result:=result or FontStyleItalic;

      if fsUnderline in AFont.Style then
         result:=result or FontStyleUnderline;

      if fsStrikeOut in AFont.Style then
         result:=result or FontStyleStrikeout;
    end;
  end;

begin
  IFont.Free;
  IFont:=TGPFont.Create(AFont.Name,64*AFont.Size/96,FontStyle);

  if TBrushAccess(AFont.Brush).HasGradient then
     IFontGradient:=AFont.Brush.Gradient
  else
     IFontGradient:=nil;

  IFontBrush.SetColor(GDIPColor(AFont.Color));
end;

procedure TGdiPlusPainter.SetHorizontalAlign(const Align: THorizontalAlign);
begin
  IHorizAlign:=Align;
end;

procedure TGdiPlusPainter.FinishPen(const APen:TGPPen; const AStroke: TStroke);
begin
  APen.SetWidth(AStroke.Size);

  case AStroke.Style of
        TStrokeStyle.Solid: APen.SetDashStyle(DashStyleSolid);
         TStrokeStyle.Dash: APen.SetDashStyle(DashStyleDash);
          TStrokeStyle.Dot: APen.SetDashStyle(DashStyleDot);
      TStrokeStyle.DashDot: APen.SetDashStyle(DashStyleDashDot);
   TStrokeStyle.DashDotDot: APen.SetDashStyle(DashStyleDashDotDot);
  end;

  case AStroke.EndStyle of
      TStrokeEnd.Round: APen.SetLineCap(LineCapRound, LineCapRound, DashCapRound);
     TStrokeEnd.Square: aPen.SetLineCap(LineCapSquare, LineCapSquare, DashCapTriangle);
  else
    APen.SetLineCap(LineCapFlat, LineCapFlat, DashCapFlat);
  end;

  case AStroke.JoinStyle of
    TStrokeJoin.Round: APen.SetLineJoin(LineJoinRound);
    TStrokeJoin.Bevel: APen.SetLineJoin(LineJoinBevel);
  else
    APen.SetLineJoin(LineJoinMiter);
  end;
end;

procedure TGdiPlusPainter.SetStroke(const AStroke: TStroke);
begin
  if TBrushAccess(AStroke.Brush).HasGradient then
     IStrokeGradient:=AStroke.Brush.Gradient
  else
  begin
    IStrokeGradient:=nil;
    IPen.SetColor(GDIPColor(AStroke.Color));
  end;

  FinishPen(IPen,AStroke);
end;

procedure TGdiPlusPainter.SetVerticalAlign(const Align: TVerticalAlign);
begin
  IVertAlign:=Align;
end;

function TGdiPlusPainter.TextHeight(const AText: String): Single;
begin
  result:=TextSize(AText).Height;
end;

procedure TGdiPlusPainter.TextOut(const X, Y: Single; const AText: String);

  procedure PaintGradient(const P:TGPPointF);

    function TextBounds:TGPRectF;
    begin
      result:=TextSize(AText);

      result.X:=P.X;
      result.Y:=P.Y;
    end;

  var tmpR : TGPRectF;
      tmp : TGPBrush;
  begin
    tmpR:=TextBounds;
    tmp:=CreateGradient(IFontGradient,tmpR);

    if SysLocale.FarEast then
       IPlus.DrawString(AText,ElementToCharIndex({ ByteToCharIndex(}AText,Length(AText)),IFont,P,tmp)
    else
       IPlus.DrawString(AText,Length(AText),IFont,P,tmp);

    tmp.Free;
  end;

var P : TGPPointF;
    tmp : Single;
begin
  if IHorizAlign=THorizontalAlign.Left then
     P.X:=X
  else
  begin
    tmp:=TextWidth(AText);

    if IHorizAlign=THorizontalAlign.Center then
       P.X:=X-(0.5*tmp)
    else
       P.X:=X-tmp;
  end;

  P.Y:=Y;

  if IFontGradient=nil then
  begin
    if SysLocale.FarEast then
       IPlus.DrawString(AText,ElementToCharIndex({ ByteToCharIndex(}AText,Length(AText)),IFont,P,IFontBrush)
    else
       IPlus.DrawString(AText,Length(AText),IFont,P,IFontBrush);
  end
  else
    PaintGradient(P);
end;

const
  ZeroPoint:TGPPointF=(X:0; Y:0);

function TGdiPlusPainter.TextSize(const AText:String):TGPRectF;
begin
  IPlus.MeasureString(AText,Length(AText),IFont,ZeroPoint, {TGPStringFormat.GenericTypographic,} result);
end;

function TGdiPlusPainter.TextWidth(const AText: String): Single;
begin
  result:=TextSize(AText).Width;
end;

procedure TGdiPlusPainter.VerticalLine(const X, Y0, Y1: Single);
begin
  IPlus.DrawLine(IPen,X,Y0,X,Y1);
end;

procedure TGdiPlusPainter.UnClip;
var L : Integer;
begin
  L:=High(IClipped);

  if L>-1 then
  begin
    IPlus.SetClip(IClipped[L]);
    SetLength(IClipped,L);
  end
  else
    IPlus.ResetClip;
end;

procedure TGdiPlusPainter.Clip(const R: TRectF);
var L : Integer;
begin
  L:=Length(IClipped);
  SetLength(IClipped,L+1);

  IPlus.GetVisibleClipBounds(IClipped[L]);

  IPlus.IntersectClip(GPRectF(R));
end;

procedure TGdiPlusPainter.Draw(const P:TPointsF);
begin
  IPlus.DrawPolygon(IPen,PGPPointF(P),Length(P));
end;

function BitmapFrom(const AIcon:TIcon):TBitmap; overload;
begin
  result:=TBitmap.Create;
  result.SetSize(AIcon.Width,AIcon.Height);

  DrawIconEx(result.Canvas.Handle, 0,0, AIcon.Handle, 0, 0, 0, 0, DI_NORMAL);
end;

function BitmapFrom(const AGraphic:TGraphic):TBitmap; overload;
begin
  result:=TBitmap.Create;
  result.SetSize(AGraphic.Width,AGraphic.Height);

  result.Canvas.StretchDraw(Rect(0,0,result.Width,result.Height),AGraphic);
end;

function TGdiPlusPainter.ImageFrom(const AGraphic: TGraphic):TGPImage;

  function FromStream:TGPImage;
  var tmpAdapter: TStreamAdapter;
      tmpStream : TMemoryStream;
  begin
    tmpStream:=TMemoryStream.Create;
    try
      AGraphic.SaveToStream(tmpStream);
      tmpStream.Position:=0;

      tmpAdapter:=TStreamAdapter.Create(tmpStream);
      result:=TGPBitmap.Create(tmpAdapter as IStream);
    finally
      tmpStream.Free;
    end;
  end;

var tmp : TBitmap;
begin
  if AGraphic is TBitmap then
     result:=TGPBitmap.Create(TBitmap(AGraphic).Handle,TBitmap(AGraphic).Palette)
  else
  if AGraphic is TMetafile then
     result:=TGPMetafile.Create(TMetaFile(AGraphic).Handle,False)
  else
  if AGraphic is TIcon then
  begin
    tmp:=BitmapFrom(TIcon(AGraphic));
    try
      result:=ImageFrom(tmp);
    finally
      tmp.Free;
    end;
  end
  else
  begin
    result:=FromStream;
    {
    tmp:=BitmapFrom(AGraphic);
    try
      result:=ImageFrom(tmp);
    finally
      tmp.Free;
    end;
    }
  end;
end;

procedure TGdiPlusPainter.Draw(const APicture: TPicture; const X, Y: Single);
var tmp : TGPImage;
begin
  tmp:=ImageFrom(APicture);

  if tmp<>nil then
     IPlus.DrawImage(tmp,X,Y);
end;

procedure TGdiPlusPainter.Draw(const APicture: TPicture; const R: TRectF);
var tmp : TGPImage;
begin
  tmp:=ImageFrom(APicture);

  if tmp<>nil then
     IPlus.DrawImage(tmp,GPRectF(R));
end;

procedure TGdiPlusPainter.DrawEllipse(const R: TRectF);
begin
  IPlus.DrawEllipse(IPen,GPRectF(R));
end;

end.
