unit VCLTee.Editor.Font;

interface

uses
  {Winapi.}Windows, {Winapi.}Messages, {System.}SysUtils, {System.}Classes,
  {Vcl.}Graphics, {Vcl.}Controls, {Vcl.}Forms, {Vcl.}Dialogs, {Vcl.}StdCtrls,
  {Vcl.}ComCtrls,

  VCLTee.Editor.Brush,

  // Must be last unit
  Tee.Format;

type
  TFontEditor = class(TForm)
    PageFont: TPageControl;
    TabOptions: TTabSheet;
    Label2: TLabel;
    Label1: TLabel;
    LSize: TLabel;
    TBSize: TTrackBar;
    LBName: TListBox;
    GroupBox1: TGroupBox;
    CBBold: TCheckBox;
    CBItalic: TCheckBox;
    CBUnderline: TCheckBox;
    CBStrikeOut: TCheckBox;
    TabFill: TTabSheet;
    procedure CBBoldClick(Sender: TObject);
    procedure CBItalicClick(Sender: TObject);
    procedure CBStrikeOutClick(Sender: TObject);
    procedure CBUnderlineClick(Sender: TObject);
    procedure LBNameClick(Sender: TObject);
    procedure PageFontChange(Sender: TObject);
    procedure TBSizeChange(Sender: TObject);
  private
    { Private declarations }

    IBrush : TBrushEditor;

    IFont : TFont;

    procedure ChangeFontStyle(const Enable:Boolean; const AStyle:TFontStyle);
  public
    { Public declarations }

    procedure RefreshFont(const AFont:TFont);

    class function Edit(const AOwner:TComponent; const AFont:TFont):Boolean; static;

    class function Embedd(const AOwner:TComponent; const AParent:TWinControl;
                          const AFont:TFont):TFontEditor; static;
  end;

implementation

{$R *.dfm}

uses
  VCLTee.Editor.Stroke;

procedure TFontEditor.CBItalicClick(Sender: TObject);
begin
  ChangeFontStyle(CBItalic.Checked,TFontStyle.fsItalic);
end;

procedure TFontEditor.CBStrikeOutClick(Sender: TObject);
begin
  ChangeFontStyle(CBStrikeOut.Checked,TFontStyle.fsStrikeOut);
end;

procedure TFontEditor.CBUnderlineClick(Sender: TObject);
begin
  ChangeFontStyle(CBUnderline.Checked,TFontStyle.fsUnderline);
end;

procedure TFontEditor.ChangeFontStyle(const Enable:Boolean; const AStyle:TFontStyle);
begin
  if Enable then
     IFont.Style:=IFont.Style+[AStyle]
  else
     IFont.Style:=IFont.Style-[AStyle];
end;

class function TFontEditor.Edit(const AOwner: TComponent;
  const AFont: TFont): Boolean;
begin
  with TFontEditor.Create(AOwner) do
  try
    RefreshFont(AFont);
    result:=ShowModal=mrOk;
  finally
    Free;
  end;
end;

class function TFontEditor.Embedd(const AOwner: TComponent;
  const AParent: TWinControl; const AFont: TFont): TFontEditor;
begin
  result:=TFontEditor.Create(AOwner);
  result.RefreshFont(AFont);
  TStrokeEditor.AddForm(result,AParent);
end;

procedure TFontEditor.LBNameClick(Sender: TObject);
begin
  IFont.Name:=LBName.Items[LBName.ItemIndex];
end;

procedure TFontEditor.PageFontChange(Sender: TObject);
begin
  if IFont=nil then
     Exit;

  if PageFont.ActivePage=TabFill then
  begin
    if IBrush=nil then
    begin
      IBrush:=TBrushEditor.Embedd(Self,TabFill,IFont.Brush);
      IBrush.PanelTop.Hide;
    end;
  end;
end;

procedure TFontEditor.CBBoldClick(Sender: TObject);
begin
  ChangeFontStyle(CBBold.Checked,TFontStyle.fsBold);
end;

procedure TFontEditor.RefreshFont(const AFont:TFont);
begin
  IFont:=AFont;

  TBSize.Position:=Round(IFont.Size);
  LSize.Caption:=IntToStr(TBSize.Position);

  if LBName.Count=0 then
     LBName.Items:=Screen.Fonts;

  LBName.ItemIndex:=LBName.Items.IndexOf(IFont.Name);

  CBBold.Checked:=TFontStyle.fsBold in IFont.Style;
  CBItalic.Checked:=TFontStyle.fsItalic in IFont.Style;
  CBUnderline.Checked:=TFontStyle.fsUnderline in IFont.Style;
  CBStrikeOut.Checked:=TFontStyle.fsStrikeOut in IFont.Style;

  if IBrush<>nil then
     IBrush.RefreshBrush(IFont.Brush);
end;

procedure TFontEditor.TBSizeChange(Sender: TObject);
begin
  IFont.Size:=TBSize.Position;
  LSize.Caption:=IntToStr(TBSize.Position);
end;

end.
