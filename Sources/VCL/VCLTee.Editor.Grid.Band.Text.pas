unit VCLTee.Editor.Grid.Band.Text;

interface

uses
  {Winapi.}Windows, {Winapi.}Messages, {System.}SysUtils, {System.}Classes,
  {Vcl.}Graphics, {Vcl.}Controls, {Vcl.}Forms, {Vcl.}Dialogs, {Vcl.}ComCtrls,
  {Vcl.}StdCtrls,

  VCLTee.Editor.Render.Text, Tee.Grid.Bands, VCLTee.Editor.Coordinate;

type
  TTextBandEditor = class(TForm)
    PageText: TPageControl;
    TabText: TTabSheet;
    TabOptions: TTabSheet;
    MemoText: TMemo;
    TabHeight: TTabSheet;
    procedure FormCreate(Sender: TObject);
    procedure PageTextChange(Sender: TObject);
    procedure MemoTextChange(Sender: TObject);
  private
    { Private declarations }

    IText : TTextBand;

    IHeight : TCoordinateEditor;
    IOptions : TTextRenderEditor;
  public
    { Public declarations }

    procedure RefreshText(const AText:TTextBand);

    class function Edit(const AOwner:TComponent; const AText:TTextBand):Boolean; static;

    class function Embedd(const AOwner:TComponent; const AParent:TWinControl;
                          const AText:TTextBand):TTextBandEditor; static;
  end;

implementation

{$R *.dfm}

uses
  VCLTee.Editor.Stroke;

class function TTextBandEditor.Edit(const AOwner: TComponent;
  const AText: TTextBand): Boolean;
begin
  with TTextBandEditor.Create(AOwner) do
  try
    RefreshText(AText);
    result:=ShowModal=mrOk;
  finally
    Free;
  end;
end;

class function TTextBandEditor.Embedd(const AOwner: TComponent;
  const AParent: TWinControl; const AText: TTextBand): TTextBandEditor;
begin
  result:=TTextBandEditor.Create(AOwner);
  result.RefreshText(AText);
  TStrokeEditor.AddForm(result,AParent);
end;

procedure TTextBandEditor.FormCreate(Sender: TObject);
begin
  PageText.ActivePage:=TabText;
end;

procedure TTextBandEditor.MemoTextChange(Sender: TObject);
begin
  IText.Text:=MemoText.Text;
end;

procedure TTextBandEditor.PageTextChange(Sender: TObject);
begin
  if PageText.ActivePage=TabOptions then
  begin
    if IOptions=nil then
       IOptions:=TTextRenderEditor.Embedd(Self,TabOptions,IText.TextRender);
  end
  else
  if PageText.ActivePage=TabHeight then
  begin
    if IHeight=nil then
       IHeight:=TCoordinateEditor.Embedd(Self,TabHeight,IText.Height);
  end;
end;

procedure TTextBandEditor.RefreshText(const AText: TTextBand);
begin
  IText:=AText;

  MemoText.Text:=IText.Text;
end;

end.
