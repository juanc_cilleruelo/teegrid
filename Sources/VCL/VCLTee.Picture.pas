unit VCLTee.Picture;

interface

uses
  { Vcl.}Graphics, { Vcl.}ExtCtrls, Tee.Format;

type
  TVCLPicture=record
  private
    type
      TVCLGraphic=Graphics.TGraphic;
  public
    class function From(const AGraphic:TVCLGraphic):TPicture; overload; static;
    class function From(const APicture:Graphics.TPicture):TPicture; overload; static;
    class function From(const AImage:TImage):TPicture; overload; static;
    class function From(const AFileName:String):TPicture; overload; static;

    class function Load(const AFileName:String):Graphics.TPicture; overload; static;
    class procedure Load(const APicture:TPicture; const AFileName:String); overload; static;

    class function TypeOfGraphic(const AGraphic:TGraphic):String; static;
  end;

implementation

{$IFNDEF FPC}
uses
  PngImage, jpeg;
{$ENDIF}

class function TVCLPicture.From(const AGraphic: TVCLGraphic): TPicture;
begin
  result:=TPicture.Create(nil);
  result.SetGraphic(AGraphic);
end;

class function TVCLPicture.From(const APicture: Graphics.TPicture): TPicture;
begin
  result:=From(APicture.Graphic);
end;

class function TVCLPicture.From(const AImage: TImage): TPicture;
begin
  result:=From(AImage.Picture);
end;

class function TVCLPicture.Load(const AFileName:String):Graphics.TPicture;
begin
  result:=Graphics.TPicture.Create;
  result.LoadFromFile(AFileName);
end;

type
  TPictureAccess=class(TPicture);

class function TVCLPicture.From(const AFileName: String): TPicture;
var tmp : Graphics.TPicture;
begin
  tmp:=Load(AFileName);

  result:=From(tmp);
  TPictureAccess(result).Original:=tmp;
end;

class procedure TVCLPicture.Load(const APicture: TPicture;
  const AFileName: String);
var tmp : Graphics.TPicture;
begin
  tmp:=Load(AFileName);

  APicture.SetGraphic(tmp);
  TPictureAccess(APicture).Original:=tmp;
end;

class function TVCLPicture.TypeOfGraphic(const AGraphic: TGraphic): String;
begin
  if AGraphic is {$IFDEF FPC}TPortableNetworkGraphic{$ELSE}TPngImage{$ENDIF} then
     result:='PNG'
  else
  if AGraphic is TJPEGImage then
     result:='JPEG'
  else
  {$IFDEF FPC}
  if AGraphic is TGIFImage then
     result:='GIF'
  else
  {$ENDIF}
  if AGraphic is TBitmap then
     result:='Bitmap'
  else
     result:='';
end;

end.
