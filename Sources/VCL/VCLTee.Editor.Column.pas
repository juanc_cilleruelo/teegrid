{*********************************************}
{  TeeGrid Software Library                   }
{  VCL TColumn Editor                         }
{  Copyright (c) 2016 by Steema Software      }
{  All Rights Reserved                        }
{*********************************************}
unit VCLTee.Editor.Column;

interface

uses
  {Winapi.}Windows, {Winapi.}Messages, {System.}SysUtils, {System.}Classes, {Vcl.}Graphics,
  {Vcl.}Controls, {Vcl.}Forms, {Vcl.}Dialogs, VCLTee.Editor.Format.Text, {Vcl.}StdCtrls,
  {Vcl.}ExtCtrls, {Vcl.}ComCtrls,

  Tee.Grid.Columns,
  VCLTee.Editor.Coordinate, VCLTee.Editor.Margins, VCLTee.Editor.Render.Text;

type
  TColumnEditor = class(TForm)
    PageFormat: TPageControl;
    TabGeneral: TTabSheet;
    CBVisible: TCheckBox;
    CBExpanded: TCheckBox;
    CBReadOnly: TCheckBox;
    TabWidth: TTabSheet;
    TabData: TTabSheet;
    Label5: TLabel;
    MemoHeader: TMemo;
    Label3: TLabel;
    Label4: TLabel;
    EFloatFormat: TEdit;
    EDateTimeFormat: TEdit;
    Label6: TLabel;
    EDateFormat: TEdit;
    Label7: TLabel;
    ETimeFormat: TEdit;
    TabFormat: TTabSheet;
    TabAlign: TTabSheet;
    TabMargins: TTabSheet;
    TabHeader: TTabSheet;
    Panel1: TPanel;
    CBParentFormat: TCheckBox;
    Panel2: TPanel;
    CBHeaderParent: TCheckBox;
    CBAutoAlign: TCheckBox;
    RGHorizAlign: TRadioGroup;
    RGVerticalAlign: TRadioGroup;
    PageHeader: TPageControl;
    TabHeaderFormat: TTabSheet;
    TabHeaderAlign: TTabSheet;
    RadioGroup1: TRadioGroup;
    RadioGroup2: TRadioGroup;
    CBHeaderVisible: TCheckBox;
    procedure CBVisibleClick(Sender: TObject);
    procedure CBExpandedClick(Sender: TObject);
    procedure EFloatFormatChange(Sender: TObject);
    procedure EDateTimeFormatChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CBReadOnlyClick(Sender: TObject);
    procedure PageFormatChange(Sender: TObject);
    procedure MemoHeaderChange(Sender: TObject);
    procedure EDateFormatChange(Sender: TObject);
    procedure ETimeFormatChange(Sender: TObject);
    procedure RGHorizAlignClick(Sender: TObject);
    procedure RGVerticalAlignClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CBAutoAlignClick(Sender: TObject);
    procedure CBParentFormatClick(Sender: TObject);
    procedure CBHeaderParentClick(Sender: TObject);
    procedure PageHeaderChange(Sender: TObject);
    procedure RadioGroup2Click(Sender: TObject);
    procedure RadioGroup1Click(Sender: TObject);
    procedure CBHeaderVisibleClick(Sender: TObject);
  private
    { Private declarations }

    Column : TColumn;

    IHeader : TTextRenderEditor;
    IFormat : TTextFormatEditor;
    IMargins : TMarginsEditor;
    IWidth : TCoordinateEditor;

    IChangingAlign : Boolean;

    FOnChangedHeader : TNotifyEvent;

    procedure RefreshColumnFormat(const AFormat: TDataFormat);
  public
    { Public declarations }

    class function Edit(const AOwner:TComponent; const AColumn:TColumn):Boolean; static;

    class function Embedd(const AOwner:TComponent; const AParent:TWinControl;
                          const AColumn:TColumn):TColumnEditor; overload; static;

    procedure RefreshColumn(const AColumn:TColumn);

    property OnChangedHeader:TNotifyEvent read FOnChangedHeader
                                          write FOnChangedHeader;
  end;

implementation

{$R *.dfm}

uses
  VCLTee.Editor.Stroke, Tee.Format, Tee.Painter, Tee.Renders;

type
  TColumnAccess=class(TColumn);

procedure TColumnEditor.CBAutoAlignClick(Sender: TObject);
begin
  if Column.Render is TCellRender then
  begin
    if CBAutoAlign.Checked then
       Column.TextAlignment:=TColumnTextAlign.Automatic
    else
       Column.TextAlignment:=TColumnTextAlign.Custom;

    if CBAutoAlign.Checked then
    begin
      IChangingAlign:=True;
      try
        RGHorizAlign.ItemIndex:=Ord(TColumnAccess(Column).DefaultHorizAlign);
      finally
        IChangingAlign:=False;
      end;
    end;
  end;
end;

procedure TColumnEditor.CBExpandedClick(Sender: TObject);
begin
  Column.Expanded:=CBExpanded.Checked;
end;

procedure TColumnEditor.CBHeaderParentClick(Sender: TObject);
begin
  Column.Header.ParentFormat:=CBHeaderParent.Checked;
end;

procedure TColumnEditor.CBHeaderVisibleClick(Sender: TObject);
begin
  Column.Header.Visible:=CBHeaderVisible.Checked;
end;

procedure TColumnEditor.CBParentFormatClick(Sender: TObject);
begin
  Column.ParentFormat:=CBParentFormat.Checked;

  if CBParentFormat.Checked then
  begin
    if IFormat<>nil then
       IFormat.Hide;
  end
  else
    PageFormatChange(Self);
end;

procedure TColumnEditor.CBReadOnlyClick(Sender: TObject);
begin
  Column.ReadOnly:=CBReadOnly.Checked;
end;

procedure TColumnEditor.CBVisibleClick(Sender: TObject);
begin
  Column.Visible:=CBVisible.Checked;
end;

procedure TColumnEditor.EDateFormatChange(Sender: TObject);
begin
  Column.DataFormat.Date:=EDateFormat.Text;
end;

procedure TColumnEditor.EDateTimeFormatChange(Sender: TObject);
begin
  Column.DataFormat.DateTime:=EDateTimeFormat.Text;
end;

class function TColumnEditor.Edit(const AOwner: TComponent;
  const AColumn: TColumn): Boolean;
begin
  with TColumnEditor.Create(AOwner) do
  try
    RefreshColumn(AColumn);
    result:=ShowModal=mrOk;
  finally
    Free;
  end;
end;

procedure TColumnEditor.EFloatFormatChange(Sender: TObject);
begin
  Column.DataFormat.Float:=EFloatFormat.Text;
end;

class function TColumnEditor.Embedd(const AOwner: TComponent;
  const AParent: TWinControl; const AColumn: TColumn): TColumnEditor;
begin
  result:=TColumnEditor.Create(AOwner);
  result.RefreshColumn(AColumn);
  TStrokeEditor.AddForm(result,AParent);
end;

procedure TColumnEditor.ETimeFormatChange(Sender: TObject);
begin
  Column.DataFormat.Time:=ETimeFormat.Text;
end;

procedure TColumnEditor.FormCreate(Sender: TObject);
begin
  PageFormat.ActivePage:=TabGeneral;
end;

procedure TColumnEditor.FormShow(Sender: TObject);
begin
  PageFormatChange(Self);
end;

procedure TColumnEditor.MemoHeaderChange(Sender: TObject);
begin
  Column.Header.Text:=MemoHeader.Text;

  if Assigned(FOnChangedHeader) then
     FOnChangedHeader(Self);
end;

procedure TColumnEditor.PageFormatChange(Sender: TObject);
begin
  if Column=nil then
     Exit;

  if PageFormat.ActivePage=TabWidth then
  begin
    if IWidth=nil then
       IWidth:=TCoordinateEditor.Embedd(Self,TabWidth,Column.Width);
  end
  else
  if PageFormat.ActivePage=TabFormat then
  begin
    if IFormat=nil then
       if not CBParentFormat.Checked then
          IFormat:=TTextFormatEditor.Embedd(Self,TabFormat,Column.Format);

    if IFormat<>nil then
       IFormat.Show;
  end
  else
  if PageFormat.ActivePage=TabMargins then
  begin
    if IMargins=nil then
       if Column.Margins<>nil then
          IMargins:=TMarginsEditor.Embedd(Self,TabMargins,Column.Margins);
  end
  else
  if PageFormat.ActivePage=TabHeader then
     PageHeaderChange(Self);
end;

procedure TColumnEditor.PageHeaderChange(Sender: TObject);
begin
  if PageHeader.ActivePage=TabHeaderFormat then
  begin
    if IHeader=nil then
       IHeader:=TTextRenderEditor.Embedd(Self,TabHeaderFormat,Column.Header);
  end;
end;

procedure TColumnEditor.RefreshColumnFormat(const AFormat: TDataFormat);
begin
  EFloatFormat.Text:=AFormat.Float;
  EDateTimeFormat.Text:=AFormat.DateTime;
  EDateFormat.Text:=AFormat.Date;
  ETimeFormat.Text:=AFormat.Time;
end;

procedure TColumnEditor.RGHorizAlignClick(Sender: TObject);
begin
  Column.TextAlign.Horizontal:=THorizontalAlign(RGHorizAlign.ItemIndex);

  if not IChangingAlign then
     CBAutoAlign.Checked:=False;
end;

procedure TColumnEditor.RGVerticalAlignClick(Sender: TObject);
begin
  Column.TextAlign.Vertical:=TVerticalAlign(RGVerticalAlign.ItemIndex);

  if not IChangingAlign then
     CBAutoAlign.Checked:=False;
end;

procedure TColumnEditor.RadioGroup1Click(Sender: TObject);
begin
  Column.Header.TextAlign.Vertical:=TVerticalAlign(RadioGroup1.ItemIndex);
end;

procedure TColumnEditor.RadioGroup2Click(Sender: TObject);
begin
  Column.Header.TextAlign.Horizontal:=THorizontalAlign(RadioGroup2.ItemIndex);
end;

procedure TColumnEditor.RefreshColumn(const AColumn: TColumn);

  procedure RefreshCellAlign;
  begin
    CBAutoAlign.Enabled:=Column.HasRender and (Column.Render is TCellRender);

    if CBAutoAlign.Enabled then
       CBAutoAlign.Checked:=Column.TextAlignment=TColumnTextAlign.Automatic;

    RGHorizAlign.Enabled:=Column.HasRender and (Column.Render is TTextRender);
    RGVerticalAlign.Enabled:=RGHorizAlign.Enabled;

    if RGHorizAlign.Enabled then
    begin
      RGHorizAlign.ItemIndex:=Ord(Column.TextAlign.Horizontal);
      RGVerticalAlign.ItemIndex:=Ord(Column.TextAlign.Vertical);
    end;
  end;

  procedure TryRefreshFormat;
  begin
    if IFormat<>nil then
    begin
      IFormat.Visible:=Column.HasFormat;

      if IFormat.Visible then
         IFormat.RefreshFormat(Column.Format)
      else
       begin
         IFormat.Free;
         IFormat:=nil;
       end;
    end;
  end;

  procedure TryRefreshMargins;
  var tmp : TMargins;
  begin
    TabMargins.TabVisible:=Column.HasFormat;

    if TabMargins.TabVisible then
    begin
      tmp:=Column.Margins;

      if IMargins<>nil then
         if tmp=nil then
         begin
           IMargins.Free;
           IMargins:=nil;
         end
         else
           IMargins.RefreshMargins(tmp);
    end;
  end;

begin
  Column:=AColumn;

  MemoHeader.Text:=Column.Header.Text;

  CBVisible.Checked:=Column.Visible;

  CBExpanded.Enabled:=CBVisible.Enabled and Column.HasItems;
  CBExpanded.Checked:=Column.HasItems;

  RefreshColumnFormat(Column.DataFormat);

  CBReadOnly.Checked:=Column.ReadOnly;

  CBHeaderParent.Checked:=Column.Header.ParentFormat;
  CBHeaderVisible.Checked:=Column.Header.Visible;

  CBParentFormat.Checked:=Column.ParentFormat;

  TryRefreshFormat;
  TryRefreshMargins;
  RefreshCellAlign;
end;

end.
