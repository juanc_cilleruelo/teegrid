{*********************************************}
{  TeeGrid Software Library                   }
{  VCL TeeGrid                                }
{  Copyright (c) 2016 by Steema Software      }
{  All Rights Reserved                        }
{*********************************************}
unit VCLTee.Grid;

interface

uses
  {System.}Classes,

  {$IFNDEF FPC}
  {System.}Types,
  {$ENDIF}

  Windows, Messages,
  {VCL.}Controls, {VCL.}Graphics, {VCL.}ExtCtrls, {VCL.}Forms,

  {$IFDEF FPC}
  LCLType,
  {$ENDIF}

  Tee.Control, Tee.Grid, Tee.Grid.Columns, Tee.Painter, Tee.Renders,
  Tee.Format, Tee.Grid.Header, Tee.Grid.Data, Tee.Grid.Bands,
  Tee.Grid.Selection, Tee.Grid.Rows, Tee.Grid.RowGroup,

  VCLTee.Painter, VCLTee.Control;

type
  TTeeGrid=class;

  TVCLTeeGrid=class(TCustomTeeGrid)
  private
    IEditor : TControl;
    IGrid : TTeeGrid;

    IEditorColumn : TColumn;
    IEditorRow : Integer;

    procedure CreateEditor(const AColumn:TColumn);
    procedure EditorKeyUp(Sender: TObject; var AKey: Word; Shift: TShiftState);
    function EditorShowing:Boolean;
    procedure TryChangeEditorData;
    procedure TryShowEditor(const AColumn:TColumn; const ARow:Integer);
  protected
    procedure DataChanged; override;

    function HorizScrollBarHeight:Single; override;
    procedure HorizScrollChanged; override;

    procedure StartEditor(const AColumn:TColumn; const ARow:Integer); override;
    procedure StopEditor; override;

    function VertScrollBarWidth:Single; override;
    procedure VertScrollChanged; override;
  public
    procedure CopySelected; override;
    function Height:Single; override;
    function Painter:TPainter; override;
    function Width:Single; override;
  end;

  TShowEditorEvent=procedure(const Sender:TObject; const AEditor:TControl;
                             const AColumn:TColumn; const ARow:Integer) of object;

  {$IFNDEF FPC}
  {$IFDEF CONDITIONALEXPRESSIONS}
  {$IF CompilerVersion>=23}
  [ComponentPlatformsAttribute(pidWin32 or pidWin64)]
  {$IFEND}
  {$ENDIF}
  {$ENDIF}
  TTeeGrid=class(TScrollableControl)
  private
    FGrid : TVCLTeeGrid;
    FPainter : TPainter;
    FOnColumnResized: TColumnEvent;
    FOnShowEditor: TShowEditorEvent;

    {$IFDEF FPC}
    FParentBack : Boolean;
    {$ENDIF}

    ICanvas : TControlCanvas;

    procedure ChangePainter(const Value: TPainter);
    procedure ColumnResized(Sender:TObject);
    function MouseStateFrom(const Button: TMouseButton; const Shift: TShiftState;
                            const X,Y: Integer; const AEvent:TGridMouseEvent):TMouseState;
    procedure SetData(const Value: TVirtualData);

    procedure WMEraseBkgnd(var Message: TWmEraseBkgnd); message WM_ERASEBKGND;
    procedure WMGetDlgCode(var Message: TWMGetDlgCode); message WM_GETDLGCODE;
    procedure WMSize(var Message: TWMSize); message WM_SIZE;

    function GetColumns: TColumns;
    function GetSelected: TGridSelection;
    procedure SetColumns(const Value: TColumns);
    procedure SetSelected(const Value: TGridSelection);
    function GetHeader: TColumnHeaderBand;
    procedure SetHeader(const Value: TColumnHeaderBand);
    function GetClickedHeader: TNotifyEvent;
    procedure SetClickedHeader(const Value: TNotifyEvent);
    function GetIndicator: TIndicator;
    procedure SetIndicator(const Value: TIndicator);
    function GetData: TVirtualData;
    function GetRows: TRows;
    procedure SetRows(const Value: TRows);
    function GetAfterDraw: TNotifyEvent;
    procedure SetAfterDraw(const Value: TNotifyEvent);
    function GetReadOnly: Boolean;
    procedure SetReadOnly(const Value: Boolean);
    function GetBack: TFormat;
    procedure SetBack(const Value: TFormat);
    procedure SetPainter(const Value: TPainter);
    function GetFooter: TGridBands;
    procedure SetFooter(const Value: TGridBands);
    function GetCells: TTextRender;
    procedure SetCells(const Value: TTextRender);
    function GetEditing: TGridEditing;
    procedure SetEditing(const Value: TGridEditing);
    function GetOnNewDetail: TNewDetailEvent;
    procedure SetOnNewDetail(const Value: TNewDetailEvent);
    function GetOnSelect: TNotifyEvent;
    procedure SetOnSelect(const Value: TNotifyEvent);
    function GetHeaders: TGridBands;
    procedure SetHeaders(const Value: TGridBands);
    function GetDataSource: TComponent;
    procedure SetDataSource(const Value: TComponent);
  protected
    procedure CMMouseLeave(var Message: TMessage); message CM_MOUSELEAVE;
    procedure CreateParams(var Params: TCreateParams); override;

    procedure DblClick; override;

    function DoMouseWheelDown(Shift: TShiftState; MousePos: TPoint): Boolean; override;
    function DoMouseWheelUp(Shift: TShiftState; MousePos: TPoint): Boolean; override;

    function GetMaxBottom:Single; override;
    function GetMaxRight:Single; override;

    function GetScrollX:Single; override;
    function GetScrollY:Single; override;

    procedure Loaded; override;

    procedure SetScrollX(const Value:Single); override;
    procedure SetScrollY(const Value:Single); override;

    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure PaintWindow(DC: HDC); override;

    procedure ResetScrollBars; override;
  public
    Constructor Create(AOwner:TComponent); override;
    Destructor Destroy; override;

    procedure Assign(Source: TPersistent); override;

    procedure Changed(Sender:TObject);
    function NewExpander(const ARows:TRows):TExpanderRender;

    property Canvas:TControlCanvas read ICanvas;
    property Data:TVirtualData read GetData write SetData;
    property Grid:TVCLTeeGrid read FGrid;
    property Painter:TPainter read FPainter write SetPainter;
  published
    property Back:TFormat read GetBack write SetBack;
    property Cells:TTextRender read GetCells write SetCells;
    property Color default clWhite;
    property Columns:TColumns read GetColumns write SetColumns;
    property DataSource:TComponent read GetDataSource write SetDataSource;
    property DoubleBuffered default True;
    property Editing:TGridEditing read GetEditing write SetEditing;
    property Header:TColumnHeaderBand read GetHeader write SetHeader;
    property Headers:TGridBands read GetHeaders write SetHeaders stored False;
    property Footer:TGridBands read GetFooter write SetFooter;
    property Indicator:TIndicator read GetIndicator write SetIndicator;
    property ReadOnly:Boolean read GetReadOnly write SetReadOnly default True;
    property Rows:TRows read GetRows write SetRows;
    property Selected:TGridSelection read GetSelected write SetSelected;

    {$IFDEF FPC}
    property TabStop default True;
    {$ELSE}
    {$IFDEF CONDITIONALEXPRESSIONS}
    {$IF CompilerVersion>27}
    property TabStop default True;
    {$IFEND}
    {$ENDIF}
    {$ENDIF}

    property OnAfterDraw:TNotifyEvent read GetAfterDraw write SetAfterDraw;
    property OnClickedHeader:TNotifyEvent read GetClickedHeader write SetClickedHeader;
    property OnColumnResized:TColumnEvent read FOnColumnResized write FOnColumnResized;
    property OnNewDetail:TNewDetailEvent read GetOnNewDetail write SetOnNewDetail;
    property OnSelect:TNotifyEvent read GetOnSelect write SetOnSelect;
    property OnShowEditor:TShowEditorEvent read FOnShowEditor write FOnShowEditor;

    // inherited

    property Align;
    property Anchors;

    // NO: property Caption;

    property AutoSize;
    property BiDiMode;
    property Constraints;
    property UseDockManager default True;
    property DockSite;
    property DragCursor;
    property DragKind;

    property DragMode;
    property Enabled;
    property Height;

    {$IFNDEF FPC}
    property Margins;
    property Padding;
    {$ENDIF}

    property ParentBiDiMode;
    property ParentBackground {$IFDEF FPC}:Boolean read FParentBack write FParentBack{$ENDIF};
    property ParentColor;
    {$IFNDEF FPC}
    property ParentDoubleBuffered default False;
    {$ENDIF}
    property ParentShowHint;

    property PopupMenu;
    property ShowHint;
    property TabOrder;

    {$IFDEF HASTOUCHPROPERTY}
    property Touch;
    {$ENDIF}

    property Visible;
    property Width;

    {$IFNDEF FPC}
    {$IFDEF CONDITIONALEXPRESSIONS}
    {$IF CompilerVersion>23}
    property StyleElements;
    {$IFEND}
    {$ENDIF}
    {$ENDIF}

    property OnAlignInsertBefore;
    property OnAlignPosition;
    {$IFNDEF FPC}
    property OnCanResize;
    {$ENDIF}

    property OnClick;

    property OnConstrainedResize;
    property OnContextPopup;
    property OnDockDrop;
    property OnDockOver;

    property OnDblClick;
    property OnDragDrop;

    property OnDragOver;
    property OnEndDock;
    property OnEndDrag;

    property OnEnter;
    property OnExit;
    {$IFDEF HASONGESTURE}
    property OnGesture;
    {$ENDIF}

    property OnGetSiteInfo;
    {$IFNDEF FPC}
    property OnMouseActivate;
    {$ENDIF}

    property OnMouseDown;
    property OnMouseEnter;
    property OnMouseLeave;
    property OnMouseMove;
    property OnMouseUp;
    property OnResize;
    property OnStartDock;
    property OnStartDrag;
    property OnUnDock;
  end;

implementation

{$IFNDEF FPC}
{$DEFINE DATETIMEPICKER}
{$ENDIF}

{$IFNDEF FPC}
{$IF CompilerVersion>=23}
{$DEFINE HASGDIPLUS}
{$IFEND}
{$ENDIF}

uses
  {VCL.}StdCtrls, {System.}SysUtils, {Vcl.}Clipbrd,

  {$IFDEF FPC}
  {$IFDEF DATETIMEPICKER}
  DateTimePicker,
  {$ENDIF}
  {$ELSE}
  {Vcl.}ComCtrls,
  {$ENDIF}

  {$IFDEF HASGDIPLUS} // XE2 and up only (missing GDIPAPI / GDIPOBJ units)
  VCLTee.Painter.GdiPlus,
  {$ENDIF}

  Tee.Grid.CSV;

{ TTeeGrid }

Constructor TTeeGrid.Create(AOwner: TComponent);
begin
  inherited;

  DoubleBuffered:=True;

  Width:=400;
  Height:=250;

  ControlStyle:=ControlStyle+[csAcceptsControls]-[csOpaque,csSetCaption];

  TabStop:=True;

{
  Touch.InteractiveGestures := [igPan, igPressAndTap];
  Touch.InteractiveGestureOptions := [igoPanInertia,
    igoPanSingleFingerHorizontal, igoPanSingleFingerVertical,
    igoParentPassthrough];
}

  Color:=clWhite;

  FGrid:=TVCLTeeGrid.Create(Self);
  FGrid.IGrid:=Self;
  FGrid.Data:=Data;

  FGrid.OnChange:=Changed;
  FGrid.Header.OnColumnResized:=ColumnResized;

  ICanvas:=TControlCanvas.Create;
  ICanvas.Control:=Self;

  {$IFDEF HASGDIPLUS} // XE2 and up only (missing GDIPAPI / GDIPOBJ units)
  ChangePainter(TGdiPlusPainter.Create);
  {$ELSE}
  ChangePainter(TGdiPainter.Create(Canvas));
  {$ENDIF}
end;

Destructor TTeeGrid.Destroy;
var tmp : TVirtualData;
begin
  FPainter.Free;

  tmp:=FGrid.Data;
  FGrid.Free;

  ICanvas.Free;

  tmp.Free;

  inherited;
end;

procedure TTeeGrid.CMMouseLeave(var Message: TMessage);
begin
  Grid.MouseLeave;
end;

procedure TTeeGrid.ColumnResized(Sender:TObject);
begin
  ResetScrollBars;

  if Assigned(FOnColumnResized) then
     FOnColumnResized(Self,Grid.Header.Dragging);
end;

procedure TTeeGrid.Assign(Source: TPersistent);
begin
  if Source is TTeeGrid then
     Grid.Assign(TTeeGrid(Source).FGrid)
  else
     inherited;
end;

procedure TTeeGrid.Changed(Sender: TObject);
begin
  Invalidate;
end;

function TTeeGrid.DoMouseWheelDown(Shift: TShiftState; MousePos: TPoint): Boolean;
var tmp : TKeyState;
begin
  tmp.Key:=VK_DOWN;
  tmp.Shift:=[];
  tmp.Event:=TGridKeyEvent.Down;

  Grid.Key(tmp);
  result:=True;
end;

function TTeeGrid.DoMouseWheelUp(Shift: TShiftState; MousePos: TPoint): Boolean;
var tmp : TKeyState;
begin
  tmp.Key:=VK_UP;
  tmp.Shift:=[];
  tmp.Event:=TGridKeyEvent.Down;

  Grid.Key(tmp);
  result:=True;
end;

function TTeeGrid.GetAfterDraw: TNotifyEvent;
begin
  result:=Grid.OnAfterDraw;
end;

function TTeeGrid.GetBack: TFormat;
begin
  result:=Grid.Back;
end;

function TTeeGrid.GetCells: TTextRender;
begin
  result:=Grid.Cells;
end;

function TTeeGrid.GetClickedHeader: TNotifyEvent;
begin
  result:=Grid.Header.OnClick;
end;

function TTeeGrid.GetColumns: TColumns;
begin
  result:=Grid.Columns;
end;

function TTeeGrid.GetData: TVirtualData;
begin
  result:=Grid.Data;
end;

function TTeeGrid.GetDataSource: TComponent;
begin
  result:=Grid.DataSource;
end;

function TTeeGrid.GetEditing: TGridEditing;
begin
  result:=Grid.Editing;
end;

function TTeeGrid.GetFooter: TGridBands;
begin
  result:=Grid.Footer;
end;

function TTeeGrid.GetHeader: TColumnHeaderBand;
begin
  result:=Grid.Header;
end;

function TTeeGrid.GetHeaders: TGridBands;
begin
  result:=Grid.Headers;
end;

function TTeeGrid.GetIndicator: TIndicator;
begin
  result:=Grid.Indicator;
end;

function TTeeGrid.GetMaxBottom: Single;
begin
  result:=Grid.Root.MaxBottom;
end;

function TTeeGrid.GetMaxRight: Single;
begin
  result:=Grid.Rows.VisibleColumns.MaxRight;
end;

function TTeeGrid.GetOnNewDetail: TNewDetailEvent;
begin
  result:=Grid.Root.OnNewDetail;
end;

function TTeeGrid.GetOnSelect: TNotifyEvent;
begin
  result:=Grid.OnSelect;
end;

function TTeeGrid.GetReadOnly: Boolean;
begin
  result:=Grid.ReadOnly;
end;

function TTeeGrid.GetRows: TRows;
begin
  result:=Grid.Rows;
end;

function TTeeGrid.GetScrollX: Single;
begin
  result:=FGrid.Current.Rows.Scroll.X;
end;

function TTeeGrid.GetScrollY: Single;
begin
  result:=FGrid.Current.Rows.Scroll.Y;
end;

procedure TTeeGrid.SetScrollX(const Value:Single);
begin
  //FGrid.Current.
  Rows.Scroll.X:=Value;
end;

procedure TTeeGrid.SetScrollY(const Value:Single);
begin
  //FGrid.Current.
  Rows.Scroll.Y:=Value;
end;

function TTeeGrid.GetSelected: TGridSelection;
begin
  result:=Grid.Selected;
end;

procedure TTeeGrid.WMGetDlgCode(var Message: TWMGetDlgCode);
begin
  Message.Result := DLGC_WANTALLKEYS or DLGC_WANTARROWS or DLGC_WANTTAB;
end;

procedure TTeeGrid.WMEraseBkgnd(var Message: TWmEraseBkgnd);
begin
  Message.Result:=1;
end;

procedure TTeeGrid.KeyDown(var Key: Word; Shift: TShiftState);
var tmp : TKeyState;
begin
  inherited;

  tmp.Key:=Key;
  tmp.Shift:=Shift;
  tmp.Event:=TGridKeyEvent.Down;

  FGrid.Key(tmp);
end;

procedure TTeeGrid.Loaded;
begin
  inherited;
  Grid.DataSourceChanged;
end;

function TTeeGrid.MouseStateFrom(const Button: TMouseButton; const Shift: TShiftState;
             const X,Y: Integer; const AEvent:TGridMouseEvent):TMouseState;
begin
  result.X:=X;
  result.Y:=Y;

  case Button of
     TMouseButton.mbLeft: result.Button:=TGridMouseButton.Left;
    TMouseButton.mbRight: result.Button:=TGridMouseButton.Right;
   TMouseButton.mbMiddle: result.Button:=TGridMouseButton.Middle;
  end;

  result.Shift:=Shift;
  result.Event:=AEvent;
end;

procedure TTeeGrid.MouseDown(Button: TMouseButton; Shift: TShiftState; X,
  Y: Integer);
var tmp : TGridMouseEvent;
    tmpState : TMouseState;
begin
  inherited;

  if CanFocus then
     SetFocus;

  if ssDouble in Shift then
     tmp:=TGridMouseEvent.DoubleClick
  else
     tmp:=TGridMouseEvent.Down;

  tmpState:=MouseStateFrom(Button,Shift,X,Y,tmp);
  FGrid.Mouse(tmpState);
end;

function CursorOf(const ACursor:TMouseCursor):TCursor; overload;
begin
  case ACursor of
      TMouseCursor.Default: result:=crDefault;
    TMouseCursor.HandPoint: result:=crHandPoint;
  TMouseCursor.HorizResize: result:=crSizeWE;
 else
    result:=crSizeNS;
  end;
end;

function CursorOf(const ACursor:TCursor):TMouseCursor; overload;
begin
  case ACursor of
      crDefault: result:=TMouseCursor.Default;
    crHandPoint: result:=TMouseCursor.HandPoint;
       crSizeWE: result:=TMouseCursor.HorizResize;
  else
    result:=TMouseCursor.VertResize;
  end;
end;

procedure TTeeGrid.WMSize(var Message: TWMSize);
begin
  if Columns<>nil then
     ResetScrollBars;
end;

procedure TTeeGrid.MouseMove(Shift: TShiftState; X, Y: Integer);
var tmp : TMouseState;
begin
  inherited;

  tmp:=MouseStateFrom(TMouseButton.mbLeft,Shift,X,Y,TGridMouseEvent.Move);
  tmp.Cursor:=CursorOf(Cursor);

  FGrid.Mouse(tmp);

  Cursor:=CursorOf(tmp.Cursor);
end;

procedure TTeeGrid.MouseUp(Button: TMouseButton; Shift: TShiftState; X,
  Y: Integer);
var tmp : TMouseState;
begin
  inherited;

  tmp:=MouseStateFrom(TMouseButton.mbLeft,Shift,X,Y,TGridMouseEvent.Up);
  Grid.Mouse(tmp);
end;

// Return a new "+/-" render to enable expand / collapse
function TTeeGrid.NewExpander(const ARows:TRows): TExpanderRender;
begin
  result:=TExpanderRender.Create(Changed);

  result.OnCanExpand:=ARows.Data.CanExpand;
  result.OnGetExpanded:=ARows.IsChildrenVisible;
end;

procedure TTeeGrid.PaintWindow(DC: HDC);
begin
  inherited;

  if FPainter is TWindowsPainter then
     TWindowsPainter(FPainter).Init(DC);

  if FGrid.Root.RecalcScrollBars then
  begin
    ResetScrollBars;
    FGrid.Root.RecalcScrollBars:=False;
  end;

  if not Back.Brush.Visible then
     Painter.Fill(TRectF.Create(ClientRect),Color);

  FGrid.Current.IsFocused:=Focused;
  FGrid.Paint;

  FGrid.FinishPaint(csDesigning in ComponentState);

  if Editing.AlwaysVisible and FGrid.EditorShowing and (not Selected.IsEmpty) then
  begin
    FGrid.TryShowEditor(Selected.Column,Selected.Row);
    Changed(Self);
  end;
end;

procedure TTeeGrid.ResetScrollBars;
begin
  if Grid.Columns<>nil then
  begin
    if Rows.VisibleColumns.Count=0 then
       Rows.VisibleColumns.Add(Columns);

    Grid.Root.PrepareColumns(Painter,0,ClientWidth);

    inherited;
  end;
end;

procedure TTeeGrid.SetAfterDraw(const Value: TNotifyEvent);
begin
  Grid.OnAfterDraw:=Value;
end;

procedure TTeeGrid.SetBack(const Value: TFormat);
begin
  Grid.Back:=Value;
end;

procedure TTeeGrid.SetCells(const Value: TTextRender);
begin
  Grid.Cells:=Value;
end;

procedure TTeeGrid.SetClickedHeader(const Value: TNotifyEvent);
begin
  Grid.Header.OnClick:=Value;
end;

procedure TTeeGrid.SetColumns(const Value: TColumns);
begin
  Grid.Columns:=Value;
end;

procedure TTeeGrid.DblClick;
begin
  inherited;

  if Grid.Editing.DoubleClick then
     Grid.TryStartEditor;
end;

procedure TTeeGrid.SetData(const Value: TVirtualData);
begin
  if Data<>Value then
  begin
    FGrid.Data.Free;
    FGrid.Data:=Value;
  end;
end;

procedure TTeeGrid.SetDataSource(const Value: TComponent);
begin
  Grid.DataSource:=Value;
end;

procedure TTeeGrid.SetEditing(const Value: TGridEditing);
begin
  Grid.Editing:=Value;
end;

procedure TTeeGrid.SetFooter(const Value: TGridBands);
begin
  Grid.Footer:=Value;
end;

procedure TTeeGrid.SetHeader(const Value: TColumnHeaderBand);
begin
  Grid.Header:=Value;
end;

procedure TTeeGrid.SetHeaders(const Value: TGridBands);
begin
  Grid.Headers:=Value;
end;

procedure TTeeGrid.SetIndicator(const Value: TIndicator);
begin
  Grid.Indicator:=Value;
end;

procedure TTeeGrid.SetOnNewDetail(const Value: TNewDetailEvent);
begin
  Grid.Root.OnNewDetail:=Value;
end;

procedure TTeeGrid.SetOnSelect(const Value: TNotifyEvent);
begin
  Grid.OnSelect:=Value;
end;

procedure TTeeGrid.ChangePainter(const Value: TPainter);
begin
  FPainter:=Value;
  Rows.Painter:=FPainter;
end;

procedure TTeeGrid.SetPainter(const Value: TPainter);
begin
  if FPainter<>Value then
  begin
    FPainter.Free;
    ChangePainter(Value);

    Changed(Self);
  end;
end;

procedure TTeeGrid.SetReadOnly(const Value: Boolean);
begin
  Grid.ReadOnly:=Value;
end;

procedure TTeeGrid.SetRows(const Value: TRows);
begin
  Grid.Rows:=Value;
end;

procedure TTeeGrid.SetSelected(const Value: TGridSelection);
begin
  Grid.Selected:=Value;
end;

procedure TTeeGrid.CreateParams(var Params: TCreateParams);
begin
  inherited;

  with Params do
  begin
    Style := Style or WS_TABSTOP;
    Style := Style or WS_VSCROLL or WS_HSCROLL;
    Style := Style and not (CS_HREDRAW or CS_VREDRAW);
  end;
end;

{ TVCLTeeGrid }

procedure TVCLTeeGrid.CopySelected;
begin
  Clipboard.AsText:=TCSVData.From(Self,Current.Selected);
end;

type
  TVirtualDataAccess=class(TVirtualData);

procedure TVCLTeeGrid.DataChanged;
begin
  inherited;
  IGrid.ResetScrollBars;

  if IGrid.Data<>nil then
     TVirtualDataAccess(IGrid.Data).PictureClass:=Graphics.TPicture;
end;

procedure TVCLTeeGrid.EditorKeyUp(Sender: TObject; var AKey: Word;
  Shift: TShiftState);
begin
  if AKey=VK_ESCAPE then
  begin
    StopEditor;
    AKey:=0;
  end
  else
  case AKey of
    VK_RETURN,
    VK_TAB,
    VK_UP,
    VK_DOWN: begin
               TryChangeEditorData;

               if not Editing.AlwaysVisible then
                  StopEditor;

               IGrid.KeyDown(AKey,Shift);

               AKey:=0;
             end;
  end;
end;

function TVCLTeeGrid.Height: Single;
begin
  result:=IGrid.Height;
end;

function TVCLTeeGrid.HorizScrollBarHeight: Single;
begin
  result:=IGrid.HorizScrollHeight;
end;

function TVCLTeeGrid.Painter: TPainter;
begin
  result:=IGrid.Painter;
end;

function TVCLTeeGrid.Width: Single;
begin
  result:=IGrid.Width;
end;

function TVCLTeeGrid.VertScrollBarWidth:Single;
begin
  result:=IGrid.VertScrollWidth;
end;

procedure TVCLTeeGrid.VertScrollChanged;
begin
  if IGrid.ScrollBars.Visible then
     IGrid.UpdateScroll(SB_VERT,IGrid.GetScrollY);
end;

procedure TVCLTeeGrid.HorizScrollChanged;
begin
  if IGrid.ScrollBars.Visible then
     IGrid.UpdateScroll(SB_HORZ,IGrid.GetScrollX);
end;

{$IFDEF DATETIMEPICKER}
type
  {$IFDEF FPC}
  TCommonCalendar=TCustomDateTimePicker;
  {$ENDIF}

  TCommonCalendarAccess=class(TCommonCalendar);
{$ENDIF}

procedure TVCLTeeGrid.CreateEditor(const AColumn:TColumn);

  procedure PrepareEdit(const AEdit:TEdit);
  begin
    {$IFNDEF FPC}
    AEdit.BevelOuter:=bvNone;
    AEdit.BevelInner:=bvNone;
    AEdit.BorderStyle:=bsNone;
    {$ENDIF}

    AEdit.OnKeyUp:=EditorKeyUp;
  end;

var tmp : TClass;
begin
  tmp:=Editing.ClassOf(AColumn);

  if tmp=nil then
     tmp:=TEdit;

  if (IEditor=nil) or (IEditor.ClassType<>tmp) then
  begin
    IEditor.Free;

    if tmp.InheritsFrom(TControl) then
    begin
      IEditor:=TControlClass(tmp).Create(Self);
      IEditor.Parent:=IGrid;

      if IEditor is TEdit then
         PrepareEdit(TEdit(IEditor));
    end
    else
      IEditor:=nil;
  end;
end;

procedure TVCLTeeGrid.StartEditor(const AColumn:TColumn; const ARow:Integer);
begin
  CreateEditor(AColumn);

  if IEditor<>nil then
     TryShowEditor(AColumn,ARow);
end;

type
  TControlAccess=class(TControl);

procedure TVCLTeeGrid.TryShowEditor(const AColumn:TColumn; const ARow:Integer);

  procedure SetEditorData;
  var tmp : String;

      {$IFDEF DATETIMEPICKER}
      tmpDate : TDateTime;
      {$ENDIF}
  begin
    tmp:=Data.AsString(AColumn,ARow);

    if IEditor is TCustomEdit then
       TCustomEdit(IEditor).Text:=tmp

    {$IFDEF DATETIMEPICKER}
    else
    if IEditor is TCommonCalendar then
       if TryStrToDateTime(tmp,tmpDate) then
          TCommonCalendarAccess(IEditor).DateTime:=tmpDate
    {$ENDIF}
    ;

    if Assigned(IGrid.FOnShowEditor) then
       IGrid.FOnShowEditor(Self,IEditor,AColumn,ARow);
  end;

  procedure SetEditorBounds;
  var tmp : TRect;
  begin
    tmp:=Current.CellRect(AColumn,ARow).Round;

    if not EqualRect(IEditor.BoundsRect,tmp) then
       IEditor.BoundsRect:=tmp;
  end;

begin
  if (AColumn<>IEditorColumn) or (ARow<>IEditorRow) then
  begin
    CreateEditor(AColumn);

    if IEditor<>nil then
    begin
      SetEditorBounds;

      SetEditorData;

      TGDIPainter.ApplyFont(Current.Rows.FontOf(AColumn),TControlAccess(IEditor).Font);

      if not IEditor.Visible then
         IEditor.Show;

      if IEditor is TWinControl then
         TWinControl(IEditor).SetFocus;

      IEditorColumn:=AColumn;
      IEditorRow:=ARow;
    end;
  end
  else
    SetEditorBounds;
end;

function TVCLTeeGrid.EditorShowing:Boolean;
begin
  result:=(IEditor<>nil) and IEditor.Visible;
end;

procedure TVCLTeeGrid.TryChangeEditorData;

  function GetEditorText(out AText:String):Boolean;
  begin
    result:=IEditor is TCustomEdit;

    if result then
       AText:=TCustomEdit(IEditor).Text

    {$IFDEF DATETIMEPICKER}
    else
    begin
      result:=IEditor is TCommonCalendar;

      if result then
         AText:=DateTimeToStr(TCommonCalendarAccess(IEditor).DateTime);
    end
    {$ENDIF}
    ;
  end;

var tmp : String;
begin
  if EditorShowing then
     if GetEditorText(tmp) then
        if tmp<>Data.AsString(Selected.Column,Selected.Row) then
           Data.SetValue(Selected.Column,Selected.Row,tmp);
end;

procedure TVCLTeeGrid.StopEditor;
begin
  if EditorShowing and (not (csDestroying in ComponentState)) then
  begin
    IEditor.Hide;

    IGrid.Invalidate;
    IGrid.SetFocus;
  end;

  inherited;
end;

end.
