{*********************************************}
{  TeeGrid Software Library                   }
{  VCL THeader Editor                         }
{  Copyright (c) 2016 by Steema Software      }
{  All Rights Reserved                        }
{*********************************************}
unit VCLTee.Editor.Header;

interface

uses
  {Winapi.}Windows, {Winapi.}Messages, {System.}SysUtils, {System.}Classes, {Vcl.}Graphics,
  {Vcl.}Controls, {Vcl.}Forms, {Vcl.}Dialogs, {Vcl.}ComCtrls, {Vcl.}StdCtrls,
  {Vcl.}ExtCtrls,

  VCLTee.Editor.Stroke, VCLTee.Editor.ColumnBand,

  // Must be last used unit due to clash with THeader in VCL
  Tee.Grid.Header;

type
  THeaderEditor = class(TForm)
    PageHeader: TPageControl;
    TabFormat: TTabSheet;
    TabRowLines: TTabSheet;
    procedure PageHeaderChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }

    IHeader : TColumnHeaderBand;
    IFormat : TColumnBandEditor;
    IRowLines : TStrokeEditor;
  public
    { Public declarations }

    procedure RefreshHeader(const AHeader:TColumnHeaderBand);

    class function Edit(const AOwner:TComponent; const AHeader:TColumnHeaderBand):Boolean; static;

    class function Embedd(const AOwner:TComponent; const AParent:TWinControl;
                          const AHeader:TColumnHeaderBand):THeaderEditor; overload; static;
  end;

implementation

{$R *.dfm}

class function THeaderEditor.Edit(const AOwner: TComponent;
  const AHeader: TColumnHeaderBand): Boolean;
begin
  with THeaderEditor.Create(AOwner) do
  try
    RefreshHeader(AHeader);
    result:=ShowModal=mrOk;
  finally
    Free;
  end;
end;

class function THeaderEditor.Embedd(const AOwner: TComponent;
  const AParent: TWinControl; const AHeader: TColumnHeaderBand): THeaderEditor;
begin
  result:=THeaderEditor.Create(AOwner);
  result.RefreshHeader(AHeader);
  TStrokeEditor.AddForm(result,AParent);
end;

procedure THeaderEditor.FormCreate(Sender: TObject);
begin
  PageHeader.ActivePage:=TabFormat;
end;

procedure THeaderEditor.PageHeaderChange(Sender: TObject);
begin
  if IHeader=nil then
     Exit;

  if PageHeader.ActivePage=TabFormat then
  begin
    if IFormat=nil then
       IFormat:=TColumnBandEditor.Embedd(Self,TabFormat,IHeader);
  end
  else
  if PageHeader.ActivePage=TabRowLines then
  begin
    if IRowLines=nil then
       IRowLines:=TStrokeEditor.Embedd(Self,TabRowLines,IHeader.RowLines);
  end
end;

procedure THeaderEditor.RefreshHeader(const AHeader: TColumnHeaderBand);
begin
  IHeader:=AHeader;
  PageHeaderChange(Self);
end;

end.
