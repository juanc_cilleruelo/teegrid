unit VCLTee.Editor.Grid.Band.Totals;

interface

uses
  {Winapi.}Windows, {Winapi.}Messages, {System.}SysUtils, {System.}Classes,
  {Vcl.}Graphics, {Vcl.}Controls, {Vcl.}Forms, {Vcl.}Dialogs,
  {Vcl.}ComCtrls, {Vcl.}StdCtrls, {Vcl.}ExtCtrls,

  Tee.Grid.Columns, Tee.Grid.Totals,
  VCLTee.Editor.Format.Text, VCLTee.Editor.Stroke;

type
  TColumnTotalsEditor = class(TForm)
    PageTotals: TPageControl;
    TabCalc: TTabSheet;
    TabFormat: TTabSheet;
    TreeColumns: TTreeView;
    TabHover: TTabSheet;
    TabLines: TTabSheet;
    Panel1: TPanel;
    CBHoverVisible: TCheckBox;
    CBHoverParentFont: TCheckBox;
    RGCalc: TRadioGroup;
    BNone: TButton;
    procedure PageTotalsChange(Sender: TObject);
    procedure CBHoverVisibleClick(Sender: TObject);
    procedure CBHoverParentFontClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure RGCalcClick(Sender: TObject);
    procedure TreeColumnsChange(Sender: TObject; Node: TTreeNode);
    procedure BNoneClick(Sender: TObject);
  private
    { Private declarations }

    ITotals : TColumnTotals;

    IFormat : TTextFormatEditor;
    IHover : TTextformatEditor;
    ILines : TStrokeEditor;

    procedure AddColumns;
    function Current:TColumn;
  public
    { Public declarations }

    procedure RefreshTotals(const ATotals:TColumnTotals);

    class function Edit(const AOwner:TComponent; const ATotals:TColumnTotals):Boolean; static;

    class function Embedd(const AOwner:TComponent; const AParent:TWinControl;
                          const ATotals:TColumnTotals):TColumnTotalsEditor; static;
  end;

implementation

{$R *.dfm}

uses
  Tee.Grid.Data;

{ TColumnTotalsEditor }

procedure TColumnTotalsEditor.BNoneClick(Sender: TObject);
begin
  RGCalc.ItemIndex:=-1;
  BNone.Enabled:=False;

  RGCalcClick(Self);
end;

procedure TColumnTotalsEditor.CBHoverParentFontClick(Sender: TObject);
begin
  ITotals.Hover.ParentFont:=CBHoverParentFont.Checked;
end;

procedure TColumnTotalsEditor.CBHoverVisibleClick(Sender: TObject);
begin
  ITotals.Hover.Visible:=CBHoverVisible.Checked;
end;

function TColumnTotalsEditor.Current: TColumn;
var tmp : TTreeNode;
begin
  tmp:=TreeColumns.Selected;

  if tmp=nil then
     result:=nil
  else
     result:=TColumn(tmp.Data);
end;

class function TColumnTotalsEditor.Edit(const AOwner: TComponent;
  const ATotals: TColumnTotals): Boolean;
begin
  with TColumnTotalsEditor.Create(AOwner) do
  try
    RefreshTotals(ATotals);
    result:=ShowModal=mrOk;
  finally
    Free;
  end;
end;

class function TColumnTotalsEditor.Embedd(const AOwner: TComponent;
  const AParent: TWinControl;
  const ATotals: TColumnTotals): TColumnTotalsEditor;
begin
  result:=TColumnTotalsEditor.Create(AOwner);
  result.RefreshTotals(ATotals);
  TStrokeEditor.AddForm(result,AParent);
end;

procedure TColumnTotalsEditor.FormCreate(Sender: TObject);
begin
  PageTotals.ActivePage:=TabCalc;
end;

procedure TColumnTotalsEditor.PageTotalsChange(Sender: TObject);
begin
  if PageTotals.ActivePage=TabFormat then
  begin
    if IFormat=nil then
       IFormat:=TTextFormatEditor.Embedd(Self,TabFormat,ITotals.Format);
  end
  else
  if PageTotals.ActivePage=TabHover then
  begin
    if IHover=nil then
       IHover:=TTextFormatEditor.Embedd(Self,TabHover,ITotals.Hover.Format);
  end
  else
  if PageTotals.ActivePage=TabLines then
  begin
    if ILines=nil then
       ILines:=TStrokeEditor.Embedd(Self,TabLines,ITotals.Lines);
  end
end;

procedure TColumnTotalsEditor.AddColumns;

  procedure Add(const AParent:TTreeNode; const AColumns:TColumns);
  var tmpNode : TTreeNode;
      tmp : TColumn;
      t : Integer;
  begin
    for t:=0 to AColumns.Count-1 do
    begin
      tmp:=AColumns[t];

      tmpNode:=TreeColumns.Items.AddChildObject(AParent,tmp.Header.DisplayText,tmp);

      if tmp.HasItems then
         Add(tmpNode,tmp.Items);
    end;
  end;

begin
  TreeColumns.Items.BeginUpdate;
  try
    TreeColumns.Items.Clear;

    Add(nil,ITotals.Columns);
  finally
    TreeColumns.Items.EndUpdate;
  end;
end;

procedure TColumnTotalsEditor.RefreshTotals(const ATotals: TColumnTotals);
begin
  ITotals:=ATotals;

  AddColumns;

  CBHoverVisible.Checked:=ITotals.Hover.Visible;
  CBHoverParentFont.Checked:=ITotals.Hover.ParentFont;
end;

procedure TColumnTotalsEditor.RGCalcClick(Sender: TObject);
var tmp : Integer;
begin
  tmp:=ITotals.Calculation.IndexOf(Current);

  if tmp<>-1 then
     if RGCalc.ItemIndex=-1 then
        ITotals.Calculation.Delete(tmp)
     else
        ITotals.Calculation.Items[tmp].Calculation:=TColumnCalculation(RGCalc.ItemIndex)
  else
  if RGCalc.ItemIndex<>-1 then
     ITotals.Calculation.Add(Current,TColumnCalculation(RGCalc.ItemIndex));

  BNone.Enabled:=RGCalc.ItemIndex<>-1;

  ITotals.Changed(Self);
end;

procedure TColumnTotalsEditor.TreeColumnsChange(Sender: TObject;
  Node: TTreeNode);
var tmp : Integer;
begin
  RGCalc.Enabled:=Node<>nil;

  if RGCalc.Enabled then
  begin
    tmp:=ITotals.Calculation.IndexOf(Current);

    if tmp=-1 then
       RGCalc.ItemIndex:=-1
    else
       RGCalc.ItemIndex:=Ord(ITotals.Calculation.Items[tmp].Calculation);

    BNone.Enabled:=RGCalc.ItemIndex<>-1;
  end
  else
    BNone.Enabled:=False;
end;

end.
