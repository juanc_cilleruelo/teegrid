{*********************************************}
{  TeeGrid Software Library                   }
{  VCL TScrollableControl                     }
{  Copyright (c) 2016 by Steema Software      }
{  All Rights Reserved                        }
{*********************************************}
unit VCLTee.Control;

interface

uses
  Messages, {System.}Classes,
  {VCL.}Controls;

type
  TScrollBarVisible=(Automatic,Yes,No);

  TScrollBars=class;

  TScrollBar=class(TPersistent)
  private
    FVisible : TScrollBarVisible;

    IsVisible : Boolean;
    ThumbPosition : Integer;

    IBars : TScrollBars;

    procedure PrepareVisible(const Needed:Boolean);
    procedure SetVisible(const Value: TScrollBarVisible);
  public
    property Visible : TScrollBarVisible read FVisible write SetVisible default TScrollBarVisible.Automatic;
  end;

  TScrollableControl=class;

  TScrollBars=class(TPersistent)
  private
    FHorizontal,
    FVertical : TScrollBar;
    FVisible : Boolean;

    IControl : TScrollableControl;

    procedure SetVisible(const Value: Boolean);
  public
    Constructor Create(const AControl:TScrollableControl);
    Destructor Destroy; override;

    property Horizontal:TScrollBar read FHorizontal;
    property Vertical:TScrollBar read FVertical;
  published
    property Visible:Boolean read FVisible write SetVisible default True;
  end;

  TScrollableControl=class(TCustomControl)
  private
    FScrollBars: TScrollBars;

    function CalcScroll(const Bar:Word; const Code,Pos:Integer):Single;
    procedure WMHScroll(var Msg: TWMHScroll); message WM_HSCROLL;
    procedure WMVScroll(var Msg: TWMVScroll); message WM_VSCROLL;

    function RemainSize(const Bar:Word):Single;
    procedure SetScrollBars(const Value: TScrollBars);
  protected
    function GetMaxBottom:Single; virtual; abstract;
    function GetMaxRight:Single; virtual; abstract;

    function GetScrollX:Single; virtual; abstract;
    function GetScrollY:Single; virtual; abstract;

    function HorizScrollHeight:Integer;
    function VertScrollWidth:Integer;

    procedure ResetScrollBars; virtual;

    procedure SetScrollX(const Value:Single); virtual; abstract;
    procedure SetScrollY(const Value:Single); virtual; abstract;

    procedure UpdateScroll(const Bar:Integer; const Value:Single);
  public
    Constructor Create(AOwner:TComponent); override;
    Destructor Destroy; override;

    property ScrollBars:TScrollBars read FScrollBars write SetScrollBars;
  end;

implementation

uses
  {$IFNDEF FPC}
  {Winapi.}FlatSB,
  {$ENDIF}
  Windows;

{ TScrollableControl }

Constructor TScrollableControl.Create(AOwner: TComponent);
begin
  inherited;

  FScrollBars:=TScrollBars.Create(Self);
end;

Destructor TScrollableControl.Destroy;
begin
  FScrollBars.Free;
  inherited;
end;

function TScrollableControl.CalcScroll(const Bar:Word; const Code,Pos:Integer):Single;

  function GetScroll:Single;
  begin
    if Bar=SB_HORZ then
       result:=GetScrollX
    else
       result:=GetScrollY;
  end;

const
  Small=3;

  function Large:Single;
  begin
    result:=Small*10;
  end;

  function CanScrollMore:Boolean;
  begin
    result:=GetScroll<RemainSize(Bar);
  end;

  function ThumbPosition:Single;
  var tmp : Integer;
  begin
    if Bar=SB_HORZ then
       tmp:=ScrollBars.Horizontal.ThumbPosition
    else
       tmp:=ScrollBars.Vertical.ThumbPosition;

    tmp:=100-tmp;

    if tmp=0 then
       result:=0
    else
       result:=Pos*RemainSize(Bar)/tmp;
  end;

begin
  case Code of
 SB_THUMBPOSITION,
    SB_THUMBTRACK: result:=ThumbPosition;

        SB_BOTTOM: result:=0;

           SB_TOP: if Bar=SB_HORZ then
                      result:=Width-Large
                   else
                      result:=Height-Large;
  else
  begin
    result:=GetScroll;

    case Code of
        SB_LINEUP: if result>Small then result:=result-Small else result:=0;
      SB_LINEDOWN: if CanScrollMore then result:=result+Small;
        SB_PAGEUP: if result>Large then result:=result-Large else result:=0;
      SB_PAGEDOWN: if CanScrollMore then result:=result+Large;
    end;
  end;
  end;

  if result<0 then
     result:=0;
end;

procedure TScrollableControl.WMHScroll(var Msg: TWMHScroll);
var tmp : Single;
begin
  tmp:=CalcScroll(SB_HORZ,Msg.ScrollCode,Msg.Pos);
  SetScrollX(tmp);
  UpdateScroll(SB_HORZ,tmp);
end;

procedure TScrollableControl.UpdateScroll(const Bar:Integer; const Value:Single);
var tmp : Integer;
    tmpHandle : HWND;
    tmpRemain : Single;
begin
  Invalidate;
  Update;

  tmpRemain:=RemainSize(Bar);

  if tmpRemain=0 then
     tmp:=0
  else
     tmp:=Round(100*Value/tmpRemain);

  tmpHandle:=Handle;

  if GetScrollPos(tmpHandle,Bar)<>tmp then
     SetScrollPos(tmpHandle,Bar,tmp,True);
end;

function TScrollableControl.HorizScrollHeight:Integer;
begin
  if ScrollBars.Horizontal.IsVisible then
     result:=GetSystemMetrics(SM_CYHSCROLL)
  else
     result:=0;
end;

function TScrollableControl.VertScrollWidth:Integer;
begin
  if ScrollBars.Vertical.IsVisible then
     result:=GetSystemMetrics(SM_CXVSCROLL)
  else
     result:=0;
end;

function TScrollableControl.RemainSize(const Bar:Word):Single;
begin
  if Bar=SB_HORZ then
     result:=(GetMaxRight-Width)+VertScrollWidth
  else
     result:=(GetMaxBottom-Height)+HorizScrollHeight;
end;

procedure TScrollableControl.ResetScrollBars;

  procedure ShowBar(const ABar:Word);
  var tmp : TScrollInfo;
      tmpBar : TScrollBar;
  begin
    tmp.cbSize:=SizeOf(TScrollInfo);
    tmp.fMask:=SIF_ALL;
    tmp.nMin:=0;

    if ABar=SB_HORZ then
       tmpBar:=ScrollBars.Horizontal
    else
       tmpBar:=ScrollBars.Vertical;

    if tmpBar.IsVisible then
    begin
      tmp.nMax:=100;

      tmp.nPage:=tmpBar.ThumbPosition;
    end
    else
    begin
      tmp.nMax:=0;
      tmp.nPage:=0;
    end;

    tmp.nPos:=0;
    tmp.nTrackPos:=0;

    {$IFDEF FPC}
    SetScrollInfo(Handle,ABar,tmp,True);
    {$ELSE}
    FlatSB_SetScrollInfo(Handle,ABar,tmp,True);
    {$ENDIF}
  end;

  procedure AreScrollBarsVisible;
  var tmpW,
      tmpH : Single;
  begin
    tmpW:=GetMaxRight;
    tmpH:=GetMaxBottom;

    ScrollBars.FHorizontal.PrepareVisible(tmpW>Width);
    ScrollBars.FVertical.PrepareVisible(tmpH>Height);

    if not ScrollBars.Horizontal.IsVisible then
       if ScrollBars.Vertical.IsVisible then
          ScrollBars.FHorizontal.IsVisible:=tmpW>(Width-VertScrollWidth);

    if not ScrollBars.Vertical.IsVisible then
       if ScrollBars.Horizontal.IsVisible then
          ScrollBars.FVertical.IsVisible:=tmpH>(Height-HorizScrollHeight);
  end;

  function Check(const AThumb:Integer):Integer;
  begin
    if AThumb>100 then
       result:=100
    else
       result:=AThumb;
  end;

  procedure InitHorizScroll;
  var tmp : Single;
  begin
    ScrollBars.FHorizontal.ThumbPosition:=0;

    if ScrollBars.Horizontal.IsVisible then
    begin
      tmp:=(Width-VertScrollWidth);

      if tmp<>0 then
      begin
        tmp:=(GetMaxRight/tmp);

        if tmp<>0 then
           ScrollBars.FHorizontal.ThumbPosition:=Check(Round(100/tmp));
      end;
    end;
  end;

  procedure InitVertScroll;
  var tmp : Single;
  begin
    ScrollBars.FVertical.ThumbPosition:=0;

    if ScrollBars.Vertical.IsVisible then
    begin
      tmp:=(Height-HorizScrollHeight);

      if tmp<>0 then
      begin
        tmp:=(GetMaxBottom/tmp);

        if tmp<>0 then
           ScrollBars.FVertical.ThumbPosition:=Check(Round(100/tmp));
      end;
    end;
  end;

begin
  if ScrollBars.Visible then
     AreScrollBarsVisible
  else
  begin
    ScrollBars.FHorizontal.IsVisible:=False;
    ScrollBars.FVertical.IsVisible:=False;
  end;

  InitHorizScroll;
  InitVertScroll;

  ShowBar(SB_HORZ);
  ShowBar(SB_VERT);
end;

procedure TScrollableControl.SetScrollBars(const Value: TScrollBars);
begin
  FScrollBars.Assign(Value);
end;

procedure TScrollableControl.WMVScroll(var Msg: TWMVScroll);
var tmp : Single;
begin
  tmp:=CalcScroll(SB_VERT,Msg.ScrollCode,Msg.Pos);
  SetScrollY(tmp);
  UpdateScroll(SB_VERT,tmp);
end;

{ TScrollBars }

constructor TScrollBars.Create(const AControl: TScrollableControl);
begin
  inherited Create;

  FVisible:=True;
  IControl:=AControl;

  FHorizontal:=TScrollBar.Create;
  FHorizontal.IBars:=Self;

  FVertical:=TScrollBar.Create;
  FVertical.IBars:=Self;
end;

Destructor TScrollBars.Destroy;
begin
  FVertical.Free;
  FHorizontal.Free;

  inherited;
end;

procedure TScrollBars.SetVisible(const Value: Boolean);
begin
  if FVisible<>Value then
  begin
    FVisible:=Value;
    IControl.ResetScrollBars;
  end;
end;

{ TScrollBar }

procedure TScrollBar.PrepareVisible(const Needed: Boolean);
begin
  if Visible=TScrollBarVisible.Yes then
     IsVisible:=True
  else
  if Visible=TScrollBarVisible.Automatic then
     IsVisible:=Needed
  else
     IsVisible:=False;
end;

procedure TScrollBar.SetVisible(const Value: TScrollBarVisible);
begin
  if FVisible<>Value then
  begin
    FVisible:=Value;
    IBars.IControl.ResetScrollBars;
  end;
end;

end.
