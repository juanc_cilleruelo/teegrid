{*********************************************}
{  TeeGrid Software Library                   }
{  VCL THeader Editor                         }
{  Copyright (c) 2016 by Steema Software      }
{  All Rights Reserved                        }
{*********************************************}
unit VCLTee.Editor.ColumnBand;

interface

uses
  {Winapi.}Windows, {Winapi.}Messages, {System.}SysUtils, {System.}Classes, {Vcl.}Graphics,
  {Vcl.}Controls, {Vcl.}Forms, {Vcl.}Dialogs, {Vcl.}ComCtrls, {Vcl.}StdCtrls,
  {Vcl.}ExtCtrls,

  VCLTee.Editor.Format.Text, VCLTee.Editor.Coordinate, VCLTee.Editor.Stroke,

  // Must be last used unit due to clash with THeader in VCL
  Tee.Grid.Header;

type
  TColumnBandEditor = class(TForm)
    PageBand: TPageControl;
    TabFormat: TTabSheet;
    TabHover: TTabSheet;
    TabHeight: TTabSheet;
    Panel1: TPanel;
    CBHoverVisible: TCheckBox;
    CBHoverParentFont: TCheckBox;
    TabLines: TTabSheet;
    procedure PageBandChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CBHoverVisibleClick(Sender: TObject);
  private
    { Private declarations }

    IBand : TColumnBand;

    IHover,
    IFormat : TTextFormatEditor;

    ILines : TStrokeEditor;

    IHeight : TCoordinateEditor;
  public
    { Public declarations }

    procedure RefreshBand(const ABand:TColumnBand);

    class function Edit(const AOwner:TComponent; const ABand:TColumnBand):Boolean; static;

    class function Embedd(const AOwner:TComponent; const AParent:TWinControl;
                          const ABand:TColumnBand):TColumnBandEditor; overload; static;
  end;

implementation

{$R *.dfm}

procedure TColumnBandEditor.CBHoverVisibleClick(Sender: TObject);
begin
  IBand.Hover.Visible:=CBHoverVisible.Checked;
end;

class function TColumnBandEditor.Edit(const AOwner: TComponent;
  const ABand: TColumnBand): Boolean;
begin
  with TColumnBandEditor.Create(AOwner) do
  try
    RefreshBand(ABand);
    result:=ShowModal=mrOk;
  finally
    Free;
  end;
end;

class function TColumnBandEditor.Embedd(const AOwner: TComponent;
  const AParent: TWinControl; const ABand: TColumnBand): TColumnBandEditor;
begin
  result:=TColumnBandEditor.Create(AOwner);
  result.RefreshBand(ABand);
  TStrokeEditor.AddForm(result,AParent);
end;

procedure TColumnBandEditor.FormCreate(Sender: TObject);
begin
  PageBand.ActivePage:=TabFormat;
end;

procedure TColumnBandEditor.PageBandChange(Sender: TObject);
begin
  if IBand=nil then
     Exit;

  if PageBand.ActivePage=TabFormat then
  begin
    if IFormat=nil then
       IFormat:=TTextFormatEditor.Embedd(Self,TabFormat,IBand.Format);
  end
  else
  if PageBand.ActivePage=TabHeight then
  begin
    if IHeight=nil then
       IHeight:=TCoordinateEditor.Embedd(Self,TabHeight,IBand.Height);
  end
  else
  if PageBand.ActivePage=TabHover then
  begin
    if IHover=nil then
       IHover:=TTextFormatEditor.Embedd(Self,TabHover,IBand.Hover.Format);
  end
  else
  if PageBand.ActivePage=TabLines then
  begin
    if ILines=nil then
       ILines:=TStrokeEditor.Embedd(Self,TabLines,IBand.Lines);
  end
end;

procedure TColumnBandEditor.RefreshBand(const ABand: TColumnBand);
begin
  IBand:=ABand;

  CBHoverVisible.Checked:=IBand.Hover.Visible;
  CBHoverParentFont.Checked:=IBand.Hover.ParentFont;

  PageBandChange(Self);
end;

end.
