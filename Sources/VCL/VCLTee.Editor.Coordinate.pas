{*********************************************}
{  TeeGrid Software Library                   }
{  VCL TCoordinate Editor                     }
{  Copyright (c) 2016 by Steema Software      }
{  All Rights Reserved                        }
{*********************************************}
unit VCLTee.Editor.Coordinate;

interface

uses
  {Winapi.}Windows, {Winapi.}Messages, {System.}SysUtils, {System.}Classes, {Vcl.}Graphics,
  {Vcl.}Controls, {Vcl.}Forms, {Vcl.}Dialogs, {Vcl.}StdCtrls, {Vcl.}ExtCtrls, {Vcl.}ComCtrls,
  Tee.Format;

type
  TCoordinateEditor = class(TForm)
    Label5: TLabel;
    CBAuto: TCheckBox;
    UDValue: TUpDown;
    EValue: TEdit;
    RGUnits: TRadioGroup;
    procedure CBAutoClick(Sender: TObject);
    procedure EValueChange(Sender: TObject);
    procedure RGUnitsClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }

    IChanging : Boolean;
    ICoord : TCoordinate;

  public
    { Public declarations }

    procedure RefreshCoordinate(const ACoordinate:TCoordinate);

    class function Embedd(const AOwner:TComponent; const AParent:TWinControl;
                          const ACoordinate:TCoordinate):TCoordinateEditor; overload; static;
  end;

implementation

{$R *.dfm}

uses
  VCLTee.Editor.Stroke;

procedure TCoordinateEditor.CBAutoClick(Sender: TObject);
begin
  ICoord.Automatic:=CBAuto.Checked;
end;

class function TCoordinateEditor.Embedd(const AOwner: TComponent;
  const AParent: TWinControl;
  const ACoordinate: TCoordinate): TCoordinateEditor;
begin
  result:=TCoordinateEditor.Create(AOwner);
  result.RefreshCoordinate(ACoordinate);
  TStrokeEditor.AddForm(result,AParent);
end;

procedure TCoordinateEditor.EValueChange(Sender: TObject);
begin
  ICoord.Value:=UDValue.Position;

  if Showing and (not IChanging) then
     CBAuto.Checked:=False;
end;

procedure TCoordinateEditor.FormCreate(Sender: TObject);
begin
  IChanging:=True;
end;

procedure TCoordinateEditor.FormShow(Sender: TObject);
begin
  IChanging:=False;
end;

procedure TCoordinateEditor.RefreshCoordinate(const ACoordinate: TCoordinate);
begin
  IChanging:=True;

  ICoord:=ACoordinate;

  UDValue.Position:=Round(ICoord.Value);
  CBAuto.Checked:=ICoord.Automatic;
  RGUnits.ItemIndex:=Ord(ICoord.Units);

  IChanging:=False;
end;

procedure TCoordinateEditor.RGUnitsClick(Sender: TObject);
begin
  ICoord.Units:=TSizeUnits(RGUnits.ItemIndex);

  if Showing and (not IChanging) then
     CBAuto.Checked:=False;
end;

end.
