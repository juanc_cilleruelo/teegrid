unit VCLTee.Editor.Grid.Ticker;

interface

uses
  {Winapi.}Windows, {Winapi.}Messages, {System.}SysUtils, {System.}Classes,
  {Vcl.}Graphics, {Vcl.}Controls, {Vcl.}Forms, {Vcl.}Dialogs, {Vcl.}ComCtrls,
  {Vcl.}StdCtrls, {Vcl.}ExtCtrls,

  Tee.Grid.Ticker, VCLTee.Editor.Brush;

type
  TGridTickerEditor = class(TForm)
    PageTicker: TPageControl;
    TabOptions: TTabSheet;
    TabHigher: TTabSheet;
    TabLower: TTabSheet;
    Label2: TLabel;
    LDelay: TLabel;
    CBFade: TCheckBox;
    CBEnabled: TCheckBox;
    TBDelay: TTrackBar;
    procedure TBDelayChange(Sender: TObject);
    procedure CBEnabledClick(Sender: TObject);
    procedure CBFadeClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PageTickerChange(Sender: TObject);
  private
    { Private declarations }

    ITicker : TGridTicker;

    IHigher,
    ILower : TBrushEditor;

    procedure RefreshDelay;
  public
    { Public declarations }

    procedure RefreshTicker(const ATicker:TGridTicker);

    class function Edit(const AOwner:TComponent; const ATicker:TGridTicker):Boolean; static;

    class function Embedd(const AOwner:TComponent; const AParent:TWinControl;
                          const ATicker:TGridTicker):TGridTickerEditor; static;
  end;

implementation

{$R *.dfm}

uses
  VCLTee.Editor.Stroke, Tee.Format;

{ TGridTickerEditor }

procedure TGridTickerEditor.RefreshTicker(const ATicker: TGridTicker);
begin
  ITicker:=ATicker;

  RefreshDelay;
end;

procedure TGridTickerEditor.TBDelayChange(Sender: TObject);
begin
  ITicker.Delay:=TBDelay.Position;
  RefreshDelay;
end;

procedure TGridTickerEditor.CBEnabledClick(Sender: TObject);
begin
  ITicker.Enabled:=CBEnabled.Checked;
end;

procedure TGridTickerEditor.CBFadeClick(Sender: TObject);
begin
  ITicker.FadeColors:=CBFade.Checked;
end;

class function TGridTickerEditor.Edit(const AOwner: TComponent;
  const ATicker: TGridTicker): Boolean;
begin
  with TGridTickerEditor.Create(AOwner) do
  try
    RefreshTicker(ATicker);
    result:=ShowModal=mrOk;
  finally
    Free;
  end;
end;

class function TGridTickerEditor.Embedd(const AOwner: TComponent;
  const AParent: TWinControl; const ATicker: TGridTicker): TGridTickerEditor;
begin
  result:=TGridTickerEditor.Create(AOwner);
  result.RefreshTicker(ATicker);
  TStrokeEditor.AddForm(result,AParent);
end;

procedure TGridTickerEditor.FormCreate(Sender: TObject);
begin
  PageTicker.ActivePage:=TabOptions;
end;

procedure TGridTickerEditor.PageTickerChange(Sender: TObject);

  procedure TryBrush(var AEditor:TBrushEditor; const AParent:TWinControl;
                     const ABrush:TBrush);
  begin
    if AEditor=nil then
    begin
      AEditor:=TBrushEditor.Embedd(Self,AParent,ABrush);
      AEditor.PanelTop.Hide;
    end;
  end;

begin
  if PageTicker.ActivePage=TabHigher then
     TryBrush(IHigher,TabHigher,ITicker.Higher)
  else
  if PageTicker.ActivePage=TabLower then
     TryBrush(ILower,TabLower,ITicker.Lower);
end;

procedure TGridTickerEditor.RefreshDelay;
begin
  LDelay.Caption:=IntToStr(ITicker.Delay)+' msec';
end;

end.
