unit VCLTee.Editor.Gradient;

interface

uses
  {Winapi.}Windows, {Winapi.}Messages, {System.}SysUtils, {System.}Classes,
  {Vcl.}Graphics, {Vcl.}Controls, {Vcl.}Forms, {Vcl.}Dialogs, {Vcl.}ComCtrls,
  {Vcl.}StdCtrls, {Vcl.}ExtCtrls,

  {$IFDEF FPC}
  ColorBox,
  {$ENDIF}

  Tee.Format;

type
  TGradientEditor = class(TForm)
    PageGradient: TPageControl;
    TabColors: TTabSheet;
    TabDirection: TTabSheet;
    CBVisible: TCheckBox;
    CBColor0: TColorBox;
    CBColor1: TColorBox;
    Label1: TLabel;
    TBOpacity0: TTrackBar;
    LOpacity0: TLabel;
    Label2: TLabel;
    TBOpacity1: TTrackBar;
    LOpacity1: TLabel;
    RGDirection: TRadioGroup;
    CBInverted: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure CBColor0Change(Sender: TObject);
    procedure CBColor1Change(Sender: TObject);
    procedure CBInvertedClick(Sender: TObject);
    procedure CBVisibleClick(Sender: TObject);
    procedure TBOpacity0Change(Sender: TObject);
    procedure TBOpacity1Change(Sender: TObject);
    procedure RGDirectionClick(Sender: TObject);
  private
    { Private declarations }

    IGradient : TGradient;

    procedure ChangeColor0;
    procedure SetOpacityLabel0;

    procedure ChangeColor1;
    procedure SetOpacityLabel1;
  public
    { Public declarations }

    procedure RefreshGradient(const AGradient:TGradient);

    class function Edit(const AOwner:TComponent; const AGradient:TGradient):Boolean; static;

    class function Embedd(const AOwner:TComponent; const AParent:TWinControl;
                          const AGradient:TGradient):TGradientEditor; static;
  end;

implementation

{$R *.dfm}

uses
  VCLTee.Editor.Stroke;

procedure TGradientEditor.CBColor0Change(Sender: TObject);
begin
  if IGradient.Colors.Count=0 then
     IGradient.Colors.Add;

  IGradient.Colors[0].Color:=CBColor0.Selected;
end;

procedure TGradientEditor.CBColor1Change(Sender: TObject);
begin
  while IGradient.Colors.Count<2 do
     IGradient.Colors.Add;

  IGradient.Colors[1].Color:=CBColor1.Selected;
end;

procedure TGradientEditor.CBInvertedClick(Sender: TObject);
begin
  IGradient.Inverted:=CBInverted.Checked;
end;

procedure TGradientEditor.CBVisibleClick(Sender: TObject);
begin
  IGradient.Visible:=CBVisible.Checked;
end;

procedure TGradientEditor.ChangeColor0;
begin
  while IGradient.Colors.Count<1 do
        IGradient.Colors.Add;

  IGradient.Colors[0].Color:=TColorHelper.From(CBColor0.Selected,TBOpacity0.Position*0.01);
end;

procedure TGradientEditor.ChangeColor1;
begin
  while IGradient.Colors.Count<2 do
        IGradient.Colors.Add;

  IGradient.Colors[1].Color:=TColorHelper.From(CBColor1.Selected,TBOpacity1.Position*0.01);
end;

class function TGradientEditor.Edit(const AOwner: TComponent;
  const AGradient: TGradient): Boolean;
begin
  with TGradientEditor.Create(AOwner) do
  try
    RefreshGradient(AGradient);
    result:=ShowModal=mrOk;
  finally
    Free;
  end;
end;

class function TGradientEditor.Embedd(const AOwner: TComponent;
  const AParent: TWinControl; const AGradient: TGradient): TGradientEditor;
begin
  result:=TGradientEditor.Create(AOwner);
  result.RefreshGradient(AGradient);
  TStrokeEditor.AddForm(result,AParent);
end;

procedure TGradientEditor.FormCreate(Sender: TObject);
begin
  PageGradient.ActivePage:=TabColors;
end;

procedure TGradientEditor.RefreshGradient(const AGradient: TGradient);
begin
  IGradient:=AGradient;

  CBVisible.Checked:=IGradient.Visible;
  CBInverted.Checked:=IGradient.Inverted;
  RGDirection.ItemIndex:=Ord(IGradient.Direction);

  if IGradient.Colors.Count>0 then
     CBColor0.Selected:=IGradient.Colors[0].Color;

  if IGradient.Colors.Count>1 then
     CBColor1.Selected:=IGradient.Colors[1].Color;
end;

procedure TGradientEditor.RGDirectionClick(Sender: TObject);
begin
  IGradient.Direction:=TGradientDirection(RGDirection.ItemIndex);
end;

procedure TGradientEditor.SetOpacityLabel0;
begin
  LOpacity0.Caption:=IntToStr(TBOpacity0.Position)+'%';
end;

procedure TGradientEditor.SetOpacityLabel1;
begin
  LOpacity1.Caption:=IntToStr(TBOpacity1.Position)+'%';
end;

procedure TGradientEditor.TBOpacity0Change(Sender: TObject);
begin
  ChangeColor0;
  SetOpacityLabel0;
end;

procedure TGradientEditor.TBOpacity1Change(Sender: TObject);
begin
  ChangeColor1;
  SetOpacityLabel1;
end;

end.
