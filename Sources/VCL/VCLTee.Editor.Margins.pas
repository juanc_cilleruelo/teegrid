{*********************************************}
{  TeeGrid Software Library                   }
{  VCL TMargins Editor                        }
{  Copyright (c) 2016 by Steema Software      }
{  All Rights Reserved                        }
{*********************************************}
unit VCLTee.Editor.Margins;

interface

uses
  {Winapi.}Windows, {Winapi.}Messages, {System.}SysUtils, {System.}Classes, {Vcl.}Graphics,
  {Vcl.}Controls, {Vcl.}Forms, {Vcl.}Dialogs, {Vcl.}ComCtrls,
  VCLTee.Editor.Coordinate,
  Tee.Format, Tee.Renders;

type
  TMarginsEditor = class(TForm)
    PageControl1: TPageControl;
    TabLeft: TTabSheet;
    TabTop: TTabSheet;
    TabRight: TTabSheet;
    TabBottom: TTabSheet;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
  private
    { Private declarations }

    IMargins : TMargins;

    ILeft,
    ITop,
    IRight,
    IBottom : TCoordinateEditor;
  public
    { Public declarations }

    procedure RefreshMargins(const AMargins:TMargins);

    class function Edit(const AOwner:TComponent; const AMargins:TMargins):Boolean; static;

    class function Embedd(const AOwner:TComponent; const AParent:TWinControl;
                          const AMargins:TMargins):TMarginsEditor; static;
  end;

implementation

{$R *.dfm}

uses
  VCLTee.Editor.Stroke;

class function TMarginsEditor.Edit(const AOwner: TComponent;
  const AMargins: TMargins): Boolean;
begin
  with TMarginsEditor.Create(AOwner) do
  try
    RefreshMargins(AMargins);
    result:=ShowModal=mrOk;
  finally
    Free;
  end;
end;

class function TMarginsEditor.Embedd(const AOwner: TComponent;
  const AParent: TWinControl; const AMargins: TMargins): TMarginsEditor;
begin
  result:=TMarginsEditor.Create(AOwner);
  result.RefreshMargins(AMargins);
  TStrokeEditor.AddForm(result,AParent);
end;

procedure TMarginsEditor.FormCreate(Sender: TObject);
begin
  PageControl1.ActivePage:=TabLeft;
end;

procedure TMarginsEditor.FormShow(Sender: TObject);
begin
  PageControl1Change(Self);
end;

procedure TMarginsEditor.PageControl1Change(Sender: TObject);

  procedure TryShow(var AEditor:TCoordinateEditor; const AParent:TWinControl;
                    const ACoordinate:TCoordinate);
  begin
    if AEditor=nil then
       AEditor:=TCoordinateEditor.Embedd(Self,AParent,ACoordinate);
  end;

begin
  if IMargins=nil then
     Exit;

  if PageControl1.ActivePage=TabLeft then
     TryShow(ILeft,TabLeft,IMargins.Left)
  else
  if PageControl1.ActivePage=TabTop then
     TryShow(ITop,TabTop,IMargins.Top)
  else
  if PageControl1.ActivePage=TabRight then
     TryShow(IRight,TabRight,IMargins.Right)
  else
  if PageControl1.ActivePage=TabBottom then
     TryShow(IBottom,TabBottom,IMargins.Bottom);
end;

procedure TMarginsEditor.RefreshMargins(const AMargins: TMargins);

  procedure TryRefresh(const AEditor:TCoordinateEditor; const AValue:TCoordinate);
  begin
    if AEditor<>nil then
       AEditor.RefreshCoordinate(AValue);
  end;

begin
  IMargins:=AMargins;

  TryRefresh(ILeft,IMargins.Left);
  TryRefresh(ITop,IMargins.Top);
  TryRefresh(IRight,IMargins.Right);
  TryRefresh(IBottom,IMargins.Bottom);
end;

end.
