{*********************************************}
{  TeeGrid Software Library                   }
{  Abstract Painter class                     }
{  Copyright (c) 2016 by Steema Software      }
{  All Rights Reserved                        }
{*********************************************}
unit VCLTee.Painter;
{$I Tee.inc}

interface

uses
  {$IFNDEF FPC}
  {System.}Types,
  {$ENDIF}

  {WinAPI.}Windows, {VCL.}Graphics,
  Tee.Painter, Tee.Format;

type
  TTeeFont=Tee.Format.TFont;

  TWindowsPainter=class(TPainter)
  protected
    ICanvas : TCanvas;
    IDC : HDC;
  public
    Constructor Create(const ACanvas:TCanvas);

    property Canvas:TCanvas read ICanvas;

    procedure Init(const DC:HDC); virtual;
  end;

  TGDIPainter=class(TWindowsPainter)
  private
    type
      TAlign=record
      public
        Horizontal : THorizontalAlign;
        Vertical : TVerticalAlign;

        function Flags:Cardinal;
      end;

    var
      IAlign : TAlign;
      IClipHistory : Array of HRGN;

      //IFontGradient,
      //IStrokeGradient,
      IBrushGradient : TGradient;

      IBrushPicture : TPicture;

    function GraphicOf(const APicture: TPicture):TGraphic;
    function ImageFrom(const APicture: TPicture): TGraphic;
    procedure SetTextAlignments;
  public
    class procedure ApplyFont(const ASource:TTeeFont; const ADest:Graphics.TFont); static;

    procedure Clip(const R:TRectF); override;
    procedure Clip(const R:TRect); overload;
    procedure UnClip; override;

    procedure HideBrush; override;

    procedure SetBrush(const ABrush:TBrush); override;
    procedure SetFont(const AFont:TFont); override;
    procedure SetHorizontalAlign(const Value:THorizontalAlign); override;
    procedure SetStroke(const AStroke:TStroke); override;
    procedure SetVerticalAlign(const Value:TVerticalAlign); override;

    procedure Draw(const R:TRectF); override;
    procedure Draw(const P:TPointsF); override;
    procedure Draw(const APicture:TPicture; const X,Y:Single); overload; override;
    procedure Draw(const APicture:TPicture; const R:TRectF); overload; override;
    procedure DrawEllipse(const R:TRectF); override;

    procedure Fill(const R:TRectF); override;
    procedure Fill(const R:TRectF; const AColor:TColor); override;
    procedure Fill(const P:TPointsF); override;
    procedure FillEllipse(const R:TRectF); override;

    procedure HorizontalLine(const Y,X0,X1:Single); override;
    procedure Line(const X0,Y0,X1,Y1:Single); override;
    procedure Lines(const P:TPointsF); override;
    procedure Paint(const AFormat: TFormat; const R: TRectF); override;
    procedure Paint(const AFormat: TFormat; const P: TPointsF); override;
    function TextHeight(const AText:String):Single; override;
    procedure TextOut(const X,Y:Single; const AText:String); override;
    function TextWidth(const AText:String):Single; override;
    procedure VerticalLine(const X,Y0,Y1:Single); override;
  end;

implementation

{$IFNDEF NOUITYPES}
uses
  {System.}UITypes;
{$ENDIF}

{ TWindowsPainter }

Constructor TWindowsPainter.Create(const ACanvas: TCanvas);
begin
  inherited Create;
  ICanvas:=ACanvas;
end;

procedure TWindowsPainter.Init(const DC: HDC);
begin
  if (Canvas<>nil) and (Canvas.Handle<>DC) then
     Canvas.Handle:=DC;

  IDC:=DC;
end;

{ TGDIPainter }

procedure TGDIPainter.Draw(const R: TRectF);
var Old : TBrushStyle;
begin
  Old:=ICanvas.Brush.Style;

  ICanvas.Brush.Style:=bsClear;
  ICanvas.Rectangle(R.Round);

  ICanvas.Brush.Style:=Old;
end;

function FixRect(const R:TRectF):TRect;
begin
  result:=R.Round;
  Inc(result.Right);
  Inc(result.Bottom);
end;

type
{$IFDEF FPC}
  TVCLPicture=Graphics.TPicture;
  TVCLGraphic=Graphics.TGraphic;
{$ELSE}
  TVCLPicture=Graphics.TPicture;
  TVCLGraphic=Graphics.TGraphic;
{$ENDIF}

type
  TPictureAccess=class(TPicture);

function TGDIPainter.ImageFrom(const APicture: TPicture): TGraphic;
var tmp : TObject;
begin
  if TPictureAccess(APicture).Internal=nil then
  begin
    tmp:=TPictureAccess(APicture).TagObject;

    if tmp is TVCLPicture then
       TPictureAccess(APicture).Internal:=TVCLPicture(tmp).Graphic
    else
    if tmp is TVCLGraphic then
       TPictureAccess(APicture).Internal:=TVCLGraphic(tmp)
  end;

  result:=TGraphic(TPictureAccess(APicture).Internal);
end;

procedure TGDIPainter.Fill(const R: TRectF);
var tmpGraphic : TGraphic;
begin
  if IBrushPicture<>nil then
  begin
    tmpGraphic:=ImageFrom(IBrushPicture);

    if tmpGraphic<>nil then
       ICanvas.StretchDraw(FixRect(R),tmpGraphic);
  end
  else
//  if IBrushGradient=nil then
    ICanvas.FillRect(FixRect(R));
end;

type
  TPoints=Array of TPoint;

function RoundPoints(const P:TPointsF):TPoints;
var t,
    L : Integer;
begin
  L:=Length(P);
  SetLength(result,L);

  for t:=0 to L-1 do
  begin
    result[t].X:=Round(P[t].X);
    result[t].Y:=Round(P[t].Y);
  end;
end;

procedure TGDIPainter.Fill(const P:TPointsF);
begin
  ICanvas.Polygon(RoundPoints(P));
end;

procedure TGDIPainter.FillEllipse(const R: TRectF);
begin
  ICanvas.Ellipse(R.Round);
end;

procedure TGDIPainter.HideBrush;
begin
  ICanvas.Brush.Style:=TBrushStyle.bsClear;
end;

procedure TGDIPainter.HorizontalLine(const Y, X0, X1: Single);
var tmpY : Integer;
begin
  tmpY:=Round(Y);
  ICanvas.MoveTo(Round(X0),tmpY);
  ICanvas.LineTo(Round(X1),tmpY);
end;

procedure TGDIPainter.Line(const X0, Y0, X1, Y1: Single);
begin
  ICanvas.MoveTo(Round(X0),Round(Y0));
  ICanvas.LineTo(Round(X1),Round(Y1));
end;

procedure TGDIPainter.Lines(const P:TPointsF);
begin
  ICanvas.Polyline(RoundPoints(P));
end;

procedure TGDIPainter.Paint(const AFormat: TFormat; const R: TRectF);
var tmp : TRect;
    tmpP,
    tmpW : Integer;
begin
  SetBrush(AFormat.Brush);
  SetStroke(AFormat.Stroke);

  if AFormat.Stroke.Visible then
  begin
    tmp:=R.Round;

    tmpP:=ICanvas.Pen.Width;
    tmpW:=(tmpP div 2);

    Inc(tmp.Left,tmpW);
    Inc(tmp.Top,tmpW);

    tmpW:=((tmpP-1) div 2);

    Dec(tmp.Right,tmpW);
    Dec(tmp.Bottom,tmpW);

    Fill(TRectF.Create(tmp));
  end
  else
  if AFormat.Brush.Visible then
     Fill(R);
end;

procedure TGDIPainter.Paint(const AFormat: TFormat; const P: TPointsF);
begin
  SetBrush(AFormat.Brush);
  SetStroke(AFormat.Stroke);

  ICanvas.Polygon(RoundPoints(P));
end;

type
  TBrushAccess=class(TBrush);

procedure TGDIPainter.SetBrush(const ABrush: TBrush);
begin
  if TBrushAccess(ABrush).HasPicture then
     IBrushPicture:=ABrush.Picture
  else
  begin
    IBrushPicture:=nil;

    if TBrushAccess(ABrush).HasGradient then
       IBrushGradient:=ABrush.Gradient
    else
    begin
      ICanvas.Brush.Color:=TColorHelper.RemoveOpacity(ABrush.Color);

      if ABrush.Visible then
         ICanvas.Brush.Style:=bsSolid
      else
         HideBrush;

      IBrushGradient:=nil;
    end;
  end;
end;

procedure TGDIPainter.SetFont(const AFont: TFont);
begin
  ApplyFont(AFont,ICanvas.Font);
end;

procedure TGDIPainter.SetTextAlignments;
begin
  SetTextAlign(IDC,IAlign.Flags);
end;

procedure TGDIPainter.SetHorizontalAlign(const Value: THorizontalAlign);
begin
  IAlign.Horizontal:=Value;
  SetTextAlignments;
end;

procedure TGDIPainter.SetStroke(const AStroke: TStroke);

  function PenStyle(const AStyle:TStrokeStyle):TPenStyle;
  begin
    case AStyle of
      TStrokeStyle.Solid: result:=TPenStyle.psSolid;
       TStrokeStyle.Dash: result:=TPenStyle.psDash;
        TStrokeStyle.Dot: result:=TPenStyle.psDot;
    TStrokeStyle.DashDot: result:=TPenStyle.psDashDot;
 TStrokeStyle.DashDotDot: result:=TPenStyle.psDashDotDot;
    else
       result:=TPenStyle.{$IFDEF FPC}psPattern{$ELSE}psUserStyle{$ENDIF};
    end;
  end;

begin
  if AStroke.Visible then
  begin
    ICanvas.Pen.Color:=TColorHelper.RemoveOpacity(AStroke.Color);
    ICanvas.Pen.Width:=Round(AStroke.Size);
    ICanvas.Pen.Style:=PenStyle(AStroke.Style);
  end
  else
    ICanvas.Pen.Style:=TPenStyle.psClear;
end;

procedure TGDIPainter.SetVerticalAlign(const Value: TVerticalAlign);
begin
  IAlign.Vertical:=Value;
  SetTextAlignments;
end;

procedure TGDIPainter.TextOut(const X, Y: Single; const AText: String);
begin
  ICanvas.Brush.Style:=bsClear;
  ICanvas.TextOut(Round(X),Round(Y),AText);
end;

function TGDIPainter.TextHeight(const AText:String):Single;
begin
  result:=ICanvas.TextHeight(AText);
end;

function TGDIPainter.TextWidth(const AText: String): Single;
begin
  result:=ICanvas.TextWidth(AText);
end;

procedure TGDIPainter.VerticalLine(const X, Y0, Y1: Single);
var tmpX : Integer;
begin
  tmpX:=Round(X);
  ICanvas.MoveTo(tmpX,Round(Y0));
  ICanvas.LineTo(tmpX,Round(Y1));
end;

procedure TGDIPainter.UnClip;
var L : Integer;
begin
  L:=Length(IClipHistory);

  if L>0 then
  begin
    Dec(L);

    SelectClipRgn(IDC,IClipHistory[L]);
    DeleteObject(IClipHistory[L]);
    SetLength(IClipHistory,L);
  end
  else
    SelectClipRgn(IDC,0)
end;

{$IFNDEF FPC}
{$IF CompilerVersion<=22}
type
  TVCLFontStyles=Graphics.TFontStyles;
  TTeeFontStyle=TFontStyle;

function StylesFrom(const AStyle:TFontStyles):TVCLFontStyles;
begin
  if TTeeFontStyle.fsBold in AStyle then
     result:=[fsBold]
  else
     result:=[];

  if TTeeFontStyle.fsItalic in AStyle then
     result:=result+[fsItalic];

  if TTeeFontStyle.fsUnderline in AStyle then
     result:=result+[fsUnderline];

  if TTeeFontStyle.fsStrikeOut in AStyle then
     result:=result+[fsStrikeOut];
end;
{$IFEND}
{$ENDIF}

{
  ICanvas.Font.Name:=AFont.Name;
  ICanvas.Font.Color:=TColorHelper.RemoveOpacity(AFont.Color);
  ICanvas.Font.Size:=Round(64*AFont.Size/96);
  ICanvas.Font.Style:=Graphics.TFontStyles(AFont.Style);
}

class procedure TGDIPainter.ApplyFont(const ASource: TTeeFont;
  const ADest: Graphics.TFont);
begin
  ADest.Name:=ASource.Name;
  ADest.Size:=Round(64*ASource.Size/96);

  {$IFDEF FPC}
  ADest.Style:=ASource.Style;
  {$ELSE}

  {$IF CompilerVersion<=22}
  ADest.Style:=StylesFrom(ASource.Style);
  {$ELSE}
  ADest.Style:=ASource.Style;
  {$IFEND}

  {$ENDIF}

  ADest.Color:=ASource.Color;
end;

procedure TGDIPainter.Clip(const R: TRect);
var L : Integer;
    OldRegion,
    Region : HRGN;
begin
//  LPToDP(IDC,R,2);

  with R do
       Region:=CreateRectRgn(Left,Top,Right,Bottom);

  OldRegion:=CreateRectRgn(0,0,0,0);

  if GetClipRgn(IDC,OldRegion)=1 then
  begin
    L:=Length(IClipHistory);
    SetLength(IClipHistory,L+1);
    IClipHistory[L]:=OldRegion;
  end
  else
    DeleteObject(OldRegion);

  ExtSelectClipRgn(IDC,Region,RGN_AND);
  DeleteObject(Region);
end;

procedure TGDIPainter.Clip(const R: TRectF);
begin
  Clip(R.Round);
end;

procedure TGDIPainter.Draw(const P:TPointsF);
begin
  ICanvas.Brush.Style:=bsClear;
  ICanvas.Polygon(RoundPoints(P));
end;

procedure SetDefaultFontNameAndSize;
begin
  Tee.Format.TFont.DefaultName:='Tahoma'; //'Segoe UI';
  Tee.Format.TFont.DefaultSize:=12;
end;

function TGDIPainter.GraphicOf(const APicture: TPicture):TGraphic;
var tmp : TObject;
begin
  tmp:=TPictureAccess(APicture).TagObject;

  if tmp is Graphics.TPicture then
     result:=Graphics.TPicture(tmp).Graphic
  else
  if tmp is Graphics.TGraphic then
     result:=Graphics.TGraphic(tmp)
  else
     result:=nil;
end;

procedure TGDIPainter.Draw(const APicture: TPicture; const X, Y: Single);
var tmp : TGraphic;
begin
  tmp:=GraphicOf(APicture);

  if tmp<>nil then
     ICanvas.Draw(Round(X),Round(Y),tmp);
end;

procedure TGDIPainter.Draw(const APicture: TPicture; const R: TRectF);
var tmp : TGraphic;
begin
  tmp:=GraphicOf(APicture);

  if tmp<>nil then
     ICanvas.StretchDraw(R.Round,tmp);
end;

procedure TGDIPainter.DrawEllipse(const R: TRectF);
var Old : TBrushStyle;
begin
  Old:=ICanvas.Brush.Style;

  ICanvas.Brush.Style:=bsClear;
  ICanvas.Ellipse(R.Round);

  ICanvas.Brush.Style:=Old;
end;

procedure TGDIPainter.Fill(const R: TRectF; const AColor: Tee.Format.TColor);
begin
  ICanvas.Brush.Style:=TBrushStyle.bsSolid;
  ICanvas.Brush.Color:=TColorHelper.RemoveOpacity(AColor);

  Fill(R);

  HideBrush;
end;

{ TGDIPainter.TAlign }

function TGDIPainter.TAlign.Flags: Cardinal;
begin
  case Horizontal of
    THorizontalAlign.Left: result:=TA_LEFT;
  THorizontalAlign.Center: result:=TA_CENTER;
  else
   {THorizontalAlign.Right:} result:=TA_RIGHT;
  end;

  case Vertical of
       TVerticalAlign.Top,
       TVerticalAlign.Center: result:=result+TA_TOP;
    TVerticalAlign.Bottom: result:=result+TA_BOTTOM;
  end;
end;

initialization
  SetDefaultFontNameAndSize;
end.
