{*********************************************}
{  TeeGrid Software Library                   }
{  VCL TTextFormat Editor                     }
{  Copyright (c) 2016 by Steema Software      }
{  All Rights Reserved                        }
{*********************************************}
unit VCLTee.Editor.Format.Text;
{$I Tee.inc}

interface

uses
  {Winapi.}Windows, {Winapi.}Messages, {System.}SysUtils, {System.}Classes, {Vcl.}Graphics,
  {Vcl.}Controls, {Vcl.}Forms, {Vcl.}Dialogs, {Vcl.}ComCtrls, {Vcl.}StdCtrls, Tee.Format,
  {Vcl.}ExtCtrls,
  {$IFDEF FPC}
  ColorBox,
  {$ENDIF}

  VCLTee.Editor.Stroke, VCLTee.Editor.Brush, VCLTee.Editor.Font;

type
  TTextFormatEditor = class(TForm)
    PageFormat: TPageControl;
    TabFont: TTabSheet;
    TabStroke: TTabSheet;
    TabBrush: TTabSheet;
    procedure FormCreate(Sender: TObject);
    procedure PageFormatChange(Sender: TObject);
  private
    { Private declarations }

    IFormat : TTextFormat;

    IFont : TFontEditor;
    IBrush : TBrushEditor;
    IStroke : TStrokeEditor;
  public
    { Public declarations }

    procedure RefreshFormat(const AFormat:TTextFormat);

    class function Edit(const AOwner:TComponent; const AFormat:TTextFormat):Boolean; static;

    class function Embedd(const AOwner:TComponent; const AParent:TWinControl;
                          const AFormat:TTextFormat):TTextFormatEditor; static;
  end;

implementation

{$R *.dfm}

{$IFNDEF NOUITYPES}
uses
  System.UITypes;
{$ENDIF}

{ TFormatEditor }

class function TTextFormatEditor.Edit(const AOwner: TComponent;
  const AFormat: TTextFormat): Boolean;
begin
  with TTextFormatEditor.Create(AOwner) do
  try
    RefreshFormat(AFormat);
    result:=ShowModal=mrOk;
  finally
    Free;
  end;
end;

class function TTextFormatEditor.Embedd(const AOwner: TComponent;
  const AParent: TWinControl; const AFormat: TTextFormat): TTextFormatEditor;
begin
  result:=TTextFormatEditor.Create(AOwner);
  result.RefreshFormat(AFormat);
  TStrokeEditor.AddForm(result,AParent);
end;

procedure TTextFormatEditor.FormCreate(Sender: TObject);
begin
  PageFormat.ActivePage:=TabFont;
end;

procedure TTextFormatEditor.PageFormatChange(Sender: TObject);
begin
  if IFormat=nil then
     Exit;

  if PageFormat.ActivePage=TabStroke then
  begin
    if IStroke=nil then
       IStroke:=TStrokeEditor.Embedd(Self,TabStroke,IFormat.Stroke);
  end
  else
  if PageFormat.ActivePage=TabBrush then
  begin
    if IBrush=nil then
       IBrush:=TBrushEditor.Embedd(Self,TabBrush,IFormat.Brush);
  end
  else
  if PageFormat.ActivePage=TabFont then
  begin
    if IFont=nil then
       IFont:=TFontEditor.Embedd(Self,TabFont,IFormat.Font);
  end;
end;

procedure TTextFormatEditor.RefreshFormat(const AFormat: TTextFormat);
begin
  IFormat:=AFormat;

  if IBrush<>nil then
     IBrush.RefreshBrush(IFormat.Brush);

  if IFont<>nil then
     IFont.RefreshFont(IFormat.Font);

  if IStroke<>nil then
     IStroke.RefreshStroke(IFormat.Stroke);

  PageFormatChange(Self);
end;

end.
