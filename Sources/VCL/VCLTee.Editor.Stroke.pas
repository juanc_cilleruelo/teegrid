{*********************************************}
{  TeeGrid Software Library                   }
{  VCL TStroke Editor                         }
{  Copyright (c) 2016 by Steema Software      }
{  All Rights Reserved                        }
{*********************************************}
unit VCLTee.Editor.Stroke;
{$I Tee.inc}

interface

uses
  {Winapi.}Windows, {Winapi.}Messages, {System.}SysUtils, {System.}Classes, {Vcl.}Graphics,
  {Vcl.}Controls, {Vcl.}Forms, {Vcl.}Dialogs, {Vcl.}StdCtrls, {Vcl.}ExtCtrls, {Vcl.}ComCtrls,
  {$IFDEF FPC}
  ColorBox,
  {$ENDIF}

  VCLTee.Editor.Brush,

  Tee.Format;

type
  TStrokeEditor = class(TForm)
    PageStroke: TPageControl;
    TabPen: TTabSheet;
    TabBrush: TTabSheet;
    Label3: TLabel;
    CBVisible: TCheckBox;
    TBSize: TTrackBar;
    CBColor: TColorBox;
    LSize: TLabel;
    TabStyle: TTabSheet;
    Label4: TLabel;
    LBStyle: TListBox;
    Label1: TLabel;
    LBEndStyle: TListBox;
    Label2: TLabel;
    LBJoinStyle: TListBox;
    procedure CBColorChange(Sender: TObject);
    procedure CBVisibleClick(Sender: TObject);
    procedure LBStyleClick(Sender: TObject);
    procedure TBSizeChange(Sender: TObject);
    procedure PageStrokeChange(Sender: TObject);
    procedure LBEndStyleClick(Sender: TObject);
    procedure LBJoinStyleClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }

    IBrush : TBrushEditor;
    IStroke : TStroke;

    procedure ChangedColor(Sender: TObject);
    procedure SetLabelSize;
  public
    { Public declarations }

    class procedure AddForm(const AForm: TCustomForm; const AParent: TWinControl); static;

    procedure RefreshStroke(const AStroke:TStroke);

    class function Edit(const AOwner:TComponent; const AStroke:TStroke):Boolean; static;

    class function Embedd(const AOwner:TComponent; const AParent:TWinControl;
                          const AStroke:TStroke):TStrokeEditor; static;
  end;

implementation

{$R *.dfm}

{$IFNDEF NOUITYPES}
uses
  {System.}UITypes;
{$ENDIF}

// (Duplicated from BI.VCL.Grid.TUICommon)
class procedure TStrokeEditor.AddForm(const AForm: TCustomForm; const AParent: TWinControl);
begin
  AForm.Align:=alClient;
  AForm.Parent:=AParent;
  AForm.BorderStyle:=bsNone;
  AForm.Show;
end;

procedure TStrokeEditor.CBColorChange(Sender: TObject);
begin
  IStroke.Color:=CBColor.Selected;
end;

procedure TStrokeEditor.CBVisibleClick(Sender: TObject);
begin
  IStroke.Visible:=CBVisible.Checked;
end;

class function TStrokeEditor.Edit(const AOwner: TComponent;
  const AStroke: TStroke): Boolean;
begin
  with TStrokeEditor.Create(AOwner) do
  try
    RefreshStroke(AStroke);
    result:=ShowModal=mrOk;
  finally
    Free;
  end;
end;

class function TStrokeEditor.Embedd(const AOwner: TComponent;
  const AParent: TWinControl; const AStroke: TStroke): TStrokeEditor;
begin
  result:=TStrokeEditor.Create(AOwner);
  result.RefreshStroke(AStroke);
  TStrokeEditor.AddForm(result,AParent);
end;

procedure TStrokeEditor.FormCreate(Sender: TObject);
begin
  PageStroke.ActivePage:=TabPen;
end;

procedure TStrokeEditor.LBEndStyleClick(Sender: TObject);
begin
  IStroke.EndStyle:=TStrokeEnd(LBEndStyle.ItemIndex);
end;

procedure TStrokeEditor.LBJoinStyleClick(Sender: TObject);
begin
  IStroke.JoinStyle:=TStrokeJoin(LBJoinStyle.ItemIndex);
end;

procedure TStrokeEditor.LBStyleClick(Sender: TObject);
begin
  IStroke.Style:=TStrokeStyle(LBStyle.ItemIndex);
end;

procedure TStrokeEditor.ChangedColor(Sender: TObject);
begin
  CBColor.Selected:=Graphics.TColor(IStroke.Color);
end;

procedure TStrokeEditor.PageStrokeChange(Sender: TObject);
begin
  if IStroke=nil then
     Exit;

  if PageStroke.ActivePage=TabBrush then
  begin
    if IBrush=nil then
    begin
      IBrush:=TBrushEditor.Embedd(Self,TabBrush,IStroke.Brush);
      IBrush.PanelTop.Hide;

      IBrush.OnColorChange:=ChangedColor;
    end;
  end;
end;

procedure TStrokeEditor.RefreshStroke(const AStroke: TStroke);
begin
  IStroke:=AStroke;

  TBSize.Position:=Round(IStroke.Size);
  SetLabelSize;

  CBColor.Selected:=IStroke.Color;

  LBStyle.ItemIndex:=Ord(IStroke.Style);
  LBEndStyle.ItemIndex:=Ord(IStroke.EndStyle);
  LBJoinStyle.ItemIndex:=Ord(IStroke.JoinStyle);

  CBVisible.Checked:=IStroke.Visible;
end;

procedure TStrokeEditor.SetLabelSize;
begin
  LSize.Caption:=IntToStr(TBSize.Position);
end;

procedure TStrokeEditor.TBSizeChange(Sender: TObject);
begin
  IStroke.Size:=TBSize.Position;
  SetLabelSize;
end;

end.
