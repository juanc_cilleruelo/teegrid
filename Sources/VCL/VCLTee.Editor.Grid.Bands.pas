unit VCLTee.Editor.Grid.Bands;

interface

uses
  {Winapi.}Windows, {Winapi.}Messages, {System.}SysUtils, {System.}Classes,
  {Vcl.}Graphics, {Vcl.}Controls, {Vcl.}Forms, {Vcl.}Dialogs, {Vcl.}StdCtrls,
  {Vcl.}ExtCtrls, {Vcl.}ComCtrls, {Vcl.}Buttons,
  Tee.Grid.Bands;

type
  TGridBandsEditor = class(TForm)
    PanelTop: TPanel;
    CBVisible: TCheckBox;
    LBBands: TListBox;
    PanelMain: TPanel;
    PanelBandTop: TPanel;
    CBBandVisible: TCheckBox;
    SBUp: TSpeedButton;
    SBDown: TSpeedButton;
    procedure LBBandsClick(Sender: TObject);
    procedure CBVisibleClick(Sender: TObject);
    procedure CBBandVisibleClick(Sender: TObject);
    procedure SBUpClick(Sender: TObject);
    procedure SBDownClick(Sender: TObject);
  private
    { Private declarations }

    IBands : TGridBands;
    IEditor : TCustomForm;

    function Current:TGridBand;
    procedure MoveBand(const Delta:Integer);
    procedure RefreshUpDown;
  public
    { Public declarations }

    procedure RefreshBands(const ABands:TGridBands);

    class function Edit(const AOwner:TComponent; const ABands:TGridBands):Boolean; static;

    class function Embedd(const AOwner:TComponent; const AParent:TWinControl;
                          const ABands:TGridBands):TGridBandsEditor; static;
  end;

implementation

{$R *.dfm}

uses
  VCLTee.Editor.Stroke, VCLTee.Editor.Render.Text, Tee.Renders,
  VCLTee.Editor.Header, Tee.Grid.Header, VCLTee.Editor.Grid.Band.Text,
  Tee.Grid.Totals,
  VCLTee.Editor.Grid.Band.Totals;

{ TGridBandsEditor }

procedure TGridBandsEditor.CBBandVisibleClick(Sender: TObject);
begin
  Current.Visible:=CBBandVisible.Checked;
end;

procedure TGridBandsEditor.CBVisibleClick(Sender: TObject);
begin
  IBands.Visible:=CBVisible.Checked;
end;

class function TGridBandsEditor.Edit(const AOwner: TComponent;
  const ABands: TGridBands): Boolean;
begin
  with TGridBandsEditor.Create(AOwner) do
  try
    RefreshBands(ABands);
    result:=ShowModal=mrOk;
  finally
    Free;
  end;
end;

class function TGridBandsEditor.Embedd(const AOwner: TComponent;
  const AParent: TWinControl; const ABands: TGridBands): TGridBandsEditor;
begin
  result:=TGridBandsEditor.Create(AOwner);
  result.RefreshBands(ABands);
  TStrokeEditor.AddForm(result,AParent);
end;

function TGridBandsEditor.Current:TGridBand;
begin
  if LBBands.ItemIndex>-1 then
     result:=TGridBand(LBBands.Items.Objects[LBBands.ItemIndex])
  else
     result:=nil;
end;

procedure TGridBandsEditor.LBBandsClick(Sender: TObject);
var tmp : TGridBand;
begin
  tmp:=Current;

  if tmp<>nil then
  begin
    CBBandVisible.Checked:=tmp.Visible;

    IEditor.Free;
    IEditor:=nil;

    if tmp is TColumnHeaderBand then
       IEditor:=THeaderEditor.Embedd(Self,PanelMain,tmp as TColumnHeaderBand)
    else
    if tmp is TTextBand then
       IEditor:=TTextBandEditor.Embedd(Self,PanelMain,tmp as TTextBand)
    else
    if tmp is TColumnTotals then
       IEditor:=TColumnTotalsEditor.Embedd(Self,PanelMain,tmp as TColumnTotals)
    else
//    if tmp is TTotalsHeader then
//       IEditor:=TTotalsHeaderEditor.Embedd(Self,PanelMain,tmp as TTotalsHeader)
//    else
    if tmp.HasRender and (tmp.Render is TTextRender) then
       IEditor:=TTextRenderEditor.Embedd(Self,PanelMain,tmp.Render as TTextRender)
  end;

  RefreshUpDown;
end;

procedure TGridBandsEditor.RefreshUpDown;
var tmp : TGridBand;
begin
  tmp:=Current;

  SBUp.Enabled:=(tmp<>nil) and (tmp.Index>0);
  SBDown.Enabled:=(tmp<>nil) and (tmp.Index<IBands.Count-1);
end;

procedure TGridBandsEditor.RefreshBands(const ABands: TGridBands);

  procedure AddBands;
  var t : Integer;
  begin
    LBBands.Items.BeginUpdate;
    try
      LBBands.Clear;

      for t:=0 to IBands.Count-1 do
          LBBands.Items.AddObject(IBands[t].Description,IBands[t]);
    finally
      LBBands.Items.EndUpdate;
    end;
  end;

begin
  IBands:=ABands;

  CBVisible.Checked:=IBands.Visible;

  AddBands;

  if LBBands.Count>0 then
  begin
    LBBands.ItemIndex:=0;
    LBBandsClick(Self);
  end;
end;

procedure TGridBandsEditor.MoveBand(const Delta:Integer);
var tmp : Integer;
begin
  tmp:=Current.Index;
  Current.Index:=tmp+Delta;

  LBBands.Items.Exchange(tmp,tmp+Delta);

  RefreshUpDown;
end;

procedure TGridBandsEditor.SBDownClick(Sender: TObject);
begin
  MoveBand(1);
end;

procedure TGridBandsEditor.SBUpClick(Sender: TObject);
begin
  MoveBand(-1);
end;

end.
