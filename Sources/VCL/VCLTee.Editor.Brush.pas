{*********************************************}
{  TeeGrid Software Library                   }
{  VCL TBrush Editor                          }
{  Copyright (c) 2016 by Steema Software      }
{  All Rights Reserved                        }
{*********************************************}
unit VCLTee.Editor.Brush;

interface

uses
  {Winapi.}Windows, {Winapi.}Messages, {System.}SysUtils, {System.}Classes, {Vcl.}Graphics,
  {$IFDEF FPC}
  ColorBox,
  {$ENDIF}

  {Vcl.}Controls, {Vcl.}Forms, {Vcl.}Dialogs, {Vcl.}StdCtrls, {Vcl.}ExtCtrls,
  {Vcl.}ComCtrls, {Vcl.}ExtDlgs,

  VCLTee.Editor.Gradient,

  // Must be last unit in uses:
  Tee.Format;

type
  TBrushEditor = class(TForm)
    PageBrush: TPageControl;
    PanelTop: TPanel;
    CBVisible: TCheckBox;
    TabSolid: TTabSheet;
    TabGradient: TTabSheet;
    TabPicture: TTabSheet;
    CBColor: TColorBox;
    Label1: TLabel;
    TBOpacity: TTrackBar;
    LOpacity: TLabel;
    Image1: TImage;
    BClearPicture: TButton;
    Shape1: TShape;
    Button1: TButton;
    OpenPictureDialog1: TOpenPictureDialog;
    LPictureSize: TLabel;
    LPictureType: TLabel;
    procedure CBVisibleClick(Sender: TObject);
    procedure CBColorChange(Sender: TObject);
    procedure PageBrushChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure TBOpacityChange(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure BClearPictureClick(Sender: TObject);
  private
    { Private declarations }

    IBrush : TBrush;
    IGradient : TGradientEditor;

    FOnColorChange : TNotifyEvent;

    procedure ChangeColor;
    procedure RefreshPicture;
    procedure SetOpacityLabel;
  public
    { Public declarations }

    procedure RefreshBrush(const ABrush:TBrush);
    procedure RefreshColor;

    class function Edit(const AOwner:TComponent; const ABrush:TBrush):Boolean; static;

    class function Embedd(const AOwner:TComponent; const AParent:TWinControl;
                          const ABrush:TBrush):TBrushEditor; static;

    property OnColorChange:TNotifyEvent read FOnColorChange write FOnColorChange;
  end;

implementation

{$R *.dfm}

uses
  VCLTee.Editor.Stroke, VCLTee.Picture;

function SizeOfPicture(const APicture:Graphics.TPicture):String;
begin
  result:=IntToStr(APicture.Width)+' x '+IntToStr(APicture.Height);
end;

type
  TBrushAccess=class(TBrush);
  TPictureAccess=class(TPicture);

procedure TBrushEditor.RefreshPicture;
begin
  BClearPicture.Enabled:=TBrushAccess(IBrush).HasPicture;

  if BClearPicture.Enabled then
  begin
    Image1.Picture:=TPictureAccess(IBrush.Picture).TagObject as Graphics.TPicture;

    LPictureSize.Caption:=SizeOfPicture(Image1.Picture);
    LPictureType.Caption:=TVCLPicture.TypeOfGraphic(Image1.Picture.Graphic);
  end
  else
    Image1.Picture:=nil;

  LPictureSize.Visible:=BClearPicture.Enabled;
  LPictureType.Visible:=LPictureSize.Visible;
end;

procedure TBrushEditor.BClearPictureClick(Sender: TObject);
begin
  IBrush.Picture.Clear;
  RefreshPicture;
end;

procedure TBrushEditor.Button1Click(Sender: TObject);
begin
  if OpenPictureDialog1.Execute then
  begin
    TVCLPicture.Load(IBrush.Picture,OpenPictureDialog1.FileName);
    RefreshPicture;
  end;
end;

procedure TBrushEditor.CBColorChange(Sender: TObject);
begin
  ChangeColor;
end;

procedure TBrushEditor.CBVisibleClick(Sender: TObject);
begin
  IBrush.Visible:=CBVisible.Checked;
end;

class function TBrushEditor.Edit(const AOwner: TComponent;
  const ABrush: TBrush): Boolean;
begin
  with TBrushEditor.Create(AOwner) do
  try
    RefreshBrush(ABrush);
    result:=ShowModal=mrOk;
  finally
    Free;
  end;
end;

class function TBrushEditor.Embedd(const AOwner: TComponent;
  const AParent: TWinControl; const ABrush: TBrush): TBrushEditor;
begin
  result:=TBrushEditor.Create(AOwner);
  result.RefreshBrush(ABrush);
  TStrokeEditor.AddForm(result,AParent);
end;

procedure TBrushEditor.FormShow(Sender: TObject);
begin
  if TBrushAccess(IBrush).HasPicture then
     PageBrush.ActivePage:=TabPicture
  else
  if TBrushAccess(IBrush).HasGradient then
     PageBrush.ActivePage:=TabGradient
  else
     PageBrush.ActivePage:=TabSolid;

  PageBrushChange(Self);
end;

procedure TBrushEditor.PageBrushChange(Sender: TObject);
begin
  if IBrush=nil then
     Exit;

  if PageBrush.ActivePage=TabGradient then
  begin
    if IGradient=nil then
       IGradient:=TGradientEditor.Embedd(Self,TabGradient,IBrush.Gradient);
  end;
end;

procedure TBrushEditor.SetOpacityLabel;
begin
  LOpacity.Caption:=IntToStr(TBOpacity.Position)+'%';
end;

procedure TBrushEditor.RefreshColor;
var tmp : Single;
begin
  CBColor.Selected:=TColorHelper.Split(IBrush.Color,tmp);

  TBOpacity.Position:=Round(tmp*100);
  SetOpacityLabel;
end;

procedure TBrushEditor.RefreshBrush(const ABrush:TBrush);
begin
  IBrush:=ABrush;

  CBVisible.Checked:=IBrush.Visible;

  RefreshColor;

  if IGradient<>nil then
     IGradient.RefreshGradient(IBrush.Gradient);

  RefreshPicture;
end;

procedure TBrushEditor.ChangeColor;
begin
  IBrush.Color:=TColorHelper.From(CBColor.Selected,TBOpacity.Position*0.01);

  if Assigned(FOnColorChange) then
     FOnColorChange(Self);
end;

procedure TBrushEditor.TBOpacityChange(Sender: TObject);
begin
  ChangeColor;
  SetOpacityLabel;
end;

end.
