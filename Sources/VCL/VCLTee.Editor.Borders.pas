{*********************************************}
{  TeeGrid Software Library                   }
{  VCL TBorders Editor                        }
{  Copyright (c) 2016 by Steema Software      }
{  All Rights Reserved                        }
{*********************************************}
unit VCLTee.Editor.Borders;

interface

uses
  {Winapi.}Windows, {Winapi.}Messages, {System.}SysUtils, {System.}Classes, {Vcl.}Graphics,
  {Vcl.}Controls, {Vcl.}Forms, {Vcl.}Dialogs, {Vcl.}ComCtrls,
  VCLTee.Editor.Stroke, Tee.Format, Tee.Renders;

type
  TBordersEditor = class(TForm)
    PageBorders: TPageControl;
    TabLeft: TTabSheet;
    TabTop: TTabSheet;
    TabRight: TTabSheet;
    TabBottom: TTabSheet;
    procedure PageBordersChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }

    IBorders : TBorders;

    ILeft,
    ITop,
    IRight,
    IBottom : TStrokeEditor;
  public
    { Public declarations }

    procedure RefreshBorders(const ABorders:TBorders);

    class function Edit(const AOwner:TComponent; const ABorders:TBorders):Boolean; static;

    class function Embedd(const AOwner:TComponent; const AParent:TWinControl;
                          const ABorders:TBorders):TBordersEditor; static;
  end;

implementation

{$R *.dfm}

class function TBordersEditor.Edit(const AOwner: TComponent;
  const ABorders: TBorders): Boolean;
begin
  with TBordersEditor.Create(AOwner) do
  try
    RefreshBorders(ABorders);
    result:=ShowModal=mrOk;
  finally
    Free;
  end;
end;

class function TBordersEditor.Embedd(const AOwner: TComponent;
  const AParent: TWinControl; const ABorders: TBorders): TBordersEditor;
begin
  result:=TBordersEditor.Create(AOwner);
  result.RefreshBorders(ABorders);
  TStrokeEditor.AddForm(result,AParent);
end;

procedure TBordersEditor.FormCreate(Sender: TObject);
begin
  PageBorders.ActivePage:=TabLeft;
end;

procedure TBordersEditor.PageBordersChange(Sender: TObject);

  procedure TryShow(var AEditor:TStrokeEditor; const AParent:TWinControl;
       const ABorder:TBorder);
  begin
    if AEditor=nil then
       AEditor:=TStrokeEditor.Embedd(Self,AParent,ABorder);
  end;

begin
  if IBorders=nil then
     Exit;

  if PageBorders.ActivePage=TabLeft then
     TryShow(ILeft,TabLeft,IBorders.Left)
  else
  if PageBorders.ActivePage=TabTop then
     TryShow(ITop,TabTop,IBorders.Top)
  else
  if PageBorders.ActivePage=TabRight then
     TryShow(IRight,TabRight,IBorders.Right)
  else
  if PageBorders.ActivePage=TabBottom then
     TryShow(IBottom,TabBottom,IBorders.Bottom)
  else
end;

procedure TBordersEditor.RefreshBorders(const ABorders: TBorders);
begin
  IBorders:=ABorders;
  PageBordersChange(Self);
end;

end.
