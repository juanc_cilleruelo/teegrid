{*********************************************}
{  TeeGrid Software Library                   }
{  VCL TTextRender Editor                     }
{  Copyright (c) 2016 by Steema Software      }
{  All Rights Reserved                        }
{*********************************************}
unit VCLTee.Editor.Render.Text;

interface

uses
  {Winapi.}Windows, {Winapi.}Messages, {System.}SysUtils, {System.}Classes, {Vcl.}Graphics,
  {Vcl.}Controls, {Vcl.}Forms, {Vcl.}Dialogs, {Vcl.}ComCtrls, {Vcl.}StdCtrls, {Vcl.}ExtCtrls,

  VCLTee.Editor.Format.Text, VCLTee.Editor.Borders, VCLTee.Editor.Margins,
  Tee.Renders;

type
  TTextRenderEditor = class(TForm)
    PageSelected: TPageControl;
    TabFormat: TTabSheet;
    TabBorders: TTabSheet;
    TabMargins: TTabSheet;
    TabAlign: TTabSheet;
    RGVerticalAlign: TRadioGroup;
    RGHorizAlign: TRadioGroup;
    procedure PageSelectedChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure RGHorizAlignClick(Sender: TObject);
    procedure RGVerticalAlignClick(Sender: TObject);
  private
    { Private declarations }

    IText : TTextRender;

    IBorders : TBordersEditor;
    IFormat : TTextFormatEditor;
    IMargins : TMarginsEditor;
  public
    { Public declarations }

    procedure RefreshTextRender(const ATextRender:TTextRender);

    class function Edit(const AOwner:TComponent; const ATextRender:TTextRender):Boolean; static;

    class function Embedd(const AOwner:TComponent; const AParent:TWinControl;
                          const ATextRender:TTextRender):TTextRenderEditor; static;
  end;

implementation

{$R *.dfm}

uses
  VCLTee.Editor.Stroke, Tee.Painter;

class function TTextRenderEditor.Edit(const AOwner: TComponent;
  const ATextRender: TTextRender): Boolean;
begin
  with TTextRenderEditor.Create(AOwner) do
  try
    RefreshTextRender(ATextRender);
    result:=ShowModal=mrOk;
  finally
    Free;
  end;
end;

class function TTextRenderEditor.Embedd(const AOwner: TComponent;
  const AParent: TWinControl;
  const ATextRender: TTextRender): TTextRenderEditor;
begin
  result:=TTextRenderEditor.Create(AOwner);
  result.RefreshTextRender(ATextRender);
  TStrokeEditor.AddForm(result,AParent);
end;

procedure TTextRenderEditor.FormCreate(Sender: TObject);
begin
  PageSelected.ActivePage:=TabFormat;
end;

procedure TTextRenderEditor.PageSelectedChange(Sender: TObject);
begin
  if IText=nil then
     Exit;

  if PageSelected.ActivePage=TabFormat then
  begin
    if IFormat=nil then
       IFormat:=TTextFormatEditor.Embedd(Self,TabFormat,IText.Format);
  end
  else
  if PageSelected.ActivePage=TabBorders then
  begin
    if IBorders=nil then
       IBorders:=TBordersEditor.Embedd(Self,TabBorders,IText.Borders);
  end
  else
  if PageSelected.ActivePage=TabMargins then
  begin
    if IMargins=nil then
       IMargins:=TMarginsEditor.Embedd(Self,TabMargins,IText.Margins);
  end;
end;

procedure TTextRenderEditor.RefreshTextRender(const ATextRender: TTextRender);
begin
  IText:=ATextRender;

  RGHorizAlign.ItemIndex:=Ord(IText.TextAlign.Horizontal);
  RGVerticalAlign.ItemIndex:=Ord(IText.TextAlign.Vertical);

  PageSelectedChange(Self);
end;

procedure TTextRenderEditor.RGHorizAlignClick(Sender: TObject);
begin
  IText.TextAlign.Horizontal:=THorizontalAlign(RGHorizAlign.ItemIndex);
end;

procedure TTextRenderEditor.RGVerticalAlignClick(Sender: TObject);
begin
  IText.TextAlign.Vertical:=TVerticalAlign(RGVerticalAlign.ItemIndex);
end;

end.
