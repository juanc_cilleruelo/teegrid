{*********************************************}
{  TeeGrid Software Library                   }
{  Ticker Component                           }
{  Copyright (c) 2016 by Steema Software      }
{  All Rights Reserved                        }
{*********************************************}
unit Tee.Grid.Ticker;
{$I Tee.inc}

interface

{
  Helper class to implement a "ticker", automatic repaint of grid cells
  with changing background cell color when a cell value is changed.

  Usage:

  1) Create a Ticker

  var Ticker : TGridTicker;

    Ticker:=TGridTicker.Create(TeeGrid1.Grid.Current);

  2) After a cell value is changed, update ticker:

    Ticker.Change(Col,Row,OldValue);


  Options:

    Enabled or paused:

      Ticker.Enabled:= True;

    Colors:

      Ticker.Higher.Color:= TColors.Green;
      Ticker.Lower.Color:= TColors.Red;

    Fade effect:

      Ticker.FadeColors:= True;

    Delay:

      Ticker.Delay:= 2000; // milliseconds

    Speed for internal thread:

      Ticker.RefreshSpeed:= 30;  // milliseconds

}

uses
  {System.}Classes,

  {$IFNDEF NOUITYPES}
  System.UITypes,
  {$ENDIF}

  Tee.Grid.Data, Tee.Grid.Columns, Tee.Grid.RowGroup, Tee.Renders, Tee.Format;

type
  TTickerCell=record
  private
    function Color(const OldColor,NewColor:TColor):TColor;
    procedure Init(const AOld,ANew,ASpeed:Double);
    function Update(const NewValue:Double):Boolean;
  public
    Enabled : Boolean;
    Delta,
    Range,
    Old,
    Value : Double;
  end;

  TGridTicker=class;

  TTickerThread=class(TThread)
  private
    Ticker : TGridTicker;
  protected
    procedure Execute; override;
  end;

  TGridTicker=class(TPersistentChange)
  private
    FDelay : Integer;
    FEnabled : Boolean;
    FFadeColors : Boolean;
    FGroup : TRowGroup;
    FHigherBrush : TBrush;
    FLowerBrush : TBrush;
    FRefreshSpeed : Integer;

    FValues : Array of Array of TTickerCell;

    ICount : Integer;
    IThread : TTickerThread;

    procedure CheckSize(const ACol, ARow: Integer);

    procedure ColumnPaint(const Sender:TColumn; var AData:TRenderData;
                          var DefaultPaint:Boolean);

    procedure ColumnPaintEvent;
    procedure DoChanged(Sender:TObject);
    procedure ProcessThread;
    procedure SetHigherBrush(const Value: TBrush);
    procedure SetLowerBrush(const Value: TBrush);
    function ValueAt(const AColumn:TColumn; const ARow:Integer):Double;
  public
    Constructor Create(const AGroup:TRowGroup); reintroduce;
    {$IFNDEF AUTOREFCOUNT}
    Destructor Destroy; override;
    {$ENDIF}

    procedure Change(const ACol,ARow:Integer; const AOldValue:Double);
    procedure Paint(const AColumn:TColumn; var AData:TRenderData);
    procedure Resize;
  published
    property Delay:Integer read FDelay write FDelay default 1000;
    property Enabled:Boolean read FEnabled write FEnabled default True;
    property FadeColors:Boolean read FFadeColors write FFadeColors default True;
    property Higher:TBrush read FHigherBrush write SetHigherBrush;
    property Lower:TBrush read FLowerBrush write SetLowerBrush;
    property RefreshSpeed:Integer read FRefreshSpeed write FRefreshSpeed default 10;
  end;

implementation

uses
  {System.}SysUtils;

{$IFDEF FPC}
{$DEFINE THREADPARAM}
{$ELSE}
{$IF CompilerVersion<22}
{$DEFINE THREADPARAM}
{$IFEND}
{$ENDIF}

{ TGridTicker }

Constructor TGridTicker.Create(const AGroup: TRowGroup);
begin
  inherited Create(DoChanged);

  FRefreshSpeed:=10;
  FFadeColors:=True;
  FDelay:=1000;
  FEnabled:=True;

  FHigherBrush:=TBrush.Create(IChanged);
  FHigherBrush.InitColor(TColors.Green);

  FLowerBrush:=TBrush.Create(IChanged);
  FLowerBrush.InitColor(TColors.Red);

  FGroup:=AGroup;

  Resize;

  IThread:=TTickerThread.Create{$IFDEF THREADPARAM}(False){$ENDIF};
  IThread.Ticker:=Self;
end;

{$IFNDEF AUTOREFCOUNT}
Destructor TGridTicker.Destroy;
begin
  IThread.Free;
  FLowerBrush.Free;
  FHigherBrush.Free;
  inherited;
end;
{$ENDIF}

procedure TGridTicker.DoChanged(Sender: TObject);
begin
  FGroup.Changed(Sender);
end;

function TGridTicker.ValueAt(const AColumn:TColumn; const ARow:Integer):Double;
begin
  result:=StrToFloat(FGroup.Data.AsString(AColumn,ARow));
end;

procedure TGridTicker.Paint(const AColumn:TColumn; var AData:TRenderData);

  function CalcColor(const ACol:Integer):TColor;
  begin
    if FValues[ACol,AData.Row].Delta>0 then
       result:=Higher.Color
    else
       result:=Lower.Color;
  end;

var Col : Integer;
    tmp : TColor;
begin
  Col:=AColumn.Index;

  CheckSize(Col,AData.Row);

  if FValues[Col,AData.Row].Enabled then
  begin
    tmp:=CalcColor(Col);

    if FadeColors then
       tmp:=FValues[Col,AData.Row].Color(AColumn.Format.Brush.Color,tmp);

    AData.Painter.Fill(AData.Rect,tmp);

    if FValues[Col,AData.Row].Update(ValueAt(AColumn,AData.Row)) then
       Dec(ICount);
  end;
end;

procedure TGridTicker.CheckSize(const ACol, ARow: Integer);
var L : Integer;
begin
  L:=Length(FValues);

  if (L<=ACol) or ((L>0) and (Length(FValues[0])<=ARow)) then
     Resize;
end;

procedure TGridTicker.ColumnPaint(const Sender: TColumn; var AData: TRenderData;
  var DefaultPaint: Boolean);
begin
  DefaultPaint:=True;

  if Enabled then
     Paint(Sender,AData);
end;

procedure TGridTicker.ColumnPaintEvent;
var Col : Integer;
begin
  for Col:=1 to FGroup.Columns.Count-1 do
      FGroup.Columns[Col].OnPaint:=ColumnPaint;
end;

procedure TGridTicker.Change(const ACol, ARow: Integer; const AOldValue: Double);
var tmp : Double;
begin
  CheckSize(ACol,ARow);

  if RefreshSpeed=0 then
     tmp:=0.01
  else
  begin
    tmp:=Delay/RefreshSpeed;

    if tmp=0 then
       tmp:=0.01
    else
       tmp:=1/tmp;
  end;

  FValues[ACol,ARow].Init(AOldValue,ValueAt(FGroup.Columns[ACol],ARow),tmp);

  Inc(ICount);
end;

procedure TGridTicker.Resize;
begin
  SetLength(FValues,FGroup.Columns.Count,FGroup.Data.Count);
  ColumnPaintEvent;
end;

procedure TGridTicker.SetHigherBrush(const Value: TBrush);
begin
  FHigherBrush.Assign(Value);
end;

procedure TGridTicker.SetLowerBrush(const Value: TBrush);
begin
  FLowerBrush.Assign(Value);
end;

procedure TGridTicker.ProcessThread;
begin
  Sleep(RefreshSpeed);

  if Enabled and (ICount>0) then
     FGroup.Changed(Self);
end;

{ TTickerThread }

procedure TTickerThread.Execute;
begin
  //inherited;

  while not Terminated do
    Ticker.ProcessThread;
end;

{ TTickerCell }

function TTickerCell.Color(const OldColor,NewColor: TColor): TColor;
begin
  if Range=0 then
     result:=OldColor
  else
     result:=TUIColor.Interpolate(OldColor,NewColor,(Value-Old)/Range);
end;

procedure TTickerCell.Init(const AOld, ANew, ASpeed: Double);
begin
  Enabled:=True;
  Old:=AOld;
  Value:=AOld;
  Range:=ANew-Old;
  Delta:=Range*ASpeed;
end;

function TTickerCell.Update(const NewValue: Double): Boolean;
begin
  Value:=Value+Delta;

  result:=((Delta>0) and (Value>=NewValue)) or
          ((Delta<0) and (Value<=NewValue));

  if result then
     Enabled:=False;
end;

end.
