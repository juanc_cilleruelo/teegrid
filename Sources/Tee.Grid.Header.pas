{*********************************************}
{  TeeGrid Software Library                   }
{  Grid Header class                          }
{  Copyright (c) 2016 by Steema Software      }
{  All Rights Reserved                        }
{*********************************************}
unit Tee.Grid.Header;
{$I Tee.inc}

interface

{
  Custom Grid Band classes to display content for Columns.

    TColumnBand :

      Base abstract class for grid bands with Columns.

      Provides:

         Column mouse-dragging
         Mouse-hover highlighting
         Horizontal scrolling

    TColumnHeaderBand :

      Implements a TeeGrid Header (to show column names) with multiple
      sub-levels if the columns have sub-columns.

      Includes a stroke "RowLines" property to paint lines between sub-levels.


  Needs: Tee.Painter, Tee.Format, Tee.Renders, Tee.Grid.Bands and Tee.Grid.Columns
}


uses
  {System.}Classes,

  {$IFDEF FPC}
  Graphics,
  {$ENDIF}

  {$IFNDEF FPC}
  {System.}Types,
  {$ENDIF}

  Tee.Painter, Tee.Format, Tee.Renders,
  Tee.Grid.Columns, Tee.Grid.Bands, Tee.Grid.Data;

type
  // Grid band with Columns
  TColumnBand=class(TGridBandLines)
  private
    FHover: THover;
    FOnColumnResized: TNotifyEvent;

    IDragging  : TColumn;
    IHighLight : TColumn;

    OldWidth,
    OldX : Single;

    procedure ChangeDraggedWidth(const AValue:Single);
    procedure DoChangedRepaint;
    procedure PaintLines(var AData:TRenderData; const DrawFirst:Boolean);
    procedure SetHighLight(const Value: TColumn);
    procedure SetHover(const Value: THover);
    procedure SetColumns(const Value: TColumns);
  protected
    IColumns : TColumns;
    IJustRepaint : Boolean;

    function AdjustBounds(const AColumn:TColumn; const R:TRectF):TRectF; virtual;
    function AsString(const AColumn:TColumn):String; virtual; abstract;
    procedure DoClick; virtual;
  public
    MouseColumn : TColumn; // current column under mouse XY

    Width : Single;

    // Temporary
    MinX,
    OffsetX,
    StartX : Single;

    Constructor Create(const ACollection:TCollection;
                       const AColumns:TColumns); reintroduce; virtual;

    {$IFNDEF AUTOREFCOUNT}
    Destructor Destroy; override;
    {$ENDIF}

    procedure Assign(Source:TPersistent); override;

    function CalcFont(const AColumn:TColumn; const AFont:TFont):TFont;
    procedure InitFormat;

    function Mouse(var AState:TMouseState; const AWidth,AHeight:Single):Boolean; override;

    procedure Paint(var AData:TRenderData; const ARender:TRender); override;

    property Columns:TColumns read IColumns write SetColumns;

    // Current column being dragged
    property Dragging:TColumn read IDragging write IDragging;

    // Current column to highlight
    property HighLight:TColumn read IHighLight write SetHighLight;
  published
    property Hover:THover read FHover write SetHover;
    property OnColumnResized:TNotifyEvent read FOnColumnResized write FOnColumnResized;
  end;

  TSortState=(None,Ascending,Descending);

  TSortableHeader=class(TFormatRender)
  private
    const
      DefaultSize=6;

    var
      FSize: Single;

    function IsSizeStored: Boolean;
    procedure SetSize(const Value: Single);
  public
    State : TSortState;

    Constructor Create(const AChanged:TNotifyEvent); override;

    function Hit(const R:TRectF; const X,Y:Single):Boolean; override;
    procedure Paint(var AData:TRenderData); override;
  published
    property Size:Single read FSize write SetSize stored IsSizeStored;
  end;

  // Grid Header main class
  TColumnHeaderBand=class(TColumnBand)
  private
    FRowLines : TStroke;
    FSortable: Boolean;

    IData : TVirtualData;
    IHeights : Array of Single;

    function HeaderRender(const AColumn:TColumn):TRender;
    //function HeaderRowHeight:Single;
    function LevelTop(const ALevel:Integer):Single;
    function MaxColumnHeight(const ATotal:Single):Single;
    procedure PaintRowLines(const APainter:TPainter; const AColumns:TColumns; const ALevel:Integer);
    procedure SetRowLines(const Value: TStroke);
    procedure SetSortable(const Value: Boolean);
  protected
    function AdjustBounds(const AColumn:TColumn; const R:TRectF):TRectF; override;
    function AsString(const AColumn:TColumn):String; override;
    procedure DoClick; override;
  public
    var
      SortRender : TSortableHeader;

    Constructor Create(const ACollection:TCollection;
                       const AColumns:TColumns;
                       const AData:TVirtualData); reintroduce; virtual;

    {$IFNDEF AUTOREFCOUNT}
    Destructor Destroy; override;
    {$ENDIF}

    function AutoWidth(const APainter:TPainter; const AColumn:TColumn):Single;
    procedure CalcHeight(const ATotal:Single); override;
    function CanSort(const AColumn:TColumn):Boolean;
    class function Description:String; override;

    procedure Paint(var AData:TRenderData; const ARender:TRender); override;

    function RowCount:Integer;

    property Data:TVirtualData read IData write IData;
  published
    property RowLines:TStroke read FRowLines write SetRowLines;
    property Sortable:Boolean read FSortable write SetSortable default True;
  end;

implementation

{$IFNDEF NOUITYPES}
uses
  {System.}UITypes;
{$ENDIF}

{ TColumnBand }

Constructor TColumnBand.Create(const ACollection:TCollection;
                               const AColumns:TColumns);
begin
  inherited Create(ACollection);

  IColumns:=AColumns;

  FHover:=THover.Create(Changed);

  InitFormat;
end;

{$IFNDEF AUTOREFCOUNT}
Destructor TColumnBand.Destroy;
begin
  FHover.Free;
  inherited;
end;
{$ENDIF}

function TColumnBand.AdjustBounds(const AColumn: TColumn; const R: TRectF): TRectF;
begin
  result:=R;
end;

procedure TColumnBand.Assign(Source: TPersistent);
begin
  if Source is TColumnBand then
     Hover:=TColumnBand(Source).FHover;

  inherited;
end;

function TColumnBand.CalcFont(const AColumn:TColumn; const AFont:TFont):TFont;
begin
  result:=AFont;

  if (AColumn=IHighLight) and Hover.Visible then
     if not Hover.ParentFont then
        result:=Hover.Format.Font;
end;

procedure TColumnBand.DoClick;
begin
  if Assigned(OnClick) then
     if MouseColumn<>nil then
        OnClick(MouseColumn);
end;

procedure TColumnBand.Paint(var AData:TRenderData; const ARender:TRender);

  procedure PaintBackground;

    procedure FillColumn(const AColumn:TColumn);
    var tmpR: TRectF;
        tmpRight : Single;
    begin
      tmpRight:=AColumn.Right;

      if tmpRight>=MinX then
      begin
        tmpR:=TRectF.Create(AColumn.Left,Top,tmpRight,Top+Height.Pixels);

        AData.Painter.Paint(Format,tmpR);

        if (AColumn=IHighLight) and Hover.Visible then
           AData.Painter.Paint(Hover.Format,AdjustBounds(AColumn,tmpR));
      end;
    end;

    procedure FillColumns(const AColumns:TColumns);
    var t : Integer;
        tmp : TColumn;
    begin
      for t:=0 to AColumns.Count-1 do
      begin
        tmp:=AColumns[t];

        if tmp.Visible then
        begin
          if tmp.HasItems then
             FillColumns(tmp.Items)
          else
          if tmp.Left>AData.Rect.Right then
             break
          else
             FillColumn(tmp);
        end;
      end;
    end;

  begin
    Top:=AData.Rect.Top;

    if OffsetX>0 then
       AData.Painter.Paint(Format,TRectF.Create(MinX,AData.Rect.Top,MinX+OffsetX,AData.Rect.Height));

    if IColumns<>nil then
       FillColumns(IColumns);
  end;

begin
  Width:=AData.Rect.Width;

  if Format.Brush.Visible or Format.Stroke.Visible then
     PaintBackground;

  if Lines.Visible then
     PaintLines(AData,OffsetX>0);
end;

procedure TColumnBand.PaintLines(var AData: TRenderData; const DrawFirst:Boolean);
var
  tmpHalf,
  tmpWidth,
  tmpBottom : Single;

  procedure DrawColumns(const AColumns:TColumns);

    procedure DrawLine(const AColumn:TColumn);
    var tmpR : TRectF;
    begin
      tmpR:=AdjustBounds(AColumn,AData.Rect);
      AData.Painter.VerticalLine(AColumn.Left-tmpHalf-1,tmpR.Top,tmpBottom);
    end;

  var t : Integer;
      tmp : TColumn;
  begin
    for t:=0 to AColumns.Count-1 do
    begin
      tmp:=AColumns[t];

      if tmp.Visible then
      begin
        if tmp.HasItems then
           DrawColumns(tmp.Items)
        else
        if tmp.Left>tmpWidth then
           break
        else
           DrawLine(tmp);
      end;
    end;
  end;

begin
  AData.Painter.SetStroke(Lines);

  tmpHalf:=(Lines.Size-1)*0.5;

  tmpWidth:=AData.Rect.Width;
  tmpBottom:=AData.Rect.Top+Height.Pixels;

  if DrawFirst then
     AData.Painter.VerticalLine(StartX,AData.Rect.Top,tmpBottom);

  if IColumns<>nil then
     DrawColumns(IColumns);
end;

procedure TColumnBand.InitFormat;
begin
  Format.Brush.InitVisible(True);
  Format.Brush.InitColor($F0F0F0{TColors.Lightgray});
  Format.Brush.Gradient.Show;

  Format.Font.InitColor(TColors.Black);
end;

procedure TColumnBand.SetColumns(const Value: TColumns);
begin
  IColumns.Assign(Value);
end;

procedure TColumnBand.DoChangedRepaint;
begin
  IJustRepaint:=True;
  DoChanged;
  IJustRepaint:=False;
end;

procedure TColumnBand.SetHighLight(const Value: TColumn);
begin
  if IHighLight<>Value then
  begin
    IHighLight:=Value;
    DoChangedRepaint;
  end;
end;

procedure TColumnBand.SetHover(const Value: THover);
begin
  FHover.Assign(Value);
end;

function TColumnBand.Mouse(var AState:TMouseState; const AWidth,AHeight:Single):Boolean;

  procedure DoDown;
  begin
    if IDragging=nil then
       DoClick
    else
    begin
      OldWidth:=IDragging.Width.Pixels;
      OldX:=AState.X;
      IHighLight:=IDragging;
    end;
  end;

  function ColumnHit:TColumn;
  begin
    if Contains(AState.X,AState.Y) and (IColumns<>nil) then
       result:=IColumns.FindAt(AState.X,Width)
    else
       result:=nil;
  end;

  function DoMove:Boolean;
  begin
    result:=IDragging<>nil;

    if IDragging=nil then
       HighLight:=ColumnHit {MouseColumn}
    else
       ChangeDraggedWidth(OldWidth+(AState.X-OldX));
  end;

begin
  result:=inherited;

  if not result then
  case AState.Event of
    TGridMouseEvent.Down: begin DoDown; result:=True; end;
    TGridMouseEvent.Move: result:=DoMove;

      TGridMouseEvent.Up: begin
                            result:=IDragging<>nil;
                            IDragging:=nil;
                          end;
  end;
end;

procedure TColumnBand.ChangeDraggedWidth(const AValue:Single);
var tmp : Single;
begin
  if AValue<0 then
     tmp:=0
  else
     tmp:=AValue;

  if tmp<>IDragging.Width.Value then
  begin
    IDragging.Width.Automatic:=False;
    IDragging.Width.Units:=TSizeUnits.Pixels;
    IDragging.Width.Value:=tmp;

    if Assigned(FOnColumnResized) then
       FOnColumnResized(Self);
  end;
end;

{ TColumnHeader }

Constructor TColumnHeaderBand.Create(const ACollection:TCollection;
                                     const AColumns:TColumns;
                                     const AData:TVirtualData);
begin
  inherited Create(ACollection,AColumns);

  IData:=AData;

  Format.Font.InitStyle([TFontStyle.fsBold]);
  Format.Brush.Gradient.InitVisible(True);

  FRowLines:=TStroke.Create(Changed);
  FRowLines.Brush.InitColor($D0D0D0);

  FSortable:=True;
end;

{$IFNDEF AUTOREFCOUNT}
Destructor TColumnHeaderBand.Destroy;
begin
  FRowLines.Free;
  SortRender.Free;
  inherited;
end;
{$ENDIF}

class function TColumnHeaderBand.Description: String;
begin
  result:='Column Header';
end;

procedure TColumnHeaderBand.DoClick;
begin
  if MouseColumn<>nil then
     if CanSort(MouseColumn) then
     begin
       IData.SortBy(MouseColumn);
       DoChanged;
     end;

  inherited;
end;

function TColumnHeaderBand.RowCount:Integer;
begin
  result:=IColumns.LevelCount;
end;

procedure TColumnHeaderBand.SetRowLines(const Value: TStroke);
begin
  FRowLines.Assign(Value);
end;

procedure TColumnHeaderBand.SetSortable(const Value: Boolean);
begin
  if FSortable<>Value then
  begin
    FSortable:=Value;
    DoChanged;
  end;
end;

function TColumnHeaderBand.HeaderRender(const AColumn:TColumn):TRender;
begin
  if AColumn.Header.ParentFormat then
     result:=FRender
  else
  begin
    result:=AColumn.Header;

    if result=nil then
       result:=FRender;
  end;
end;

function TColumnHeaderBand.MaxColumnHeight(const ATotal:Single):Single;

  function Calc(const AColumn:TColumn):Single; overload;
  var tmpRender : TRender;
  begin
    tmpRender:=HeaderRender(AColumn);

    if tmpRender is TTextRender then
    begin
      TTextRender(tmpRender).TextLines:=TTextRender(tmpRender).CalcTextLines(AColumn.Header.Text);

      result:=TTextRender(tmpRender).CalcHeight;
      //HeaderRowHeight*MaxTextLines
    end
    else
      result:=0;
  end;

  procedure Calc(const AColumns:TColumns; const ALevel:Integer); overload;
  var t : Integer;
      tmp : TColumn;
      tmpH : Single;
  begin
    IHeights[ALevel]:=0;

    for t:=0 to AColumns.Count-1 do
    begin
      tmp:=AColumns[t];

      if tmp.Visible then
      begin
        tmpH:=Calc(tmp);

        if tmpH>IHeights[ALevel] then
           IHeights[ALevel]:=tmpH;

        if tmp.HasItems then
           Calc(tmp.Items,ALevel+1);
      end;
    end;
  end;

var t,
    tmp : Integer;
begin
  result:=0;

  tmp:=RowCount;

  SetLength(IHeights,tmp);

  if tmp>0 then
  begin
    Calc(Columns,0);

    for t:=0 to High(IHeights) do
        result:=result+IHeights[t];
  end;
end;

procedure TColumnHeaderBand.CalcHeight(const ATotal:Single);
begin
  inherited; // <-- useless

  if Height.Automatic then
     Height.Pixels:=MaxColumnHeight(ATotal);
end;

function TColumnHeaderBand.AsString(const AColumn:TColumn):String;
begin
  result:=AColumn.Header.Text;
end;

function TColumnHeaderBand.CanSort(const AColumn:TColumn):Boolean;
begin
  result:=FSortable and (IData<>nil) and IData.CanSortBy(AColumn);

  if result then
     if SortRender=nil then
        SortRender:=TSortableHeader.Create(Changed);
end;

function TColumnHeaderBand.AutoWidth(const APainter: TPainter;
  const AColumn: TColumn): Single;
var tmp : TRender;
    tmpS : String;
begin
  tmpS:=AColumn.Header.Text;

  if tmpS<>'' then
  begin
    tmp:=HeaderRender(AColumn);

    if tmp is TFormatRender then
    begin
      APainter.SetFont(CalcFont(AColumn,TFormatRender(tmp).Format.Font));

      result:=2*TFormatRender(tmp).Format.Stroke.Size;
    end
    else
    begin
      APainter.SetFont(CalcFont(AColumn,Format.Font));
      result:=0;
    end;

    result:=result+APainter.TextWidth(tmpS+'W');
  end
  else
    result:=0;

  AColumn.Header.Margins.Prepare(result,0);

  result:=result+AColumn.Header.Margins.Horizontal;

  if CanSort(AColumn) then
     result:=result+SortRender.Size;
end;

{
function TColumnHeaderBand.HeaderRowHeight:Single;
begin
  result:=(Render as TFormatRender).Format.TextHeight;
end;
}

function TColumnHeaderBand.LevelTop(const ALevel:Integer):Single;
var t : Integer;
begin
  result:=0;

  for t:=0 to ALevel-1 do
      result:=result+IHeights[t]; // ALevel*(HeaderRowHeight+RowSpacing);
end;

function TColumnHeaderBand.AdjustBounds(const AColumn:TColumn; const R:TRectF):TRectF;
begin
  result:=inherited;
  result.Top:=result.Top+LevelTop(AColumn.Level);
end;

procedure TColumnHeaderBand.PaintRowLines(const APainter:TPainter;
            const AColumns:TColumns; const ALevel:Integer);
var t : Integer;
    tmp : TColumn;
    First : Boolean;

    tmpX0,
    tmpX1 : Single;
begin
  First:=True;
  tmpX0:=0;
  tmpX1:=0;

  for t:=0 to AColumns.Count-1 do
  begin
    tmp:=AColumns[t];

    if tmp.Visible then
    begin
      if tmp.HasItems then
         PaintRowLines(APainter,tmp.Items,ALevel+1);

      if First then
      begin
        tmpX0:=tmp.Left;
        First:=False;
      end;

      tmpX1:=tmp.Right;
    end;
  end;

  if (ALevel>0) and (not First) then
     APainter.HorizontalLine(Top+LevelTop(ALevel),tmpX0,tmpX1);
end;

procedure TColumnHeaderBand.Paint(var AData:TRenderData; const ARender:TRender);

  function HasRender(const AColumn:TColumn):Boolean;
  begin
    result:=CanSort(AColumn);
  end;

  function RenderAlign(const AColumn:TColumn):THorizontalAlign;
  begin
    if AColumn.TextAlignment=TColumnTextAlign.Automatic then
       result:=AColumn.HorizAlign
    else
       result:=AColumn.TextAlign.Horizontal;
  end;

  function RenderRect(const ASize:Single; const R:TRectF):TRectF;
  var tmpY : Single;
  begin
    result.Left:=R.Right;
    result.Right:=result.Left+ASize;

    tmpY:=0.5*(R.Top+R.Bottom);

    result.Top:=tmpY-(0.5*ASize);
    result.Bottom:=tmpY+(0.5*ASize);
  end;

  procedure DrawHeaderTexts(const AColumns:TColumns; const ALevel:Integer);

    procedure DrawHeader(const AColumn:TColumn);

      procedure PaintSortRender;
      begin
        AData.Rect:=RenderRect(SortRender.Size,AData.Rect);
        Render.Paint(AData);
        AData.Painter.HideBrush;
      end;

      function CalcSortState:TSortState;
      var tmpAsc : Boolean;
      begin
        if IData.IsSorted(AColumn,tmpAsc) then
           if tmpAsc then
              result:=TSortState.Ascending
           else
              result:=TSortState.Descending
        else
           result:=TSortState.None;
      end;

    var tmpY : Single;
        Old : TRectF;
        tmpR: TRectF;
        tmpSort : TSortState;
        tmpRender : TRender;
    begin
      tmpY:=AData.Rect.Top+LevelTop(ALevel);

      tmpRender:=HeaderRender(AColumn);

     // before TryClip
      if tmpRender is TFormatRender then
         AData.Painter.SetFont(CalcFont(AColumn,TFormatRender(tmpRender).Format.Font))
      else
         AData.Painter.SetFont(CalcFont(AColumn,Format.Font));

      AData.Text:=AsString(AColumn);

      tmpR:=TRectF.Create(AColumn.Left,tmpY,AColumn.Right,tmpY+IHeights[ALevel]{HeaderRowHeight});

      // Reset X if < minX
      if AColumn.Left<MinX then
         tmpR.Left:=MinX;

      if HasRender(AColumn) then
         tmpSort:=CalcSortState
      else
         tmpSort:=TSortState.None;

      if tmpSort<>TSortState.None then
         tmpR.Right:=tmpR.Right-SortRender.Size;

      Old:=AData.Rect;
      AData.Rect:=tmpR;

      if AColumn.Header.TextAlignment=TColumnTextAlign.Automatic then
         AColumn.Header.TextAlign.InitHorizontal(RenderAlign(AColumn));

      AColumn.Header.Paint(AData);

      if tmpSort<>TSortState.None then
      begin
        SortRender.State:=tmpSort;
        PaintSortRender;
      end;

      AData.Rect:=Old;
    end;

  var t : Integer;
      tmp : TColumn;
  begin
    AData.Painter.HideBrush;

    for t:=0 to AColumns.Count-1 do
    begin
      tmp:=AColumns[t];

      if tmp.Visible then
      begin
        if tmp.Left>AData.Rect.Right then
           break
        else
        begin
          if tmp.Header.Visible and (tmp.Header.Text<>'') then
             if tmp.Right>=MinX then
                DrawHeader(tmp);

          if tmp.HasItems then
             DrawHeaderTexts(tmp.Items,ALevel+1);
        end;
      end;
    end;
  end;

var Old : TRectF;
begin
  inherited;

  if FRowLines.Visible then
  begin
    AData.Painter.SetStroke(FRowLines);
    PaintRowLines(AData.Painter,IColumns,0);
  end;

  Old:=AData.Rect;
  DrawHeaderTexts(IColumns,0);
  AData.Rect:=Old;

  AData.Rect.Top:=AData.Rect.Top+Height.Pixels;
end;

{ TSortableHeader }

Constructor TSortableHeader.Create(const AChanged: TNotifyEvent);
begin
  inherited;

  FSize:=DefaultSize;

  Format.Brush.InitVisible(True);
  Format.Brush.InitColor(TColors.Black);
end;

function TSortableHeader.Hit(const R: TRectF; const X, Y: Single): Boolean;
begin
  result:=R.Contains(TPointF.Create(X,Y));
end;

function TSortableHeader.IsSizeStored: Boolean;
begin
  result:=FSize<>DefaultSize;
end;

procedure TSortableHeader.Paint(var AData: TRenderData);

  function Triangle(const ATop,ABottom:Single):TPointsF;
  begin
    SetLength(result,3);

    result[0].X:=AData.Rect.Left;
    result[0].Y:=ATop;

    result[1].X:=AData.Rect.Right;
    result[1].Y:=ATop;

    result[2].X:=0.5*(result[0].X+result[1].X);
    result[2].Y:=ABottom;
  end;

  function DownTriangle:TPointsF;
  begin
    result:=Triangle(AData.Rect.Top,AData.Rect.Bottom);
  end;

  function UpTriangle:TPointsF;
  begin
    result:=Triangle(AData.Rect.Bottom,AData.Rect.Top);
  end;

  procedure Draw(const P:TPointsF);
  begin
    AData.Painter.Paint(Format,P);
  end;

begin
//  inherited;

  if State=TSortState.Ascending then
     Draw(DownTriangle)
  else
     Draw(UpTriangle);
end;

procedure TSortableHeader.SetSize(const Value: Single);
begin
  ChangeSingle(FSize,Value);
end;

end.


