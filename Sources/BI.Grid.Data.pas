{*********************************************}
{  TeeGrid Software Library                   }
{  TeeBI Virtual Data class                   }
{  Copyright (c) 2016 by Steema Software      }
{  All Rights Reserved                        }
{*********************************************}
unit BI.Grid.Data;

interface

uses
  System.Classes,
  Tee.Grid, Tee.Grid.Columns, Tee.Painter, Tee.Renders, Tee.Grid.Data,
  BI.Data, BI.DataSource, BI.Data.RTTI, System.Generics.Collections;

type
  TBIGridData=class(TVirtualData)
  private
    FCursor : TDataCursor;

    class function DataOf(const AColumn:TColumn):TDataItem; inline; static;

    function GetData: TDataItem;
    function RowOf(const ARow:Integer):Integer;
    procedure SetData(const Value: TDataItem);
  protected
    Master : TDataItem;
  public
    Detail : TDataItem;

    Constructor Create(const AProvider:TDataProvider=nil);
    Destructor Destroy; override;

    procedure AddColumns(const AColumns:TColumns); override;
    function AsFloat(const AColumn:TColumn; const ARow:Integer):TFloat; override;
    function AsString(const AColumn:TColumn; const ARow:Integer):String; override;
    function AutoWidth(const APainter:TPainter; const AColumn:TColumn):Single; override;

    function CanExpand(const Sender:TRender; const ARow:Integer):Boolean; override;
    function CanSortBy(const AColumn:TColumn):Boolean; override;
    class function ColumnOf(const AGrid:TCustomTeeGrid; const AItem:TDataItem):TColumn; static;

    function Count:Integer; override;

    class function From(const ASource:TComponent):TVirtualData; override;
    class function From(const AData:TDataItem):TBIGridData; overload;

    class function From<T>(const Value:Array of T):TBIGridData; overload; static;
    class function From<T>(const Value:TList<T>):TBIGridData; overload; static;

    function GetDetail(const ARow:Integer; const AColumns:TColumns; out AParent:TColumn): TVirtualData; override;

    class function IsNumeric(const AColumn:TColumn):Boolean; override;
    function IsSorted(const AColumn:TColumn; out Ascending:Boolean):Boolean; override;

    procedure Load; override;
    procedure SetValue(const AColumn:TColumn; const ARow:Integer; const AText:String); override;
    procedure SortBy(const AColumn:TColumn); override;

    property Data:TDataItem read GetData write SetData;
  end;

implementation

uses
  System.SysUtils, BI.Arrays, BI.Persist;

{ TBIGridData }

Constructor TBIGridData.Create(const AProvider: TDataProvider);
begin
  inherited Create;

  FCursor:=TDataCursor.Create(nil);

  if AProvider<>nil then
  begin
    if AProvider is TBaseDataImporter then
       Data:=TBaseDataImporter(AProvider).Data
    else
       Data:=AProvider.NewData;
  end;
end;

function TBIGridData.AutoWidth(const APainter:TPainter; const AColumn:TColumn):Single;

  function MaxItemLength(const AItem:TDataItem):Integer;
  var t,
      tmp : Integer;
  begin
    result:=0;

    for t:=0 to AItem.Items.Count-1 do
    begin
      tmp:=Length(AItem.Items[t].Name);

      if tmp>result then
         result:=tmp;
    end;
  end;

  function AverageCharWidth:Single;
  begin
    result:=APainter.TextWidth('0');
  end;

  function MaxCharLength(const AItem:TDataItem):Integer;
  begin
    case AItem.Kind of
      TDataKind.dkInt32,
      TDataKind.dkInt64: result:=10;

     TDataKind.dkSingle,
     TDataKind.dkDouble,
   TDataKind.dkExtended: result:=14;

       TDataKind.dkText: result:=AItem.TextData.MaxLength;

   TDataKind.dkDateTime: result:=Length(SampleDateTime(AColumn));

    TDataKind.dkBoolean: result:=5; // 'True' or 'False'
    else
      result:=0; // <-- should not pass
    end;
  end;

var tmpItem : TDataItem;
begin
  tmpItem:=TDataItem(AColumn.TagObject);

  if tmpItem.Kind=TDataKind.dkUnknown then
     {if tmpItem.AsTable then
        result:=APainter.WidthOf(AColumn.Items)
     else
     }
        result:=MaxItemLength(tmpItem)*AverageCharWidth
  else
     result:=AverageCharWidth*MaxCharLength(tmpItem);
end;

type
  TDataItemAccess=class(TDataItem);

procedure TBIGridData.AddColumns(const AColumns: TColumns);

  function HorizAlignOf(const AItem:TDataItem):THorizontalAlign;
  begin
    if AItem.Kind.IsNumeric then
       result:=THorizontalAlign.Right
    else
       result:=THorizontalAlign.Left;
  end;

  function NewColumn(const AColumns:TColumns; const AItem:TDataItem):TColumn;
  begin
    result:=AColumns.Add(AItem.Name);
    result.TagObject:=AItem;
    result.InitAlign(HorizAlignOf(AItem));

    if AItem.Kind=TDataKind.dkBoolean then
    begin
      result.Render:=TBooleanRender.Create(result.Changed);
      result.TextAlignment:=TColumnTextAlign.Custom;
    end;
  end;

  function IsDetail(const AItem:TDataItem):Boolean;
  begin
    result:=(Master<>nil) and
            TDataItemAccess(AItem).HasMaster and
            (AItem.Master.Parent=Master);
  end;

  procedure DoAdd(const AColumns:TColumns; const AItems:TDataArray);
  var t : Integer;
      tmp : TColumn;
      tmpItem : TDataItem;
  begin
    for t:=0 to AItems.Count-1 do
    begin
      tmpItem:=AItems[t];

      if not IsDetail(tmpItem) then
      begin
        tmp:=NewColumn(AColumns,tmpItem);

        if tmpItem.AsTable and (tmpItem.Master=nil) and (tmpItem.Items.Count>0) then
           DoAdd(tmp.Items,tmpItem.Items.AsArray);
      end;
    end;
  end;

begin
  if Data<>nil then
     if Data.AsTable then
        DoAdd(AColumns,Data.Items.AsArray)
     else
        NewColumn(AColumns,Data);
end;

class function TBIGridData.DataOf(const AColumn:TColumn):TDataItem;
begin
  result:=TDataItem(AColumn.TagObject);
end;

Destructor TBIGridData.Destroy;
begin
  FCursor.Free;
  inherited;
end;

class function TBIGridData.From<T>(const Value: array of T): TBIGridData;
begin
  result:=TBIGridData.Create(TTypeProvider<T>.CreateArray(nil,Value));
end;

class function TBIGridData.From(const ASource:TComponent): TVirtualData;
begin
  {
  if ASource is TDataItem then
  begin
    result:=TBIGridData.Create;
    TBIGridData(result).Data:=ASource as TDataItem;
  end
  else
  }
  if ASource is TDataProvider then
  begin
    result:=TBIGridData.Create;
    TBIGridData(result).Data:=(ASource as TDataProvider).NewData;
  end
  else
    result:=nil;
end;

class function TBIGridData.From(const AData: TDataItem): TBIGridData;
begin
  result:=TBIGridData.Create;
  result.Data:=AData;
end;

class function TBIGridData.From<T>(const Value: TList<T>): TBIGridData;
var tmp : TTypeProvider<T>;
begin
  tmp:=TTypeProvider<T>.Create(nil);
  tmp.Add(Value);
  result:=TBIGridData.Create(tmp);
end;

function TBIGridData.GetData: TDataItem;
begin
  result:=FCursor.Data;
end;

function TBIGridData.GetDetail(const ARow:Integer; const AColumns:TColumns;
                               out AParent:TColumn): TVirtualData;

  function FindColumn(const AData:TDataItem):TColumn;
  var t : Integer;
  begin
    for t:=0 to AColumns.Count-1 do
        if DataOf(AColumns[t])=AData then
           Exit(AColumns[t]);

    result:=nil;
  end;

  function FindMasters(const ADetail:TDataArray):TDataArray;
  var t : Integer;
  begin
    result:=nil;

    for t:=0 to ADetail.Count-1 do
        if ADetail[t].Master<>nil then
           if ADetail[t].Master.Parent=Data then
           begin
             result.Add(ADetail[t].Master);

             if AParent=nil then
                AParent:=FindColumn(ADetail[t].Master);
           end;
  end;

var tmp : TDataCursor;
begin
  if Detail=nil then
     result:=nil
  else
  begin
    result:=TBIGridData.From(Detail);
    TBIGridData(result).Master:=Data;

    AParent:=nil;

    tmp:=TBIGridData(result).FCursor;
    tmp.Index:=tmp.MasterDetailIndex(Detail,FindMasters(Detail.Items.AsArray),RowOf(ARow));
  end;
end;

function TBIGridData.CanExpand(const Sender:TRender; const ARow:Integer):Boolean;
begin
  result:=Detail<>nil;
end;

class function TBIGridData.IsNumeric(const AColumn: TColumn): Boolean;
var tmp : TDataItem;
begin
  if AColumn=nil then
     result:=False
  else
  begin
    tmp:=DataOf(AColumn);

    result:=(tmp<>nil) and tmp.Kind.IsNumeric;
  end;
end;

procedure TBIGridData.Load;
begin
  if Data<>nil then
     Data.Load;
end;

function TBIGridData.RowOf(const ARow: Integer): Integer;
begin
  result:=FCursor.Position(ARow);
end;

procedure TBIGridData.SetData(const Value: TDataItem);
begin
  FCursor.Data:=Value;
end;

procedure TBIGridData.SetValue(const AColumn: TColumn; const ARow: Integer;
  const AText: String);
var tmp : TDataItem;
    tmpRow : Integer;
begin
  tmp:=DataOf(AColumn);

  tmpRow:=RowOf(ARow);

  if tmp.Kind=TDataKind.dkUnknown then
  begin
    if not tmp.AsTable then
       tmp.Items[tmpRow].Name:=AText;
  end
  else
  begin
    case tmp.Kind of
        dkInt32: tmp.Int32Data[tmpRow]:=StrToInt(AText);
        dkInt64: tmp.Int64Data[tmpRow]:=StrToInt64(AText);
       dkSingle: tmp.SingleData[tmpRow]:=StrToFloat(AText);
       dkDouble: tmp.DoubleData[tmpRow]:=StrToFloat(AText);
     dkExtended: tmp.ExtendedData[tmpRow]:=StrToFloat(AText);
         dkText: tmp.TextData[tmpRow]:=AText;
     dkDateTime: tmp.DateTimeData[tmpRow]:=StrToDateTime(AText);
      dkBoolean: tmp.BooleanData[tmpRow]:=StrToBool(AText);
    end;
  end;
end;

function TBIGridData.AsFloat(const AColumn: TColumn;
  const ARow: Integer): TFloat;
var tmp : TDataItem;
    tmpRow : Integer;
begin
  tmp:=DataOf(AColumn);

  tmpRow:=RowOf(ARow);

  if tmp.Missing[tmpRow] then
     result:=0
  else
  case tmp.Kind of
       dkInt32: result:=tmp.Int32Data[tmpRow];
       dkInt64: result:=tmp.Int64Data[tmpRow];
      dkSingle: result:=tmp.SingleData[tmpRow];
      dkDouble: result:=tmp.DoubleData[tmpRow];
    dkExtended: result:=tmp.ExtendedData[tmpRow];
    dkDateTime: result:=tmp.DateTimeData[tmpRow];
  else
    result:=0;
  end;
end;

function TBIGridData.AsString(const AColumn:TColumn; const ARow:Integer):String;
var tmp : TDataItem;
    tmpFloat : String;
    tmpRow : Integer;
begin
  tmp:=DataOf(AColumn);

  tmpRow:=RowOf(ARow);

  if tmp.Kind=TDataKind.dkUnknown then
     if tmp.AsTable then
        result:='...'
     else
        result:=tmp.Items[tmpRow].Name
  else
  if tmp.Missing[tmpRow] then
     result:=''
  else
  begin
    tmpFloat:=AColumn.DataFormat.Float;

    case tmp.Kind of
      dkSingle   : if tmpFloat='' then
                      result:=FloatToStr(tmp.SingleData[tmpRow])
                   else
                      result:=FormatFloat(tmpFloat,tmp.SingleData[tmpRow]);

      dkDouble   : if tmpFloat='' then
                      result:=FloatToStr(tmp.DoubleData[tmpRow])
                   else
                      result:=FormatFloat(tmpFloat,tmp.DoubleData[tmpRow]);

      dkExtended : if tmpFloat='' then
                      result:=FloatToStr(tmp.ExtendedData[tmpRow])
                   else
                      result:=FormatFloat(tmpFloat,tmp.ExtendedData[tmpRow]);

      dkDateTime : if AColumn.DataFormat.DateTime='' then
                      result:=DateTimeToStr(tmp.DateTimeData[tmpRow])
                   else
                      result:=FormatDateTime(AColumn.DataFormat.DateTime,tmp.DateTimeData[tmpRow]);
    else
      result:=tmp.DataToString(tmpRow);
    end;
  end;
end;

function TBIGridData.CanSortBy(const AColumn: TColumn): Boolean;
var tmp : TDataItem;
begin
  tmp:=DataOf(AColumn);

  result:=(tmp<>nil) and (tmp.Kind<>TDataKind.dkUnknown);
end;

function TBIGridData.IsSorted(const AColumn:TColumn; out Ascending:Boolean):Boolean;
var tmp : TDataItem;
    tmpIndex : Integer;
begin
  result:=False;

  tmp:=DataOf(AColumn);

  if tmp<>nil then
  begin
    tmpIndex:=FCursor.SortBy.IndexOf(tmp);

    if tmpIndex<>-1 then
    begin
      Ascending:=not FCursor.SortBy.Items[tmpIndex].Descending;
      result:=True;
    end;
  end;
end;

procedure TBIGridData.SortBy(const AColumn:TColumn);
var tmpAsc : Boolean;
begin
  if IsSorted(AColumn,tmpAsc) then
     tmpAsc:=not tmpAsc
  else
     tmpAsc:=True;

  FCursor.SortBy.Clear;
  FCursor.SortBy.Add(DataOf(AColumn),tmpAsc);

  FCursor.PrepareIndex(FCursor.Index);
end;

class function TBIGridData.ColumnOf(const AGrid:TCustomTeeGrid; const AItem: TDataItem): TColumn;

  function Find(const AColumns:TColumns):TColumn;
  var t : Integer;
      tmp : TColumn;
  begin
    for t:=0 to AColumns.Count-1 do
    begin
      tmp:=AColumns[t];

      if DataOf(tmp)=AItem then
         Exit(tmp)
      else
      if tmp.HasItems then
      begin
        result:=Find(tmp.Items);

        if result<>nil then
           Exit;
      end;
    end;

    result:=nil;
  end;

begin
  result:=Find(AGrid.Columns);
end;

function TBIGridData.Count: Integer;
begin
  result:=FCursor.Count;
end;

initialization
  TVirtualDataClasses.Register(TBIGridData);
finalization
  TVirtualDataClasses.UnRegister(TBIGridData);
end.
