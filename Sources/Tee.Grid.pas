{*********************************************}
{  TeeGrid Software Library                   }
{  Base abstract Grid class                   }
{  Copyright (c) 2016 by Steema Software      }
{  All Rights Reserved                        }
{*********************************************}
unit Tee.Grid;
{$I Tee.inc}

interface

{
  Top high-level agnostic grid class:

  TCustomTeeGrid

  Its an abstract class. Cannot be created directly.

  Other units implement a derived TeeGrid class:

     VCLTee.Grid ->  TTeeGrid for VCL
     FMXTee.Grid ->  TTeeGrid for Firemonkey
}

uses
  {System.}Classes,

  {$IFNDEF FPC}
  {System.}Types,
  {$ENDIF}

  {System.}SysUtils,

  {$IFNDEF NOUITYPES}
  System.UITypes,
  {$ENDIF}

  Tee.Painter, Tee.Format, Tee.Renders, Tee.Control,
  Tee.Grid.Columns, Tee.Grid.Header, Tee.Grid.Bands, Tee.Grid.Rows,
  Tee.Grid.Selection, Tee.Grid.Data, Tee.Grid.RowGroup;

type
  TGridEditing=class(TPersistentChange)
  private
    FActive: Boolean;
    FAlwaysVisible : Boolean;
    FClass : TClass;
    FDoubleClick : Boolean;
  public
    Constructor Create(const AChanged:TNotifyEvent); override;

    procedure Assign(Source:TPersistent); override;

    function ClassOf(const AColumn:TColumn):TClass;

    property Active:Boolean read FActive write FActive default False;
    property EditorClass:TClass read FClass write FClass;
  published
    property AlwaysVisible:Boolean read FAlwaysVisible write FAlwaysVisible default False;
    property DoubleClick:Boolean read FDoubleClick write FDoubleClick default True;
  end;

  TScroll=TPointF;

  TCustomTeeGrid=class(TCustomTeeControl)
  private
    FData : TVirtualData;
    FDataSource: TComponent;
    FEditing : TGridEditing;
    FMargins : TMargins;
    FOnAfterDraw: TNotifyEvent;
    FOnSelect: TNotifyEvent;
    FRoot: TRowGroup;

    IBands : TGridBands;

    function AvailableHeight:Single;
    procedure ChangedRow(const Sender:TObject; const ARow:Integer);
    procedure CheckHorizScroll(const AColumn:TColumn);
    procedure CheckScroll(const ASelected:TGridSelection);
    procedure CheckVertScroll(const ARow:Integer);
    procedure DataRefresh(Sender:TObject);

    function GetCells: TTextRender;
    function GetColumns: TColumns;
    function GetCurrent: TRowGroup; inline;
    function GetFooter: TGridBands;
    function GetHeader:TColumnHeaderBand;
    function GetHeaders: TGridBands;
    function GetIndicator: TIndicator;
    function GetReadOnly: Boolean;
    function GetSelected: TGridSelection;
    function GetRows:TRows;

    function IsColumnsStored: Boolean;

    procedure SelectedChanged(Sender:TObject);

    procedure SetCells(const Value: TTextRender);
    procedure SetColumns(const Value: TColumns);
    procedure SetCurrent(const Value: TRowGroup); inline;
    procedure SetData(const Value:TVirtualData);
    procedure SetDataSource(const Value: TComponent);
    procedure SetEditing(const Value: TGridEditing);
    procedure SetFooter(const Value: TGridBands);
    procedure SetHeader(const Value: TColumnHeaderBand);
    procedure SetHeaders(const Value: TGridBands);
    procedure SetIndicator(const Value: TIndicator);
    procedure SetMargins(const Value: TMargins);
    procedure SetReadOnly(const Value: Boolean);
    procedure SetRows(const Value: TRows);
    procedure SetSelected(const Value: TGridSelection);
  protected
    procedure ChangeHorizScroll(const Value:Single);
    procedure ChangeVertScroll(const Value:Single);
    procedure DataChanged; virtual;
    procedure DataSourceChanged;
    procedure Key(const AState:TKeyState);
    function HorizScrollBarHeight:Single; virtual; abstract;
    procedure HorizScrollChanged; virtual; abstract;
    procedure Mouse(var AState:TMouseState);
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    procedure FinishPaint(const IsDesign:Boolean);
    property Root:TRowGroup read FRoot;
    procedure StartEditor(const AColumn:TColumn; const ARow:Integer); overload; virtual; abstract;
    procedure StartEditor; overload;
    procedure StopEditor; virtual;
    procedure TryStartEditor;
    function VertScrollBarWidth:Single; virtual; abstract;
    procedure VertScrollChanged; virtual; abstract;
  public
    Constructor Create(AOwner:TComponent); override;

    {$IFNDEF AUTOREFCOUNT}
    Destructor Destroy; override;
    {$ENDIF}

    procedure Assign(Source:TPersistent); override;

    function CanExpand(const Sender:TRender; const ARow:Integer):Boolean;
    function ClientHeight:Single; override;
    function ClientWidth:Single; override;
    procedure CopySelected; virtual; abstract;
    procedure MouseLeave;
    procedure Paint; override;
    procedure RefreshData;

    // Focused rows group
    property Current:TRowGroup read GetCurrent write SetCurrent;

    // Data for main rows
    property Data:TVirtualData read FData write SetData;

    // Main rows
    property Rows:TRows read GetRows write SetRows;
  published
    property Cells:TTextRender read GetCells write SetCells;
    property Columns:TColumns read GetColumns write SetColumns stored IsColumnsStored;
    property DataSource:TComponent read FDataSource write SetDataSource;
    property Editing:TGridEditing read FEditing write SetEditing;
    property Footer:TGridBands read GetFooter write SetFooter;
    property Header:TColumnHeaderBand read GetHeader write SetHeader;
    property Headers:TGridBands read GetHeaders write SetHeaders stored False;
    property Indicator:TIndicator read GetIndicator write SetIndicator;
    property Margins:TMargins read FMargins write SetMargins;
    property ReadOnly:Boolean read GetReadOnly write SetReadOnly default False;
    property Selected:TGridSelection read GetSelected write SetSelected;

    // Events
    property OnAfterDraw:TNotifyEvent read FOnAfterDraw write FOnAfterDraw;
    property OnSelect:TNotifyEvent read FOnSelect write FOnSelect;
  end;

implementation

{$IFDEF FPC}
uses
  LCLType;

const
  vkF2 = VK_F2;
  vkEscape = VK_ESCAPE;
  vkInsert = VK_INSERT;
{$ELSE}

{$IFDEF NOUITYPES}
const
  vkEscape = 27;
  vkF2 = 113;
  vkInsert = 45;
{$ENDIF}

{$ENDIF}

{ TGridEditing }

Constructor TGridEditing.Create(const AChanged: TNotifyEvent);
begin
  inherited;
  FDoubleClick:=True;
end;

procedure TGridEditing.Assign(Source: TPersistent);
begin
  if Source is TGridEditing then
  begin
    FAlwaysVisible:=TGridEditing(Source).FAlwaysVisible;
    FClass:=TGridEditing(Source).FClass;
    FDoubleClick:=TGridEditing(Source).FDoubleClick;
  end
  else
    inherited;
end;

function TGridEditing.ClassOf(const AColumn: TColumn): TClass;
begin
  result:=AColumn.EditorClass;

  if result=nil then
     result:=EditorClass;
end;

{ TCustomTeeGrid }

Constructor TCustomTeeGrid.Create(AOwner: TComponent);
begin
  inherited;

  FEditing:=TGridEditing.Create(Changed);

  IBands:=TGridBands.Create(Self,Changed);

  FRoot:=TRowGroup.Create(IBands,nil);
  FRoot.OnChangedSelected:=SelectedChanged;

  FRoot.Current:=FRoot;

  FMargins:=TMargins.Create(Changed);
end;

{$IFNDEF AUTOREFCOUNT}
Destructor TCustomTeeGrid.Destroy;
begin
  FMargins.Free;
  FEditing.Free;
  IBands.Free;

  inherited;
end;
{$ENDIF}

function TCustomTeeGrid.GetCurrent: TRowGroup;
begin
  result:=FRoot.Current;
end;

procedure TCustomTeeGrid.DataChanged;
begin
  Rows.VisibleColumns.Clear;
  Columns.ValidWidth:=False;

  Changed(Self);
end;

procedure TCustomTeeGrid.RefreshData;

  procedure DoRefreshData;
  begin
    BeginUpdate;
    try
      StopEditor;

      FRoot.Current:=FRoot;
      FRoot.RefreshData(Data);

      DataChanged;
    finally
      EndUpdate;
    end;
  end;

begin
  if not (csDestroying in ComponentState) then
     DoRefreshData;
end;

procedure TCustomTeeGrid.ChangeVertScroll(const Value:Single);
begin
  if Current.Rows.Scroll.Y<>Value then
  begin
    Current.Rows.Scroll.Y:=Value;
    VertScrollChanged;
  end;
end;

procedure TCustomTeeGrid.CheckVertScroll(const ARow:Integer);
var tmpH,
    tmpY,
    tmpClient : Single;
begin
  if ARow<=0 then
     ChangeVertScroll(0)
  else
  begin
    tmpY:=Current.Rows.TopOf(ARow);

    if tmpY<0 then
       ChangeVertScroll(-tmpY)
    else
    begin
      tmpH:=Current.Rows.TotalHeight(ARow);
      tmpY:=tmpY+tmpH;

      tmpClient:=AvailableHeight;

      if tmpY>tmpClient then
         ChangeVertScroll(Current.Rows.Scroll.Y+(tmpY-tmpClient));
    end;
  end;
end;

procedure TCustomTeeGrid.TryStartEditor;
begin
  if Current.CanStartEditor then
  begin
    StopEditor;
    StartEditor;

    Editing.Active:=True;
  end;
end;

procedure TCustomTeeGrid.Key(const AState:TKeyState);
begin
  case AState.Key of

      vkF2: TryStartEditor;

  vkEscape: if Editing.Active then
               StopEditor;

  vkInsert,
  Ord('c'),
  Ord('C'): if not Editing.Active then
               if ssCtrl in AState.Shift then
                  CopySelected;
  else
    Current.Key(AState);
  end;
end;

procedure TCustomTeeGrid.Mouse(var AState:TMouseState);
begin
  FRoot.Mouse(AState,ClientWidth,AvailableHeight);
end;

procedure TCustomTeeGrid.MouseLeave;
begin
  Header.HighLight:=nil;
end;

procedure TCustomTeeGrid.Notification(AComponent: TComponent;
  Operation: TOperation);
begin
  inherited;

  if (AComponent=FDataSource) and (Operation=TOperation.opRemove) then
     DataSource:=nil;
end;

function TCustomTeeGrid.GetCells: TTextRender;
begin
  result:=FRoot.Cells;
end;

function TCustomTeeGrid.GetColumns: TColumns;
begin
  result:=FRoot.Columns;
end;

function TCustomTeeGrid.GetFooter: TGridBands;
begin
  result:=FRoot.Footer;
end;

function TCustomTeeGrid.GetHeader: TColumnHeaderBand;
begin
  result:=FRoot.Header;
end;

function TCustomTeeGrid.GetHeaders: TGridBands;
begin
  result:=FRoot.Headers;
end;

function TCustomTeeGrid.GetIndicator: TIndicator;
begin
  result:=FRoot.Indicator;
end;

function TCustomTeeGrid.GetReadOnly: Boolean;
begin
  result:=FRoot.ReadOnly;
end;

function TCustomTeeGrid.GetRows: TRows;
begin
  result:=FRoot.Rows;
end;

function TCustomTeeGrid.GetSelected: TGridSelection;
begin
  result:=FRoot.Selected;
end;

function TCustomTeeGrid.CanExpand(const Sender: TRender;
  const ARow: Integer): Boolean;
begin
  result:=(Current.Data<>nil) and Current.Data.HasDetail(ARow);
end;

procedure TCustomTeeGrid.ChangeHorizScroll(const Value: Single);
begin
  if Current.Rows.Scroll.X<>Value then
  begin
    Current.Rows.Scroll.X:=Value;
    HorizScrollChanged;
  end;
end;

procedure TCustomTeeGrid.Assign(Source: TPersistent);
begin
  if Source is TCustomTeeGrid then
  begin
    Editing:=TCustomTeeGrid(Source).FEditing;
    Margins:=TCustomTeeGrid(Source).FMargins;
    FRoot.Assign(TCustomTeeGrid(Source).FRoot);
  end;

  inherited;
end;

function TCustomTeeGrid.AvailableHeight:Single;
begin
  result:=ClientHeight-Footer.Height;
end;

function TCustomTeeGrid.ClientHeight:Single;
begin
  result:=Height-HorizScrollBarHeight;
end;

function TCustomTeeGrid.ClientWidth:Single;
begin
  result:=Width-VertScrollBarWidth;
end;

// Try to scroll until AColumn is visible
procedure TCustomTeeGrid.CheckHorizScroll(const AColumn: TColumn);
var tmpX : Single;
begin
  tmpX:=AColumn.Left;

  if tmpX>ClientWidth then
     ChangeHorizScroll(tmpX-Width+Current.Rows.Scroll.X+AColumn.Width.Pixels+VertScrollBarWidth)
  else
  if tmpX<0 then
     ChangeHorizScroll(tmpX+Current.Rows.Scroll.X);
end;

// Main paint procedure
procedure TCustomTeeGrid.Paint;

  function CalcMainRect:TRectF;
  var tmp : Single;
  begin
    if Back.Stroke.Visible then
       tmp:=Back.Stroke.Size
    else
       tmp:=0;

    result:=TRectF.Create(tmp,tmp,ClientWidth-tmp,ClientHeight-tmp);
    result:=FMargins.Adjust(result);
  end;

var tmpData : TRenderData;
begin
  inherited;

  FMargins.Prepare(Width,Height);

//  Old:=not ValidRowsHeight;

  tmpData.Painter:=Painter;
  tmpData.Rect:=CalcMainRect;

  FRoot.Current:=Current;
  FRoot.Paint(tmpData,nil);

  if Assigned(FOnAfterDraw) then
     FOnAfterDraw(Self);
end;

{.$DEFINE MADEWITH}

procedure TCustomTeeGrid.FinishPaint(const IsDesign:Boolean);

  procedure PaintEmpty;
  var tmp : TFont;
  begin
    tmp:=TFont.Create(nil);
    try
      Painter.SetFont(tmp);
      Painter.TextOut(10,10,'(DataSource is empty)');
    finally
      tmp.Free;
    end;
  end;

  {$IFDEF MADEWITH}
  procedure PaintMadeWith;
  const MadeWith='Made with TeeGrid. http://www.steema.com';
  var tmp : TFont;
      tmpW : Single;
  begin
    tmp:=TFont.Create(nil);
    try
      tmp.Size:=24;
      tmp.Color:=TColors.Navy;

      repeat
        Painter.SetFont(tmp);
        tmpW:=Painter.TextWidth(MadeWith);

        if tmpW>ClientWidth-10 then
        begin
          tmp.Size:=tmp.Size-1;

          if tmp.Size<8 then
             break;
        end
        else
           break;

      until False;

      Painter.SetFont(tmp);
      Painter.TextOut(10,ClientHeight-Painter.TextHeight('0')-4,MadeWith);
    finally
      tmp.Free;
    end;
  end;
  {$ENDIF}

begin
  if IsDesign then
  begin
    if (FRoot.Rows.VisibleColumns.Count=0) and (Data=nil) then
       PaintEmpty;
  end
  {$IFDEF MADEWITH}
  else
    PaintMadeWith;
  {$ENDIF}
end;

procedure TCustomTeeGrid.CheckScroll(const ASelected:TGridSelection);
begin
  if ASelected.Column<>nil then
  begin
    CheckHorizScroll(ASelected.Column);
    CheckVertScroll(ASelected.Row);
  end;
end;

procedure TCustomTeeGrid.SelectedChanged(Sender: TObject);

  procedure DoChange(const AGroup:TRowGroup);
  begin
    if FRoot.Current<>AGroup then
    begin
      StopEditor;
      FRoot.Current:=AGroup;
    end;

    if FRoot.Current=FRoot then
       CheckScroll(Current.Selected);

    if not Editing.AlwaysVisible then
       StopEditor;

    if Assigned(FOnSelect) then
       FOnSelect(Self);
  end;

begin
  if not (csDestroying in ComponentState) then
     DoChange(Sender as TRowGroup);
end;

procedure TCustomTeeGrid.SetCells(const Value: TTextRender);
begin
  FRoot.Cells:=Value;
end;

procedure TCustomTeeGrid.SetColumns(const Value: TColumns);
begin
  FRoot.Columns.Assign(Value);
end;

procedure TCustomTeeGrid.SetCurrent(const Value: TRowGroup);
begin
  FRoot.Current:=Value;
end;

procedure TCustomTeeGrid.DataRefresh(Sender:TObject);
begin
  RefreshData;
end;

procedure TCustomTeeGrid.ChangedRow(const Sender:TObject; const ARow:Integer);
begin
  Selected.Row:=ARow;
end;

type
  TDataAccess=class(TVirtualData);

procedure TCustomTeeGrid.SetData(const Value: TVirtualData);
begin
  if FData<>Value then
  begin
    FData:=Value;

    if FData<>nil then
    begin
      TDataAccess(FData).FOnChangeRow:=ChangedRow;
      TDataAccess(FData).FOnRefresh:=DataRefresh;
      TDataAccess(FData).FOnRepaint:=Changed;
    end;

    RefreshData;
  end;
end;

procedure TCustomTeeGrid.SetDataSource(const Value: TComponent);
begin
  if FDataSource<>Value then
  begin
    if FDataSource<>nil then
       TComponent(FDataSource).RemoveFreeNotification(Self);

    FDataSource:=Value;

    if FDataSource<>nil then
       TComponent(FDataSource).FreeNotification(Self);

    if not (csLoading in Owner.ComponentState) then
       DataSourceChanged;
  end;
end;

procedure TCustomTeeGrid.DataSourceChanged;
var tmp : TVirtualData;
begin
  if FDataSource=nil then
  begin
    FData.Free;
    FData:=nil;
  end
  else
  begin
    tmp:=TVirtualDataClasses.Guess(FDataSource);

    if tmp<>nil then
       Data:=tmp;
  end;
end;

procedure TCustomTeeGrid.SetEditing(const Value: TGridEditing);
begin
  FEditing.Assign(Value);
end;

procedure TCustomTeeGrid.SetMargins(const Value: TMargins);
begin
  FMargins.Assign(Value);
end;

procedure TCustomTeeGrid.SetFooter(const Value: TGridBands);
begin
  FRoot.Footer:=Value;
end;

procedure TCustomTeeGrid.SetHeader(const Value: TColumnHeaderBand);
begin
  FRoot.Header:=Value;
end;

procedure TCustomTeeGrid.SetHeaders(const Value: TGridBands);
begin
  FRoot.Headers:=Value;
end;

procedure TCustomTeeGrid.SetIndicator(const Value: TIndicator);
begin
  FRoot.Indicator:=Value;
end;

procedure TCustomTeeGrid.SetReadOnly(const Value: Boolean);
begin
  if ReadOnly<>Value then
  begin
    Root.ReadOnly:=Value;

    if Editing.Active then
       StopEditor;
  end;
end;

procedure TCustomTeeGrid.SetRows(const Value: TRows);
begin
  FRoot.Rows:=Value;
end;

procedure TCustomTeeGrid.SetSelected(const Value: TGridSelection);
begin
  FRoot.Selected:=Value;
end;

procedure TCustomTeeGrid.StartEditor;
begin
  StartEditor(Current.Selected.Column,Current.Selected.Row);
end;

procedure TCustomTeeGrid.StopEditor;
begin
  Editing.Active:=False;
end;

function TCustomTeeGrid.IsColumnsStored: Boolean;
begin
  result:=Columns.Count>0;
end;

end.
