{*********************************************}
{  TeeGrid Software Library                   }
{  Grid Totals classes                        }
{  Copyright (c) 2016 by Steema Software      }
{  All Rights Reserved                        }
{*********************************************}
unit Tee.Grid.Totals;
{$I Tee.inc}

interface

{
   Classes to paint "Totals" (or "SubTotals") grid bands.

   Automatic:  (all numeric visible columns as Sum)

     TeeGrid1.Footer.Add( TColumnTotals.From( TeeGrid.Data, TeeGrid1.Columns ) );

   Manual:

     var Totals : TColumnTotals;

     Totals:= TColumnTotals.Create(nil, TeeGrid1.Columns);

     Totals.Calculation.Add( TeeGrid1.Columns['Amount'], TColumnCalculation.Average );

     TeeGrid1.Footer.Add( Totals );

}

uses
  {System.}Classes,

  {$IFNDEF FPC}
  {System.}Types,
  {$ENDIF}

  Tee.Grid.Columns, Tee.Format, Tee.Renders,
  Tee.Grid.Data, Tee.Grid.Header;

type
  TBaseTotals=class(TColumnBand)
  public
    Constructor Create(const ACollection:TCollection; const AColumns:TColumns); override;

    procedure Paint(var AData:TRenderData; const ARender:TRender); override;
  end;

  TColumnTotals=class(TBaseTotals)
  private
    FData : TVirtualData;
  protected
    function AsString(const AColumn:TColumn):String; override;
  public
    type
      // A Column total calculation
      TTotalCalculation=record // <-- convert to TCollectionItem
      public
        Column: TColumn;
        Calculation : TColumnCalculation;
      end;

      // List of calculations
      TTotals=record  // <-- convert to TCollectionChange
      public
        Items : Array of TTotalCalculation;

        procedure Add(const AColumn:TColumn; const ACalculation:TColumnCalculation);
        procedure Clear; inline;
        function Count:Integer; inline;
        procedure Delete(const AIndex:Integer);
        function Find(const AColumn:TColumn; out ACalculation:TColumnCalculation):Boolean;
        function IndexOf(const AColumn:TColumn):Integer;
      end;

    var
      Calculation : TTotals;

    Constructor Create(const ACollection:TCollection; const AColumns:TColumns; const AData:TVirtualData); reintroduce;

    class function Description:String; override;
  end;

  // Displays the names of a "Column Totals" band items
  TTotalsHeader=class(TBaseTotals)
  private
    FTotals : TColumnTotals;

    procedure SetTotals(const Value: TColumnTotals);
  protected
    function AsString(const AColumn:TColumn):String; override;
  public
    Constructor CreateTotals(const ACollection:TCollection; const ATotals:TColumnTotals);

    class function Description:String; override;

    property Totals : TColumnTotals read FTotals write SetTotals;
  end;

implementation

uses
  {$IFDEF FPC}
  {VCL.}Graphics,
  {$ENDIF}
  {$IFNDEF NOUITYPES}
  {System.}UITypes,
  {$ENDIF}
  {System.}SysUtils, Tee.Painter;

{ TBaseTotals }

Constructor TBaseTotals.Create(const ACollection:TCollection; const AColumns:TColumns);
begin
  inherited;

  (Render as TTextRender).TextLines:=1;

  Format.Brush.InitVisible(True);
  Format.Brush.InitColor($F0F0F0{TColors.Lightgray});
  Format.Font.InitColor(TColors.Black);
end;

procedure TBaseTotals.Paint(var AData: TRenderData; const ARender:TRender);

  procedure PaintTotal(const AColumn:TColumn);
  var Old : TRectF;
  begin
    AData.Painter.SetFont(CalcFont(AColumn,Format.Font)); // before TryClip

    Old:=AData.Rect;

    // Reset X if < minX
    if AColumn.Left<MinX then
       AData.Rect.Left:=MinX
    else
       AData.Rect.Left:=AColumn.Left;

    AData.Rect.Right:=AColumn.Right;

    (FRender as TTextRender).TextAlign.Horizontal:=THorizontalAlign.Right;
    FRender.Paint(AData);

    AData.Rect:=Old;
  end;

  procedure DrawTotals(const AColumns:TColumns);
  var t : Integer;
      tmp : TColumn;
  begin
    for t:=0 to AColumns.Count-1 do
    begin
      tmp:=AColumns[t];

      if tmp.Visible then
      begin
        if tmp.HasItems then
           DrawTotals(tmp.Items)
        else
        if tmp.Left>AData.Rect.Right then
           break
        else
        begin
          AData.Text:=AsString(tmp);

          if AData.Text<>'' then
             PaintTotal(tmp);
        end;
      end;
    end;
  end;

begin
  inherited;

  AData.Painter.HideBrush;

  if IColumns<>nil then
     DrawTotals(IColumns);
end;

{ TColumnTotals }

Constructor TColumnTotals.Create(const ACollection:TCollection; const AColumns:TColumns; const AData:TVirtualData);
begin
  inherited Create(ACollection,AColumns);
  FData:=AData;
end;

class function TColumnTotals.Description: String;
begin
  result:='Column Totals';
end;

function TColumnTotals.AsString(const AColumn: TColumn): String;
var tmp : TColumnCalculation;
    tmpFormat : String;
    tmpValue : Double;
begin
  if Calculation.Find(AColumn,tmp) then
  begin
    tmpValue:=FData.Calculate(AColumn,tmp);

    tmpFormat:=AColumn.DataFormat.Float;

    if tmpFormat='' then
       result:=FloatToStr(tmpValue)
    else
       result:=FormatFloat(tmpFormat,tmpValue);
  end
  else
     result:='';
end;

{ TColumnTotals.TTotals }

procedure TColumnTotals.TTotals.Clear;
begin
  Items:=nil;
end;

function TColumnTotals.TTotals.Count: Integer;
begin
  result:=Length(Items);
end;

procedure TColumnTotals.TTotals.Delete(const AIndex: Integer);
var t : Integer;
begin
  for t:=AIndex to Count-2 do
      Items[t]:=Items[t+1];

  SetLength(Items,Count-1);
end;

procedure TColumnTotals.TTotals.Add(const AColumn: TColumn;
  const ACalculation: TColumnCalculation);
var L : Integer;
begin
  L:=Count;
  SetLength(Items,L+1);

  Items[L].Column:=AColumn;
  Items[L].Calculation:=ACalculation;
end;

function TColumnTotals.TTotals.IndexOf(const AColumn:TColumn):Integer;
var t : Integer;
begin
  for t:=0 to High(Items) do
      if Items[t].Column=AColumn then
         Exit(t);

  result:=-1;
end;

function TColumnTotals.TTotals.Find(const AColumn: TColumn;
  out ACalculation: TColumnCalculation): Boolean;
var tmp : Integer;
begin
  tmp:=IndexOf(AColumn);

  result:=tmp<>-1;

  if result then
     ACalculation:=Items[tmp].Calculation;
end;

{ TTotalsHeader }

Constructor TTotalsHeader.CreateTotals(const ACollection:TCollection; const ATotals: TColumnTotals);
begin
  inherited Create(ACollection,ATotals.Columns);

  Format.Font.InitStyle([TFontStyle.fsBold]);
  FTotals:=ATotals;
end;

class function TTotalsHeader.Description: String;
begin
  result:='Totals Header';
end;

function CalculationToString(const ACalculation:TColumnCalculation):String;
begin
  case ACalculation of
    Count: result:='Count';
      Sum: result:='Sum';
      Min: result:='Min';
      Max: result:='Max';
  Average: result:='Average';
  end;
end;

function TTotalsHeader.AsString(const AColumn: TColumn): String;
var tmp : TColumnCalculation;
begin
  if (Totals<>nil) and Totals.Calculation.Find(AColumn,tmp) then
     result:=CalculationToString(tmp)
  else
     result:='';
end;

procedure TTotalsHeader.SetTotals(const Value: TColumnTotals);
begin
  if FTotals<>Value then
  begin
    FTotals:=Value;
    DoChanged;
  end;
end;

end.
