{*********************************************}
{  TeeGrid Software Library                   }
{  Basic Grid "Bands"                         }
{  Copyright (c) 2016 by Steema Software      }
{  All Rights Reserved                        }
{*********************************************}
unit Tee.Grid.Bands;
{$I Tee.inc}

{$SCOPEDENUMS ON}

interface

{
   Base classes for "Grid Bands" collection.

   A "Grid Band" is a rectangle to display at a Grid with support for
   mouse-hover highlighting, custom height and OnClick.

   TTextBand is an example of a custom TGridBand to display text at for
   example grid header and footer.

   Other units implement different kinds of Grid Bands, like Grid Header,
   Grid Totals and Grid Rows.

   Needs: Tee.Format and Tee.Renders
}

uses
  {System.}Classes,

  Tee.Format, Tee.Renders;

type
  // Mouse and keyboard agnostic types

  TGridMouseButton=(Left,Middle,Right);

  TGridMouseEvent=(Down,Move,Up,DoubleClick);

  TMouseCursor=(Default,HandPoint,HorizResize,VertResize);

  TMouseState=record
  public
    Shift : TShiftState;
    X,Y : Single;
    Button : TGridMouseButton;
    Event : TGridMouseEvent;
    Cursor : TMouseCursor;
  end;

  TGridKeyEvent=(Down,Up);

  TKeyState=record
    Key : Word;
    Shift : TShiftState;
    Event : TGridKeyEvent;
  end;

  // Just an alias
  TBandHeight=class(TCoordinate);

  // Base class with Hover and Height properties
  TGridBand=class(TVisibleRenderItem)
  private
    FHeight: TBandHeight;
    FOnClick: TNotifyEvent;

    procedure Changed(Sender: TObject);
    procedure SetHeight(const Value: TBandHeight);
  protected
    Top : Single;
  public
    Constructor Create(ACollection:TCollection); override;

    {$IFNDEF AUTOREFCOUNT}
    Destructor Destroy; override;
    {$ENDIF}

    procedure Assign(Source:TPersistent); override;

    procedure CalcHeight(const ATotal:Single); virtual;
    function Contains(const X,Y:Single):Boolean;
    class function Description:String; virtual;
    function Mouse(var AState:TMouseState; const AWidth,AHeight:Single): Boolean; overload; virtual;
    procedure Paint(var AData:TRenderData; const ARender:TRender); override;
  published
    property Height:TBandHeight read FHeight write SetHeight;
    property OnClick:TNotifyEvent read FOnClick write FOnClick;
  end;

  TGridBandLines=class(TGridBand)
  private
    FLines : TStroke;

    procedure SetLines(const Value: TStroke);
  public
    Constructor Create(ACollection:TCollection); override;

    {$IFNDEF AUTOREFCOUNT}
    Destructor Destroy; override;
    {$ENDIF}
  published
    property Lines:TStroke read FLines write SetLines;
  end;

  // Basic grid band with Text, usable at grid header and footer
  TTextBand=class(TGridBand)
  private
    function GetText:String;
    function GetTextRender: TCellRender;
    procedure SetText(const Value: String);
  protected
    function CreateRender:TRender; override;
  public
    procedure CalcHeight(const ATotal:Single); override;
    class function Description:String; override;
    procedure Paint(var AData:TRenderData; const ARender:TRender); override;

    property TextRender:TCellRender read GetTextRender;
  published
    property Text:String read GetText write SetText;
  end;

  // Collection of Grid Bands
  TGridBands=class(TCollectionChange)
  private
    FVisible : Boolean;

    function Get(Index: Integer): TGridBand;
    procedure Put(Index: Integer; const Value: TGridBand);
    procedure SetVisible(const Value: Boolean);
  public
    const
      Spacing=0;

    var
      Floating : Boolean;
      Height : Single;

    Constructor Create(AOwner: TPersistent; const AChanged:TNotifyEvent); reintroduce;

    procedure Assign(Source:TPersistent); override;

    function AddText(const AText:String):TTextBand;

    procedure CalcHeight(const ATotal:Single);
    function CanDisplay:Boolean; inline;
    procedure Mouse(var AState:TMouseState; const AWidth,AHeight:Single);
    procedure Paint(var AData:TRenderData);

    property Items[Index: Integer]: TGridBand read Get write Put; default;
  published
    property Visible:Boolean read FVisible write SetVisible default True;
  end;

implementation

uses
  {$IFNDEF FPC}
  {System.}Types,
  {$ENDIF}

  {$IFNDEF NOUITYPES}
  {System.}UITypes,
  {$ENDIF}

  Tee.Painter;

{ TGridBand }

Constructor TGridBand.Create(ACollection:TCollection);
begin
  inherited;
  FHeight:=TBandHeight.Create(Changed);
end;

{$IFNDEF AUTOREFCOUNT}
Destructor TGridBand.Destroy;
begin
  FHeight.Free;
  inherited;
end;
{$ENDIF}

class function TGridBand.Description: String;
begin
  result:=ClassName;

  if Copy(result,1,1)='T' then
     result:=Copy(result,2,Length(result));
end;

procedure TGridBand.Changed(Sender: TObject);
begin
  TGridBands(Collection).DoChanged(Self);
end;

procedure TGridBand.SetHeight(const Value: TBandHeight);
begin
  FHeight.Assign(Value);
end;

function TGridBand.Contains(const X,Y:Single): Boolean;
begin
  result:=Visible and (Y>=Top) and (Y<=(Top+Height.Pixels));
end;

procedure TGridBand.Assign(Source: TPersistent);
begin
  if Source is TGridBand then
     Height:=TGridBand(Source).FHeight
  else
     inherited;
end;

procedure TGridBand.CalcHeight(const ATotal:Single);
begin
  if Height.Automatic then
  begin
    if FRender is TTextRender then
       Height.Pixels:=TTextRender(FRender).CalcHeight
  end
  else
     Height.Calculate(ATotal);
end;

function TGridBand.Mouse(var AState:TMouseState; const AWidth,AHeight:Single): Boolean;
begin
  result:=False;
end;

procedure TGridBand.Paint(var AData: TRenderData; const ARender: TRender);
begin
  inherited;
  Top:=AData.Rect.Top;
end;

{ TTextBand }

function TTextBand.CreateRender:TRender;
begin
  result:=TCellRender.Create(Changed);
  TCellRender(result).TextAlign.InitVertical(TVerticalAlign.Center);
end;

class function TTextBand.Description: String;
begin
  result:='Text';
end;

function TTextBand.GetTextRender: TCellRender;
begin
  if Render is TCellRender then
     result:=TCellRender(FRender)
  else
     result:=nil;
end;

function TTextBand.GetText: String;
begin
  result:=(Render as TCellRender).Text;
end;

procedure TTextBand.CalcHeight(const ATotal: Single);
begin
  inherited;

  if Height.Automatic then
     Height.Pixels:=(Render as TCellRender).CalcHeight;
end;

procedure TTextBand.Paint(var AData:TRenderData; const ARender:TRender);
begin
  AData.Text:=Text;

  CalcHeight(AData.Rect.Height);

  AData.Rect.Bottom:=AData.Rect.Top+Height.Pixels;

  AData.Painter.SetFont(Format.Font);

  inherited;
end;

procedure TTextBand.SetText(const Value: String);
begin
  (Render as TCellRender).Text:=Value;
end;

{ TGridBands }

Constructor TGridBands.Create(AOwner: TPersistent;
  const AChanged: TNotifyEvent);
begin
  inherited Create(AOwner,TGridBand);

  FVisible:=True;

  OnChanged:=AChanged;
end;

function TGridBands.Get(Index: Integer): TGridBand;
begin
  result:=TGridBand(inherited Items[Index]);
end;

function TGridBands.AddText(const AText: String): TTextBand;
begin
  result:=TTextBand.Create(Self);
  result.Text:=AText;
end;

procedure TGridBands.Assign(Source: TPersistent);
begin
  if Source is TGridBands then
     FVisible:=TGridBands(Source).FVisible;

  inherited;
end;

procedure TGridBands.CalcHeight(const ATotal:Single);
var t : Integer;
    tmp : TGridBand;
begin
  Height:=0;

  for t:=0 to Count-1 do
  begin
    tmp:=Items[t];

    if tmp.Visible then
    begin
      tmp.CalcHeight(ATotal);
      Height:=Height+tmp.Height.Pixels+Spacing;
    end;
  end;
end;

function TGridBands.CanDisplay: Boolean;
begin
  result:=FVisible and (Count>0);
end;

procedure TGridBands.Mouse(var AState:TMouseState; const AWidth,AHeight:Single);
var t : Integer;
    tmp : TGridBand;
begin
  for t:=0 to Count-1 do
  begin
    tmp:=Items[t];

    if tmp.Visible then
       tmp.Mouse(AState,AWidth,AHeight);
  end;
end;

procedure TGridBands.Paint(var AData: TRenderData);

  procedure PaintBand(const ABand:TGridBand);
  var Old : TRectF;
  begin
    AData.Rect.Bottom:=AData.Rect.Top+ABand.Height.Pixels+Spacing;

    Old:=AData.Rect;

    ABand.Paint(AData,nil);

    AData.Rect:=Old;

    AData.Rect.Top:=AData.Rect.Bottom;
  end;

var t : Integer;
    tmp : TGridBand;
begin
  for t:=0 to Count-1 do
  begin
    tmp:=Items[t];

    if tmp.Visible then
       PaintBand(tmp);
  end;
end;

procedure TGridBands.Put(Index: Integer; const Value: TGridBand);
begin
  inherited Items[Index]:=Value;
end;

procedure TGridBands.SetVisible(const Value: Boolean);
begin
  if FVisible<>Value then
  begin
    FVisible:=Value;
    DoChanged(Self);
  end;
end;

{ TGridBandLines }

Constructor TGridBandLines.Create(ACollection:TCollection);
begin
  inherited;

  FLines:=TStroke.Create(Changed);
  FLines.Brush.InitColor($D0D0D0);
end;

{$IFNDEF AUTOREFCOUNT}
Destructor TGridBandLines.Destroy;
begin
  FLines.Free;
  inherited;
end;
{$ENDIF}

procedure TGridBandLines.SetLines(const Value: TStroke);
begin
  FLines.Assign(Value);
end;

end.
