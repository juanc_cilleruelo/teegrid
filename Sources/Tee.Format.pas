{*********************************************}
{  TeeGrid Software Library                   }
{  Abstract Formatting classes                }
{  Copyright (c) 2016 by Steema Software      }
{  All Rights Reserved                        }
{*********************************************}
unit Tee.Format;
{$I Tee.inc}
{$SCOPEDENUMS ON}

{
   Basic agnostic classes for typical formatting properties:

   TFont
   TStroke (pen)
   TBrush
   TPicture
   TGradient

   and other small classes derived from TPersistent to reuse them in
   higher-level containers:

   TFormat (Brush and Stroke)

   TVisibleFormat (TFormat and Visible:Boolean)

   TTextFormat  (TFormat and TFont)

   TCoordinate  (Value:Single and Units: pixels or % )


   Needs: (none)

}

interface

uses
  {$IFDEF FPC}
  Graphics,
  {$ELSE}
  {$IFNDEF NOUITYPES}
  System.UITypes,
  {$ENDIF}
  {$ENDIF}
  {System.}Classes;

type
  TPersistentChange=class(TPersistent)
  protected
    IChanged : TNotifyEvent;

    procedure ChangeBoolean(var Variable:Boolean; const Value: Boolean);
    procedure ChangeInteger(var Variable: Integer; const Value: Integer);
    procedure ChangeSingle(var Variable:Single; const Value: Single);
    procedure ChangeString(var Variable:String; const Value: String);
  public
    Constructor Create(const AChanged:TNotifyEvent); virtual;

    procedure Changed;

    property OnChange:TNotifyEvent read IChanged write IChanged;
  end;

  // Helper TCollection with OnChanged event
  TCollectionChange=class(TOwnedCollection)
  private
    FOnChanged : TNotifyEvent;
  protected
    procedure Update(Item: TCollectionItem); override;
  public
    procedure DoChanged(Sender:TObject);
  published
    property OnChanged:TNotifyEvent read FOnChanged write FOnChanged;
  end;

  TVisiblePersistentChange=class(TPersistentChange)
  private
    FVisible: Boolean;

    IDefaultVisible : Boolean;

    procedure SetVisible(const Value: Boolean);
    function IsVisibleStored: Boolean;
  public
    Constructor Create(const AChanged:TNotifyEvent); override;

    procedure Assign(Source:TPersistent); override;
    procedure Hide;
    procedure InitVisible(const Value:Boolean);
    procedure Show;
  published
    property Visible:Boolean read FVisible write SetVisible stored IsVisibleStored;
  end;

  {$IFDEF FPC}
  TColor=Cardinal;
  {$ELSE}
  {$IF CompilerVersion>22}
  TColor=TAlphaColor;
  {$ELSE}
  TColor=Cardinal;
  {$IFEND}
  {$ENDIF}

  TColorOffset=Single;

  TGradientColor=class(TCollectionItem)
  private
    FColor: TColor;
    FOffset: TColorOffset;

    procedure DoChanged;
    procedure SetColor(const Value: TColor);
    procedure SetOffset(const Value: TColorOffset);
  public
    procedure Assign(Source:TPersistent); override;
  published
    property Color:TColor read FColor write SetColor;
    property Offset:TColorOffset read FOffset write SetOffset;
  end;

  TGradientColors=class(TCollectionChange)
  private
    function Get(const Index: Integer): TGradientColor; {$IFNDEF FPC}inline;{$ENDIF}
    procedure Put(const Index: Integer; const Value: TGradientColor); {$IFNDEF FPC}inline;{$ENDIF}
  public
    function Add:TGradientColor; overload; {$IFNDEF FPC}inline;{$ENDIF}
    function Add(const AColor:TColor; const AOffset:TColorOffset):TGradientColor; overload;

    property Items[const Index:Integer]:TGradientColor read Get write Put; default;
  end;

  TAngle=Single;

  TGradientDirection=(Vertical,Horizontal,Diagonal,BackDiagonal,Radial);

  TGradient=class(TVisiblePersistentChange)
  private
    FDirection: TGradientDirection;
    FColors: TGradientColors;
    FAngle: TAngle;
    FInverted: Boolean;

    procedure SetAngle(const Value: TAngle);
    procedure SetColors(const Value: TGradientColors);
    procedure SetDirection(const Value: TGradientDirection);
    procedure SetInverted(const Value: Boolean);
    function IsColorsStored: Boolean;
  public
    Constructor Create(const AChanged:TNotifyEvent); override;

    {$IFNDEF AUTOREFCOUNT}
    Destructor Destroy; override;
    {$ENDIF}

    procedure Assign(Source:TPersistent); override;
  published
    property Angle:TAngle read FAngle write SetAngle;
    property Colors:TGradientColors read FColors write SetColors stored IsColorsStored;

    property Direction:TGradientDirection read FDirection write SetDirection
                              default TGradientDirection.Vertical;

    property Inverted:Boolean read FInverted write SetInverted default False;
  end;

  TPicture=class(TPersistentChange)
  protected
    Internal,
    Original,
    TagObject : TObject;

    procedure FreeInternal;
  public
    Destructor Destroy; override;

    procedure Clear;
    procedure SetGraphic(const AObject:TObject);
  end;

  {$IFNDEF FPC}

  {$IF CompilerVersion<=22}
  TFontStyle=(fsBold, fsItalic, fsUnderline, fsStrikeOut);
  TFontStyles=set of TFontStyle;
  {$IFEND}

  {$ENDIF}

  TColorHelper=record
  public
    class function From(const AColor:TColor; const AOpacity:Single):TColor; static;
    class function Opacity(const AColor: TColor):Byte; inline; static;
    class function RemoveOpacity(const AColor: TColor):TColor; inline; static;
    class function Split(const AColor:TColor; out AOpacity:Single):TColor; static;
    class function Swap(const AColor:TColor):TColor; static;
    class function SwapCheck(const AColor:TColor):TColor; static;
  end;

  TBrush=class(TVisiblePersistentChange)
  private
    FColor: TColor;
    FGradient: TGradient;
    FPicture: TPicture;

    IDefaultColor : TColor;

    function GetGradient:TGradient;
    function GetPicture: TPicture;

    function IsColorStored:Boolean;
    function IsGradientStored: Boolean; inline;
    function IsPictureStored: Boolean;

    procedure SetColor(const Value: TColor);
    procedure SetGradient(const Value: TGradient);
    procedure SetPicture(const Value: TPicture);
  protected
    function HasGradient:Boolean; inline;
    function HasPicture: Boolean; inline;
  public
    Constructor Create(const AChanged:TNotifyEvent); override;

    {$IFNDEF AUTOREFCOUNT}
    Destructor Destroy; override;
    {$ENDIF}

    procedure Assign(Source:TPersistent); override;

    procedure InitColor(const AColor:TColor);
  published
    property Color:TColor read FColor write SetColor stored IsColorStored;
    property Gradient:TGradient read GetGradient write SetGradient stored IsGradientStored;
    property Picture:TPicture read GetPicture write SetPicture stored IsPictureStored;
  end;

  TFont=class(TPersistentChange)
  private
    FBrush : TBrush;
    FName: String;
    FSize: Single;
    FStyle : TFontStyles;

    DefaultStyle : TFontStyles;

    function GetColor: TColor; inline;
    function IsNameStored: Boolean;
    function IsSizeStored: Boolean;
    function IsStyleStored: Boolean;
    procedure SetBrush(const Value: TBrush);
    procedure SetColor(const Value: TColor); inline;
    procedure SetName(const Value: String);
    procedure SetSize(const Value: Single);
    procedure SetStyle(const Value: TFontStyles);
  public
    class var
      DefaultSize : Single;
      DefaultName : String;

    Constructor Create(const AChanged:TNotifyEvent); override;

    {$IFNDEF AUTOREFCOUNT}
    Destructor Destroy; override;
    {$ENDIF}

    procedure Assign(Source:TPersistent); override;

    procedure InitColor(const AColor:TColor);
    procedure InitStyle(const AStyle:TFontStyles);
  published
    property Brush:TBrush read FBrush write SetBrush;
    property Color:TColor read GetColor write SetColor stored False;
    property Name:String read FName write SetName stored IsNameStored;
    property Size:Single read FSize write SetSize stored IsSizeStored;
    property Style:TFontStyles read FStyle write SetStyle stored IsStyleStored;
  end;

  TStrokeStyle=(Solid,Dash,Dot,DashDot,DashDotDot,Custom);

  TStrokeEnd=(Round,Square,Flat);
  TStrokeJoin=(Round,Bevel,Mitter);

  TStroke=class(TVisiblePersistentChange)
  private
    FBrush: TBrush;
    FEnd: TStrokeEnd;
    FJoin: TStrokeJoin;
    FSize: Single;
    FStyle: TStrokeStyle;

    IDefaultStyle : TStrokeStyle;

    function GetColor: TColor; inline;
    function IsSizeStored: Boolean;
    function IsStyleStored: Boolean;
    procedure SetBrush(const Value: TBrush);
    procedure SetColor(const Value: TColor);
    procedure SetSize(const Value: Single);
    procedure SetStyle(const Value: TStrokeStyle);
    procedure SetEnd(const Value: TStrokeEnd);
    procedure SetJoin(const Value: TStrokeJoin);
  public
    Constructor Create(const AChanged:TNotifyEvent); override;
    Constructor CreateColor(const AChanged:TNotifyEvent; const AColor:TColor);

    {$IFNDEF AUTOREFCOUNT}
    Destructor Destroy; override;
    {$ENDIF}

    procedure Assign(Source:TPersistent); override;

    procedure InitStyle(const Value:TStrokeStyle);
  published
    property Brush:TBrush read FBrush write SetBrush;
    property Color:TColor read GetColor write SetColor stored False;
    property EndStyle:TStrokeEnd read FEnd write SetEnd default TStrokeEnd.Flat;
    property JoinStyle:TStrokeJoin read FJoin write SetJoin default TStrokeJoin.Mitter;
    property Size:Single read FSize write SetSize stored IsSizeStored;
    property Style:TStrokeStyle read FStyle write SetStyle stored IsStyleStored;
  end;

  // TStroke with default Visible=False
  THiddenStroke=class(TStroke)
  public
    Constructor Create(const AChanged:TNotifyEvent); override;
  published
    property Visible default False;
  end;

  TFormat=class(TPersistentChange)
  private
    FBrush: TBrush;
    FStroke: TStroke;

    procedure SetBrush(const Value: TBrush);
    procedure SetStroke(const Value: TStroke);
  public
    Constructor Create(const AChanged:TNotifyEvent); override;

    {$IFNDEF AUTOREFCOUNT}
    Destructor Destroy; override;
    {$ENDIF}

    procedure Assign(Source:TPersistent); override;
  published
    property Brush:TBrush read FBrush write SetBrush;
    property Stroke:TStroke read FStroke write SetStroke;
  end;

  TSizeUnits=(Pixels,Percent);

  TCoordinate=class(TPersistentChange)
  private
    FAutomatic : Boolean;
    FUnits : TSizeUnits;
    FValue : Single;

    DefaultAutomatic : Boolean;
    DefaultValue : Single;

    function IsAutomaticStored: Boolean;
    function IsValueStored: Boolean;
    procedure SetAutomatic(const Value: Boolean);
    procedure SetUnits(const Value: TSizeUnits);
    procedure SetValue(const AValue: Single);
  public
    Pixels : Single; // <-- calculated

    Constructor Create(const AChanged:TNotifyEvent); override;

    procedure Assign(Source:TPersistent); override;

    function Calculate(const ATotal:Single):Single;
    procedure InitValue(const AValue:Single);
    procedure Prepare(const ATotal:Single);
  published
    property Automatic:Boolean read FAutomatic write SetAutomatic stored IsAutomaticStored;
    property Units:TSizeUnits read FUnits write SetUnits default TSizeUnits.Pixels;
    property Value:Single read FValue write SetValue stored IsValueStored;
  end;

  TVisibleFormat=class(TFormat)
  private
    FVisible : Boolean;
    DefaultVisible : Boolean;

    function IsVisibleStored: Boolean;
    procedure SetVisible(const Value: Boolean);
  public
    procedure Hide;
    procedure InitVisible(const Value:Boolean);
    procedure Show;
  published
    property Visible:Boolean read FVisible write SetVisible stored IsVisibleStored;
  end;

  TTextFormat=class(TFormat)
  private
    FFont: TFont;

    procedure SetFont(const Value: TFont);
  public
    Constructor Create(const AChanged:TNotifyEvent); override;

    {$IFNDEF AUTOREFCOUNT}
    Destructor Destroy; override;
    {$ENDIF}

    procedure Assign(Source:TPersistent); override;
    function TextHeight: Single;
  published
    property Font:TFont read FFont write SetFont;
  end;

  {$IFDEF NOUITYPES}
  TColors=record
  public
    const
      Aqua=$FFFF00;
      Black=0;
      Cream=$F0FBFF;
      DkGray=$808080;
      Green=$008000;
      LightGray=$D3D3D3;
      Navy=$800000;
      Red=$800000;
      SkyBlue=$87CEEB;
      White=$FFFFFF;
  end;
  {$ENDIF}

  // From TeeBI BI.UI.Colors.pas
  TUIColor=record
  public
    class function Interpolate(const AOld,ANew:TColor; const APercent:Single):TColor; static;
  end;

implementation

{$IFDEF NOUITYPES}
type
  TAlphaColorRec=record
  public
    A,B,G,R: Byte;
  end;
{$ENDIF}

{ TPersistentChange }

Constructor TPersistentChange.Create(const AChanged: TNotifyEvent);
begin
  inherited Create;
  IChanged:=AChanged;
end;

procedure TPersistentChange.ChangeSingle(var Variable: Single;
  const Value: Single);
begin
  if Variable<>Value then
  begin
    Variable:=Value;
    Changed;
  end;
end;

procedure TPersistentChange.ChangeString(var Variable: String;
  const Value: String);
begin
  if Variable<>Value then
  begin
    Variable:=Value;
    Changed;
  end;
end;

procedure TPersistentChange.ChangeBoolean(var Variable: Boolean;
  const Value: Boolean);
begin
  if Variable<>Value then
  begin
    Variable:=Value;
    Changed;
  end;
end;

procedure TPersistentChange.ChangeInteger(var Variable: Integer;
  const Value: Integer);
begin
  if Variable<>Value then
  begin
    Variable:=Value;
    Changed;
  end;
end;

procedure TPersistentChange.Changed;
begin
  if Assigned(IChanged) then
     IChanged(Self);
end;

{ TCollectionChange }

procedure TCollectionChange.DoChanged(Sender:TObject);
begin
  if Assigned(FOnChanged) then
     FOnChanged(Self);
end;

procedure TCollectionChange.Update(Item: TCollectionItem);
begin
  inherited;
  DoChanged(Self);
end;

{ TVisiblePersistentChange }

Constructor TVisiblePersistentChange.Create(const AChanged: TNotifyEvent);
begin
  inherited;

  IDefaultVisible:=True;
  FVisible:=True;
end;

procedure TVisiblePersistentChange.Hide;
begin
  Visible:=False;
end;

procedure TVisiblePersistentChange.Assign(Source: TPersistent);
begin
  if Source is TVisiblePersistentChange then
     FVisible:=TVisiblePersistentChange(Source).FVisible
  else
     inherited;
end;

procedure TVisiblePersistentChange.InitVisible(const Value: Boolean);
begin
  FVisible:=Value;
  IDefaultVisible:=Value;
end;

function TVisiblePersistentChange.IsVisibleStored: Boolean;
begin
  result:=FVisible<>IDefaultVisible;
end;

procedure TVisiblePersistentChange.SetVisible(const Value: Boolean);
begin
  ChangeBoolean(FVisible,Value);
end;

procedure TVisiblePersistentChange.Show;
begin
  Visible:=True;
end;

{ TColorHelper }

class function TColorHelper.From(const AColor:TColor; const AOpacity:Single):TColor;
begin
  TAlphaColorRec(result).R:=TAlphaColorRec(AColor).R;
  TAlphaColorRec(result).G:=TAlphaColorRec(AColor).G;
  TAlphaColorRec(result).B:=TAlphaColorRec(AColor).B;
  TAlphaColorRec(result).A:=Round(AOpacity*255);
end;

class function TColorHelper.Opacity(const AColor: TColor):Byte;
begin
  result:=(AColor and $FF000000) shr 24;
end;

class function TColorHelper.RemoveOpacity(const AColor: TColor): TColor;
begin
  result:=AColor and not $FF000000;
end;

class function TColorHelper.Split(const AColor: TColor; out AOpacity: Single): TColor;
const Div255=1/255;
begin
  result:=RemoveOpacity(AColor);
  AOpacity:=Div255*Opacity(AColor);
end;

class function TColorHelper.Swap(const AColor:TColor):TColor;
begin
  TAlphaColorRec(result).R:=TAlphaColorRec(AColor).B;
  TAlphaColorRec(result).G:=TAlphaColorRec(AColor).G;
  TAlphaColorRec(result).B:=TAlphaColorRec(AColor).R;
  TAlphaColorRec(result).A:=TAlphaColorRec(AColor).A;
end;

class function TColorHelper.SwapCheck(const AColor:TColor):TColor;
begin
  result:=Swap(AColor);

  if Opacity(AColor)=0 then
     TAlphaColorRec(result).A:=$FF;
end;

{ TBrush }

Constructor TBrush.Create(const AChanged: TNotifyEvent);
begin
  inherited;

  FColor:=TColors.White;
  IDefaultColor:=FColor;
end;

{$IFNDEF AUTOREFCOUNT}
Destructor TBrush.Destroy;
begin
  FGradient.Free;
  FPicture.Free;
  inherited;
end;
{$ENDIF}

procedure TBrush.Assign(Source: TPersistent);
begin
  if Source is TBrush then
  begin
    Gradient:=TBrush(Source).FGradient;
    FColor:=TBrush(Source).FColor;
    Picture:=TBrush(Source).FPicture;
  end;

  inherited;
end;

function TBrush.GetGradient:TGradient;
begin
  if FGradient=nil then
  begin
    FGradient:=TGradient.Create(IChanged);
    FGradient.InitVisible(False);
  end;

  result:=FGradient;
end;

function TBrush.GetPicture: TPicture;
begin
  if FPicture=nil then
     FPicture:=TPicture.Create(IChanged);

  result:=FPicture;
end;

function TBrush.HasGradient: Boolean;
begin
  result:=(FGradient<>nil) and FGradient.Visible;
end;

function TBrush.HasPicture: Boolean;
begin
  result:=(FPicture<>nil) and (FPicture.TagObject<>nil);
end;

procedure TBrush.InitColor(const AColor: TColor);
begin
  FColor:=AColor;
  IDefaultColor:=FColor;
end;

function TBrush.IsColorStored: Boolean;
begin
  result:=FColor<>IDefaultColor;
end;

function TBrush.IsGradientStored: Boolean;
begin
  result:=FGradient<>nil;
end;

function TBrush.IsPictureStored: Boolean;
begin
  result:=FPicture<>nil;
end;

procedure TBrush.SetColor(const Value: TColor);
begin
  if FColor<>Value then
  begin
    FColor:=Value;
    Changed;
  end;
end;

procedure TBrush.SetGradient(const Value: TGradient);
begin
  if Value=nil then
  begin
    FGradient.Free;
    FGradient:=nil;
  end
  else
    Gradient.Assign(Value);
end;

procedure TBrush.SetPicture(const Value: TPicture);
begin
  if Value=nil then
  begin
    FPicture.Free;
    FPicture:=nil;
  end
  else
    Picture.Assign(Value);
end;

{ TFormat }

Constructor TFormat.Create(const AChanged: TNotifyEvent);
begin
  inherited;
  FBrush:=TBrush.Create(AChanged);
  FStroke:=TStroke.Create(AChanged);
end;

{$IFNDEF AUTOREFCOUNT}
Destructor TFormat.Destroy;
begin
  FStroke.Free;
  FBrush.Free;
  inherited;
end;
{$ENDIF}

procedure TFormat.Assign(Source: TPersistent);
begin
  if Source is TFormat then
  begin
    Brush:=TFormat(Source).FBrush;
    Stroke:=TFormat(Source).FStroke;
  end
  else
    inherited;
end;

procedure TFormat.SetBrush(const Value: TBrush);
begin
  FBrush.Assign(Value);
end;

procedure TFormat.SetStroke(const Value: TStroke);
begin
  FStroke.Assign(Value);
end;

{ TCoordinate }

Constructor TCoordinate.Create(const AChanged:TNotifyEvent);
begin
  inherited;

  DefaultAutomatic:=True;
  FAutomatic:=True;
end;

procedure TCoordinate.InitValue(const AValue: Single);
begin
  FAutomatic:=False;
  DefaultAutomatic:=False;

  FValue:=AValue;
  DefaultValue:=FValue;

  if FUnits=TSizeUnits.Pixels then
     Pixels:=FValue;
end;

function TCoordinate.IsAutomaticStored: Boolean;
begin
  result:=FAutomatic<>DefaultAutomatic;
end;

function TCoordinate.IsValueStored: Boolean;
begin
  result:=FValue<>DefaultValue;
end;

procedure TCoordinate.Prepare(const ATotal: Single);
begin
  if Units=TSizeUnits.Pixels then
     Pixels:=Value
  else
     Pixels:=Calculate(ATotal);
end;

procedure TCoordinate.Assign(Source: TPersistent);
begin
  if Source is TCoordinate then
  begin
    FAutomatic:=TCoordinate(Source).FAutomatic;
    FUnits:=TCoordinate(Source).FUnits;
    FValue:=TCoordinate(Source).FValue;
  end
  else
    inherited;
end;

function TCoordinate.Calculate(const ATotal: Single): Single;
begin
  if Units=TSizeUnits.Pixels then
     result:=Value
  else
     result:=Value*ATotal*0.01;
end;

procedure TCoordinate.SetAutomatic(const Value: Boolean);
begin
  ChangeBoolean(FAutomatic,Value);
end;

procedure TCoordinate.SetUnits(const Value: TSizeUnits);
begin
  if FUnits<>Value then
  begin
    FUnits:=Value;
    Changed;
  end;
end;

procedure TCoordinate.SetValue(const AValue: Single);
begin
  if FValue<>AValue then
  begin
    FValue:=AValue;

    FAutomatic:=False;

    if FUnits=TSizeUnits.Pixels then
       Pixels:=FValue
    else
       Pixels:=0;

    Changed;
  end;
end;

{ TTextFormat }

Constructor TTextFormat.Create(const AChanged: TNotifyEvent);
begin
  inherited;
  FFont:=TFont.Create(AChanged);
end;

{$IFNDEF AUTOREFCOUNT}
Destructor TTextFormat.Destroy;
begin
  FFont.Free;
  inherited;
end;
{$ENDIF}

procedure TTextFormat.Assign(Source: TPersistent);
begin
  if Source is TTextFormat then
     Font:=TTextFormat(Source).FFont;

  inherited;
end;

procedure TTextFormat.SetFont(const Value: TFont);
begin
  FFont.Assign(Value);
end;

function TTextFormat.TextHeight: Single;
begin
  result:=2*Font.Size*(96/72);
end;

{ TFont }

Constructor TFont.Create(const AChanged: TNotifyEvent);
begin
  inherited;

  FName:=DefaultName;

  DefaultStyle:=[];

  FBrush:=TBrush.Create(AChanged);
  FBrush.InitColor(TColors.Black);

  FSize:=DefaultSize;
end;

{$IFNDEF AUTOREFCOUNT}
Destructor TFont.Destroy;
begin
  FBrush.Free;
  inherited;
end;
{$ENDIF}

procedure TFont.Assign(Source: TPersistent);
begin
  if Source is TFont then
  begin
    Brush:=TFont(Source).FBrush;
    FName:=TFont(Source).Name;
    FSize:=TFont(Source).FSize;
    FStyle:=TFont(Source).FStyle;
  end
  else
    inherited;
end;

function TFont.GetColor: TColor;
begin
  result:=Brush.Color;
end;

procedure TFont.InitColor(const AColor: TColor);
begin
  FBrush.InitColor(AColor);
end;

procedure TFont.InitStyle(const AStyle: TFontStyles);
begin
  FStyle:=AStyle;
  DefaultStyle:=AStyle;
end;

function TFont.IsNameStored: Boolean;
begin
  result:=FName<>DefaultName;
end;

function TFont.IsSizeStored: Boolean;
begin
  result:=FSize<>DefaultSize;
end;

function TFont.IsStyleStored: Boolean;
begin
  result:=FStyle<>DefaultStyle;
end;

procedure TFont.SetBrush(const Value: TBrush);
begin
  FBrush.Assign(Value);
end;

procedure TFont.SetColor(const Value: TColor);
begin
  Brush.Color:=Value;
end;

procedure TFont.SetName(const Value: String);
begin
  ChangeString(FName,Value);
end;

procedure TFont.SetSize(const Value: Single);
begin
  if FSize<>Value then
  begin
    FSize:=Value;
    Changed;
  end;
end;

procedure TFont.SetStyle(const Value: TFontStyles);
begin
  if FStyle<>Value then
  begin
    FStyle:=Value;
    Changed;
  end;
end;

{ TStroke }

Constructor TStroke.Create(const AChanged: TNotifyEvent);
begin
  inherited;

  FBrush:=TBrush.Create(AChanged);
  FBrush.InitColor(TColors.Black);

  FEnd:=TStrokeEnd.Flat;
  FJoin:=TStrokeJoin.Mitter;

  FSize:=1;
end;

Constructor TStroke.CreateColor(const AChanged: TNotifyEvent;
  const AColor: TColor);
begin
  Create(AChanged);
  Brush.InitColor(AColor);
end;

{$IFNDEF AUTOREFCOUNT}
Destructor TStroke.Destroy;
begin
  FBrush.Free;
  inherited;
end;
{$ENDIF}

function TStroke.GetColor: TColor;
begin
  result:=FBrush.Color;
end;

procedure TStroke.Assign(Source: TPersistent);
begin
  if Source is TStroke then
  begin
    Brush:=TStroke(Source).Brush;
    FEnd:=TStroke(Source).FEnd;
    FJoin:=TStroke(Source).FJoin;
    FSize:=TStroke(Source).FSize;
    FStyle:=TStroke(Source).FStyle;
  end;

  inherited;
end;

procedure TStroke.InitStyle(const Value: TStrokeStyle);
begin
  FStyle:=Value;
  IDefaultStyle:=Value;
end;

function TStroke.IsSizeStored: Boolean;
begin
  result:=FSize<>1;
end;

function TStroke.IsStyleStored: Boolean;
begin
  result:=FStyle<>IDefaultStyle;
end;

procedure TStroke.SetBrush(const Value: TBrush);
begin
  FBrush.Assign(Value);
end;

procedure TStroke.SetColor(const Value: TColor);
begin
  FBrush.Color:=Value;
end;

procedure TStroke.SetEnd(const Value: TStrokeEnd);
begin
  if FEnd<>Value then
  begin
    FEnd:=Value;
    Changed;
  end;
end;

procedure TStroke.SetJoin(const Value: TStrokeJoin);
begin
  if FJoin<>Value then
  begin
    FJoin:=Value;
    Changed;
  end;
end;

procedure TStroke.SetSize(const Value: Single);
begin
  if FSize<>Value then
  begin
    FSize:=Value;
    Changed;
  end;
end;

procedure TStroke.SetStyle(const Value: TStrokeStyle);
begin
  if FStyle<>Value then
  begin
    FStyle:=Value;
    Changed;
  end;
end;

{ THiddenStroke }

Constructor THiddenStroke.Create(const AChanged: TNotifyEvent);
begin
  inherited;
  InitVisible(False);
end;

{ TGradient }

Constructor TGradient.Create(const AChanged: TNotifyEvent);
begin
  inherited;

  FColors:=TGradientColors.Create(Self,TGradientColor);

  FColors.Add(TColors.White,0);
  FColors.Add(TColors.LightGray,1);

  FColors.OnChanged:=IChanged;
end;

{$IFNDEF AUTOREFCOUNT}
Destructor TGradient.Destroy;
begin
  FColors.Free;
  inherited;
end;
{$ENDIF}

function TGradient.IsColorsStored: Boolean;

  function ColorEquals(const AItem:TGradientColor; const AColor:TColor; const AOffset:Single):Boolean;
  begin
    result:=(AItem.Color=AColor) and (AItem.Offset=AOffset);
  end;

begin
  result:=(Colors.Count<>2) or
          (not ColorEquals(Colors[0],TColors.White,0)) or
          (not ColorEquals(Colors[1],TColors.LightGray,1));
end;

procedure TGradient.Assign(Source: TPersistent);
begin
  if Source is TGradient then
  begin
    FAngle:=TGradient(Source).FAngle;
    Colors:=TGradient(Source).FColors;
    FDirection:=TGradient(Source).FDirection;
    FInverted:=TGradient(Source).FInverted;
  end
  else
    inherited;
end;

procedure TGradient.SetAngle(const Value: TAngle);
begin
  ChangeSingle(FAngle,Value);
end;

procedure TGradient.SetColors(const Value: TGradientColors);
begin
  FColors.Assign(Value);
end;

procedure TGradient.SetDirection(const Value: TGradientDirection);
begin
  if FDirection<>Value then
  begin
    FDirection:=Value;
    Changed;
  end;
end;

procedure TGradient.SetInverted(const Value: Boolean);
begin
  ChangeBoolean(FInverted,Value);
end;

{ TVisibleFormat }

procedure TVisibleFormat.Hide;
begin
  Visible:=False;
end;

procedure TVisibleFormat.InitVisible(const Value: Boolean);
begin
  FVisible:=Value;
  DefaultVisible:=Value;
end;

function TVisibleFormat.IsVisibleStored: Boolean;
begin
  result:=FVisible<>DefaultVisible;
end;

procedure TVisibleFormat.SetVisible(const Value: Boolean);
begin
  ChangeBoolean(FVisible,Value);
end;

procedure TVisibleFormat.Show;
begin
  Visible:=True;
end;

{ TGradientColor }

procedure TGradientColor.Assign(Source: TPersistent);
begin
  if Source is TGradientColor then
  begin
    FColor:=TGradientColor(Source).FColor;
    FOffset:=TGradientColor(Source).FOffset;
  end
  else
    inherited;
end;

procedure TGradientColor.DoChanged;
begin
  TGradientColors(Collection).Changed;
end;

procedure TGradientColor.SetColor(const Value: TColor);
begin
  if FColor<>Value then
  begin
    FColor:=Value;
    DoChanged;
  end;
end;

procedure TGradientColor.SetOffset(const Value: TColorOffset);
begin
  if FOffset<>Value then
  begin
    FOffset:=Value;
    DoChanged;
  end;
end;

{ TGradientColors }

function TGradientColors.Add: TGradientColor;
begin
  result:=inherited Add as TGradientColor;
end;

function TGradientColors.Add(const AColor: TColor;
  const AOffset: TColorOffset): TGradientColor;
begin
  result:=Add;

  result.FColor:=AColor;
  result.FOffset:=AOffset;
end;

function TGradientColors.Get(const Index: Integer): TGradientColor;
begin
  result:=TGradientColor(inherited Items[Index]);
end;

procedure TGradientColors.Put(const Index: Integer;
  const Value: TGradientColor);
begin
  inherited Items[Index]:=Value;
end;

{ TPicture }

procedure TPicture.Clear;
begin
  SetGraphic(nil);
end;

Destructor TPicture.Destroy;
begin
  FreeInternal;
  inherited;
end;

procedure TPicture.FreeInternal;
begin
  Internal.Free;
  Internal:=nil;

  Original.Free;
  Original:=nil;
end;

procedure TPicture.SetGraphic(const AObject: TObject);
begin
  TagObject:=AObject;
  FreeInternal;
end;

{ TUIColor }

// From TeeBI BI.UI.Colors.pas
class function TUIColor.Interpolate(const AOld, ANew: TColor; const APercent: Single): TColor;
var IRange: TAlphaColorRec;
begin
  IRange.A:=TAlphaColorRec(AOld).A-TAlphaColorRec(ANew).A;
  IRange.R:=TAlphaColorRec(AOld).R-TAlphaColorRec(ANew).R;
  IRange.G:=TAlphaColorRec(AOld).G-TAlphaColorRec(ANew).G;
  IRange.B:=TAlphaColorRec(AOld).B-TAlphaColorRec(ANew).B;

  {$IFDEF FPC}
  TAlphaColorRec(result).A:=0;
  {$ELSE}
  TAlphaColorRec(result).A:=TAlphaColorRec(ANew).A+Round(APercent*IRange.A);
  {$ENDIF}

  TAlphaColorRec(result).R:=TAlphaColorRec(ANew).R+Round(APercent*IRange.R);
  TAlphaColorRec(result).G:=TAlphaColorRec(ANew).G+Round(APercent*IRange.G);
  TAlphaColorRec(result).B:=TAlphaColorRec(ANew).B+Round(APercent*IRange.B);
end;

end.
