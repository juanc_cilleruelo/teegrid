{*********************************************}
{  TeeGrid Software Library                   }
{  Base abstract Control class                }
{  Copyright (c) 2016 by Steema Software      }
{  All Rights Reserved                        }
{*********************************************}
unit Tee.Control;
{$I Tee.inc}

interface

uses
  {System.}Classes,

  {$IFNDEF FPC}
  {System.}Types,
  {$ENDIF}

  Tee.Format, Tee.Painter;

type
  TCustomTeeControl=class(TComponent)
  private
    FBack: TFormat;

    IChanged : TNotifyEvent;

    procedure SetBack(const Value: TFormat);
  protected
    IUpdating : Integer;

    procedure BeginUpdate; inline;
    procedure EndUpdate;

    procedure Changed(Sender:TObject);
    property OnChange:TNotifyEvent read IChanged write IChanged;
  public
    Constructor Create(AOwner:TComponent); override;
    Destructor Destroy; override;

    procedure Assign(Source:TPersistent); override;

    procedure Paint; virtual;

    function ClientHeight:Single; virtual;
    function ClientWidth:Single; virtual;
    function Height:Single; virtual; abstract;
    function Painter:TPainter; virtual; abstract;
    function Width:Single; virtual; abstract;
  published
    property Back:TFormat read FBack write SetBack;
  end;

implementation

{$IFNDEF FPC}
{$IF CompilerVersion>22}
uses
  System.UITypes;
{$IFEND}
{$ENDIF}

{ TCustomTeeControl }

Constructor TCustomTeeControl.Create(AOwner: TComponent);
begin
  inherited;

  FBack:=TFormat.Create(Changed);
  FBack.Brush.InitColor(TColors.White);
  FBack.Stroke.InitVisible(False);
end;

Destructor TCustomTeeControl.Destroy;
begin
  FBack.Free;
  inherited;
end;

procedure TCustomTeeControl.EndUpdate;
begin
  Dec(IUpdating);

  if IUpdating=0 then
     Changed(Self);
end;

procedure TCustomTeeControl.Paint;
begin
  Painter.Paint(Back,TRectF.Create(0,0,ClientWidth,ClientHeight));
end;

procedure TCustomTeeControl.Assign(Source: TPersistent);
begin
  if Source is TCustomTeeControl then
     Back:=TCustomTeeControl(Source).FBack
  else
     inherited;
end;

procedure TCustomTeeControl.BeginUpdate;
begin
  Inc(IUpdating);
end;

procedure TCustomTeeControl.Changed(Sender: TObject);
begin
  if (IUpdating=0) and Assigned(IChanged) then
     IChanged(Self);
end;

function TCustomTeeControl.ClientHeight: Single;
begin
  result:=Height;
end;

function TCustomTeeControl.ClientWidth: Single;
begin
  result:=Width;
end;

procedure TCustomTeeControl.SetBack(const Value: TFormat);
begin
  FBack.Assign(Value);
end;

end.
