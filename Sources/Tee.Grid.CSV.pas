{*********************************************}
{  TeeGrid Software Library                   }
{  CSV Data Export                            }
{  Copyright (c) 2016 by Steema Software      }
{  All Rights Reserved                        }
{*********************************************}
unit Tee.Grid.CSV;

interface

{
  Returns a string with all cell contents from a Grid Selected range, in
  CSV (comma separated values) format.

  Supports single cell and range-selection.

  Example:

    var S : String;

    S:= TCSVData.From( TeeGrid1.Grid, TeeGrid1.Selected );
}

uses
  Tee.Grid, Tee.Grid.Selection;

type
  TCSVData=record
  public
    class function From(const AGrid:TCustomTeeGrid; const ASelected:TGridSelection):String; static;
  end;

implementation

uses
  Tee.Grid.Columns;

{ TCSVData }

class function TCSVData.From(const AGrid:TCustomTeeGrid; const ASelected: TGridSelection): String;
var
  tmpFrom,
  tmpTo : Integer;

  function RowAsText(const ARow:Integer):String;
  var t : Integer;
  begin
    result:='';

    for t:=tmpFrom to tmpTo do
    begin
      if t>tmpFrom then
         result:=result+#9;

      result:=result+AGrid.Data.AsString(AGrid.Rows.VisibleColumns[t],ARow);
    end;
  end;

var t : Integer;
    tmpFromRow,
    tmpToRow: Integer;
begin
  result:='';

  if not ASelected.IsEmpty then
  begin
    ASelected.GetColumns(AGrid.Rows.VisibleColumns,tmpFrom,tmpTo);

    tmpFromRow:=ASelected.Range.FromRow;
    tmpToRow:=ASelected.Range.ToRow;

    for t:=tmpFromRow to tmpToRow do
    begin
      if t>tmpFromRow then
         result:=result+#13#10;

      result:=result+RowAsText(t);
    end;
  end;
end;

end.

