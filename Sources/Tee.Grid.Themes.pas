{*********************************************}
{  TeeGrid Software Library                   }
{  Themes                                     }
{  Copyright (c) 2016 by Steema Software      }
{  All Rights Reserved                        }
{*********************************************}
unit Tee.Grid.Themes;
{$I Tee.inc}

interface

{
  Small helper methods to change the formatting aspect of a TeeGrid.

  Example:

    TAndroidTheme.ApplyTo( TeeGrid1.Grid );
}

uses
  Tee.Grid, Tee.Grid.RowGroup;

type
  TDefaultTheme=record
  public
    class procedure ApplyTo(const AGrid:TCustomTeeGrid); static;
  end;

  TBlackTheme=record
  public
    class procedure ApplyTo(const AGrid:TCustomTeeGrid); static;
  end;

  TiOSTheme=record
  private
  const
    TealBlue=$FAC85A;
  public
    class procedure ApplyTo(const AGroup: TRowGroup); overload; static;
    class procedure ApplyTo(const AGrid:TCustomTeeGrid); overload; static;
  end;

  TAndroidTheme=record
  public
    class procedure ApplyTo(const AGroup:TRowGroup); overload; static;
    class procedure ApplyTo(const AGrid:TCustomTeeGrid); overload; static;
  end;

  TGridThemes=record
  private
    class procedure CheckScrollBars(const AGrid:TCustomTeeGrid); static;
  public
    class var
      Default : TDefaultTheme;
      Black : TBlackTheme;
      iOS : TiOSTheme;
      Android : TAndroidTheme;
  end;

implementation

uses
  {System.}Classes,
  {$IFNDEF NOUITYPES}
  System.UITypes,
  {$ENDIF}

  Tee.Grid.Columns, Tee.Control, Tee.Format, Tee.Grid.Bands;

type
  TControlAccess=class(TCustomTeeGrid);

procedure ResetFonts(const AGroup:TRowGroup; const AName:String; const ASize:Single);

  procedure SetFont(const AFont:TFont); overload;
  begin
    AFont.Name:=AName;
    AFont.Size:=ASize;
    AFont.Color:=TColors.Black;
  end;

  procedure SetFont(const ABand:TGridBand); overload;
  begin
    SetFont(ABand.Format.Font);
  end;

  procedure SetFont(const ABands:TGridBands); overload;
  var t : Integer;
  begin
    for t:=0 to ABands.Count-1 do
        SetFont(ABands[t]);
  end;

  procedure ResetColumns(const AColumns:TColumns);
  var t : Integer;
      tmp : TColumn;
  begin
    for t:=0 to AColumns.Count-1 do
    begin
      tmp:=AColumns[t];

      SetFont(tmp.Format.Font);
      SetFont(tmp.Header.Format.Font);

      if tmp.HasItems then
         ResetColumns(tmp.Items);
    end;
  end;

begin
  ResetColumns(AGroup.Columns);
  SetFont(AGroup.Header.Format.Font);
  SetFont(AGroup.Footer);
end;

procedure ResetFontsColor(const AGrid: TCustomTeeGrid; const AColor:TColor);

  procedure SetFont(const AFont:TFont);
  begin
    AFont.Color:=AColor;
  end;

  procedure ResetColumns(const AColumns:TColumns);
  var t : Integer;
      tmp : TColumn;
  begin
    for t:=0 to AColumns.Count-1 do
    begin
      tmp:=AColumns[t];

      SetFont(tmp.Format.Font);
      SetFont(tmp.Header.Format.Font);

      if tmp.HasItems then
         ResetColumns(tmp.Items);
    end;
  end;

begin
  ResetColumns(AGrid.Columns);
  SetFont(AGrid.Header.Format.Font);
end;

{ TiOSTheme }

class procedure TiOSTheme.ApplyTo(const AGrid: TCustomTeeGrid);
begin
  TControlAccess(AGrid).BeginUpdate;
  try
    AGrid.Back.Brush.InitVisible(True);
    AGrid.Back.Brush.InitColor(TColors.White);

    ApplyTo(AGrid.Current);
    AGrid.Selected.Format.Brush.InitColor(TealBlue);

    TGridThemes.CheckScrollBars(AGrid);
  finally
    TControlAccess(AGrid).EndUpdate;
  end;
end;

class procedure TiOSTheme.ApplyTo(const AGroup: TRowGroup);

  procedure SetGradientColors(const AGradient:TGradient; const AColor0,AColor1:TColor);
  begin
    while AGradient.Colors.Count<2 do
          AGradient.Colors.Add;

    AGradient.Colors[0].Color:=AColor0;
    AGradient.Colors[1].Color:=AColor1;

    AGradient.Direction:=TGradientDirection.Vertical;
    AGradient.Inverted:=False;
  end;

begin
  AGroup.Rows.Alternate.Visible:=True;
  AGroup.Rows.RowLines.Visible:=False;

  AGroup.Header.Hover.Format.Brush.Color:=TealBlue;
  AGroup.Header.Format.Brush.Color:=$FF7A00;

  SetGradientColors(AGroup.Header.Format.Brush.Gradient,$FF7A00,$FFAA40);

  AGroup.Rows.Hover.Format.Stroke.Color:=TealBlue;

  ResetFonts(AGroup,'Segoe UI',12);

  AGroup.Header.Format.Font.Color:=TColors.White;
end;

{ TDefaultTheme }

class procedure TDefaultTheme.ApplyTo(const AGrid: TCustomTeeGrid);
begin
  TControlAccess(AGrid).BeginUpdate;
  try
    AGrid.Back.Brush.InitVisible(False);
    AGrid.Back.Brush.InitColor(TColors.White);

    AGrid.Rows.Alternate.Visible:=False;

    AGrid.Header.InitFormat;

    AGrid.Header.Hover.InitFormat;
    AGrid.Rows.Hover.InitFormat;

    AGrid.Selected.InitFormat;

    ResetFonts(AGrid.Current,TFont.DefaultName,TFont.DefaultSize);

    TGridThemes.CheckScrollBars(AGrid);
  finally
    TControlAccess(AGrid).EndUpdate;
  end;
end;

{ TAndroidTheme }

class procedure TAndroidTheme.ApplyTo(const AGrid: TCustomTeeGrid);
begin
  TControlAccess(AGrid).BeginUpdate;
  try
    AGrid.Back.Brush.InitVisible(True);
    AGrid.Back.Brush.InitColor(TColors.White);

    ApplyTo(AGrid.Current);

    AGrid.Selected.Format.Brush.InitColor($888888);

    TGridThemes.CheckScrollBars(AGrid);
  finally
    TControlAccess(AGrid).EndUpdate;
  end;
end;

class procedure TAndroidTheme.ApplyTo(const AGroup: TRowGroup);
begin
  ResetFonts(AGroup,'Roboto',12);

  AGroup.Rows.Alternate.Visible:=True;
  AGroup.Rows.Alternate.Brush.InitColor($F6F6F6);

  AGroup.Rows.RowLines.Visible:=True;

  AGroup.Header.Hover.Format.Brush.Color:=$484848;
  AGroup.Header.Format.Brush.Color:=$888888;
  AGroup.Header.Format.Font.Color:=TColors.White;

  AGroup.Rows.Hover.Format.Stroke.Color:=$888888;
end;

{ TBlackTheme }

class procedure TBlackTheme.ApplyTo(const AGrid: TCustomTeeGrid);
begin
  TControlAccess(AGrid).BeginUpdate;
  try
    AGrid.Back.Brush.InitVisible(True);
    AGrid.Back.Brush.InitColor(TColors.Black);

    AGrid.Header.Format.Brush.Color:=$888888;
    AGrid.Rows.Alternate.Brush.InitColor($484848);

    AGrid.Indicator.Format.Brush.InitColor(TColors.White);

    ResetFontsColor(AGrid,TColors.Cream);

    TGridThemes.CheckScrollBars(AGrid);
  finally
    TControlAccess(AGrid).EndUpdate;
  end;
end;

{ TGridThemes }

type
  TCustomTeeGridAccess=class(TCustomTeeGrid);

class procedure TGridThemes.CheckScrollBars(const AGrid: TCustomTeeGrid);
begin
  TCustomTeeGridAccess(AGrid).Root.RecalcScrollBars:=True;
end;

end.
