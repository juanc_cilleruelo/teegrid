{*********************************************}
{  TeeGrid Software Library                   }
{  Basic Render shapes                        }
{  Copyright (c) 2016 by Steema Software      }
{  All Rights Reserved                        }
{*********************************************}
unit Tee.Renders;
{$I Tee.inc}

interface

{
   Basic shape objects:

   Rectangle, rectangle with text, checkbox, progress bar, expander etc.

   Needs: Tee.Format and Tee.Painter
}

uses
  {System.}Classes,

  {$IFNDEF FPC}
  {System.}Types,

  {$IFNDEF NOUITYPES}
  {System.}UITypes,
  {$ENDIF}

  {$ENDIF}

  Tee.Format, Tee.Painter;

type
  // Internal
  TRenderData=record
  public
    Painter : TPainter;
    Rect : TRectF;
    Row : Integer;
    Text : String;
  end;

  // Base class
  TRender=class(TPersistentChange)
  public
    function Hit(const R:TRectF; const X,Y:Single):Boolean; virtual;
    procedure Paint(var AData:TRenderData); virtual;
  end;

  // Just an alias
  TBorder=class(THiddenStroke);

  // Left,Top,Right,Bottom "Border" Strokes
  TBorders=class(TPersistentChange)
  private
    FRight: TBorder;
    FBottom: TBorder;
    FTop: TBorder;
    FLeft: TBorder;

    procedure SetBottom(const Value: TBorder);
    procedure SetLeft(const Value: TBorder);
    procedure SetRight(const Value: TBorder);
    procedure SetTop(const Value: TBorder);
  public
    Constructor Create(const AChanged:TNotifyEvent); override;

    {$IFNDEF AUTOREFCOUNT}
    Destructor Destroy; override;
    {$ENDIF}

    procedure Assign(Source:TPersistent); override;
    procedure Paint(const APainter: TPainter; const ARect: TRectF);
  published
    property Left:TBorder read FLeft write SetLeft;
    property Top:TBorder read FTop write SetTop;
    property Right:TBorder read FRight write SetRight;
    property Bottom:TBorder read FBottom write SetBottom;
  end;

  // Rectangle shape with Borders
  TFormatRender=class(TRender)
  private
    FBorders : TBorders;
    FFormat : TTextFormat;

    function GetBorders: TBorders;
    function GetFormat: TTextFormat;
    procedure SetBorders(const Value: TBorders);
    procedure SetFormat(const Value: TTextFormat);
  public
    {$IFNDEF AUTOREFCOUNT}
    Destructor Destroy; override;
    {$ENDIF}

    procedure Assign(Source:TPersistent); override;

    function HasFormat:Boolean; inline;
    procedure Paint(var AData:TRenderData); override;
  published
    property Borders:TBorders read GetBorders write SetBorders;
    property Format:TTextFormat read GetFormat write SetFormat;
  end;

  // Left,Top,Right,Bottom edge properties (pixels or %)
  TEdges=class(TPersistentChange)
  private
    FRight: TCoordinate;
    FBottom: TCoordinate;
    FTop: TCoordinate;
    FLeft: TCoordinate;

    procedure SetBottom(const Value: TCoordinate);
    procedure SetLeft(const Value: TCoordinate);
    procedure SetRight(const Value: TCoordinate);
    procedure SetTop(const Value: TCoordinate);
  public
    Constructor Create(const AChanged:TNotifyEvent); override;

    {$IFNDEF AUTOREFCOUNT}
    Destructor Destroy; override;
    {$ENDIF}

    function Adjust(const R:TRectF):TRectF;
    procedure Assign(Source:TPersistent); override;
    function Horizontal:Single;
    procedure Prepare(const AWidth,AHeight:Single);
    function Vertical:Single;
  published
    property Left:TCoordinate read FLeft write SetLeft;
    property Top:TCoordinate read FTop write SetTop;
    property Right:TCoordinate read FRight write SetRight;
    property Bottom:TCoordinate read FBottom write SetBottom;
  end;

  TMargins=class(TEdges);

  // Horizontal and Vertical alignments
  TAlignments=class(TPersistentChange)
  private
    FHorizontal : THorizontalAlign;
    FVertical : TVerticalAlign;

    DefaultHorizontal : THorizontalAlign;
    DefaultVertical : TVerticalAlign;

    function IsHorizontalStored: Boolean;
    function IsVerticalStored: Boolean;
    procedure SetHorizontal(const Value: THorizontalAlign);
    procedure SetVertical(const Value: TVerticalAlign);
  public
    procedure Assign(Source:TPersistent); override;

    procedure InitHorizontal(const Value:THorizontalAlign);
    procedure InitVertical(const Value:TVerticalAlign);
  published
    property Horizontal:THorizontalAlign read FHorizontal write SetHorizontal stored IsHorizontalStored;
    property Vertical:TVerticalAlign read FVertical write SetVertical stored IsVerticalStored;
  end;

  // Just an alias
  TTextAlign=class(TAlignments);

  // Rectangle shape with Text, Margins and Alignments
  TTextRender=class(TFormatRender)
  private
    FAlign : TTextAlign;
    FMargins : TMargins;

    procedure SetAlign(const Value: TTextAlign);
    procedure SetMargins(const Value: TMargins);
  protected
  public
    PaintText : Boolean;
    TextLines : Integer;

    Constructor Create(const AChanged:TNotifyEvent); override;

    {$IFNDEF AUTOREFCOUNT}
    Destructor Destroy; override;
    {$ENDIF}

    procedure Assign(Source:TPersistent); override;

    function CalcHeight:Single;
    function CalcTextLines(const AText:String):Integer;
    procedure Paint(var AData:TRenderData); override;
  published
    property Margins:TMargins read FMargins write SetMargins;
    property TextAlign:TTextAlign read FAlign write SetAlign;
  end;

  // Rectangle Text shape with Visible:Boolean property
  TVisibleTextRender=class(TTextRender)
  private
    FVisible : Boolean;

    procedure SetVisible(const Value: Boolean);
  public
    Constructor Create(const AChanged:TNotifyEvent); override;

    procedure Assign(Source:TPersistent); override;

    procedure Hide;
    procedure Show;
  published
    property Visible:Boolean read FVisible write SetVisible default True;
  end;

  TCellRender=class(TVisibleTextRender)
  private
    FText: String;

    procedure SetText(const Value: String);
  public
    procedure Assign(Source:TPersistent); override;
  published
    property Text:String read FText write SetText;
  end;

  // Square shape with format (Brush and Stroke)
  TBox=class(TPersistentChange)
  private
    const
      DefaultSize=5;

    var
    FFormat : TFormat;
    FSize : Single;

    procedure SetFormat(const Value: TFormat);
    function IsSizeStored: Boolean;
    procedure SetSize(const Value: Single);
  protected
    DrawBox,
    Draw : Boolean;
  public
    Constructor Create(const AChanged: TNotifyEvent); override;

    {$IFNDEF AUTOREFCOUNT}
    Destructor Destroy; override;
    {$ENDIF}

    procedure Assign(Source:TPersistent); override;

  published
    property Format:TFormat read FFormat write SetFormat;
    property Size:Single read FSize write SetSize stored IsSizeStored;
  end;

  // Rectangle Text shape with an extra Box (square)
  TBoxRender=class(TTextRender)
  private
    FBox : TBox;

    procedure SetBox(const Value: TBox);
  protected
    function Calculate(const R:TRectF):TRectF;
  public
    Constructor Create(const AChanged:TNotifyEvent); override;

    {$IFNDEF AUTOREFCOUNT}
    Destructor Destroy; override;
    {$ENDIF}

    procedure Assign(Source:TPersistent); override;

    function Hit(const R:TRectF; const X,Y:Single):Boolean; override;
    procedure Paint(var AData:TRenderData); override;
  published
    property Box:TBox read FBox write SetBox;
  end;

  TBooleanRenderStyle=(Check,Text);

  // Rectangle shape with Box to emulate a "CheckBox"
  TBooleanRender=class(TBoxRender)
  private
    FCheckFormat : TFormat;
    FStyle: TBooleanRenderStyle;

    procedure SetStyle(const Value: TBooleanRenderStyle);
    procedure SetCheckFormat(const Value: TFormat);
  public
    Constructor Create(const AChanged:TNotifyEvent); override;

    {$IFNDEF AUTOREFCOUNT}
    Destructor Destroy; override;
    {$ENDIF}

    procedure Assign(Source:TPersistent); override;

    procedure Paint(var AData:TRenderData); override;
  published
    property CheckFormat:TFormat read FCheckFormat write SetCheckFormat;
    property Style:TBooleanRenderStyle read FStyle write SetStyle default TBooleanRenderStyle.Check;
  end;

  TExpanderRender=class;

  TCanExpandEvent=function(const Sender:TRender; const ARow:Integer):Boolean of object;
  TGetExpandedEvent=function(const Sender:TRender; const ARow:Integer):Boolean of object;

  TExpanderStyle=(PlusMinus,Triangle,Arrow);

  // Rectangle shape with Box to emulate a "+" "-" expander (also triangles or arrows)
  TExpanderRender=class(TBoxRender)
  private
    FExpandFormat : TFormat;
    FExpandLine: TStroke;
    FOnCanExpand : TCanExpandEvent;
    FOnGetExpanded : TGetExpandedEvent;
    FStyle : TExpanderStyle;

    procedure SetExpandFormat(const Value: TFormat);
    procedure SetExpandLine(const Value: TStroke);
    procedure SetStyle(const Value: TExpanderStyle);
  public
    Constructor Create(const AChanged:TNotifyEvent); override;

    {$IFNDEF AUTOREFCOUNT}
    Destructor Destroy; override;
    {$ENDIF}

    procedure Assign(Source:TPersistent); override;

    procedure Paint(var AData:TRenderData); override;
    procedure PaintLines(var AData:TRenderData);

    property OnCanExpand:TCanExpandEvent read FOnCanExpand write FOnCanExpand;
    property OnGetExpanded:TGetExpandedEvent read FOnGetExpanded write FOnGetExpanded;
  published
    property ExpandFormat:TFormat read FExpandFormat write SetExpandFormat;
    property ExpandLine:TStroke read FExpandLine write SetExpandLine;
    property Style:TExpanderStyle read FStyle write SetStyle default TExpanderStyle.PlusMinus;
  end;

  TOrientation=(Horizontal,Vertical);

  // Rectangle shape with partial filling to emulate a "ProgressBar"
  TProgressRender=class(TFormatRender)
  private
    FMax : Single;
    FMin: Single;
    FOrientation: TOrientation;

    IRange : Single;

    function IsMaxStored: Boolean;
    function IsMinStored: Boolean;
    procedure SetMax(const Value: Single);
    procedure SetMin(const Value: Single);
    procedure SetOrientation(const Value: TOrientation);
  public
    Constructor Create(const AChanged:TNotifyEvent); override;

    procedure Assign(Source:TPersistent); override;

    procedure Paint(var AData:TRenderData); override;
  published
    property Maximum:Single read FMax write SetMax stored IsMaxStored;
    property Minimum:Single read FMin write SetMin stored IsMinStored;
    property Orientation:TOrientation read FOrientation write SetOrientation default TOrientation.Horizontal;
  end;

  // Rectangle with Text and "ParentFont" boolean property
  THover=class(TVisibleTextRender)
  private
    FParentFont: Boolean;

    procedure SetParentFont(const Value: Boolean);
  public
    Constructor Create(const AChanged:TNotifyEvent); override;

    procedure InitFormat;
  published
    property ParentFont:Boolean read FParentFont write SetParentFont default True;
  end;

  // Base class for TGridBand and TColumn
  TVisibleRenderItem=class(TCollectionItem)
  private
    FTagObject : TObject;
    FVisible: Boolean;

    function GetRender: TRender;
    procedure SetFormat(const Value: TTextFormat);
    procedure SetRender(const Value: TRender);
    procedure SetVisible(const Value: Boolean);
    function IsFormatStored: Boolean;
  protected
    FRender: TRender;

    function CreateRender:TRender; virtual;
    procedure DoChanged; virtual;
    function GetFormat: TTextFormat; virtual;
  public
    Constructor Create(ACollection:TCollection); override;

    {$IFNDEF AUTOREFCOUNT}
    Destructor Destroy; override;
    {$ENDIF}

    procedure Assign(Source:TPersistent); override;

    procedure Changed(Sender:TObject);
    function HasFormat:Boolean; inline;
    function HasRender:Boolean; inline;
    procedure Paint(var AData:TRenderData; const ARender:TRender); virtual;

    property Render:TRender read GetRender write SetRender;
    property TagObject:TObject read FTagObject write FTagObject;
  published
    property Format:TTextFormat read GetFormat write SetFormat stored IsFormatStored;
    property Visible:Boolean read FVisible write SetVisible default True;
  end;

implementation

uses
  {System.}SysUtils;

{ TRender }

function TRender.Hit(const R:TRectF; const X, Y: Single): Boolean;
begin
  result:=False;
end;

procedure TRender.Paint(var AData:TRenderData);
begin
end;

{ TFormatRender }

procedure TFormatRender.Assign(Source: TPersistent);
begin
  if Source is TFormatRender then
  begin
    Borders:=TFormatRender(Source).FBorders;
    Format:=TFormatRender(Source).FFormat;
  end
  else
    inherited;
end;

{$IFNDEF AUTOREFCOUNT}
Destructor TFormatRender.Destroy;
begin
  FBorders.Free;
  FFormat.Free;
  inherited;
end;
{$ENDIF}

function TFormatRender.GetBorders: TBorders;
begin
  if FBorders=nil then
     FBorders:=TBorders.Create(IChanged);

  result:=FBorders;
end;

function TFormatRender.GetFormat: TTextFormat;
begin
  if FFormat=nil then
  begin
    FFormat:=TTextFormat.Create(IChanged);
    FFormat.Stroke.InitVisible(False);
    FFormat.Brush.InitVisible(False);
  end;

  result:=FFormat;
end;

function TFormatRender.HasFormat: Boolean;
begin
  result:=FFormat<>nil;
end;

procedure TFormatRender.Paint(var AData:TRenderData);
begin
  inherited;

  if FFormat<>nil then
     AData.Painter.Paint(FFormat,AData.Rect);

  if FBorders<>nil then
     FBorders.Paint(AData.Painter,AData.Rect);
end;

procedure TFormatRender.SetBorders(const Value: TBorders);
begin
  if Value=nil then
  begin
    FBorders.Free;
    FBorders:=nil;
  end
  else
    Borders.Assign(Value);
end;

procedure TFormatRender.SetFormat(const Value: TTextFormat);
begin
  if Value=nil then
  begin
    FFormat.Free;
    FFormat:=nil;
  end
  else
    Format.Assign(Value);
end;

{ TTextRender }

Constructor TTextRender.Create(const AChanged: TNotifyEvent);
begin
  inherited;

  PaintText:=True;

  FAlign:=TTextAlign.Create(IChanged);
  FMargins:=TMargins.Create(IChanged);

  // Pending: TextWidth('0')
  FMargins.Left.InitValue(4);
  FMargins.Right.InitValue(4);
  FMargins.Top.InitValue(2);
  FMargins.Bottom.InitValue(2);
end;

{$IFNDEF AUTOREFCOUNT}
Destructor TTextRender.Destroy;
begin
  FMargins.Free;
  FAlign.Free;
  inherited;
end;
{$ENDIF}

procedure TTextRender.Assign(Source: TPersistent);
begin
  if Source is TTextRender then
  begin
    TextAlign:=TTextRender(Source).FAlign;
    Margins:=TTextRender(Source).FMargins;
  end;

  inherited;
end;

function TTextRender.CalcHeight: Single;
begin
  result:=Format.TextHeight*TextLines;

  if Format.Stroke.Visible then
     result:=result+Format.Stroke.Size*2;
end;

function TTextRender.CalcTextLines(const AText: String): Integer;
var t : Integer;
begin
  result:=0;

  if AText<>'' then
  begin
    t:=1;

    while t<=Length(AText) do
    begin
      if AText[t]=#13 then
      begin
        Inc(result);

        Inc(t);

        if AText[t]=#10 then
           Inc(t);
      end
      else
        Inc(t);
    end;

    Inc(result);
  end;
end;

procedure TTextRender.Paint(var AData:TRenderData);

  function PenSize:Single;
  begin
    if Format.Stroke.Visible then
       result:=Format.Stroke.Size
    else
       result:=0;
  end;

  function HorizPos(const R:TRectF):Single;
  begin
    case FAlign.Horizontal of
        THorizontalAlign.Left: result:=R.Left+FMargins.Left.Calculate(R.Width)+PenSize;
      THorizontalAlign.Center: result:=0.5*(R.Left+R.Right);
    else
       result:=R.Right-FMargins.Right.Calculate(R.Width)-PenSize;
    end;
  end;

  function CalcTextHeight:Single;
  begin
    result:=TextLines*AData.Painter.TextHeight('0');
  end;

  function VertPos(const R:TRectF):Single;
  begin
    case FAlign.Vertical of
           TVerticalAlign.Top: result:=R.Top+FMargins.Top.Calculate(R.Height)+PenSize;

        TVerticalAlign.Center: result:=0.5*(R.Top+R.Bottom-CalcTextHeight);
    else
       result:=R.Bottom-FMargins.Bottom.Calculate(R.Height)-PenSize-CalcTextHeight;
    end;
  end;

  procedure DoPaint(const APainter:TPainter; const R:TRectF);

    function NextLine(const S:String; var APos:Integer):String;
    var L, Start : Integer;
    begin
      Start:=APos;

      L:=Length(S);

      while APos<=L do
      begin
        if S[APos]=#13 then
        begin
          Inc(APos);

          if S[APos]=#10 then
             Inc(APos);

          result:=Copy(S,Start,APos-Start-1);

          Exit;
        end;

        Inc(APos);
      end;

      result:=Copy(S,Start,L-Start+1);
    end;

    procedure PaintMultiple;
    var t,
        tmpPos : Integer;
        tmp : String;
        X,Y,tmpY : Single;
    begin
      X:=HorizPos(R);
      Y:=VertPos(R);
      tmpY:=AData.Painter.TextHeight('0');

      tmpPos:=1;

      for t:=1 to TextLines do
      begin
        tmp:=NextLine(AData.Text,tmpPos);

        APainter.TextOut(X,Y,tmp);
        Y:=Y+tmpY;
      end;
    end;

  begin
    APainter.Clip(R);

    APainter.SetHorizontalAlign(FAlign.Horizontal);
    APainter.SetVerticalAlign(FAlign.Vertical);

    if TextLines>1 then
       PaintMultiple
    else
       APainter.TextOut(HorizPos(R),VertPos(R),AData.Text);

    APainter.UnClip;
  end;

begin
  inherited;

  if PaintText and (AData.Text<>'') then
     DoPaint(AData.Painter,AData.Rect);
end;

procedure TTextRender.SetAlign(const Value: TTextAlign);
begin
  FAlign.Assign(Value);
end;

procedure TTextRender.SetMargins(const Value: TMargins);
begin
  FMargins.Assign(Value);
end;

{ TVisibleTextRender }

Constructor TVisibleTextRender.Create(const AChanged: TNotifyEvent);
begin
  inherited;
  FVisible:=True;
end;

procedure TVisibleTextRender.Hide;
begin
  Visible:=False;
end;

procedure TVisibleTextRender.Assign(Source: TPersistent);
begin
  if Source is TVisibleTextRender then
     FVisible:=TVisibleTextRender(Source).FVisible;

  inherited;
end;

procedure TVisibleTextRender.SetVisible(const Value: Boolean);
begin
  ChangeBoolean(FVisible,Value);
end;

procedure TVisibleTextRender.Show;
begin
  Visible:=True;
end;

{ TBox }

procedure TBox.Assign(Source: TPersistent);
begin
  if Source is TBox then
  begin
    Format:=TBox(Source).Format;
    FSize:=TBox(Source).FSize;
  end;

  inherited;
end;

Constructor TBox.Create(const AChanged: TNotifyEvent);
begin
  inherited;

  Draw:=True;
  DrawBox:=True;

  FSize:=DefaultSize;

  FFormat:=TFormat.Create(IChanged);

  FFormat.Brush.InitVisible(False);
  FFormat.Stroke.InitVisible(True);
  FFormat.Stroke.Brush.InitColor(TColors.DkGray);
end;

{$IFNDEF AUTOREFCOUNT}
Destructor TBox.Destroy;
begin
  FFormat.Free;
  inherited;
end;
{$ENDIF}

function TBox.IsSizeStored: Boolean;
begin
  result:=FSize<>DefaultSize;
end;

procedure TBox.SetFormat(const Value: TFormat);
begin
  FFormat.Assign(Value);
end;

procedure TBox.SetSize(const Value: Single);
begin
  ChangeSingle(FSize,Value);
end;

{ TBoxRender }

Constructor TBoxRender.Create(const AChanged: TNotifyEvent);
begin
  inherited;
  FBox:=TBox.Create(IChanged);
end;

{$IFNDEF AUTOREFCOUNT}
Destructor TBoxRender.Destroy;
begin
  FBox.Free;
  inherited;
end;
{$ENDIF}

function TBoxRender.Hit(const R:TRectF; const X, Y: Single): Boolean;
begin
  result:=FBox.Draw and Calculate(R).Contains(TPointF.Create(X,Y));
end;

function TBoxRender.Calculate(const R:TRectF):TRectF;

  function CalcXY(const R:TRectF):TPointF;
  begin
    result:=R.CenterPoint;

    case TextAlign.Horizontal of
       THorizontalAlign.Left: result.X:=R.Left+FBox.Size;
      THorizontalAlign.Right: result.X:=R.Right-FBox.Size;
    end;
  end;

var C : TPointF;
begin
  Margins.Prepare(R.Width,R.Height);

  C:=CalcXY(Margins.Adjust(R));

  result.Left:=C.X-FBox.Size;
  result.Right:=C.X+FBox.Size;

  result.Top:=C.Y-FBox.Size;
  result.Bottom:=C.Y+FBox.Size;
end;

procedure TBoxRender.Assign(Source:TPersistent);
begin
  if Assigned(Source) then
     Box:=TBoxRender(Source).Box;

  inherited;
end;

procedure TBoxRender.Paint(var AData:TRenderData);
var tmp,
    Old : TRectF;
begin
  if FBox.Draw then
  begin
    Old:=AData.Rect;

    tmp:=Calculate(AData.Rect);
    AData.Rect.Left:=tmp.Right+Margins.Left.Pixels;

    inherited;

    AData.Rect:=Old;

    if FBox.DrawBox then
       AData.Painter.Paint(FBox.Format,tmp);
  end
  else
    inherited;
end;

procedure TBoxRender.SetBox(const Value: TBox);
begin
  FBox.Assign(Value);
end;

{ TBooleanRender }

Constructor TBooleanRender.Create(const AChanged:TNotifyEvent);
begin
  inherited;
  FCheckFormat:=TFormat.Create(IChanged);
  FAlign.InitHorizontal(THorizontalAlign.Center);
end;

{$IFNDEF AUTOREFCOUNT}
Destructor TBooleanRender.Destroy;
begin
  FCheckFormat.Free;
  inherited;
end;
{$ENDIF}

procedure TBooleanRender.Assign(Source: TPersistent);
begin
  if Source is TBooleanRender then
  begin
    FStyle:=TBooleanRender(Source).FStyle;
    CheckFormat:=TBooleanRender(Source).CheckFormat;
  end;

  inherited;
end;

procedure TBooleanRender.Paint(var AData:TRenderData);

  procedure DrawCheck(const Checked:Boolean);
  var tmp : TRectF;
  begin
//    AData.Painter.Paint(CheckFormat,tmp);

    if Checked then
    begin
      tmp:=Calculate(AData.Rect);

      AData.Painter.SetStroke(CheckFormat.Stroke);

      AData.Painter.Line(tmp.Left+2,tmp.Top+3,tmp.Right-2,tmp.Bottom-2);
      AData.Painter.Line(tmp.Left+2,tmp.Bottom-3,tmp.Right-2,tmp.Top+2);
    end;
  end;

var tmp : Boolean;
begin
  if not TryStrToBool(AData.Text,tmp) then
     tmp:=False;

  AData.Text:='';

  inherited;

  if Style=TBooleanRenderStyle.Check then
     DrawCheck(tmp);
end;

procedure TBooleanRender.SetCheckFormat(const Value: TFormat);
begin
  if FCheckFormat<>Value then
  begin
    FCheckFormat:=Value;
    Changed;
  end;
end;

procedure TBooleanRender.SetStyle(const Value: TBooleanRenderStyle);
begin
  if FStyle<>Value then
  begin
    FStyle:=Value;
    Changed;
  end;
end;

{ TTextAlign }

procedure TAlignments.Assign(Source: TPersistent);
begin
  if Source is TTextAlign then
  begin
    FHorizontal:=TTextAlign(Source).FHorizontal;
    FVertical:=TTextAlign(Source).FVertical;
  end
  else
    inherited;
end;

procedure TAlignments.InitHorizontal(const Value: THorizontalAlign);
begin
  FHorizontal:=Value;
  DefaultHorizontal:=FHorizontal;
end;

procedure TAlignments.InitVertical(const Value: TVerticalAlign);
begin
  FVertical:=Value;
  DefaultVertical:=FVertical;
end;

function TAlignments.IsHorizontalStored: Boolean;
begin
  result:=FHorizontal<>DefaultHorizontal;
end;

function TAlignments.IsVerticalStored: Boolean;
begin
  result:=FVertical<>DefaultVertical;
end;

procedure TAlignments.SetHorizontal(const Value: THorizontalAlign);
begin
  if FHorizontal<>Value then
  begin
    FHorizontal:=Value;
    Changed;
  end;
end;

procedure TAlignments.SetVertical(const Value: TVerticalAlign);
begin
  if FVertical<>Value then
  begin
    FVertical:=Value;
    Changed;
  end;
end;

{ TMargins }

Constructor TEdges.Create(const AChanged: TNotifyEvent);
begin
  inherited;

  FLeft:=TCoordinate.Create(IChanged);
  FTop:=TCoordinate.Create(IChanged);
  FRight:=TCoordinate.Create(IChanged);
  FBottom:=TCoordinate.Create(IChanged);
end;

{$IFNDEF AUTOREFCOUNT}
Destructor TEdges.Destroy;
begin
  FBottom.Free;
  FRight.Free;
  FTop.Free;
  FLeft.Free;
  inherited;
end;
{$ENDIF}

function TEdges.Adjust(const R: TRectF): TRectF;
begin
  result.Left:=R.Left+FLeft.Pixels;
  result.Top:=R.Top+FTop.Pixels;
  result.Right:=R.Right-FRight.Pixels;
  result.Bottom:=R.Bottom-FBottom.Pixels;
end;

procedure TEdges.Assign(Source: TPersistent);
begin
  if Source is TMargins then
  begin
    Left:=TMargins(Source).FLeft;
    Top:=TMargins(Source).FTop;
    Right:=TMargins(Source).FRight;
    Bottom:=TMargins(Source).FBottom;
  end
  else
    inherited;
end;

function TEdges.Horizontal: Single;
begin
  result:=FLeft.Pixels+FRight.Pixels;
end;

procedure TEdges.Prepare(const AWidth, AHeight: Single);
begin
  FLeft.Prepare(AWidth);
  FRight.Prepare(AWidth);
  FTop.Prepare(AHeight);
  FBottom.Prepare(AHeight);
end;

procedure TEdges.SetBottom(const Value: TCoordinate);
begin
  FBottom.Assign(Value);
end;

procedure TEdges.SetLeft(const Value: TCoordinate);
begin
  FLeft.Assign(Value);
end;

procedure TEdges.SetRight(const Value: TCoordinate);
begin
  FRight.Assign(Value);
end;

procedure TEdges.SetTop(const Value: TCoordinate);
begin
  FTop.Assign(Value);
end;

function TEdges.Vertical: Single;
begin
  result:=FTop.Pixels+FBottom.Pixels;
end;

{ TBorders }

Constructor TBorders.Create(const AChanged: TNotifyEvent);
begin
  inherited;

  FLeft:=TBorder.Create(IChanged);
  FTop:=TBorder.Create(IChanged);
  FRight:=TBorder.Create(IChanged);
  FBottom:=TBorder.Create(IChanged);
end;

{$IFNDEF AUTOREFCOUNT}
Destructor TBorders.Destroy;
begin
  FBottom.Free;
  FRight.Free;
  FTop.Free;
  FLeft.Free;
  inherited;
end;
{$ENDIF}

procedure TBorders.Assign(Source: TPersistent);
begin
  if Source is TBorders then
  begin
    Left:=TBorders(Source).FLeft;
    Top:=TBorders(Source).FTop;
    Right:=TBorders(Source).FRight;
    Bottom:=TBorders(Source).FBottom;
  end
  else
    inherited;
end;

procedure TBorders.Paint(const APainter: TPainter; const ARect: TRectF);
begin
  if FLeft.Visible then
  begin
    APainter.SetStroke(FLeft);
    APainter.VerticalLine(ARect.Left,ARect.Top,ARect.Bottom);
  end;

  if FTop.Visible then
  begin
    APainter.SetStroke(FTop);
    APainter.HorizontalLine(ARect.Top,ARect.Left,ARect.Right);
  end;

  if FRight.Visible then
  begin
    APainter.SetStroke(FRight);
    APainter.VerticalLine(ARect.Right,ARect.Top,ARect.Bottom);
  end;

  if FBottom.Visible then
  begin
    APainter.SetStroke(FBottom);
    APainter.HorizontalLine(ARect.Bottom,ARect.Left,ARect.Right);
  end;
end;

procedure TBorders.SetBottom(const Value: TBorder);
begin
  FBottom.Assign(Value);
end;

procedure TBorders.SetLeft(const Value: TBorder);
begin
  FLeft.Assign(Value);
end;

procedure TBorders.SetRight(const Value: TBorder);
begin
  FRight.Assign(Value);
end;

procedure TBorders.SetTop(const Value: TBorder);
begin
  FTop.Assign(Value);
end;

{ TProgressRender }

Constructor TProgressRender.Create(const AChanged: TNotifyEvent);
begin
  inherited;

  FMax:=100;

  Format.Brush.InitVisible(True);
  Format.Brush.InitColor($671200);

  Format.Font.InitColor(TColors.White);
end;

procedure TProgressRender.Assign(Source: TPersistent);
begin
  if Source is TProgressRender then
  begin
    FMax:=TProgressRender(Source).FMax;
    FMin:=TProgressRender(Source).FMin;
    FOrientation:=TProgressRender(Source).FOrientation;
  end;

  inherited;
end;

function TProgressRender.IsMaxStored: Boolean;
begin
  result:=FMax<>100;
end;

function TProgressRender.IsMinStored: Boolean;
begin
  result:=FMin<>0;
end;

procedure TProgressRender.Paint(var AData:TRenderData);
var tmp : Single;
    tmpR : TRectF;
begin
  if (IRange<>0) and TryStrToFloat(AData.Text,tmp) then
  begin
    tmpR:=AData.Rect;

    tmp:=(tmp-Minimum)/IRange;

    if FOrientation=TOrientation.Horizontal then
    begin
      tmp:=AData.Rect.Width*tmp;

      if tmp>0 then
         tmpR.Right:=tmpR.Left+tmp
      else
         Exit;
    end
    else
    begin
      tmp:=AData.Rect.Height*tmp;

      if tmp>0 then
         tmpR.Top:=tmpR.Bottom-tmp
      else
         Exit;
    end;

    AData.Text:='';
    AData.Rect:=tmpR;

    inherited Paint(AData);
  end;
end;

procedure TProgressRender.SetMax(const Value: Single);
begin
  if FMax<>Value then
  begin
    FMax:=Value;
    IRange:=(Maximum-Minimum);
    Changed;
  end;
end;

procedure TProgressRender.SetMin(const Value: Single);
begin
  if FMin<>Value then
  begin
    FMin:=Value;
    IRange:=(Maximum-Minimum);
    Changed;
  end;
end;

procedure TProgressRender.SetOrientation(const Value: TOrientation);
begin
  if FOrientation<>Value then
  begin
    FOrientation:=Value;
    Changed;
  end;
end;

{ TExpanderRender }

Constructor TExpanderRender.Create(const AChanged: TNotifyEvent);
begin
  inherited;

  FExpandFormat:=TFormat.Create(AChanged);

  FExpandFormat.Brush.InitVisible(True);
  FExpandFormat.Brush.InitColor(TColors.Black);

  FExpandLine:=TStroke.Create(AChanged);
  FExpandLine.Brush.InitColor(TColors.DkGray);
end;

{$IFNDEF AUTOREFCOUNT}
Destructor TExpanderRender.Destroy;
begin
  FExpandLine.Free;
  FExpandFormat.Free;
  inherited;
end;
{$ENDIF}

procedure TExpanderRender.Assign(Source: TPersistent);
begin
  if Source is TExpanderRender then
  begin
    ExpandFormat:=TExpanderRender(Source).ExpandFormat;
    ExpandLine:=TExpanderRender(Source).ExpandLine;
  end;

  inherited;
end;

procedure TExpanderRender.Paint(var AData: TRenderData);

  procedure DrawPlusMinus(const ARect:TRectF; const IsExpanded:Boolean);
  var tmp : TPointF;
  begin
    tmp:=ARect.CenterPoint;

    AData.Painter.HorizontalLine(tmp.Y,ARect.Left,ARect.Right);

    if not IsExpanded then
       AData.Painter.VerticalLine(tmp.X,ARect.Bottom,ARect.Top);
  end;

  procedure DrawTriangle(const ARect:TRectF; const IsExpanded:Boolean);

    function VerticalTriangle:TPointsF;
    begin
      SetLength(result,3);

      result[0]:=TPointF.Create(ARect.Left,ARect.Top);
      result[1]:=TPointF.Create(ARect.Right,ARect.Top);
      result[2]:=TPointF.Create(ARect.CenterPoint.X,ARect.Bottom);
    end;

    function HorizontalTriangle:TPointsF;
    begin
      SetLength(result,3);

      result[0]:=TPointF.Create(ARect.Left,ARect.Top);
      result[1]:=TPointF.Create(ARect.Left,ARect.Bottom);
      result[2]:=TPointF.Create(ARect.Right,ARect.CenterPoint.Y);
    end;

  begin
    if IsExpanded then
       AData.Painter.Fill(VerticalTriangle)
    else
       AData.Painter.Fill(HorizontalTriangle);
  end;

  procedure DrawArrows(const ARect:TRectF; const IsExpanded:Boolean);

    function DownArrow:TPointsF;
    begin
      SetLength(result,3);

      result[0]:=TPointF.Create(ARect.Left,ARect.Top);
      result[1]:=TPointF.Create(ARect.CenterPoint.X,ARect.Bottom);
      result[2]:=TPointF.Create(ARect.Right,ARect.Top);
    end;

    function RightArrow:TPointsF;
    begin
      SetLength(result,3);

      result[0]:=TPointF.Create(ARect.Left,ARect.Top);
      result[1]:=TPointF.Create(ARect.Right,ARect.CenterPoint.Y);
      result[2]:=TPointF.Create(ARect.Left,ARect.Bottom);
    end;

  var tmp : TPointsF;
  begin
    if IsExpanded then
       tmp:=DownArrow
    else
       tmp:=RightArrow;

    AData.Painter.Lines(tmp);
  end;

  procedure DrawExpandSymbol;
  var tmp : TRectF;
      tmpExpanded : Boolean;
  begin
    tmp:=Calculate(AData.Rect);

    tmp.Inflate(-2,-2);

    AData.Painter.SetBrush(ExpandFormat.Brush);
    AData.Painter.SetStroke(ExpandFormat.Stroke);

    tmpExpanded:=Assigned(FOnGetExpanded) and FOnGetExpanded(Self,AData.Row);

    case FStyle of
      TExpanderStyle.PlusMinus: DrawPlusMinus(tmp,tmpExpanded);
       TExpanderStyle.Triangle: DrawTriangle(tmp,tmpExpanded);
    else
       DrawArrows(tmp,tmpExpanded);
    end;
  end;

var tmpCan : Boolean;
begin
  tmpCan:=Assigned(FOnCanExpand) and FOnCanExpand(Self,AData.Row);

  FBox.Draw:=tmpCan;

  if tmpCan then
     FBox.DrawBox:=(FStyle=TExpanderStyle.PlusMinus);

  TextAlign.Horizontal:=THorizontalAlign.Left;

  inherited;

  if tmpCan then
     DrawExpandSymbol;
end;

procedure TExpanderRender.PaintLines(var AData: TRenderData);

  procedure PaintLine(const R:TRectF);
  begin
    AData.Painter.VerticalLine(R.Left,R.Top,R.Bottom);
  end;

begin
  AData.Painter.SetStroke(ExpandLine);
  PaintLine(AData.Rect);
end;

procedure TExpanderRender.SetExpandFormat(const Value: TFormat);
begin
  FExpandFormat.Assign(Value);
end;

procedure TExpanderRender.SetExpandLine(const Value: TStroke);
begin
  FExpandLine.Assign(Value);
end;

procedure TExpanderRender.SetStyle(const Value: TExpanderStyle);
begin
  if FStyle<>Value then
  begin
    FStyle:=Value;
    Changed;
  end;
end;

{ THover }

Constructor THover.Create(const AChanged:TNotifyEvent);
begin
  inherited;

  InitFormat;
  FParentFont:=True;
end;

procedure THover.InitFormat;
begin
  Format.Brush.InitVisible(True);
  Format.Brush.InitColor(TColors.Skyblue);
end;

procedure THover.SetParentFont(const Value: Boolean);
begin
  ChangeBoolean(FParentFont,Value);
end;

{ TVisibleRenderItem }

Constructor TVisibleRenderItem.Create(ACollection: TCollection);
begin
  inherited;
  FVisible:=True;
end;

function TVisibleRenderItem.CreateRender: TRender;
begin
  result:=TTextRender.Create(Changed);
end;

{$IFNDEF AUTOREFCOUNT}
Destructor TVisibleRenderItem.Destroy;
begin
  FRender.Free;
  inherited;
end;
{$ENDIF}

procedure TVisibleRenderItem.DoChanged;
begin
  TCollectionChange(Collection).DoChanged(Self);
end;

function TVisibleRenderItem.GetFormat: TTextFormat;
begin
  // Use "Render" to force getter
  if Render is TFormatRender then
     result:=TFormatRender(FRender).Format
  else
     result:=nil; // raise ?
end;

procedure TVisibleRenderItem.Assign(Source: TPersistent);
begin
  if Source is TVisibleRenderItem then
  begin
    FVisible:=TVisibleRenderItem(Source).FVisible;
  end
  else
    inherited;
end;

procedure TVisibleRenderItem.Changed(Sender: TObject);
begin
  DoChanged;
end;

function TVisibleRenderItem.GetRender: TRender;
begin
  if FRender=nil then
     FRender:=CreateRender;

  result:=FRender;
end;

procedure TVisibleRenderItem.SetFormat(const Value: TTextFormat);
begin
  if Render is TFormatRender then
     TFormatRender(Render).Format:=Value
  // else
  //   raise ?
end;

procedure TVisibleRenderItem.SetRender(const Value: TRender);
begin
  if FRender<>Value then
  begin
    FRender.Free;
    FRender:=Value;

    DoChanged;
  end;
end;

procedure TVisibleRenderItem.SetVisible(const Value: Boolean);
begin
  if FVisible<>Value then
  begin
    FVisible:=Value;
    DoChanged;
  end;
end;

function TVisibleRenderItem.HasFormat: Boolean;
begin
  result:=(FRender is TFormatRender) and TFormatRender(FRender).HasFormat;
end;

function TVisibleRenderItem.HasRender: Boolean;
begin
  result:=FRender<>nil;
end;

function TVisibleRenderItem.IsFormatStored: Boolean;
begin
  result:=HasFormat;
end;

procedure TVisibleRenderItem.Paint(var AData: TRenderData;
  const ARender: TRender);
var tmp : TRender;
begin
  tmp:=FRender;

  if tmp=nil then
     tmp:=ARender;

  if tmp<>nil then
     tmp.Paint(AData);
end;

{ TCellRender }

procedure TCellRender.Assign(Source: TPersistent);
begin
  if Source is TCellRender then
     FText:=TCellRender(Source).Text;

  inherited;
end;

procedure TCellRender.SetText(const Value: String);
begin
  if FText<>Value then
  begin
    FText:=Value;
    TextLines:=CalcTextLines(FText);
    Changed;
  end;
end;

end.

