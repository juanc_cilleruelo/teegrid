{*********************************************}
{  TeeGrid Software Library                   }
{  Abstract Columns class                     }
{  Copyright (c) 2016 by Steema Software      }
{  All Rights Reserved                        }
{*********************************************}
unit Tee.Grid.Columns;
{$I Tee.inc}

interface

{
   TColumn and TColumns classes, define a "column" in a TeeGrid.

   Columns have an "Items" property to (optionally) include sub-columns.

   Other TColumn properties:

     DataFormat (formatting strings for numbers and date-time data types)

     Expanded (default true, show column sub-columns, if they exist)

     Format (custom brush, stroke, font for a column)

     Header (text and formatting to display column names at TeeGrid headers.

     Margins (edge spacing inside grid cells)

     ReadOnly (enable or disable editing a grid cell content using keyboard)

     Render (an optional TRender instance, used to paint column cell contents)

     TagObject (a custom user-defined TObject associated to each TColumn)

     TextAlignment (automatic depending on data type, or custom)

     Visible (default True)

     Width (automatic or custom)


   Needs: Tee.Painter, Tee.Format and Tee.Renders
}

uses
  {System.}Classes,
  {$IFDEF FPC}
  Graphics,
  {$ELSE}
  {System.}Types,
  {$IFNDEF NOUITYPES}
  System.UITypes,
  {$ENDIF}
  {$ENDIF}

  Tee.Painter, Tee.Format, Tee.Renders;

type
  // String formatting properties for Date-Time and Float numbers.
  // TColumn includes this property to configure grid cell display
  TDataFormat=class(TPersistentChange)
  private
    FDate,
    FDateTime,
    FFloat,
    FTime : String;

    function IsFloatStored: Boolean;

    procedure SetDate(const Value: String);
    procedure SetDateTime(const Value: String);
    procedure SetFloat(const Value: String);
    procedure SetTime(const Value: String);
  protected
    const
      DefaultFloat='0.###';
  public
    Constructor Create(const AChanged:TNotifyEvent); override;
    procedure Assign(Source:TPersistent); override;
  published
    property Date:String read FDate write SetDate;
    property DateTime:String read FDateTime write SetDateTime;
    property Float:String read FFloat write SetFloat stored IsFloatStored;
    property Time:String read FTime write SetTime;
  end;

  // Just an alias
  TColumnWidth=class(TCoordinate);

  // Cell text alignment. Automatic means to right-align numbers etc.
  TColumnTextAlign=(Automatic,Custom);

  // Properties to paint a column name at a TeeGrid header
  TColumnHeader=class(TCellRender)
  private
    FParentFormat: Boolean;
    FTextAlignment: TColumnTextAlign;

    procedure SetParentFormat(const Value: Boolean);
    procedure SetTextAlignment(const Value: TColumnTextAlign);
  public
    Constructor Create(const AChanged:TNotifyEvent); override;
    procedure Assign(Source:TPersistent); override;

    function DisplayText:String;
  published
    property ParentFormat:Boolean read FParentFormat write SetParentFormat default True;
    property TextAlignment:TColumnTextAlign read FTextAlignment
                     write SetTextAlignment default TColumnTextAlign.Automatic;
  end;

  TColumn=class;

  TColumnPaintEvent=procedure(const Sender:TColumn; var AData:TRenderData; var DefaultPaint:Boolean) of object;

  TColumns=class;

  // TColumn class
  // Defines the properties used to paint rows of a TeeGrid column,
  // and an optional "Items" property to contain sub-columns
  TColumn=class(TVisibleRenderItem)
  private
    FDataFormat: TDataFormat;
    FExpanded: Boolean;
    FHeader: TColumnHeader;
    FItems : TColumns;
    FOnPaint : TColumnPaintEvent;
    FParentFormat: Boolean;
    FReadOnly: Boolean;
    FTextAlignment: TColumnTextAlign;
    FWidth: TColumnWidth;

    IHorizontal : THorizontalAlign;
    ITopParent : TColumns;

    function GetAlign: TTextAlign;
    function GetItems: TColumns;
    function GetMargins: TMargins;
    function GetParent: TColumn;
    procedure GetTopParent;
    function MaxLevel:Integer;

    procedure SetAlign(const Value: TTextAlign);
    procedure SetDataFormat(const Value: TDataFormat);
    procedure SetExpanded(const Value: Boolean);
    procedure SetHeader(const Value: TColumnHeader);
    procedure SetItems(const Value: TColumns);
    procedure SetMargins(const Value: TMargins);
    procedure SetParentFormat(const Value: Boolean);
    procedure SetTextAlignment(const Value: TColumnTextAlign);
    procedure SetWidth(const Value: TColumnWidth);
  protected
    procedure DoChanged; override;

    property DefaultHorizAlign:THorizontalAlign read IHorizontal;
  public
    EditorClass : TClass;

    Left : Single;
    ValidWidth : Boolean;

    Constructor Create(ACollection:TCollection); override;

    {$IFNDEF AUTOREFCOUNT}
    Destructor Destroy; override;
    {$ENDIF}

    procedure Assign(Source:TPersistent); override;

    function CanDisplay:Boolean;
    function ParentColumns:TColumns; inline;

    function HasItems:Boolean; inline;

    function HorizAlign:THorizontalAlign; inline;

    procedure InitAlign(const AHorizontal:THorizontalAlign);
    function Level:Integer;
    function Right:Single; inline;

    procedure Paint(var AData:TRenderData; const ARender:TRender); override;

    function ToString:String; override;

    property Parent:TColumn read GetParent;
    property TopParent:TColumns read ITopParent;
  published
    property DataFormat:TDataFormat read FDataFormat write SetDataFormat;
    property Expanded:Boolean read FExpanded write SetExpanded default True;
    property Header:TColumnHeader read FHeader write SetHeader;
    property Items:TColumns read GetItems write SetItems;
    property Margins:TMargins read GetMargins write SetMargins;
    property ParentFormat:Boolean read FParentFormat write SetParentFormat default True;
    property ReadOnly:Boolean read FReadOnly write FReadOnly default False;
    property TextAlign:TTextAlign read GetAlign write SetAlign;
    property TextAlignment:TColumnTextAlign read FTextAlignment
                     write SetTextAlignment default TColumnTextAlign.Automatic;
    property Width:TColumnWidth read FWidth write SetWidth;

    property OnPaint:TColumnPaintEvent read FOnPaint write FOnPaint;
  end;

  TColumnEvent=procedure(Sender:TObject; const AColumn:TColumn) of object;

  // Collection of TColumn,
  // with a "Spacing" property that determines the horizontal separation
  // between columns.
  TColumns=class(TCollectionChange)
  private
    FOnRemoved : TColumnEvent;
    FSpacing : TCoordinate;

    function Get(const Index: Integer): TColumn; {$IFNDEF FPC}inline;{$ENDIF}
    function GetByName(const AName:String): TColumn;
    function GetParent: TColumn;
    function GetSpacing:TCoordinate;
    function MaxLevel:Integer;
    procedure Put(const Index: Integer; const Value: TColumn); {$IFNDEF FPC}inline;{$ENDIF}
    procedure SetSpacing(const Value: TCoordinate);
  protected
    procedure Notify(Item: TCollectionItem; Action: TCollectionNotification); override;
  public
    ValidWidth : Boolean;

    {$IFNDEF AUTOREFCOUNT}
    Destructor Destroy; override;
    {$ENDIF}

    procedure Assign(Source: TPersistent); override;

    function Add:TColumn; overload; {$IFNDEF FPC}inline;{$ENDIF} // not yet supported in FPC
    function Add(const AText:String):TColumn; overload;

    function FindAt(const X,MaxRight:Single):TColumn;
    function FindFirst(const AName:String):TColumn;
    function FirstVisible:TColumn;
    //function GetEnumerator: TColumnsEnumerator;
    function HasSpacing:Boolean; inline;
    function LevelCount:Integer;

    property Items[const Index:Integer]:TColumn read Get write Put; default;

    {$IFNDEF FPC}
    {$IF CompilerVersion>28} // C++ Builder compatibility
    // *** RSP-14999 Workaround: https://quality.embarcadero.com/browse/RSP-14999
    [HPPGEN('__property TColumn* Item2[const System::UnicodeString Name] = {read=GetByName/*, default*/};'+
            sLineBreak+
            'TColumn* operator[](const System::UnicodeString Name) { return this->Item2[Name]; }'
            )]
    {$IFEND}
    {$ENDIF}

    {$IFNDEF FPC}
    property Items[const AName:String]:TColumn read GetByName; default;
    {$ENDIF}

    property Parent:TColumn read GetParent;
  published
    property Spacing: TCoordinate read GetSpacing write SetSpacing;

    property OnRemoved:TColumnEvent read FOnRemoved write FOnRemoved;
  end;

  // Internal use.
  // Contains an array of TColumn with all "Visible" columns of a TeeGrid.
  TVisibleColumns=record
  private
    FVisible : Array of TColumn;

    function Get(const Index: Integer): TColumn; inline;
  public
    procedure Add(const AColumn:TColumn); overload;
    procedure Add(const AColumns:TColumns); overload;
    function AllSameFormat:Boolean;
    procedure Clear; inline;
    function Count:Integer; inline;
    function First:TColumn;
    function IndexOf(const AColumn: TColumn): Integer;
    function Last:TColumn;
    function MaxRight:Single;
    function Next(const AColumn:TColumn):TColumn;
    function Previous(const AColumn:TColumn):TColumn;

    property Items[const Index:Integer]:TColumn read Get; default;
  end;

  // Special column class.
  // Rectangle shape with symbols, to optionally paint as left-most first column
  TIndicator=class(TVisibleTextRender)
  private
    FWidth: TColumnWidth;

    procedure SetWidth(const Value: TColumnWidth);
    function Triangle(const R:TRectF):TPointsF;
  public
    const
      DefaultWidth=10;

    Constructor Create(const AChanged:TNotifyEvent); override;

    {$IFNDEF AUTOREFCOUNT}
    Destructor Destroy; override;
    {$ENDIF}

    procedure Assign(Source:TPersistent); override;

    procedure Paint(var AData:TRenderData); override;
  published
    property Width:TColumnWidth read FWidth write SetWidth;
  end;

  // Experimental, internal
  TColumnUtils=record
  public
    class procedure ChangeHeaderFont(const AColumns:TColumns;
                                     const ASelected:TColumn;
                                     const AStyle:TFontStyles); static;
  end;

implementation

uses
  {System.}SysUtils;

{ TColumn }

// Moved to top, due to inline
function TColumn.HasItems: Boolean;
begin
  result:=(FItems<>nil) and (FItems.Count>0);
end;

function TColumn.ParentColumns:TColumns;
begin
  result:=TColumns(Collection);
end;

Constructor TColumn.Create(ACollection: TCollection);
begin
  inherited;

  FDataFormat:=TDataFormat.Create(Changed);

  FWidth:=TColumnWidth.Create(Changed);

  FHeader:=TColumnHeader.Create(Changed);

  FExpanded:=True;

  FParentFormat:=True;

  GetTopParent;
end;

{$IFNDEF AUTOREFCOUNT}
Destructor TColumn.Destroy;
begin
  FDataFormat.Free;
  FWidth.Free;
  FItems.Free;
  FHeader.Free;

  inherited;
end;
{$ENDIF}

procedure TColumn.Assign(Source: TPersistent);
begin
  if Source is TColumn then
  begin
    DataFormat:=TColumn(Source).FDataFormat;
    FExpanded:=TColumn(Source).FExpanded;
    Header:=TColumn(Source).FHeader;
    FParentFormat:=TColumn(Source).FParentFormat;
    FReadOnly:=TColumn(Source).FReadOnly;
    FTextAlignment:=TColumn(Source).FTextAlignment;
    Width:=TColumn(Source).FWidth;
  end;

  inherited;
end;

procedure TColumn.GetTopParent;
begin
  ITopParent:=ParentColumns;

  while ITopParent.Owner is TColumn do
        ITopParent:=ITopParent.Parent.ParentColumns;
end;

function TColumn.CanDisplay: Boolean;
var tmp : TColumns;
begin
  result:=Visible;

  if result then
  begin
    tmp:=ParentColumns;

    if tmp<>nil then
    while tmp.Owner is TColumn do
    begin
      if not TColumn(tmp.Owner).Visible then
         Exit(False)
      else
         tmp:=TColumn(tmp.Owner).ParentColumns;
    end;
  end;
end;

type
  TTextRenderAccess=class(TTextRender);

procedure TColumn.Paint(var AData:TRenderData; const ARender:TRender);
var tmp : TRender;
    tmpOk : Boolean;
begin
  tmp:=FRender;

  if tmp=nil then
     tmp:=ARender;

  if FTextAlignment=TColumnTextAlign.Automatic then
     if tmp is TTextRender then
        TTextRenderAccess(tmp).TextAlign.InitHorizontal(IHorizontal);

  if Assigned(FOnPaint) then
  begin
    tmpOk:=False;
    FOnPaint(Self,AData,tmpOk);
  end
  else
    tmpOk:=True;

  if tmpOk then
  begin
    if tmp is TTextRender then
       TTextRender(tmp).TextLines:=1; // <-- multi-line cell text?

    tmp.Paint(AData);
  end;
end;

procedure TColumn.SetAlign(const Value: TTextAlign);
begin
  if Render is TTextRender then
     TTextRender(Render).TextAlign:=Value;
  // else
  //   raise ?
end;

procedure TColumn.SetDataFormat(const Value: TDataFormat);
begin
  FDataFormat.Assign(Value);
end;

procedure TColumn.SetExpanded(const Value: Boolean);
begin
  if FExpanded<>Value then
  begin
    FExpanded:=Value;
    DoChanged;
  end;
end;

procedure TColumn.SetHeader(const Value: TColumnHeader);
begin
  FHeader.Assign(Value);
end;

procedure TColumn.SetItems(const Value: TColumns);
begin
  if Value=nil then
  begin
    FItems.Free;
    FItems:=nil;
  end
  else
    Items.Assign(Value);
end;

procedure TColumn.SetMargins(const Value: TMargins);
begin
  if Render is TTextRender then
     TTextRender(FRender).Margins:=Value
  // else
  //   raise ?
end;

procedure TColumn.SetParentFormat(const Value: Boolean);
begin
  if FParentFormat<>Value then
  begin
    FParentFormat:=Value;
    DoChanged;
  end;
end;

procedure TColumn.DoChanged;
begin
  ValidWidth:=False;
  ITopParent.Changed;
end;

function TColumn.GetAlign: TTextAlign;
begin
  if Render is TTextRender then
     result:=TTextRender(FRender).TextAlign
  else
     result:=nil; // raise ?
end;

function TColumn.GetItems: TColumns;
begin
  if FItems=nil then
  begin
    FItems:=TColumns.Create(Self,TCollectionItemClass(ClassType));
    FItems.OnChanged:=ITopParent.OnChanged;
  end;

  result:=FItems;
end;

function TColumn.GetMargins: TMargins;
begin
  if Render is TTextRender then
     result:=TTextRender(Render).Margins
  else
     result:=nil; // raise ?
end;

function TColumn.GetParent:TColumn;
begin
  result:=ParentColumns.Parent;
end;

function TColumn.Right: Single;
begin
  result:=Left+Width.Pixels;
end;

function TColumn.HorizAlign: THorizontalAlign;
begin
  result:=IHorizontal;
end;

function TColumn.Level: Integer;
var tmp : TColumns;
begin
  result:=0;

  tmp:=ParentColumns;

  while tmp<>nil do
  begin
    if tmp.Parent=nil then
       break
    else
    begin
      tmp:=tmp.Parent.ParentColumns;
      Inc(result);
    end;
  end;
end;

function TColumn.MaxLevel: Integer;
begin
  if Visible and (FItems<>nil) then
     result:=FItems.MaxLevel
  else
     result:=0;
end;

procedure TColumn.SetWidth(const Value: TColumnWidth);
begin
  FWidth.Assign(Value);
end;

function TColumn.ToString: String;
begin
  result:=Header.Text;

  if result='' then
     result:=inherited;
end;

procedure TColumn.InitAlign(const AHorizontal: THorizontalAlign);
begin
  IHorizontal:=AHorizontal;
end;

procedure TColumn.SetTextAlignment(const Value: TColumnTextAlign);
begin
  if FTextAlignment<>Value then
  begin
    FTextAlignment:=Value;
    DoChanged;
  end;
end;

{ TColumns }

{$IFNDEF AUTOREFCOUNT}
Destructor TColumns.Destroy;
begin
  FSpacing.Free;
  inherited;
end;
{$ENDIF}

function TColumns.Add:TColumn;
begin
  result:=inherited Add as TColumn;
end;

function TColumns.Add(const AText: String):TColumn;
begin
  result:=Add;
  result.Header.Text:=AText;
end;

procedure TColumns.Assign(Source: TPersistent);
begin
  if Source is TColumns then
     Spacing:=TColumns(Source).FSpacing;

  inherited;
end;

procedure TColumns.SetSpacing(const Value: TCoordinate);
begin
  if Value=nil then
  begin
    FSpacing.Free;
    FSpacing:=nil;
  end
  else
    Spacing.Assign(Value);
end;

function TColumns.Get(const Index: Integer): TColumn;
begin
  result:=TColumn(inherited Items[Index]);
end;

function TColumns.GetByName(const AName: String): TColumn;
begin
  result:=FindFirst(AName);
end;

function TColumns.GetParent: TColumn;
begin
  if Owner is TColumn then
     result:=TColumn(Owner)
  else
     result:=nil;
end;

function TColumns.GetSpacing: TCoordinate;
begin
  if FSpacing=nil then
     FSpacing:=TCoordinate.Create(DoChanged);

  result:=FSpacing;
end;

function TColumns.HasSpacing: Boolean;
begin
  result:=FSpacing<>nil;
end;

function TColumns.FindAt(const X,MaxRight: Single): TColumn;

  // Returns visible column that contains "X"
  function FindColumn(const AColumns:TColumns):TColumn;
  var t : Integer;
      tmp : TColumn;
  begin
    for t:=0 to AColumns.Count-1 do
    begin
      tmp:=AColumns[t];

      if tmp.Visible then
         if tmp.HasItems then
         begin
           result:=FindColumn(tmp.Items);

           if result<>nil then
              Exit;
         end
         else
         if tmp.Left>MaxRight then
            break
         else
         if (X>=tmp.Left) and (X<=tmp.Right) then
            Exit(tmp)
    end;

    result:=nil;
  end;

begin
  result:=FindColumn(Self);
end;

function TColumns.FindFirst(const AName: String): TColumn;
var t : Integer;
    tmp : TColumn;
begin
  for t:=0 to Count-1 do
  begin
    tmp:=Self[t];

    if SameText(tmp.Header.Text,AName) then
       Exit(tmp)
    else
    if tmp.HasItems then
    begin
      result:=tmp.Items.FindFirst(AName);

      if result<>nil then
         Exit;
    end;
  end;

  result:=nil;
end;

function TColumns.FirstVisible: TColumn;
var t : Integer;
begin
  for t:=0 to Count-1 do
      if Items[t].Visible then
         Exit(Items[t]);

  result:=nil;
end;

function TColumns.MaxLevel:Integer;
var t : Integer;
    tmpLevel : Integer;
begin
  result:=0;

  for t:=0 to Count-1 do
  begin
    tmpLevel:=Items[t].MaxLevel+1;

    if tmpLevel>result then
       result:=tmpLevel;
  end;
end;

procedure TColumns.Notify(Item: TCollectionItem;
  Action: TCollectionNotification);
begin
  inherited;

  if (Action=cnExtracting) or (Action=cnDeleting) then
     if Assigned(FOnRemoved) then
        FOnRemoved(Self,Item as TColumn);
end;

function TColumns.LevelCount: Integer;
begin
  result:=MaxLevel;
end;

procedure TColumns.Put(const Index: Integer; const Value: TColumn);
begin
  inherited Items[Index]:=Value;
end;

{ TDataFormat }

Constructor TDataFormat.Create(const AChanged:TNotifyEvent);
begin
  inherited;
  FFloat:=DefaultFloat;
end;

procedure TDataFormat.Assign(Source: TPersistent);
begin
  if Source is TDataFormat then
  begin
    FDate:=TDataFormat(Source).FDate;
    FDateTime:=TDataFormat(Source).FDateTime;
    FFloat:=TDataFormat(Source).FFloat;
    FTime:=TDataFormat(Source).FTime;
  end
  else
    inherited;
end;

procedure TDataFormat.SetDate(const Value: String);
begin
  ChangeString(FDate,Value);
end;

procedure TDataFormat.SetDateTime(const Value: String);
begin
  ChangeString(FDateTime,Value);
end;

procedure TDataFormat.SetFloat(const Value: String);
begin
  ChangeString(FFloat,Value);
end;

procedure TDataFormat.SetTime(const Value: String);
begin
  ChangeString(FTime,Value);
end;

function TDataFormat.IsFloatStored: Boolean;
begin
  result:=FFloat<>DefaultFloat;
end;

{ TColumnHeader }

Constructor TColumnHeader.Create(const AChanged: TNotifyEvent);
begin
  inherited;
  FParentFormat:=True;
end;

function TColumnHeader.DisplayText: String;
begin
  result:=Text;

  if result='' then
     result:='(empty)';
end;

procedure TColumnHeader.Assign(Source: TPersistent);
begin
  if Source is TColumnHeader then
  begin
    FParentFormat:=TColumnHeader(Source).ParentFormat;
    FTextAlignment:=TColumnHeader(Source).TextAlignment;
  end;

  inherited;
end;

procedure TColumnHeader.SetParentFormat(const Value: Boolean);
begin
  ChangeBoolean(FParentFormat,Value);
end;

procedure TColumnHeader.SetTextAlignment(const Value: TColumnTextAlign);
begin
  if FTextAlignment<>Value then
  begin
    FTextAlignment:=Value;
    Changed;
  end;
end;

{ TVisibleColumns }

procedure TVisibleColumns.Clear;
begin
  FVisible:=nil;
end;

function TVisibleColumns.Count: Integer;
begin
  result:=Length(FVisible);
end;

procedure TVisibleColumns.Add(const AColumn: TColumn);
var L : Integer;
begin
  L:=Length(FVisible);
  SetLength(FVisible,L+1);
  FVisible[L]:=AColumn;
end;

// Recursive fill VisibleColumns array
procedure TVisibleColumns.Add(const AColumns: TColumns);

  procedure AddColumns(const AColumns:TColumns);
  var t : Integer;
      tmp : TColumn;
  begin
    for t:=0 to AColumns.Count-1 do
    begin
      tmp:=AColumns[t];

      if tmp.Visible then
         if tmp.HasItems and tmp.Expanded then
            AddColumns(tmp.Items)
         else
            Add(tmp);
    end;
  end;

begin
  AddColumns(AColumns);
end;

function TVisibleColumns.AllSameFormat: Boolean;
var t : Integer;
begin
  for t:=0 to High(FVisible) do
      if (not FVisible[t].ParentFormat) and FVisible[t].HasFormat then
         Exit(False);

  result:=True;
end;

function TVisibleColumns.First: TColumn;
begin
  if FVisible=nil then
     result:=nil
  else
     result:=FVisible[0];
end;

function TVisibleColumns.Get(const Index: Integer): TColumn;
begin
  result:=FVisible[Index];
end;

function TVisibleColumns.Last: TColumn;
begin
  if FVisible=nil then
     result:=nil
  else
     result:=FVisible[High(FVisible)];
end;

function TVisibleColumns.MaxRight: Single;
begin
  if Count=0 then
     result:=0
  else
     result:=FVisible[Count-1].Right;
end;

function TVisibleColumns.IndexOf(const AColumn: TColumn): Integer;
var t : Integer;
begin
  for t:=0 to High(FVisible) do
      if FVisible[t]=AColumn then
         Exit(t);

  result:=-1;
end;

function TVisibleColumns.Next(const AColumn: TColumn): TColumn;
var tmp : Integer;
begin
  tmp:=IndexOf(AColumn);

  if (tmp=-1) or (tmp=High(FVisible)) then
     result:=nil
  else
     result:=FVisible[tmp+1];
end;

function TVisibleColumns.Previous(const AColumn: TColumn): TColumn;
var tmp : Integer;
begin
  tmp:=IndexOf(AColumn);

  if tmp>0 then
     result:=FVisible[tmp-1]
  else
     result:=nil;
end;

{ TIndicator }

procedure TIndicator.Assign(Source: TPersistent);
begin
  if Source is TIndicator then
     Width:=TIndicator(Source).FWidth;

  inherited;
end;

Constructor TIndicator.Create(const AChanged:TNotifyEvent);
begin
  inherited;

  Format.Brush.InitVisible(True);
  Format.Brush.InitColor(TColors.Black);

  FWidth:=TColumnWidth.Create(IChanged);
  FWidth.InitValue(DefaultWidth);
  FWidth.Pixels:=DefaultWidth;
end;

{$IFNDEF AUTOREFCOUNT}
Destructor TIndicator.Destroy;
begin
  FWidth.Free;
  inherited;
end;
{$ENDIF}

function TIndicator.Triangle(const R:TRectF):TPointsF;
var tmpXSize,
    tmpYSize : Single;
    tmpX,
    tmpY : Single;
begin
  tmpXSize:=R.Width*0.3;
  tmpYSize:=tmpXSize*1.4;

  if tmpYSize>R.Height*0.4 then
  begin
    tmpYSize:=R.Height*0.4;
    tmpXSize:=tmpYSize;
  end;

  tmpX:=R.CenterPoint.X;
  tmpY:=R.CenterPoint.Y;

  SetLength(result,3);

  result[0]:=TPointF.Create(tmpX-tmpXSize,tmpY-tmpYSize);
  result[1]:=TPointF.Create(tmpX-tmpXSize,tmpY+tmpYSize);
  result[2]:=TPointF.Create(tmpX+tmpXSize,tmpY);
end;

procedure TIndicator.Paint(var AData:TRenderData);
begin
  AData.Painter.Paint(Format,Triangle(AData.Rect));
end;

procedure TIndicator.SetWidth(const Value: TColumnWidth);
begin
  FWidth.Assign(Value);
end;

{ TColumnUtils }

class procedure TColumnUtils.ChangeHeaderFont(const AColumns:TColumns;
                                              const ASelected:TColumn;
                                              const AStyle:TFontStyles);

  procedure SwitchBold(const AColumns:TColumns);
  var tmp : TColumn;
      t : Integer;
      tmpFont : TFont;
  begin
    for t:=0 to AColumns.Count-1 do
    begin
      tmp:=AColumns[t];

      if tmp.Visible then
      begin
        tmpFont:=tmp.Header.Format.Font;

        if tmp=ASelected then
        begin
          tmpFont.Style:=AStyle;
          //tmp.Header.ParentFormat:=False;
        end
        else
        begin
          tmpFont.Style:=[];
          //tmp.Header.ParentFormat:=True;
        end;

        SwitchBold(tmp.Items);
      end;
    end;
  end;

begin
  SwitchBold(AColumns);
end;

end.
