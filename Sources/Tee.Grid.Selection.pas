{*********************************************}
{  TeeGrid Software Library                   }
{  Grid Cells selection class                 }
{  Copyright (c) 2016 by Steema Software      }
{  All Rights Reserved                        }
{*********************************************}
unit Tee.Grid.Selection;
{$I Tee.inc}

interface

{
   TGridSelection class is responsible to paint and mouse-hover highlight
   grid cells.

   Supports single and multi-cell range selection.

   Examples:

     TeeGrid1.Selected.Column:= TeeGrid1.Columns[3];
     TeeGrid1.Selected.Row:= 42;

     // Multi-cell range:

     TeeGrid1.Selected.Range.FromRow:= 10;
     TeeGrid1.Selected.Range.ToRow:= 10;

     TeeGrid1.Selected.Range.FromColumn:= TeeGrid1.Columns[3];
     TeeGrid1.Selected.Range.ToColumn:= TeeGrid1.Columns[6];

   Needs: Tee.Painter, Tee.Format, Tee.Grid.Columns, Tee.Renders
}


uses
  {System.}Classes,

  Tee.Painter, Tee.Format, Tee.Grid.Columns, Tee.Renders;

type
  TSelectionRange=class(TPersistentChange)
  private
    FFromRow: Integer;
    FFromColumn: TColumn;
    FToRow: Integer;
    FToColumn: TColumn;
    FEnabled: Boolean;

    procedure SetFromColumn(const Value: TColumn);
    procedure SetFromRow(const Value: Integer);
    procedure SetToColumn(const Value: TColumn);
    procedure SetToRow(const Value: Integer);
    procedure SetEnabled(const Value: Boolean);
  public
    Constructor Create(const AChanged:TNotifyEvent); override;

    procedure Assign(Source:TPersistent); override;
  published
    property Enabled:Boolean read FEnabled write SetEnabled default True;
    property FromColumn:TColumn read FFromColumn write SetFromColumn;
    property FromRow:Integer read FFromRow write SetFromRow default -1;
    property ToColumn:TColumn read FToColumn write SetToColumn;
    property ToRow:Integer read FToRow write SetToRow default -1;
  end;

  TGridSelection=class(TVisibleTextRender)
  private
    FColumn : TColumn;
    FRange : TSelectionRange;
    FRow : Integer;
    FFull: Boolean;
    FOnChange: TNotifyEvent;
    FParentFont: Boolean;
    FUnFocused : TVisibleTextRender;

    procedure ChangeColumn(const Value:TColumn);
    procedure DoChanged;
    procedure ResetRange;
    procedure SetColumn(const Value: TColumn);
    procedure SetRange(const Value: TSelectionRange);
    procedure SetRow(const Value: Integer);
    procedure SetFull(const Value: Boolean);
    procedure SetParentFont(const Value: Boolean);
    procedure SetUnfocused(const Value: TVisibleTextRender);
  protected
    CheckScroll : Boolean;
  public
    Dragging : Single;

    Constructor Create(const AChanged:TNotifyEvent); override;

    {$IFNDEF AUTOREFCOUNT}
    Destructor Destroy; override;
    {$ENDIF}

    procedure Assign(Source:TPersistent); override;

    procedure Change(const AColumn:TColumn; const ARow:Integer);
    procedure Clear;
    procedure GetColumns(const Visible:TVisibleColumns; out AFrom,ATo:Integer);
    procedure InitFormat;
    function IsEmpty:Boolean;

    procedure PaintColumn(var AData:TRenderData; const AColumn:TColumn; const AFont:TFont; const IsFocused:Boolean);

    property OnChange:TNotifyEvent read FOnChange write FOnChange;
  published
    property Column:TColumn read FColumn write SetColumn stored False;
    property FullRow:Boolean read FFull write SetFull default False;
    property ParentFont:Boolean read FParentFont write SetParentFont default True;
    property Range:TSelectionRange read FRange write SetRange;
    property Row:Integer read FRow write SetRow stored False;
    property UnFocused:TVisibleTextRender read FUnfocused write SetUnfocused;
  end;

implementation

{$IFNDEF NOUITYPES}
uses
  {System.}UITypes;
{$ENDIF}

{ TSelectionRange }

Constructor TSelectionRange.Create(const AChanged: TNotifyEvent);
begin
  inherited;

  FEnabled:=True;
  FFromRow:=-1;
  FToRow:=-1;
end;

procedure TSelectionRange.Assign(Source: TPersistent);
begin
  if Source is TSelectionRange then
  begin
    FFromColumn:=TSelectionRange(Source).FFromColumn;
    FFromRow:=TSelectionRange(Source).FFromRow;
    FToColumn:=TSelectionRange(Source).FToColumn;
    FToRow:=TSelectionRange(Source).FToRow;
  end
  else
    inherited;
end;

procedure TSelectionRange.SetEnabled(const Value: Boolean);
begin
  ChangeBoolean(FEnabled,Value);

  if not FEnabled then
  begin
    FToColumn:=FFromColumn;
    FToRow:=FFromRow;
  end;
end;

procedure TSelectionRange.SetFromColumn(const Value: TColumn);
begin
  if FFromColumn<>Value then
  begin
    FFromColumn:=Value;
    Changed;
  end;
end;

procedure TSelectionRange.SetFromRow(const Value: Integer);
begin
  ChangeInteger(FFromRow,Value);
end;

procedure TSelectionRange.SetToColumn(const Value: TColumn);
begin
  if FToColumn<>Value then
  begin
    FToColumn:=Value;
    Changed;
  end;
end;

procedure TSelectionRange.SetToRow(const Value: Integer);
var tmp : Integer;
begin
  ChangeInteger(FToRow,Value);

  if FToRow<FFromRow then
  begin
    tmp:=FFromRow;
    FFromRow:=FToRow;
    FToRow:=tmp;
  end;
end;

{ TGridSelection }

Constructor TGridSelection.Create(const AChanged:TNotifyEvent);
begin
  inherited Create(AChanged);

  FRow:=-1;

  FRange:=TSelectionRange.Create(IChanged);

  FUnFocused:=TVisibleTextRender.Create(IChanged);

  FParentFont:=True;

  InitFormat;

  CheckScroll:=True;
end;

{$IFNDEF AUTOREFCOUNT}
Destructor TGridSelection.Destroy;
begin
  FUnFocused.Free;
  FRange.Free;

  inherited;
end;
{$ENDIF}

procedure TGridSelection.ChangeColumn(const Value:TColumn);
begin
  if (Value<>nil) and Value.HasItems then
     FColumn:=Value.Items[0]
  else
     FColumn:=Value;
end;

procedure TGridSelection.Clear;
begin
  Change(nil,-1);
end;

procedure TGridSelection.Assign(Source: TPersistent);
begin
  if Source is TGridSelection then
  begin
    FFull:=TGridSelection(Source).FFull;
    Range:=TGridSelection(Source).FRange;
    UnFocused:=TGridSelection(Source).FUnFocused;
  end;

  inherited;
end;

procedure TGridSelection.Change(const AColumn: TColumn; const ARow: Integer);
begin
  if (Column<>AColumn) or (Row<>ARow) then
  begin
    ChangeColumn(AColumn);
    FRow:=ARow;

    ResetRange;
    DoChanged;
  end;
end;

procedure TGridSelection.InitFormat;
begin
  Format.Brush.InitVisible(True);
  Format.Brush.InitColor(TColors.Skyblue);

  FUnFocused.Format.Brush.InitVisible(True);
  FUnFocused.Format.Brush.InitColor(TColors.Lightgray);

  Format.Stroke.InitVisible(True);
  Format.Stroke.InitStyle(TStrokeStyle.Dot);
  Format.Stroke.Brush.InitColor(TColors.DkGray);
end;

procedure TGridSelection.ResetRange;
begin
  FRange.FromColumn:=FColumn;
  FRange.ToColumn:=FColumn;
  FRange.FromRow:=FRow;
  FRange.ToRow:=FRow;
end;

function TGridSelection.IsEmpty: Boolean;
begin
  result:=(Column=nil) or (Row=-1);
end;

procedure TGridSelection.GetColumns(const Visible:TVisibleColumns; out AFrom,ATo:Integer);
begin
  if FullRow then
  begin
    ATo:=Visible.Count-1;

    if ATo=-1 then
       AFrom:=-1
    else
       AFrom:=0;
  end
  else
  begin
    AFrom:=Visible.IndexOf(Range.FromColumn);
    ATo:=Visible.IndexOf(Range.ToColumn);
  end;
end;

procedure TGridSelection.PaintColumn(var AData:TRenderData; const AColumn:TColumn; const AFont:TFont; const IsFocused:Boolean);

  function IsCurrent:Boolean;
  begin
    result:=(AColumn=Column) and (AData.Row=Row);
  end;

  procedure FillBack;
  var tmp : TBrush;
  begin
    if IsFocused then
       tmp:=Format.Brush
    else
       tmp:=Unfocused.Format.Brush;

    if tmp.Visible then
    begin
      AData.Painter.SetBrush(tmp);
      AData.Painter.Fill(AData.Rect);
    end;
  end;

var Old : Boolean;
begin
  if IsCurrent then // highlight single cell when range is enabled
  begin
    Old:=PaintText;
    PaintText:=False;

    if IsFocused then
       inherited Paint(AData)
    else
       UnFocused.Paint(AData);

    PaintText:=Old;
  end
  else
    FillBack;

  if PaintText or Format.Brush.Visible then
  begin
    if ParentFont then
       AData.Painter.SetFont(AFont)
    else
    if IsFocused then
       AData.Painter.SetFont(Format.Font)
    else
       AData.Painter.SetFont(Unfocused.Format.Font);

    //TextAlign.Vertical:=AColumn.TextAlign...

    if IsFocused then
       AColumn.Paint(AData,Self)
    else
       AColumn.Paint(AData,UnFocused);
  end;
end;

procedure TGridSelection.DoChanged;
begin
  if Assigned(FOnChange) then
     FOnChange(Self);
end;

procedure TGridSelection.SetColumn(const Value: TColumn);
begin
  if Column<>Value then
  begin
    ChangeColumn(Value);
    ResetRange;

    DoChanged;
  end;
end;

procedure TGridSelection.SetFull(const Value: Boolean);
begin
  ChangeBoolean(FFull,Value);
end;

procedure TGridSelection.SetParentFont(const Value: Boolean);
begin
  ChangeBoolean(FParentFont,Value);
end;

procedure TGridSelection.SetRange(const Value: TSelectionRange);
begin
  FRange.Assign(Value);
end;

procedure TGridSelection.SetRow(const Value: Integer);
begin
  if Row<>Value then
  begin
    FRow:=Value;
    ResetRange;

    DoChanged;
  end;
end;

procedure TGridSelection.SetUnfocused(const Value: TVisibleTextRender);
begin
  FUnfocused.Assign(Value);
end;

end.

