{*********************************************}
{  TeeGrid Software Library                   }
{  DB TDataSet Virtual Data                   }
{  Copyright (c) 2016 by Steema Software      }
{  All Rights Reserved                        }
{*********************************************}
unit Tee.Grid.Data.DB;
{$I Tee.inc}

interface

{
  Virtual Data class to link TDataSet fields to a TeeGrid

  Usage examples:

  uses Tee.Grid.Data.DB;

  TeeGrid1.Data:= TVirtualDBData.From(DataSource1);

  TeeGrid1.Data:= TVirtualDBData.From(MyDataSet1);

}

uses
  {System.}Classes, {Data.}DB, Tee.Grid.Columns, Tee.Grid.Data, Tee.Painter;

type
  TVirtualDBData=class;

  TVirtualDataLink=class(TDataLink)
  private
    IData : TVirtualDBData;

    IChanging : Boolean;

    procedure ChangeRow(const ARow:Integer);
  protected
    procedure ActiveChanged; override;
    procedure LayoutChanged; override;
    procedure RecordChanged(Field: TField); override;
    procedure UpdateData; override;
  end;

  TVirtualDBData=class(TVirtualData)
  protected
    IDataSet : TDataSet;
    IDataSource : TDataSource;

    ILink : TVirtualDataLink;

    OwnsDataSource : Boolean;

    class function Add(const AColumns:TColumns; const AField:TField):TColumn;
    procedure CreateLink;
    function FieldOf(const AColumn:TColumn):TField; inline;
    class function HorizAlignOf(const AField:TField):THorizontalAlign; static;
    procedure RowChanged(const ARow:Integer); override;
  public
    Destructor Destroy; override;

    procedure AddColumns(const AColumns:TColumns); override;
    function AsString(const AColumn:TColumn; const ARow:Integer):String; override;
    function AutoWidth(const APainter:TPainter; const AColumn:TColumn):Single; override;
    function Count:Integer; override;

    class function From(const ASource:TComponent):TVirtualData; override;

    procedure Load; override;
    function ReadOnly(const AColumn:TColumn):Boolean; override;
    procedure SetValue(const AColumn:TColumn; const ARow:Integer; const AText:String); override;
  end;

implementation

uses
  Tee.Format;

{ TVirtualDBData }

Destructor TVirtualDBData.Destroy;
begin
  if OwnsDataSource then
     IDataSource.Free;

  ILink.Free;

  inherited;
end;

class function TVirtualDBData.HorizAlignOf(const AField:TField):THorizontalAlign;
begin
  case AField.DataType of
    {$IFNDEF FPC}
    ftShortInt,
    {$ENDIF}
    ftSmallint,
    ftInteger,
    ftWord,
    ftAutoInc,

    {$IFNDEF FPC}
    ftByte,
    {$ENDIF}

    {$IFNDEF FPC}
    ftLongWord,
    {$ENDIF}
    ftLargeint,

    ftDate,
    ftTime,
    ftDateTime,

    ftCurrency,

    {$IFNDEF FPC}
    {$IF CompilerVersion>20}
    ftSingle,
    {$IFEND}
    {$ENDIF}

    ftFloat : result:=THorizontalAlign.Right;

  else
    result:=THorizontalAlign.Left;
  end;
end;

class function TVirtualDBData.Add(const AColumns: TColumns;
  const AField: TField): TColumn;
begin
  result:=AColumns.Add;
  result.TagObject:=AField;
  result.Header.Text:=AField.DisplayName;

  result.InitAlign(HorizAlignOf(AField));
end;

procedure TVirtualDBData.AddColumns(const AColumns: TColumns);
var tmp : TField;
begin
  if IDataSet<>nil then
  begin
    if IDataSet.Fields.Count>0 then
       for tmp in IDataSet.Fields do
           if tmp.Visible then
              Add(AColumns,tmp);
  end;
end;

function TVirtualDBData.FieldOf(const AColumn:TColumn):TField;
begin
  result:=TField(AColumn.TagObject);
end;

type
  TPictureAccess=class(TPicture);

function TVirtualDBData.AsString(const AColumn: TColumn; const ARow: Integer): String;

  procedure SetBlobPicture(const AField:TField);

    function GetPicture:TPersistent;
    begin
      result:=PictureClass.Create;
      result.Assign(AField);
    end;

  var tmp : TPersistent;
  begin
    tmp:=PictureClass.Create;
    tmp.Assign(AField);

    AColumn.ParentFormat:=False;
    AColumn.Format.Brush.Show;
    AColumn.Format.Brush.Picture.SetGraphic(tmp);

    TPictureAccess(AColumn.Format.Brush.Picture).Original:=tmp;
  end;

var Old : Integer;
    tmp : TField;
begin
  Old:=ILink.ActiveRecord;
  ILink.ActiveRecord:=ARow;

  tmp:=FieldOf(AColumn);

  if tmp is TBlobField then
  begin
    SetBlobPicture(tmp);
    result:='';
  end
  else
    result:=tmp.DisplayText;

  ILink.ActiveRecord:=Old;
end;

function TVirtualDBData.AutoWidth(const APainter: TPainter;
  const AColumn: TColumn): Single;

  function GuessWidth:Integer;
  begin
    case FieldOf(AColumn).DataType of
      {$IFNDEF FPC}
      ftShortInt,
      {$ENDIF}
      ftSmallint : result:=5;

      ftInteger,
      ftWord,
      ftAutoInc  : result:=9;

      {$IFNDEF FPC}
      ftByte     : result:=3;
      {$ENDIF}

      {$IFNDEF FPC}
      ftLongWord,
      {$ENDIF}
      ftLargeint : result:=12;

      ftDate     : result:=Length(SampleDate(AColumn));
      ftTime     : result:=Length(SampleTime(AColumn));
      ftDateTime : result:=Length(SampleDateTime(AColumn));

      ftCurrency,

      {$IFNDEF FPC}
      {$IF CompilerVersion>20}
      ftSingle,
      {$IFEND}
      {$ENDIF}

      ftFloat    : result:=18;

      ftBoolean  : result:=5;

    {
      ftString,
      ftMemo,
      ftFmtMemo,
      ftFixedChar,
      ftWideString,
      ftWideMemo,
      ftFixedWideChar : result:=10;
    }
    else
      result:=10;
    end;
  end;

var tmpL : Integer;
begin
  tmpL:=FieldOf(AColumn).DisplayWidth;

  if tmpL=0 then
     tmpL:=GuessWidth;

  result:=APainter.TextWidth('0')*tmpL;
end;

function TVirtualDBData.Count: Integer;
begin
  if (IDataSet<>nil) and IDataSet.Active then
     result:=IDataSet.RecordCount
  else
     result:=0;
end;

class function TVirtualDBData.From(const ASource: TComponent): TVirtualData;

  function From(const ASource:TDataSource):TVirtualDBData;
  begin
    result:=TVirtualDBData.Create;
    result.IDataSource:=ASource;
    result.IDataSet:=result.IDataSource.DataSet;

    result.CreateLink;
  end;

var tmp : TDataSource;
begin
  if ASource is TDataSet then
  begin
    tmp:=TDataSource.Create(nil);
    tmp.DataSet:=ASource as TDataSet;

    result:=From(tmp);
    TVirtualDBData(result).OwnsDataSource:=True;
  end
  else
  if ASource is TDataSource then
     result:=From(ASource as TDataSource)
  else
     result:=nil;
end;

procedure TVirtualDBData.CreateLink;
begin
  ILink:=TVirtualDataLink.Create;
  ILink.IData:=Self;
  ILink.DataSource:=IDataSource;

  ILink.BufferCount:=10000; //??
end;

procedure TVirtualDBData.Load;
begin
  {$IFNDEF FPC}
  inherited;
  {$ENDIF}
end;

function TVirtualDBData.ReadOnly(const AColumn:TColumn): Boolean;
begin
  result:=not FieldOf(AColumn).CanModify;
end;

procedure TVirtualDBData.RowChanged(const ARow: Integer);
begin
  inherited;
  ILink.ChangeRow(ARow);
end;

procedure TVirtualDBData.SetValue(const AColumn: TColumn; const ARow: Integer;
  const AText: String);
var Old : Integer;
begin
  Old:=ILink.ActiveRecord;
  ILink.ActiveRecord:=ARow+1;

  IDataSet.Edit;

  FieldOf(AColumn).AsString:=AText;

  IDataSet.Post;

  ILink.ActiveRecord:=Old;
end;

{ TVirtualDataLink }

procedure TVirtualDataLink.ActiveChanged;
begin
  inherited;
  IData.Refresh;
end;

procedure TVirtualDataLink.ChangeRow(const ARow:Integer);
begin
  if (ARow<>-1) and (DataSet<>nil) and DataSet.Active then
  begin
    IChanging:=True;

    if DataSource.DataSet.RecNo<>ARow+1 then
       DataSource.DataSet.RecNo:=ARow+1;

    IChanging:=False;
  end;
end;

procedure TVirtualDataLink.LayoutChanged;
begin
  inherited;
  IData.Refresh;
end;

procedure TVirtualDataLink.RecordChanged(Field: TField);
begin
  inherited;

  if not IChanging then
     IData.ChangeSelectedRow(DataSet.RecNo-1);
end;

procedure TVirtualDataLink.UpdateData;
begin
  inherited;
end;

initialization
  TVirtualDataClasses.Register(TVirtualDBData);
finalization
  TVirtualDataClasses.UnRegister(TVirtualDBData);
end.
