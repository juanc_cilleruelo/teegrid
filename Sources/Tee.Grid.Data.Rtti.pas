{*********************************************}
{  TeeGrid Software Library                   }
{  VirtualData from Records and Objects       }
{  Copyright (c) 2016 by Steema Software      }
{  All Rights Reserved                        }
{*********************************************}
unit Tee.Grid.Data.Rtti;

interface

{
   Several classes to automatically link a TeeGrid with records or classes,
   or arrays of records or classes, or TList of records and classes.

   Example using arrays:

    type
      TPerson=record ... end;  // <-- or class

    var
      MyData : TArray<TPerson>;

      SetLength(MyData,10);
      ... fill MyData ....

      TeeGrid1.Data:= TVirtualData<TArray<TPerson>>.Create(MyData);

    Example using TList:

    var
      MyData : TList<TPerson>;

      TeeGrid1.Data:= TVirtualData<TList<TPerson>>.Create(MyData);

    Example using single records:

    var
      MyData : TPerson;

      TeeGrid1.Data:= TVirtualData<TPerson>.Create(MyData);

    Note:

      These are equivalent classes, for easier use:

      TeeGrid1.Data:= TVirtualArrayData<TPerson>.Create(MyData);

      TeeGrid1.Data:= TVirtualData<TArray<TPerson>>.Create(MyData);

}

uses
  Tee.Grid.Data, Tee.Grid.Columns, Tee.Painter,
  System.Generics.Collections, System.Rtti, System.TypInfo;

type
  TVirtualDataRtti=class(TVirtualData)
  private
    class function Add(const AColumns:TColumns;
                       const AMember:TRttiMember;
                       const AType:TRttiType):TColumn; overload; static;

    procedure DoSetValue(const AColumn:TColumn; const ADest:TValue; const Value:TValue);

    function FinalValue(const AColumn: TColumn; const AValue:TValue):TValue; overload;

    class function IsBoolean(const AType:TRttiType):Boolean; static;
    class function IsDate(const AType:TRttiType):Boolean; static;
    class function IsTime(const AType:TRttiType):Boolean; static;
    class function IsDateTime(const AType:TRttiType):Boolean; static;
    class function IsNumeric(const AType:TRttiType):Boolean; overload; static;

    class function MemberOf(const AColumn:TColumn):TRttiMember; inline; static;
    class function PointerOf(const AValue:TValue):Pointer; static;
    class function TypeOf(const AColumn: TColumn): TRttiType; static;
    class function ValueOf(const AMember:TRttiMember; const P:Pointer):TValue; overload; static;
    class function ValueOf(const AColumn:TColumn; const P:Pointer):TValue; overload; static; inline;
  end;

  TVisibility=set of TMemberVisibility;

  TRttiMembers=(Both, Fields, Properties);

  TVirtualData<T>=class(TVirtualDataRtti)
  private
  class var
    Context : TRttiContext;

  var
    FData : ^T;
    FMembers : TRttiMembers;
    FTypeInfo : PTypeInfo;
    FVisibility : TVisibility;

    IsDynArray : Boolean;
    IItems : TRttiIndexedProperty;
    ICount : TRttiProperty;
    IObject : TObject;

    procedure AddFields(const AColumns:TColumns; const AType:TRttiType);
    procedure AddProperties(const AColumns:TColumns; const AType:TRttiType);

    procedure DoAddColumns(const AColumns:TColumns; const AType:TRttiType);

    procedure InternalAddType(const AColumns:TColumns;
                              const AMember:TRttiMember;
                              const AType:TRttiType);

    function IsVisible(const AMember:TRttiMember):Boolean; inline;

    class function NameOf(const AType:TRttiOrdinalType):String; overload; static;
    class function NameOf(const AType:TRttiFloatType):String; overload; static;

    procedure TrySetInt64(const AColumn: TColumn; const ARow: Integer; const AText: String);
    procedure TrySetFloat(const AColumn: TColumn; const ARow: Integer; const AText: String);
  protected
    function RowValue(const ARow:Integer):TValue; virtual;
    function RttiType:TRttiType; inline;
  public
    Constructor Create(var AData:T;
                       const AVisibility:TVisibility=[mvPublic,mvPublished];
                       const AMembers:TRttiMembers=TRttiMembers.Both); overload;

    procedure AddColumns(const AColumns:TColumns); override;
    function AsString(const AColumn:TColumn; const ARow:Integer):String; override;
    function AutoWidth(const APainter:TPainter; const AColumn:TColumn):Single; override;
    function Count:Integer; override;
    procedure Load; override;
    procedure SetValue(const AColumn:TColumn; const ARow:Integer; const AText:String); override;
  end;

  // Helper classes, just aliases:
  TVirtualArrayData<T>=class(TVirtualData<TArray<T>>);
  TVirtualListData<T>=class(TVirtualData<TList<T>>);

implementation

uses
  {System.}SysUtils,
  Tee.Renders;

{ TVirtualDataRtti }

class function TVirtualDataRtti.IsNumeric(const AType:TRttiType):Boolean;
begin
  case AType.TypeKind of
    tkInteger,
      tkInt64,
      tkFloat  : result:=True;
  else
    result:=False;
  end;
end;

class function TVirtualDataRtti.IsBoolean(const AType:TRttiType):Boolean;
begin
  result:=(AType.Name='Boolean') or
          ((AType.BaseType<>nil) and (AType.BaseType.Handle=TypeInfo(Boolean)));
end;

class function TVirtualDataRtti.IsDate(const AType:TRttiType):Boolean;
var tmp : PTypeInfo;
begin
  tmp:=AType.Handle;

  if GetTypeData(tmp)^.FloatType=ftDouble then
     result:=(tmp=System.TypeInfo(TDate))
  else
     result:=False;
end;

class function TVirtualDataRtti.IsTime(const AType:TRttiType):Boolean;
var tmp : PTypeInfo;
begin
  tmp:=AType.Handle;

  if GetTypeData(tmp)^.FloatType=ftDouble then
     result:=(tmp=System.TypeInfo(TTime))
  else
     result:=False;
end;

class function TVirtualDataRtti.IsDateTime(const AType:TRttiType):Boolean;
var tmp : PTypeInfo;
begin
  tmp:=AType.Handle;

  if GetTypeData(tmp)^.FloatType=ftDouble then
     result:=(tmp=System.TypeInfo(TDateTime))
  else
     result:=False;
end;

class function TVirtualDataRtti.Add(const AColumns:TColumns;
                                    const AMember:TRttiMember;
                                    const AType:TRttiType):TColumn;
begin
  result:=Add(AColumns,AMember.Name,AMember);

  if IsNumeric(AType) then
     result.InitAlign(THorizontalAlign.Right)
  else
  if IsBoolean(AType) then
  begin
    result.Render:=TBooleanRender.Create(result.Changed);
    result.TextAlignment:=TColumnTextAlign.Custom;
  end;
end;

class function TVirtualDataRtti.MemberOf(const AColumn:TColumn):TRttiMember;
begin
  result:=TRttiMember(AColumn.TagObject);
end;

procedure TVirtualDataRtti.DoSetValue(const AColumn:TColumn; const ADest:TValue; const Value:TValue);
var P: Pointer;
    tmp : TRttiMember;
begin
  P:=PointerOf(FinalValue(AColumn,ADest));

  tmp:=MemberOf(AColumn);

  if tmp is TRttiField then
     TRttiField(tmp).SetValue(P,Value)
  else
     TRttiProperty(tmp).SetValue(P,Value);
end;

class function TVirtualDataRtti.PointerOf(const AValue:TValue):Pointer;
begin
  if AValue.IsObject then
     result:=AValue.AsObject
  else
     result:=AValue.GetReferenceToRawData;
end;

class function TVirtualDataRtti.ValueOf(const AMember:TRttiMember; const P:Pointer):TValue;
begin
  if AMember is TRttiField then
     result:=TRttiField(AMember).GetValue(P)
  else
     result:=TRttiProperty(AMember).GetValue(P);
end;

class function TVirtualDataRtti.TypeOf(const AColumn: TColumn): TRttiType;
var tmp : TRttiMember;
begin
  if AColumn.TagObject is TRttiMember then
  begin
    tmp:=MemberOf(AColumn);

    if tmp is TRttiField then
       result:=TRttiField(tmp).FieldType
    else
       result:=TRttiProperty(tmp).PropertyType;
  end
  else
  if AColumn.TagObject is TRttiType then
     result:=TRttiType(AColumn.TagObject)
  else
     result:=nil;
end;

class function TVirtualDataRtti.ValueOf(const AColumn:TColumn; const P:Pointer):TValue;
begin
  result:=ValueOf(MemberOf(AColumn),P);
end;

function TVirtualDataRtti.FinalValue(const AColumn: TColumn; const AValue:TValue):TValue;

  function FinalPointer(P:Pointer):Pointer;
  var t,L : Integer;
      tmp : TColumn;
      tmpMember : TRttiMember;
      Parents : Array[0..99] of TColumn;
  begin
    tmp:=AColumn.Parent;

    if tmp<>nil then
    begin
      L:=0;

      while tmp<>nil do
      begin
        Parents[L]:=tmp;
        Inc(L);
        tmp:=tmp.Parent;
      end;

      // Backwards traverse
      for t:=L-1 downto 0 do
      begin
        tmpMember:=MemberOf(Parents[t]);

        if tmpMember is TRttiField then
           P:=Pointer(NativeInt(P)+TRttiField(tmpMember).Offset)
        else
           P:=PointerOf(ValueOf(Parents[t],P));
      end;
    end;

    result:=P;
  end;

begin
  if AColumn.TagObject is TRttiMember then
     result:=ValueOf(AColumn,FinalPointer(PointerOf(AValue)))
  else
     result:=AValue;
end;

{ TVirtualData<T> }

Constructor TVirtualData<T>.Create(var AData:T;
                                   const AVisibility: TVisibility;
                                   const AMembers: TRttiMembers);
var tmp : TRttiType;
begin
  inherited Create;

  FData:=@AData;

  FMembers:=AMembers;
  FTypeInfo:=TypeInfo(T);
  FVisibility:=AVisibility;

  tmp:=RttiType;

  IsDynArray:=tmp is TRttiDynamicArrayType;

  if not IsDynArray then
  begin
    IItems:=tmp.GetIndexedProperty('Items');

    if IItems<>nil then
    begin
      IObject:=TValue.From<T>(FData^).AsObject;

      ICount:=tmp.GetProperty('Count');

      if ICount=nil then
         IItems:=nil;
    end;
  end;
end;

function TVirtualData<T>.Count: Integer;
begin
  if IsDynArray then
     result:=TValue.From<T>(FData^).GetArrayLength
  else
  if IItems=nil then
     result:=1
  else
     result:=ICount.GetValue(IObject).AsInteger;
end;

function TVirtualData<T>.IsVisible(const AMember: TRttiMember): Boolean;
begin
  result:=AMember.Visibility in FVisibility;
end;

procedure TVirtualData<T>.AddFields(const AColumns:TColumns; const AType:TRttiType);
var tmp : TRttiField;
begin
  for tmp in AType.GetDeclaredFields do
      if IsVisible(tmp) then
         InternalAddType(AColumns,tmp,tmp.FieldType);
end;

procedure TVirtualData<T>.AddProperties(const AColumns:TColumns; const AType:TRttiType);
var tmp : TRttiProperty;
begin
  for tmp in AType.GetDeclaredProperties do
      if IsVisible(tmp) and tmp.IsReadable then
         InternalAddType(AColumns,tmp,tmp.PropertyType);
end;

function TVirtualData<T>.AsString(const AColumn: TColumn;
  const ARow: Integer): String;
begin
  result:=FinalValue(AColumn,RowValue(ARow)).ToString;
end;

class function TVirtualData<T>.NameOf(const AType:TRttiOrdinalType):String;
begin
  case AType.OrdType of
    otSByte : result:='Byte';
    otUByte : result:='UByte';
    otSWord : result:='Word';
    otUWord : result:='UWord';
    otSLong : result:='Long';
    otULong : result:='ULong';
  end;
end;

class function TVirtualData<T>.NameOf(const AType:TRttiFloatType):String;
begin
  case AType.FloatType of
    ftSingle : result:='Single';
    ftDouble : result:='Double';
  ftExtended : result:='Extended';
      ftComp : result:='Comp';
      ftCurr : result:='Curr';
  end;
end;

procedure TVirtualData<T>.DoAddColumns(const AColumns:TColumns; const AType:TRttiType);
var tmp : TColumn;
begin
  if AType is TRttiOrdinalType then
  begin
    tmp:=Add(AColumns,NameOf(TRttiOrdinalType(AType)),AType);
    tmp.InitAlign(THorizontalAlign.Right);
  end
  else
  if AType is TRttiFloatType then
  begin
    tmp:=Add(AColumns,NameOf(TRttiFloatType(AType)),AType);
    tmp.InitAlign(THorizontalAlign.Right);
  end
  else
  if AType is TRttiInt64Type then
  begin
    tmp:=Add(AColumns,'Int64',AType);
    tmp.InitAlign(THorizontalAlign.Right);
  end
  else
  if AType is TRttiStringType then
     tmp:=Add(AColumns,'String',AType)
  else
  case FMembers of
         Both: begin AddFields(AColumns,AType); AddProperties(AColumns,AType); end;
       Fields: AddFields(AColumns,AType);
   Properties: AddProperties(AColumns,AType);
  end;
end;

procedure TVirtualData<T>.AddColumns(const AColumns:TColumns);
var tmp : TRttiType;
begin
  tmp:=RttiType;

  if tmp is TRttiDynamicArrayType then
     DoAddColumns(AColumns,TRttiDynamicArrayType(tmp).ElementType)
  else
  if IItems=nil then
     DoAddColumns(AColumns,tmp)
  else
     DoAddColumns(AColumns,IItems.PropertyType);
end;

function TVirtualData<T>.RowValue(const ARow: Integer): TValue;
var tmp : TValue;
begin
  if IsDynArray then
     result:=TValue.From<T>(FData^).GetArrayElement(ARow)
  else
  if IItems=nil then
     result:=TValue.From<T>(FData^)
  else
     result:=IItems.GetValue(IObject,[ARow]);
end;

function TVirtualData<T>.RttiType:TRttiType;
begin
  result:=Context.GetType(FTypeInfo);
end;

procedure TVirtualData<T>.InternalAddType(const AColumns:TColumns;
                                          const AMember:TRttiMember;
                                          const AType:TRttiType);
begin
  if AType.IsRecord then
     DoAddColumns(Add(AColumns,AMember,AType).Items,AType)
  else
  if AType is TRttiDynamicArrayType then
     InternalAddType(AColumns,AMember,TRttiDynamicArrayType(AType).ElementType)
  else
  if AType is TRttiArrayType then
     InternalAddType(AColumns,AMember,TRttiArrayType(AType).ElementType)
  else
     Add(AColumns,AMember,AType);
end;

procedure TVirtualData<T>.Load;
begin
end;

function TVirtualData<T>.AutoWidth(const APainter:TPainter; const AColumn: TColumn): Single;
var tmpL : Integer;
    tmpType : TRttiType;
begin
  tmpType:=TypeOf(AColumn);

  case tmpType.TypeKind of
    tkInteger : tmpL:=9;
      tkInt64 : tmpL:=12;

      tkFloat : if IsDateTime(tmpType) then
                   tmpL:=Length(SampleDateTime(AColumn))
                else
                if IsDate(tmpType) then
                   tmpL:=Length(SampleDate(AColumn))
                else
                if IsTime(tmpType) then
                   tmpL:=Length(SampleTime(AColumn))
                else
                   tmpL:=18;
  else
    if IsBoolean(tmpType) then
       tmpL:=5
    else
    begin
      // String
      result:=LongestString(APainter,AColumn);
      Exit;
    end;
  end;

  result:=APainter.TextWidth('0')*tmpL;
end;

procedure TVirtualData<T>.TrySetInt64(const AColumn: TColumn;
  const ARow: Integer; const AText: String);
var tmp : Int64;
begin
  if AColumn.TagObject is TRttiMember then
     if TryStrToInt64(AText,tmp) then // PWord, etc
        DoSetValue(AColumn,ARow,TValue.From<Int64>(tmp))
     else
        DoError(AText)
  else
//  if not TryStrToInt64(AText,PInt64(@FArray[ARow])^) then // Double, Extended
     DoError(AText);
end;

procedure TVirtualData<T>.TrySetFloat(const AColumn: TColumn;
  const ARow: Integer; const AText: String);
var tmp : Double;
begin
  if AColumn.TagObject is TRttiMember then
     if TryStrToFloat(AText,tmp) then // PWord, etc
        DoSetValue(AColumn,ARow,TValue.From<Double>(tmp))
     else
        DoError(AText)
  else
//  if not TryStrToFloat(AText,PSingle(@FArray[ARow])^) then // Double, Extended
     DoError(AText);
end;

procedure TVirtualData<T>.SetValue(const AColumn: TColumn;
  const ARow: Integer; const AText: String);
var tmpValue : TValue;
    tmpBool : Boolean;
    tmpInteger : Integer;
begin
  case TypeOf(AColumn).TypeKind of

    tkInteger : if TryStrToInt(AText,tmpInteger) then // PWord, etc
                   if AColumn.TagObject is TRttiMember then
                      DoSetValue(AColumn,ARow,TValue.From<Integer>(tmpInteger))
                   else
                   //   PInteger(@FArray[ARow])^:=tmpInteger

                else
                   DoError(AText);

      tkInt64 : TrySetInt64(AColumn,ARow,AText);
      tkFloat : TrySetFloat(AColumn,ARow,AText);

    tkString,
    tkWChar,
    tkLString,
    tkWString,
    tkUString : if AColumn.TagObject is TRttiMember then
                   DoSetValue(AColumn,ARow,TValue.From<String>(AText))
                else
                  // PString(@FArray[ARow])^:=AText;
                  ;

  else
    begin
      if IsBoolean(TypeOf(AColumn)) then
         if TryStrToBool(AText,tmpBool) then
            tmpValue:=TValue.From<Boolean>(tmpBool)
         else
            DoError(AText)
      else
         tmpValue:=TValue.From<String>(AText);

      DoSetValue(AColumn,ARow,tmpValue);
    end;
  end;
end;

(*
procedure TVirtualListData<T>.SetValue(const AColumn: TColumn;
  const ARow: Integer; const AText: String);
var tmp : TValue;
    tmpBool : Boolean;
begin
  if AText='' then
     tmp:=TValue.Empty
  else
  if IsBoolean(TypeOf(AColumn)) then
      if TryStrToBool(AText,tmpBool) then
         tmp:=TValue.From<Boolean>(tmpBool)
      else
         DoError(AText)
  else
     tmp:=TValue.From<String>(AText);

  if AColumn.TagObject is TRttiMember then
     DoSetValue(AColumn,PointerOf(FinalValue(AColumn,ARow)),tmp)
  else
//     tmp.Cast(TRttiType(AColumn.TagObject).Handle).ExtractRawData(@FList[ARow]);
end;
*)

end.
