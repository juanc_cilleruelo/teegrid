{*********************************************}
{  TeeGrid Software Library                   }
{  Abstract TVirtualData class                }
{  Copyright (c) 2016 by Steema Software      }
{  All Rights Reserved                        }
{*********************************************}
unit Tee.Grid.Data;
{$I Tee.inc}

interface

{
  Base abstract TVirtualData class.

  Provides data to a TeeGrid (main rows or sub-rows)

  See concrete implementations at the following units:

  Tee.Grid.Data.Rtti     (for generic TArray<T> or TList<T> data)
  Tee.Grid.Data.DB       (for TDataSet and TDataSource)
  Tee.Grid.Data.Strings  (to emulate a TStringGrid with Cells[Col,Row] property)

  BI.Grid.Data           (for any TeeBI TDataItem data structure)

}

uses
  {System.}Classes,
  Tee.Grid.Columns, Tee.Painter, Tee.Renders;

type
  TFloat=Double;

  TRowChangedEvent=procedure(const Sender:TObject; const ARow:Integer) of object;

  TColumnCalculation=(Count,Sum,Min,Max,Average);

  TVirtualData=class abstract
  protected
    FOnChangeRow : TRowChangedEvent;
    FOnRepaint,
    FOnRefresh : TNotifyEvent;

    PictureClass : TPersistentClass;

    class function Add(const AColumns:TColumns; const AName:String; const ATag:TObject):TColumn; overload; static;

    procedure ChangeSelectedRow(const ARow:Integer);

    class procedure DoError(const AText:String); static;

    procedure Refresh;
    procedure RowChanged(const ARow:Integer); virtual;
    procedure Repaint;

    function SampleDate(const AColumn:TColumn): String;
    function SampleDateTime(const AColumn:TColumn):String;
    function SampleTime(const AColumn:TColumn):String;
  public
    procedure AddColumns(const AColumns:TColumns); virtual; abstract;
    function AsFloat(const AColumn:TColumn; const ARow:Integer):TFloat; virtual;
    function AsString(const AColumn:TColumn; const ARow:Integer):String; virtual; abstract;
    function AutoWidth(const APainter:TPainter; const AColumn:TColumn):Single; virtual; abstract;
    function Calculate(const AColumn:TColumn; const ACalculation:TColumnCalculation):TFloat;
    function CanExpand(const Sender:TRender; const ARow:Integer):Boolean; virtual;
    function CanSortBy(const AColumn:TColumn):Boolean; virtual;
    function Count:Integer; virtual; abstract;
    class function From(const ASource:TComponent):TVirtualData; overload; virtual;
    function GetDetail(const ARow:Integer; const AColumns:TColumns; out AParent:TColumn):TVirtualData; virtual;
    function HasDetail(const ARow:Integer):Boolean; virtual;
    class function IsNumeric(const AColumn:TColumn):Boolean; overload; virtual;
    function IsSorted(const AColumn:TColumn; out Ascending:Boolean):Boolean; virtual;
    procedure Load; virtual; abstract;
    function LongestString(const APainter:TPainter; const AColumn:TColumn):Single;
    function ReadOnly(const AColumn:TColumn):Boolean; virtual;
    procedure SetValue(const AColumn:TColumn; const ARow:Integer; const AText:String); virtual; abstract;
    procedure SortBy(const AColumn:TColumn); virtual;
  end;

  TVirtualDataClass=class of TVirtualData;

  TVirtualDataClasses=record
  private
    class var
      Items : Array of TVirtualDataClass;

    class function IndexOf(const AClass:TVirtualDataClass):Integer; static;
  public
    class function Guess(const ASource:TComponent):TVirtualData; static;
    class procedure Register(const AClass:TVirtualDataClass); static;
    class procedure UnRegister(const AClass:TVirtualDataClass); static;
  end;

implementation

uses
  {System.}SysUtils;

{ TVirtualData }

function TVirtualData.SampleDate(const AColumn:TColumn): String;
var tmp : String;
begin
  tmp:=AColumn.DataFormat.Date;

  if tmp='' then
     tmp:={$IFDEF HASFORMATSETTINGS}{$IFDEF FPC}DefaultFormatSettings{$ELSE}FormatSettings{$ENDIF}.{$ENDIF}ShortDateFormat;

  result:=FormatDateTime(tmp,EncodeDate(2000,12,31));
end;

function TVirtualData.SampleDateTime(const AColumn: TColumn): String;
var tmp : String;
begin
  tmp:=AColumn.DataFormat.DateTime;

  if tmp='' then
     result:=SampleDate(AColumn)+' '+SampleTime(AColumn)
  else
     result:=FormatDateTime(tmp,EncodeDate(2000,12,31));
end;

function TVirtualData.SampleTime(const AColumn:TColumn): String;
var tmp : String;
begin
  tmp:=AColumn.DataFormat.Time;

  if tmp='' then
     tmp:={$IFDEF HASFORMATSETTINGS}{$IFDEF FPC}DefaultFormatSettings{$ELSE}FormatSettings{$ENDIF}.{$ENDIF}ShortTimeFormat;

  result:=FormatDateTime(tmp,EncodeTime(23,59,59,999));
end;

procedure TVirtualData.SortBy(const AColumn: TColumn);
begin // dummy
end;

class function TVirtualData.From(const ASource: TComponent): TVirtualData;
begin
  result:=nil;
end;

function TVirtualData.IsSorted(const AColumn: TColumn; out Ascending: Boolean): Boolean;
begin
  result:=False;
end;

class function TVirtualData.Add(const AColumns: TColumns; const AName: String; const ATag:TObject): TColumn;
begin
  result:=AColumns.Add(AName);
  result.TagObject:=ATag;
end;

function TVirtualData.AsFloat(const AColumn: TColumn; const ARow: Integer): TFloat;
begin
  result:=StrToFloat(AsString(AColumn,ARow));
end;

function TVirtualData.Calculate(const AColumn: TColumn;
  const ACalculation: TColumnCalculation): TFloat;

  function CalcSum:TFloat;
  var t : Integer;
  begin
    result:=0;

    for t:=0 to Count-1 do
        result:=result+AsFloat(AColumn,t);
  end;

  function CalcMin:TFloat;
  var t : Integer;
      tmp : TFloat;
  begin
    if Count>0 then
    begin
      result:=AsFloat(AColumn,0);

      for t:=1 to Count-1 do
      begin
        tmp:=AsFloat(AColumn,t);

        if tmp<result then
           result:=tmp;
      end;
    end
    else
      result:=0;
  end;

  function CalcMax:TFloat;
  var t : Integer;
      tmp : TFloat;
  begin
    if Count>0 then
    begin
      result:=AsFloat(AColumn,0);

      for t:=1 to Count-1 do
      begin
        tmp:=AsFloat(AColumn,t);

        if tmp>result then
           result:=tmp;
      end;
    end
    else
      result:=0;
  end;

  function CalcAverage:TFloat;
  begin
    if Count>0 then
       result:=CalcSum/Count
    else
       result:=0;
  end;

begin
  case ACalculation of
    TColumnCalculation.Count: result:=Count;
      TColumnCalculation.Sum: result:=CalcSum;
      TColumnCalculation.Min: result:=CalcMin;
      TColumnCalculation.Max: result:=CalcMax;
  else
  {Average:} result:=CalcAverage;
  end;
end;

function TVirtualData.CanExpand(const Sender: TRender;
  const ARow: Integer): Boolean;
begin
  result:=False;
end;

function TVirtualData.CanSortBy(const AColumn: TColumn): Boolean;
begin
  result:=False;
end;

procedure TVirtualData.ChangeSelectedRow(const ARow: Integer);
begin
  if Assigned(FOnChangeRow) then
     FOnChangeRow(Self,ARow);
end;

class procedure TVirtualData.DoError(const AText:String);
begin
  raise Exception.Create('Cannot convert text: '+AText);
end;

function TVirtualData.GetDetail(const ARow: Integer; const AColumns:TColumns; out AParent:TColumn): TVirtualData;
begin
  result:=nil;
end;

function TVirtualData.HasDetail(const ARow:Integer):Boolean;
begin
  result:=False;
end;

class function TVirtualData.IsNumeric(const AColumn: TColumn): Boolean;
begin
  result:=False;
end;

function TVirtualData.ReadOnly(const AColumn: TColumn): Boolean;
begin
  result:=False;
end;

procedure TVirtualData.Refresh;
begin
  if Assigned(FOnRefresh) then
     FOnRefresh(Self);
end;

procedure TVirtualData.Repaint;
begin
  if Assigned(FOnRepaint) then
     FOnRepaint(Self);
end;

procedure TVirtualData.RowChanged(const ARow: Integer);
begin
end;

function TVirtualData.LongestString(const APainter:TPainter; const AColumn:TColumn):Single;
var t : Integer;
    tmp : Single;
begin
  result:=0;

  for t:=0 to Count-1 do
  begin
    tmp:=APainter.TextWidth(AsString(AColumn,t));

    if tmp>result then
       result:=tmp;
  end;
end;

{ TVirtualDataClasses }

class function TVirtualDataClasses.Guess(const ASource: TComponent): TVirtualData;
var t : Integer;
begin
  for t:=0 to High(Items) do
  begin
    result:=Items[t].From(ASource);

    if result<>nil then
       Exit(result);
  end;

  result:=nil;
end;

class function TVirtualDataClasses.IndexOf(const AClass: TVirtualDataClass): Integer;
var t : Integer;
begin
  for t:=0 to High(Items) do
      if Items[t]=AClass then
         Exit(t);

  result:=-1;
end;

class procedure TVirtualDataClasses.Register(const AClass: TVirtualDataClass);
var tmp,
    L : Integer;
begin
  tmp:=IndexOf(AClass);

  if tmp=-1 then
  begin
    L:=Length(Items);
    SetLength(Items,L+1);
    Items[L]:=AClass;
  end;
end;

class procedure TVirtualDataClasses.UnRegister(const AClass: TVirtualDataClass);
var tmp,
    t : Integer;
begin
  tmp:=IndexOf(AClass);

  if tmp<>-1 then
  begin
    for t:=tmp to High(Items)-1 do
        Items[t]:=Items[t+1];

    SetLength(Items,High(Items));
  end;
end;

initialization
finalization
  TVirtualDataClasses.Items:=nil;
end.

