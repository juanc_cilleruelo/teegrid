{*********************************************}
{  TeeGrid Software Library                   }
{  TBrush Editor for Firemonkey               }
{  Copyright (c) 2016 by Steema Software      }
{  All Rights Reserved                        }
{*********************************************}
unit FMXTee.Editor.Painter.Brush;
{$I Tee.inc}

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms,

  {$IF CompilerVersion<=27}
  {$DEFINE HASFMX20}
  {$IFEND}

  {$IFNDEF HASFMX20}
  FMX.Graphics,
  {$ENDIF}

  {$IF CompilerVersion<25}
  {$DEFINE HASFMX21}
  {$IFEND}

  {$IFNDEF HASFMX21}
  FMX.StdCtrls,
  {$ENDIF}

  {$IF CompilerVersion<=27}
  {$DEFINE HASFMX22}
  {$IFEND}

  {$IFNDEF HASFMX22}
  FMX.Controls.Presentation,
  {$ENDIF}

  FMX.Dialogs, Tee.Format,
  FMX.Colors, FMX.TabControl,
  FMX.Layouts;

type
  TBrushEditor = class(TForm)
    LayoutTop: TLayout;
    CBVisible: TCheckBox;
    TabBrush: TTabControl;
    TabColor: TTabItem;
    TabGradient: TTabItem;
    TabPicture: TTabItem;
    CBColor: TColorPanel;
    procedure CBVisibleChange(Sender: TObject);
    procedure CBColorChange(Sender: TObject);
  private
    { Private declarations }

    IBrush : TBrush;

    FOnColorChange : TNotifyEvent;

    procedure RefreshGradient(const AGradient:TGradient);
  public
    { Public declarations }

    procedure RefreshBrush(const ABrush:TBrush);
    procedure RefreshColor;

    class function Edit(const AOwner:TComponent; const ABrush:TBrush):Boolean; static;

    class function Embedd(const AOwner:TComponent; const AParent:TControl;
                          const ABrush:TBrush):TBrushEditor; static;

    property OnColorChange:TNotifyEvent read FOnColorChange write FOnColorChange;
  end;

implementation

{$R *.fmx}

uses
  FMXTee.Editor.Painter.Stroke;

{ TBrushEditor }

procedure TBrushEditor.CBColorChange(Sender: TObject);
begin
  IBrush.Color:=TColorHelper.SwapCheck(CBColor.Color);

  if Assigned(FOnColorChange) then
     FOnColorChange(Self);
end;

procedure TBrushEditor.CBVisibleChange(Sender: TObject);
begin
  IBrush.Visible:=CBVisible.IsChecked;
end;

class function TBrushEditor.Edit(const AOwner: TComponent;
  const ABrush: TBrush): Boolean;
begin
  with TBrushEditor.Create(AOwner) do
  try
    RefreshBrush(ABrush);
    result:=ShowModal=mrOk;
  finally
    Free;
  end;
end;

class function TBrushEditor.Embedd(const AOwner: TComponent;
  const AParent: TControl; const ABrush: TBrush): TBrushEditor;
begin
  result:=TBrushEditor.Create(AOwner);
  result.RefreshBrush(ABrush);
  TStrokeEditor.AddForm(result,AParent);
end;

procedure TBrushEditor.RefreshBrush(const ABrush: TBrush);
begin
  IBrush:=ABrush;

  CBVisible.IsChecked:=IBrush.Visible;

  RefreshColor;

  RefreshGradient(IBrush.Gradient);
end;

procedure TBrushEditor.RefreshColor;
begin
  CBColor.Color:=TColorHelper.SwapCheck(IBrush.Color);
end;

procedure TBrushEditor.RefreshGradient(const AGradient: TGradient);
begin
  {
  CBGradientVisible.Checked:=AGradient.Visible;
  CBGradientInverted.Checked:=AGradient.Inverted;
  RGDirection.ItemIndex:=Ord(AGradient.Direction);

  if AGradient.Colors.Count>0 then
     CBGradientColor0.Selected:=AGradient.Colors[0].Color;

  if AGradient.Colors.Count>1 then
     CBGradientColor1.Selected:=AGradient.Colors[1].Color;
  }
end;

end.
