{*********************************************}
{  TeeGrid Software Library                   }
{  TStroke Editor for Firemonkey              }
{  Copyright (c) 2016 by Steema Software      }
{  All Rights Reserved                        }
{*********************************************}
unit FMXTee.Editor.Painter.Stroke;
{$I Tee.inc}

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms,

  {$IF CompilerVersion<=27}
  {$DEFINE HASFMX20}
  {$IFEND}

  {$IFNDEF HASFMX20}
  FMX.Graphics,
  {$ENDIF}

  {$IF CompilerVersion<25}
  {$DEFINE HASFMX21}
  {$IFEND}

  {$IFNDEF HASFMX21}
  FMX.StdCtrls,
  {$ENDIF}

  {$IF CompilerVersion<=27}
  {$DEFINE HASFMX22}
  {$IFEND}

  {$IFNDEF HASFMX22}
  FMX.Controls.Presentation,
  {$ENDIF}

  FMX.Dialogs, Tee.Format,
  FMX.TabControl, FMXTee.Editor.Painter.Brush,
  FMX.ListBox, FMX.Colors, FMX.Layouts;

type
  TStrokeEditor = class(TForm)
    TabStroke: TTabControl;
    TabOptions: TTabItem;
    TabFill: TTabItem;
    CBVisible: TCheckBox;
    CBColor: TColorComboBox;
    TBSize: TTrackBar;
    Label1: TLabel;
    LSize: TLabel;
    TabStyle: TTabItem;
    Label2: TLabel;
    LBStyle: TListBox;
    Label3: TLabel;
    LBEndStyle: TListBox;
    Label4: TLabel;
    LBJoinStyle: TListBox;
    procedure TabStrokeChange(Sender: TObject);
    procedure CBVisibleChange(Sender: TObject);
    procedure CBColorChange(Sender: TObject);
    procedure TBSizeChange(Sender: TObject);
    procedure LBStyleChange(Sender: TObject);
    procedure LBEndStyleChange(Sender: TObject);
    procedure LBJoinStyleChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }

    IStroke : TStroke;
    IBrush : TBrushEditor;

    procedure ChangedColor(Sender: TObject);
    procedure SetLabelSize;
  public
    { Public declarations }

    class procedure AddForm(const AForm: TCustomForm; const AParent: TControl); static;

    procedure RefreshStroke(const AStroke:TStroke);

    class function Edit(const AOwner:TComponent; const AStroke:TStroke):Boolean; static;

    class function Embedd(const AOwner:TComponent; const AParent:TControl;
                          const AStroke:TStroke):TStrokeEditor; static;
  end;

implementation

{$R *.fmx}

class procedure TStrokeEditor.AddForm(const AForm: TCustomForm;
  const AParent: TControl);
begin
  AForm.Visible:=False;

  while AForm.ChildrenCount>0 do
        AForm.Children[0].Parent:=AParent;
end;

procedure TStrokeEditor.CBColorChange(Sender: TObject);
begin
  IStroke.Color:=TColorHelper.SwapCheck(CBColor.Color);

  if IBrush<>nil then
     IBrush.RefreshColor;
end;

procedure TStrokeEditor.CBVisibleChange(Sender: TObject);
begin
  IStroke.Visible:=CBVisible.IsChecked;
end;

class function TStrokeEditor.Edit(const AOwner: TComponent;
  const AStroke: TStroke): Boolean;
begin
  with TStrokeEditor.Create(AOwner) do
  try
    RefreshStroke(AStroke);
    result:=ShowModal=mrOk;
  finally
    Free;
  end;
end;

class function TStrokeEditor.Embedd(const AOwner: TComponent;
  const AParent: TControl; const AStroke: TStroke): TStrokeEditor;
begin
  result:=TStrokeEditor.Create(AOwner);
  result.RefreshStroke(AStroke);
  AddForm(result,AParent);
end;

procedure TStrokeEditor.FormCreate(Sender: TObject);
begin
  TabStroke.ActiveTab:=TabOptions;
end;

procedure TStrokeEditor.LBStyleChange(Sender: TObject);
begin
  IStroke.Style:=TStrokeStyle(LBStyle.ItemIndex);
end;

procedure TStrokeEditor.LBEndStyleChange(Sender: TObject);
begin
  IStroke.EndStyle:=TStrokeEnd(LBEndStyle.ItemIndex);
end;

procedure TStrokeEditor.LBJoinStyleChange(Sender: TObject);
begin
  IStroke.JoinStyle:=TStrokeJoin(LBJoinStyle.ItemIndex);
end;

procedure TStrokeEditor.RefreshStroke(const AStroke: TStroke);
begin
  IStroke:=AStroke;

  TBSize.Value:=Round(IStroke.Size);
  SetLabelSize;

  ChangedColor(Self);

  LBStyle.ItemIndex:=Ord(IStroke.Style);
  LBEndStyle.ItemIndex:=Ord(IStroke.EndStyle);
  LBJoinStyle.ItemIndex:=Ord(IStroke.JoinStyle);

  CBVisible.IsChecked:=IStroke.Visible;
end;

procedure TStrokeEditor.ChangedColor(Sender: TObject);
begin
  CBColor.Color:=TColorHelper.SwapCheck(IStroke.Color);
end;

procedure TStrokeEditor.TabStrokeChange(Sender: TObject);
begin
  if IStroke=nil then
     Exit;

  if TabStroke.ActiveTab=TabFill then
  begin
    if IBrush=nil then
    begin
      IBrush:=TBrushEditor.Embedd(Self,TabFill,IStroke.Brush);
      IBrush.LayoutTop.Visible:=False;
      IBrush.OnColorChange:=ChangedColor;
    end;
  end;
end;

procedure TStrokeEditor.SetLabelSize;
begin
  LSize.Text:=FormatFloat('0.##',TBSize.Value);
end;

procedure TStrokeEditor.TBSizeChange(Sender: TObject);
begin
  IStroke.Size:=TBSize.Value;
  SetLabelSize;
end;

end.
