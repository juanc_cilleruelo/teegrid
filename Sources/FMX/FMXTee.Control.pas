{*********************************************}
{  TeeGrid Software Library                   }
{  FMX TScrollable control                    }
{  Copyright (c) 2016 by Steema Software      }
{  All Rights Reserved                        }
{*********************************************}
unit FMXTee.Control;
{$I Tee.inc}

interface

uses
  System.Classes,

  {$IF CompilerVersion>24}
  FMX.StdCtrls,
  {$IFEND}

  FMX.Types, FMX.Controls, Tee.Format;

type
  // Horizontal and Vertical scrollbars
  TScrollBars=class(TVisiblePersistentChange)
  private
    FHorizontal: TScrollBar;
    FVertical: TScrollBar;

    IParent : TControl;

    function Calc(const Horiz:Boolean; const AValue:Single):Single;
    procedure DoChanged(Sender:TObject);
  public
    Constructor CreateParent(const AParent:TControl);

    {$IFNDEF AUTOREFCOUNT}
    Destructor Destroy; override;
    {$ENDIF}

    procedure Reset(const W,H,AWidth,AHeight:Single);
  published
    property Horizontal:TScrollBar read FHorizontal;
    property Vertical:TScrollBar read FVertical;
  end;

  TScrollableControl=class(TStyledControl)
  private
    FScrollBars : TScrollBars;

    procedure DoHorizScroll(Sender:TObject);
    procedure DoVertScroll(Sender:TObject);
  protected
    function GetMaxBottom:Single; virtual; abstract;
    function GetMaxRight:Single; virtual; abstract;
    function RemainSize(const Horizontal:Boolean):Single;
    procedure ResetScrollBars; virtual;
    procedure SetScrollX(const Value:Single); virtual; abstract;
    procedure SetScrollY(const Value:Single); virtual; abstract;
  public
    Constructor Create(AOwner: TComponent); override;

    {$IFNDEF AUTOREFCOUNT}
    Destructor Destroy; override;
    {$ENDIF}

    property ScrollBars:TScrollBars read FScrollBars;
  end;

implementation

{$IF CompilerVersion>23}
uses
  FMX.StdActns;
{$IFEND}

{$IF CompilerVersion>23}
{$DEFINE HASVALUERANGE}
{$IFEND}

{ TScrollableControl }

Constructor TScrollableControl.Create(AOwner: TComponent);
begin
  inherited;

  FScrollBars:=TScrollBars.CreateParent(Self);

  FScrollBars.Horizontal.OnChange:=DoHorizScroll;
  FScrollBars.Vertical.OnChange:=DoVertScroll;
end;

{$IFNDEF AUTOREFCOUNT}
Destructor TScrollableControl.Destroy;
begin
  FScrollBars.Free;
  inherited;
end;
{$ENDIF}

procedure TScrollableControl.DoHorizScroll(Sender: TObject);
begin
  SetScrollX(FScrollBars.Calc(True,RemainSize(True)));
  Repaint;
end;

procedure TScrollableControl.DoVertScroll(Sender: TObject);
begin
  SetScrollY(FScrollBars.Calc(False,RemainSize(False)));
  Repaint;
end;

function TScrollableControl.RemainSize(const Horizontal:Boolean):Single;
begin
  if Horizontal then
     result:=(GetMaxRight-Width)+FScrollBars.Vertical.Width
  else
     result:=(GetMaxBottom-Height)+FScrollBars.Horizontal.Height;
end;

procedure TScrollableControl.ResetScrollBars;
begin
  if ScrollBars.Visible then
     ScrollBars.Reset(GetMaxRight,GetMaxBottom,Width,Height)
  else
  begin
    ScrollBars.Horizontal.Visible:=False;
    ScrollBars.Vertical.Visible:=False;
  end;
end;

{ TScrollBars }

Constructor TScrollBars.CreateParent(const AParent:TControl);

  function CreateScrollBar(const AOrientation:TOrientation;
                           const Align:TAlignLayout):TScrollBar;
  begin
    result:=TScrollBar.Create(nil);

    result.Stored:=False;
    result.Width:=16;
    result.Visible:=False;
    result.Enabled:=True;
    result.Parent:=AParent;
    result.Orientation:=AOrientation;
    result.Align:=Align;
  end;

const
{$IF CompilerVersion>26}
  Horiz=TOrientation.Horizontal;
  Vert=TOrientation.Vertical;
  Bottom=TAlignLayout.Bottom;
  Right=TAlignLayout.Right;
{$ELSE}
  Horiz=TOrientation.orHorizontal;
  Vert=TOrientation.orVertical;
  Bottom=TAlignLayout.alBottom;
  Right=TAlignLayout.alRight;
{$IFEND}

begin
  inherited Create(DoChanged);

  IParent:=AParent;

  FHorizontal:=CreateScrollBar(Horiz,Bottom);
  FVertical:=CreateScrollBar(Vert,Right);
end;

{$IFNDEF AUTOREFCOUNT}
Destructor TScrollBars.Destroy;
begin
  FVertical.Free;
  FHorizontal.Free;
  inherited;
end;
{$ENDIF}

procedure TScrollBars.DoChanged(Sender:TObject);
begin
  (IParent as TScrollableControl).ResetScrollBars;
end;

function TScrollBars.Calc(const Horiz:Boolean; const AValue:Single):Single;

  function CalcBar(const ABar:TScrollBar):Single;
  begin
    result:=(100-ABar.{$IFDEF HASVALUERANGE}ValueRange.{$ENDIF}ViewportSize);

    if result<>0 then
       result:=ABar.Value*AValue/result;
  end;

begin
  if Horiz then
     result:=CalcBar(Horizontal)
  else
     result:=CalcBar(Vertical);
end;

procedure TScrollBars.Reset(const W,H,AWidth,AHeight:Single);
begin
  Horizontal.Visible:=W>AWidth;
  Vertical.Visible:=H>AHeight;

  if not Horizontal.Visible then
     if Vertical.Visible then
        Horizontal.Visible:=W>(AWidth-Vertical.Width);

  if not Vertical.Visible then
     if Horizontal.Visible then
        Vertical.Visible:=H>(AHeight-Horizontal.Height);

  Horizontal.{$IFDEF HASVALUERANGE}ValueRange.{$ENDIF}ViewportSize:=100/(W/(AWidth-Vertical.Width));
  Vertical.{$IFDEF HASVALUERANGE}ValueRange.{$ENDIF}ViewportSize:=100/(H/(AHeight-Horizontal.Height));
end;

end.
