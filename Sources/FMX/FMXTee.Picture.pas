unit FMXTee.Picture;
{$I Tee.inc}

interface

uses
  FMX.Types,

  {$IF CompilerVersion<=25}
  {$DEFINE HASFMX20}
  {$IFEND}

  {$IFNDEF HASFMX20}
  FMX.Graphics,
  {$ENDIF}

  {$IF CompilerVersion>24}
  FMX.StdCtrls,
  {$IFEND}

  FMX.Objects,

  Tee.Format;

type
  TFMXPicture=record
  private
  public
    class function From(const ABitmap:TBitmap):TPicture; overload; static;
    class function From(const AImage:TImage):TPicture; overload; static;
    class function From(const AImage:TImageControl):TPicture; overload; static;
    class function From(const AFileName:String):TPicture; overload; static;

    class function Load(const AFileName:String):TBitmap; overload; static;
    class procedure Load(const APicture:TPicture; const AFileName:String); overload; static;

    class function TypeOfGraphic(const ABitmap: TBitmap): String; static;
  end;

implementation

class function TFMXPicture.From(const ABitmap: TBitmap): TPicture;
begin
  result:=TPicture.Create(nil);
  result.SetGraphic(ABitmap);
end;

class function TFMXPicture.From(const AImage: TImage): TPicture;
begin
  result:=From(AImage.Bitmap);
end;

type
  TPictureAccess=class(TPicture);

class function TFMXPicture.From(const AFileName: String): TPicture;
var tmp : TBitmap;
begin
  tmp:=TBitmap.CreateFromFile(AFileName);
  result:=From(tmp);
  TPictureAccess(result).Original:=tmp;
end;

class procedure TFMXPicture.Load(const APicture: TPicture;
  const AFileName: String);
var tmp : TBitmap;
begin
  tmp:=Load(AFileName);

  APicture.SetGraphic(tmp);
  TPictureAccess(APicture).Original:=tmp;
end;

class function TFMXPicture.From(const AImage: TImageControl): TPicture;
begin
  result:=From(AImage.Bitmap);
end;

class function TFMXPicture.Load(const AFileName:String):TBitmap;
begin
  result:=TBitmap.CreateFromFile(AFileName);
end;

class function TFMXPicture.TypeOfGraphic(const ABitmap: TBitmap): String;
begin
  result:='Bitmap';

  (*
  if AGraphic is {$IFDEF FPC}TPortableNetworkGraphic{$ELSE}TPngImage{$ENDIF} then
     result:='PNG'
  else
  if AGraphic is TJPEGImage then
     result:='JPEG'
  else
  {$IFDEF FPC}
  if AGraphic is TGIFImage then
     result:='GIF'
  else
  {$ENDIF}
  if AGraphic is TBitmap then
     result:='Bitmap'
  else
     result:='';
  *)
end;

end.
