{*********************************************}
{  TeeGrid Software Library                   }
{  FMX TeeGrid                                }
{  Copyright (c) 2016 by Steema Software      }
{  All Rights Reserved                        }
{*********************************************}
unit FMXTee.Grid;
{$I Tee.inc}

//{$IF CompilerVersion>23}
//{$LEGACYIFEND ON}
//{$IFEND}

interface

{
   TTeeGrid control for Firemonkey
}

{$IF CompilerVersion>24}
{$DEFINE USELAYOUT}
{$IFEND}

uses
  System.Classes, System.Types, System.UITypes,

  FMX.Types, FMX.Controls,

  {$IF CompilerVersion<=25}
  {$DEFINE HASFMX20}
  {$IFEND}

  {$IFNDEF HASFMX20}
  FMX.Graphics,
  {$ENDIF}

  {$IF CompilerVersion>24}
  FMX.StdCtrls,
  {$IFEND}

  Tee.Painter, Tee.Format, Tee.Renders,

  Tee.Control, Tee.Grid.Columns, Tee.Grid.Data, Tee.Grid.Rows,
  Tee.Grid.RowGroup, Tee.Grid.Header, Tee.Grid.Selection, Tee.Grid.Bands,
  Tee.Grid,

  FMXTee.Painter, FMXTee.Control;

type
  TTeeGrid=class;

  TFMXTeeGrid=class(TCustomTeeGrid)
  private
    IEditor : TControl;
    IGrid : TTeeGrid;

    IEditorColumn : TColumn;
    IEditorRow : Integer;

    procedure CreateEditor(const AColumn:TColumn);
    procedure EditorKeyUp(Sender: TObject; var Key: Word; var KeyChar: WideChar; Shift: TShiftState);
    function EditorShowing:Boolean;
    procedure TryChangeEditorData;
    procedure TryShowEditor(const AColumn: TColumn; const ARow: Integer);
  protected
    procedure DataChanged; override;

    function HorizScrollBarHeight:Single; override;
    procedure HorizScrollChanged; override;

    procedure StartEditor(const AColumn:TColumn; const ARow:Integer); override;
    procedure StopEditor; override;

    function VertScrollBarWidth:Single; override;
    procedure VertScrollChanged; override;
  public
    procedure CopySelected; override;
    function Height:Single; override;
    function Painter:TPainter; override;
    function Width:Single; override;
  end;

  TShowEditorEvent=procedure(const Sender:TObject; const AEditor:TControl;
                             const AColumn:TColumn; const ARow:Integer) of object;

  {$IFNDEF FPC}
  {$IF CompilerVersion>=23}
  [ComponentPlatformsAttribute(pidWin32 or pidWin64 or pidOSX32
              {$IF CompilerVersion>=25}or pidiOSSimulator or pidiOSDevice{$IFEND}
              {$IF CompilerVersion>=26}or pidAndroid{$IFEND}
              {$IF CompilerVersion>=29}or pidiOSDevice64{$IFEND}
              )]
  {$IFEND}
  {$ENDIF}
  TTeeGrid=class(TScrollableControl)
  private
    FGrid : TFMXTeeGrid;
    FPainter : TFMXPainter;
    FOnColumnResized: TColumnEvent;
    FOnShowEditor: TShowEditorEvent;

    procedure ChangePainter(const APainter:TFMXPainter);
    procedure ColumnResized(Sender:TObject);
    procedure SetDataItem(const Value: TVirtualData);

    function GetColumns: TColumns;
    function GetSelected: TGridSelection;
    procedure SetColumns(const Value: TColumns);
    procedure SetSelected(const Value: TGridSelection);
    function GetHeader: TColumnHeaderBand;
    procedure SetHeader(const Value: TColumnHeaderBand);
    function GetClickedHeader: TNotifyEvent;
    procedure SetClickedHeader(const Value: TNotifyEvent);
    function GetDataItem: TVirtualData;
    function GetIndicator: TIndicator;
    function GetRows: TRows;
    procedure SetIndicator(const Value: TIndicator);
    procedure SetRows(const Value: TRows);
    function GetAfterDraw: TNotifyEvent;
    procedure SetAfterDraw(const Value: TNotifyEvent);
    function GetReadOnly: Boolean;
    procedure SetReadOnly(const Value: Boolean);

    function GetBack: TFormat;
    function GetCells: TTextRender;
    function GetFooter: TGridBands;
    procedure SetBack(const Value: TFormat);
    procedure SetCells(const Value: TTextRender);
    procedure SetFooter(const Value: TGridBands);
    function GetEditing: TGridEditing;
    procedure SetEditing(const Value: TGridEditing);
    function GetOnNewDetail: TNewDetailEvent;
    procedure SetOnNewDetail(const Value: TNewDetailEvent);
    function GetOnSelect: TNotifyEvent;
    procedure SetOnSelect(const Value: TNotifyEvent);
    function GetHeaders: TGridBands;
    procedure SetHeaders(const Value: TGridBands);
    function GetDataSource: TComponent;
    procedure SetDataSource(const Value: TComponent);
  protected
    procedure DblClick; override;

    procedure DoMouseLeave; override;

    function GetMaxBottom:Single; override;
    function GetMaxRight:Single; override;

    procedure KeyDown(var Key: Word; var KeyChar: WideChar; Shift: TShiftState); override;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Single); override;
    procedure MouseMove(Shift: TShiftState; X, Y: Single); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Single); override;
    procedure MouseWheel(Shift: TShiftState; WheelDelta: Integer; var Handled: Boolean); override;
    procedure Paint; override;

    procedure ResetScrollBars; override;
    procedure Resize; override;

    procedure SetScrollX(const Value:Single); override;
    procedure SetScrollY(const Value:Single); override;
  public
    Constructor Create(AOwner:TComponent); override;

    {$IFNDEF AUTOREFCOUNT}
    Destructor Destroy; override;
    {$ENDIF}

    procedure ApplyStyle; override;
    procedure Changed(Sender:TObject);

    procedure Loaded; override;

    property Data:TVirtualData read GetDataItem write SetDataItem;
    property Grid:TFMXTeeGrid read FGrid;
    property Painter:TFMXPainter read FPainter;
  published
    property Back:TFormat read GetBack write SetBack;
    property Cells:TTextRender read GetCells write SetCells;
    property Columns:TColumns read GetColumns write SetColumns;
    property DataSource:TComponent read GetDataSource write SetDataSource;
    property Editing:TGridEditing read GetEditing write SetEditing;
    property Header:TColumnHeaderBand read GetHeader write SetHeader;
    property Headers:TGridBands read GetHeaders write SetHeaders stored False;
    property Footer:TGridBands read GetFooter write SetFooter;
    property Indicator:TIndicator read GetIndicator write SetIndicator;
    property ReadOnly:Boolean read GetReadOnly write SetReadOnly default True;
    property Rows:TRows read GetRows write SetRows;
    property ScrollBars;
    property Selected:TGridSelection read GetSelected write SetSelected;

    property OnAfterDraw:TNotifyEvent read GetAfterDraw write SetAfterDraw;
    property OnClickedHeader:TNotifyEvent read GetClickedHeader write SetClickedHeader;
    property OnColumnResized:TColumnEvent read FOnColumnResized write FOnColumnResized;
    property OnNewDetail:TNewDetailEvent read GetOnNewDetail write SetOnNewDetail;
    property OnSelect:TNotifyEvent read GetOnSelect write SetOnSelect;
    property OnShowEditor:TShowEditorEvent read FOnShowEditor write FOnShowEditor;

    // inherited
    property Align;
    property Anchors;

    // NO: property Caption;

    property ClipChildren;
    property ClipParent;
    property DragMode;
    property EnableDragHighlight;
    property Enabled;
    property Height;
    property HitTest;
    property Locked;
    property Opacity;
    property Margins;
    property Padding;
    property PopupMenu;
    property Position;
    property RotationAngle;
    property RotationCenter;
    property Scale;

    {$IF CompilerVersion>27}
    property Size;
    {$IFEND}

    {$IF CompilerVersion>23}
    property TouchTargetExpansion;
    {$IFEND}
    
    property ShowHint;
    property TabOrder;

    {$IF CompilerVersion>26}
    property TabStop default True;
    {$IFEND}

    {$IF CompilerVersion>23}
    property Touch;
    {$IFEND}
    
    property Visible;
    property Width;

    property OnClick;
    property OnDblClick;
    property OnDragDrop;
    property OnDragEnd;
    property OnDragEnter;
    property OnDragLeave;
    property OnDragOver;
    property OnEnter;
    property OnExit;

    {$IF CompilerVersion>23}
    property OnGesture;
    {$IFEND}

    property OnMouseDown;
    property OnMouseEnter;
    property OnMouseLeave;
    property OnMouseMove;
    property OnMouseUp;
    property OnPainting;
    property OnPaint;
    property OnResize;
  end;

implementation

uses
  FMX.Edit, FMX.Platform, Tee.Grid.CSV;

{ TFMXTeeGrid }

function TFMXTeeGrid.Height: Single;
begin
  result:=IGrid.Height;
end;

function TFMXTeeGrid.HorizScrollBarHeight: Single;
begin
  if IGrid.ScrollBars.Horizontal.Visible then
     result:=IGrid.ScrollBars.Horizontal.Height
  else
     result:=0;
end;

procedure TFMXTeeGrid.HorizScrollChanged;
begin
  if IGrid.ScrollBars.Visible then
     IGrid.ScrollBars.Horizontal.Value:=100*Rows.Scroll.X/IGrid.RemainSize(True);
end;

function TFMXTeeGrid.Painter: TPainter;
begin
  result:=IGrid.Painter;
end;

procedure TFMXTeeGrid.EditorKeyUp(Sender: TObject; var Key: Word; var KeyChar: WideChar; Shift: TShiftState);
begin
  if Key=vkEscape then
     StopEditor
  else
  case Key of
    vkReturn,
    vkTab,
    vkUp,
    vkDown : begin
               TryChangeEditorData;

               if not Editing.AlwaysVisible then
                  StopEditor;

               IGrid.KeyDown(Key,KeyChar,Shift);

               Key:=0;
             end;
  end;
end;

type
  TControlClass=class of TControl;

procedure TFMXTeeGrid.CreateEditor(const AColumn:TColumn);
var tmp : TClass;
begin
  tmp:=Editing.ClassOf(AColumn);

  if tmp=nil then
     tmp:=TEdit;

  if (IEditor=nil) or (IEditor.ClassType<>tmp) then
  begin
    IEditor.Free;

    if tmp.InheritsFrom(TControl) then
    begin
      IEditor:=TControlClass(tmp).Create(Self);
      IEditor.Parent:=IGrid;

      if IEditor is TCustomEdit then
         TFMXPainter.ApplyFont(Current.Rows.FontOf(AColumn),TCustomEdit(IEditor).Font);

      TControl(IEditor).OnKeyUp:=EditorKeyUp;
    end
    else
      IEditor:=nil;
  end;
end;

procedure TFMXTeeGrid.TryShowEditor(const AColumn: TColumn; const ARow: Integer);

  procedure SetEditorData;
  var tmp : String;
//      tmpDate : TDateTime;
  begin
    tmp:=Data.AsString(AColumn,ARow);

    if IEditor is TCustomEdit then
       TCustomEdit(IEditor).Text:=tmp

    {
    else
    if IEditor is TCommonCalendar then
       if TryStrToDateTime(tmp,tmpDate) then
          TCommonCalendarAccess(IEditor).DateTime:=tmpDate;
    }
    ;

    if Assigned(IGrid.FOnShowEditor) then
       IGrid.FOnShowEditor(Self,IEditor,AColumn,ARow);
  end;

  procedure SetEditorBounds;
  var tmp : TRectF;
  begin
    tmp:=Root.CellRect(AColumn,ARow);

    if IEditor.BoundsRect<>tmp then
       IEditor.BoundsRect:=tmp;
  end;

begin
  if (AColumn<>IEditorColumn) or (ARow<>IEditorRow) then
  begin
    CreateEditor(AColumn);

    if IEditor<>nil then
    begin
      SetEditorBounds;

      SetEditorData;

      if not IEditor.Visible then
         IEditor.Visible:=True;

      if IEditor is TControl then
         TControl(IEditor).SetFocus;

      IEditorColumn:=AColumn;
      IEditorRow:=ARow;
    end;
  end
  else
    SetEditorBounds;
end;

procedure TFMXTeeGrid.StartEditor(const AColumn: TColumn; const ARow: Integer);
begin
  CreateEditor(AColumn);

  if IEditor<>nil then
     TryShowEditor(AColumn,ARow);
end;

procedure TFMXTeeGrid.TryChangeEditorData;

  function GetEditorText(out AText:String):Boolean;
  begin
    result:=IEditor is TCustomEdit;

    if result then
       AText:=TCustomEdit(IEditor).Text;

    {$IFDEF DATETIMEPICKER}
    else
    begin
      result:=IEditor is TCommonCalendar;

      if result then
         AText:=DateTimeToStr(TCommonCalendarAccess(IEditor).DateTime);
    end
    {$ENDIF}
    ;
  end;

var tmp : String;
begin
  if EditorShowing then
     if GetEditorText(tmp) then
        if tmp<>Data.AsString(Selected.Column,Selected.Row) then
           Data.SetValue(Selected.Column,Selected.Row,tmp);
end;

function TFMXTeeGrid.EditorShowing:Boolean;
begin
  result:=(IEditor<>nil) and IEditor.Visible;
end;

procedure TFMXTeeGrid.StopEditor;
begin
  if EditorShowing and (not (csDestroying in ComponentState)) then
  begin
    IEditor.Visible:=False;

    IGrid.Repaint;
    IGrid.SetFocus;
  end;
end;

function TFMXTeeGrid.Width: Single;
begin
  result:=IGrid.Width;
end;

function TFMXTeeGrid.VertScrollBarWidth:Single;
begin
  if IGrid.ScrollBars.Vertical.Visible then
     result:=IGrid.ScrollBars.Vertical.Width
  else
     result:=0;
end;

procedure TFMXTeeGrid.VertScrollChanged;
begin
  if IGrid.ScrollBars.Visible then
     IGrid.ScrollBars.Vertical.Value:=100*Rows.Scroll.Y/IGrid.RemainSize(False);
end;

procedure TFMXTeeGrid.CopySelected;
{$IF CompilerVersion>23}
var ClipService : IFMXClipboardService;
{$IFEND}
begin
  {$IF CompilerVersion>23}
  if TPlatformServices.Current.SupportsPlatformService(IFMXClipboardService, IInterface(ClipService)) then
     ClipService.SetClipboard(TCSVData.From(IGrid.FGrid,Selected));
  {$ELSE}
     Platform.SetClipboard(TCSVData.From(IGrid.FGrid,Selected));
  {$IFEND}
end;

type
  TVirtualDataAccess=class(TVirtualData);

procedure TFMXTeeGrid.DataChanged;
begin
  inherited;
  IGrid.ResetScrollBars;

  if IGrid.Data<>nil then
     TVirtualDataAccess(IGrid.Data).PictureClass:=TPicture;
end;

{ TTeeGrid }

Constructor TTeeGrid.Create(AOwner: TComponent);
begin
  inherited;

  FGrid:=TFMXTeeGrid.Create(Self);
  FGrid.IGrid:=Self;
  FGrid.OnChange:=Changed;

  FGrid.Header.OnColumnResized:=ColumnResized;

  Width:=400;
  Height:=250;

  //ControlStyle:=ControlStyle+[csAcceptsControls]-[csOpaque,csSetCaption];

  CanFocus := True;
  AutoCapture := True;

  {$IF CompilerVersion>26}
  TabStop:=True;
  {$IFEND}

{
  Touch.InteractiveGestures := [igPan, igPressAndTap];
  Touch.InteractiveGestureOptions := [igoPanInertia,
    igoPanSingleFingerHorizontal, igoPanSingleFingerVertical,
    igoParentPassthrough];
}

  if Canvas=nil then
     {$IF CompilerVersion>23}
     ChangePainter(TFMXPainter.Create(TCanvasManager.MeasureCanvas))
     {$ELSE}
     begin
       // if GetMeasureBitmap.Canvas.BeginScene then
          ChangePainter(TFMXPainter.Create(GetMeasureBitmap.Canvas));
     end
     {$IFEND}
  else
     ChangePainter(TFMXPainter.Create(Canvas));
end;

{$IFNDEF AUTOREFCOUNT}
Destructor TTeeGrid.Destroy;
begin
  FPainter.Free;

  FGrid.Data.Free;
  FGrid.Free;

  inherited;
end;
{$ENDIF}

procedure TTeeGrid.ResetScrollBars;
begin
  if Columns<>nil then
     if Grid.Rows.DefaultHeight=0 then
        Grid.Root.Rows.CalcDefaultHeight;

  inherited;
end;

procedure TTeeGrid.ChangePainter(const APainter:TFMXPainter);
begin
  FPainter:=APainter;
  Rows.Painter:=FPainter;
end;

procedure TTeeGrid.ColumnResized(Sender: TObject);
begin
  ResetScrollBars;

  if Assigned(FOnColumnResized) then
     FOnColumnResized(Self,Grid.Header.Dragging);
end;

procedure TTeeGrid.DblClick;
begin
  inherited;

  if Grid.Editing.DoubleClick then
     Grid.TryStartEditor;
end;

procedure TTeeGrid.ApplyStyle;
begin
  inherited;
end;

procedure TTeeGrid.Changed(Sender: TObject);
begin
  Repaint;
end;

procedure TTeeGrid.DoMouseLeave;
begin
  inherited;
  Grid.Header.HighLight:=nil;
end;

function TTeeGrid.GetAfterDraw: TNotifyEvent;
begin
  result:=Grid.OnAfterDraw;
end;

function TTeeGrid.GetBack: TFormat;
begin
  result:=Grid.Back;
end;

function TTeeGrid.GetCells: TTextRender;
begin
  result:=Grid.Cells;
end;

function TTeeGrid.GetClickedHeader: TNotifyEvent;
begin
  result:=Grid.Header.OnClick;
end;

function TTeeGrid.GetColumns: TColumns;
begin
  result:=Grid.Columns;
end;

function TTeeGrid.GetDataItem: TVirtualData;
begin
  result:=Grid.Data;
end;

function TTeeGrid.GetDataSource: TComponent;
begin
  result:=Grid.DataSource;
end;

function TTeeGrid.GetEditing: TGridEditing;
begin
  result:=Grid.Editing;
end;

function TTeeGrid.GetFooter: TGridBands;
begin
  result:=Grid.Footer;
end;

function TTeeGrid.GetHeader: TColumnHeaderBand;
begin
  result:=Grid.Header;
end;

function TTeeGrid.GetHeaders: TGridBands;
begin
  result:=Grid.Headers;
end;

function TTeeGrid.GetIndicator: TIndicator;
begin
  result:=Grid.Indicator;
end;

function TTeeGrid.GetSelected: TGridSelection;
begin
  result:=Grid.Selected;
end;

procedure TTeeGrid.KeyDown(var Key: Word; var KeyChar: WideChar; Shift: TShiftState);
var tmp : TKeyState;
begin
  inherited;

  tmp.Key:=Key;
  tmp.Shift:=Shift;
  tmp.Event:=TGridKeyEvent.Down;

  FGrid.Key(tmp);
end;

procedure TTeeGrid.Loaded;
begin
  inherited;
  Grid.DataSourceChanged;
end;

procedure TTeeGrid.MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Single);
var tmp : TMouseState;
begin
  inherited;

  if CanFocus then
     SetFocus;

  tmp.Shift:=Shift;
  tmp.X:=X;
  tmp.Y:=Y;

  case Button of
      TMouseButton.mbLeft: tmp.Button:=TGridMouseButton.Left;
     TMouseButton.mbRight: tmp.Button:=TGridMouseButton.Right;
    TMouseButton.mbMiddle: tmp.Button:=TGridMouseButton.Middle;
  end;

  if ssDouble in Shift then
     tmp.Event:=TGridMouseEvent.DoubleClick
  else
     tmp.Event:=TGridMouseEvent.Down;

  FGrid.Mouse(tmp);
end;

function CursorOf(const ACursor:TMouseCursor):TCursor; overload;
begin
  case ACursor of
      TMouseCursor.Default: result:=crDefault;
    TMouseCursor.HandPoint: result:=crHandPoint;
  TMouseCursor.HorizResize: result:=crSizeWE;
  else
    result:=crSizeNS;
  end;
end;

function CursorOf(const ACursor:TCursor):TMouseCursor; overload;
begin
  case ACursor of
      crDefault: result:=TMouseCursor.Default;
    crHandPoint: result:=TMouseCursor.HandPoint;
       crSizeWE: result:=TMouseCursor.HorizResize;
  else
    result:=TMouseCursor.VertResize;
  end;
end;

procedure TTeeGrid.MouseMove(Shift: TShiftState; X, Y: Single);
var tmp : TMouseState;
begin
  inherited;

  tmp.Cursor:=CursorOf(Cursor);

  tmp.Shift:=Shift;
  tmp.X:=X;
  tmp.Y:=Y;

  tmp.Event:=TGridMouseEvent.Move;

  FGrid.Mouse(tmp);

  Cursor:=CursorOf(tmp.Cursor);
end;

procedure TTeeGrid.MouseUp(Button: TMouseButton; Shift: TShiftState; X,Y: Single);
var tmp : TMouseState;
begin
  inherited;

  tmp.Shift:=Shift;
  tmp.X:=X;
  tmp.Y:=Y;

  case Button of
      TMouseButton.mbLeft: tmp.Button:=TGridMouseButton.Left;
     TMouseButton.mbRight: tmp.Button:=TGridMouseButton.Right;
    TMouseButton.mbMiddle: tmp.Button:=TGridMouseButton.Middle;
  end;

  tmp.Event:=TGridMouseEvent.Up;

  FGrid.Mouse(tmp);
end;

procedure TTeeGrid.MouseWheel(Shift: TShiftState; WheelDelta: Integer;
  var Handled: Boolean);

var tmp : TKeyState;
begin
  inherited;

  tmp.Shift:=Shift;
  tmp.Event:=TGridKeyEvent.Down;

  if WheelDelta>0 then
     tmp.Key:=vkUp
  else
     tmp.Key:=vkDown;

  Grid.Key(tmp);

  Handled:=True;
end;

type
  TFMXPainterAccess=class(TFMXPainter);

procedure TTeeGrid.Paint;
begin
  TFMXPainterAccess(FPainter).SetCanvas(Canvas);

  if Grid.Root.RecalcScrollBars then
     ResetScrollBars;

  inherited;

//  if not FGrid.Back.Brush.Visible then
//     Painter.Fill(TRectF.Create(BoundsRect),Color);

  FGrid.Current.IsFocused:=IsFocused;
  FGrid.Paint;

  FGrid.FinishPaint(csDesigning in ComponentState);

  if Editing.AlwaysVisible and FGrid.EditorShowing and (not Selected.IsEmpty) then
  begin
    FGrid.TryShowEditor(Selected.Column,Selected.Row);
    Changed(Self);
  end;
end;

procedure TTeeGrid.ReSize;
begin
  if Columns<>nil then
     if ScrollBars.Horizontal<>nil then
        if Data<>nil then
           ResetScrollBars;
end;

procedure TTeeGrid.SetScrollX(const Value:Single);
begin
  Rows.Scroll.X:=Value;
end;

procedure TTeeGrid.SetScrollY(const Value:Single);
begin
  Rows.Scroll.Y:=Value;
end;

function TTeeGrid.GetMaxBottom:Single;
begin
  result:=FGrid.Current.MaxBottom;
end;

function TTeeGrid.GetMaxRight:Single;
begin
  result:=Grid.Rows.VisibleColumns.MaxRight;
end;

function TTeeGrid.GetOnNewDetail: TNewDetailEvent;
begin
  result:=Grid.Root.OnNewDetail;
end;

function TTeeGrid.GetOnSelect: TNotifyEvent;
begin
  result:=Grid.OnSelect;
end;

function TTeeGrid.GetReadOnly: Boolean;
begin
  result:=Grid.ReadOnly;
end;

function TTeeGrid.GetRows: TRows;
begin
  result:=Grid.Rows;
end;

procedure TTeeGrid.SetAfterDraw(const Value: TNotifyEvent);
begin
  Grid.OnAfterDraw:=Value;
end;

procedure TTeeGrid.SetBack(const Value: TFormat);
begin
  Grid.Back:=Value;
end;

procedure TTeeGrid.SetCells(const Value: TTextRender);
begin
  Grid.Cells:=Value;
end;

procedure TTeeGrid.SetClickedHeader(const Value: TNotifyEvent);
begin
  Grid.Header.OnClick:=Value;
end;

procedure TTeeGrid.SetColumns(const Value: TColumns);
begin
  Grid.Columns:=Value;
end;

procedure TTeeGrid.SetDataItem(const Value: TVirtualData);
begin
  if Data<>Value then
  begin
    FGrid.Data:=Value;
    FGrid.RefreshData;
  end;
end;

procedure TTeeGrid.SetDataSource(const Value: TComponent);
begin
  Grid.DataSource:=Value;
end;

procedure TTeeGrid.SetEditing(const Value: TGridEditing);
begin
  Grid.Editing:=Value;
end;

procedure TTeeGrid.SetFooter(const Value: TGridBands);
begin
  Grid.Footer:=Value;
end;

procedure TTeeGrid.SetHeader(const Value: TColumnHeaderBand);
begin
  Grid.Header:=Value;
end;

procedure TTeeGrid.SetHeaders(const Value: TGridBands);
begin
  Grid.Headers:=Value;
end;

procedure TTeeGrid.SetIndicator(const Value: TIndicator);
begin
  Grid.Indicator:=Value;
end;

procedure TTeeGrid.SetOnNewDetail(const Value: TNewDetailEvent);
begin
  Grid.Root.OnNewDetail:=Value;
end;

procedure TTeeGrid.SetOnSelect(const Value: TNotifyEvent);
begin
  Grid.OnSelect:=Value;
end;

procedure TTeeGrid.SetReadOnly(const Value: Boolean);
begin
  Grid.ReadOnly:=Value;
end;

procedure TTeeGrid.SetRows(const Value: TRows);
begin
  Grid.Rows:=Value;
end;

procedure TTeeGrid.SetSelected(const Value: TGridSelection);
begin
  Grid.Selected:=Value;
end;

end.
