{*********************************************}
{  TeeGrid Software Library                   }
{  FMX Painter class                          }
{  Copyright (c) 2016 by Steema Software      }
{  All Rights Reserved                        }
{*********************************************}
unit FMXTee.Painter;
{$I Tee.inc}

interface

{$IF CompilerVersion>24}
{$DEFINE USELAYOUT}
{$IFEND}

uses
  System.Types, System.UITypes,

  {$IFDEF USELAYOUT}
  FMX.TextLayout,
  {$ENDIF}

  FMX.Types,

  {$IF CompilerVersion<=25}
  {$DEFINE HASFMX20}
  {$IFEND}

  {$IFNDEF HASFMX20}
  FMX.Graphics,
  {$ENDIF}

  Tee.Painter, Tee.Format;

{$IF CompilerVersion>26}
{$DEFINE D20}
{$IFEND}

type
  {$IF CompilerVersion<=25}
  FMXTFont=FMX.Types.TFont;
  FMXTBrush=FMX.Types.TBrush;
  {$ELSE}
  FMXTFont=FMX.Graphics.TFont;
  FMXTBrush=FMX.Graphics.TBrush;
  {$IFEND}

  TFMXPainter=class(TPainter)
  private
    ICanvas : TCanvas;

    IClipped : Array of TCanvasSaveState;

    {$IFDEF USELAYOUT}
    ILayout : TTextLayout;
    {$ENDIF}

    ISolidBrush : FMXTBrush;

    FFontColor : TColor;
    FHorizAlign : THorizontalAlign;
    FOpacity : Single;
    FVertAlign : TVerticalAlign;

    function GraphicOf(const APicture: TPicture):TBitmap;
    procedure TextSize(const AText:String; out AWidth,AHeight:Single);

    {$IFDEF USELAYOUT}
    procedure PrepareLayout(const R:TRectF; const AText:String);
    {$ENDIF}
  protected
    procedure SetCanvas(const ACanvas:TCanvas); inline;
  public
    Constructor Create(const ACanvas:TCanvas);
    Destructor Destroy; override;

    class procedure ApplyFont(const ASource:Tee.Format.TFont; const ADest:FMXTFont); static;

    procedure Clip(const R:TRectF); override;
    procedure UnClip; override;

    procedure HideBrush; override;

    procedure SetBrush(const ABrush:TBrush); override;
    procedure SetFont(const AFont:TFont); override;
    procedure SetHorizontalAlign(const Align:THorizontalAlign); override;
    procedure SetStroke(const AStroke:TStroke); override;
    procedure SetVerticalAlign(const Align:TVerticalAlign); override;

    procedure Draw(const R:TRectF); override;
    procedure Draw(const P:TPointsF); override;
    procedure Draw(const APicture: TPicture; const X, Y: Single); override;
    procedure Draw(const APicture: TPicture; const R: TRectF); override;
    procedure DrawEllipse(const R:TRectF); override;

    procedure Fill(const R:TRectF); override;
    procedure Fill(const R:TRectF; const AColor:TColor); override;
    procedure Fill(const P:TPointsF); override;
    procedure FillEllipse(const R:TRectF); override;

    procedure HorizontalLine(const Y,X0,X1:Single); override;
    procedure Line(const X0,Y0,X1,Y1:Single); override;
    procedure Lines(const P:TPointsF); override;
    procedure Paint(const AFormat:TFormat; const R:TRectF); override;
    procedure Paint(const AFormat:TFormat; const P:TPointsF); override;
    function TextHeight(const AText:String):Single; override;
    procedure TextOut(const X,Y:Single; const AText:String); override;
    function TextWidth(const AText:String):Single; override;
    procedure VerticalLine(const X,Y0,Y1:Single); override;
  end;

implementation

uses
  System.Classes,
  {$IF CompilerVersion>26}
  System.Math.Vectors,
  {$IFEND}
  FMX.Platform;

{ TFMXPainter }

const
  BrushKindNone=TBrushKind.{$IF CompilerVersion>26}None{$ELSE}bkNone{$IFEND};
  BrushKindSolid=TBrushKind.{$IF CompilerVersion>26}Solid{$ELSE}bkSolid{$IFEND};
  BrushKindGradient=TBrushKind.{$IF CompilerVersion>26}Gradient{$ELSE}bkGradient{$IFEND};
  BrushKindBitmap=TBrushKind.{$IF CompilerVersion>26}Bitmap{$ELSE}bkBitmap{$IFEND};

Constructor TFMXPainter.Create(const ACanvas:TCanvas);
begin
  inherited Create;
  FOpacity:=1;

  ICanvas:=ACanvas;

  {$IFDEF USELAYOUT}
  ILayout:=TTextLayoutManager.TextLayoutByCanvas(ICanvas.ClassType).Create(ICanvas);
  {$ENDIF}

  ISolidBrush:=FMXTBrush.Create(BrushKindSolid,TAlphaColors.White);
end;

Destructor TFMXPainter.Destroy;
begin
  ISolidBrush.Free;

  {$IFDEF USELAYOUT}
  ILayout.Free;
  {$ENDIF}
  inherited;
end;

class procedure TFMXPainter.ApplyFont(const ASource: Tee.Format.TFont;
  const ADest: FMXTFont);
begin
  ADest.Family:=ASource.Name;
  ADest.Size:=ASource.Size;
  ADest.Style:=ASource.Style;
end;

procedure TFMXPainter.Clip(const R: TRectF);
var L : Integer;
begin
  L:=Length(IClipped);
  SetLength(IClipped,L+1);
  IClipped[L]:=ICanvas.SaveState;

  ICanvas.IntersectClipRect(R);
end;

procedure TFMXPainter.Draw(const R: TRectF);
var tmp : TRectF;
    tmpThick,
    tmpW : Single;
begin
  tmpThick:=0.5*ICanvas.{$IFDEF D20}Stroke.Thickness{$ELSE}StrokeThickness{$ENDIF};

  tmpW:=tmpThick-0.5;

  tmp.Left:=R.Left+tmpW;
  tmp.Top:=R.Top+tmpW;

  tmp.Right:=R.Right-tmpThick;
  tmp.Bottom:=R.Bottom-tmpThick;

  ICanvas.DrawRect(tmp,0,0,AllCorners,FOpacity);
end;

procedure TFMXPainter.Fill(const R: TRectF);
begin
  ICanvas.FillRect(R,0,0,AllCorners,FOpacity);
end;

procedure TFMXPainter.HideBrush;
begin
  ICanvas.Fill.Kind:=BrushKindNone;
end;

procedure TFMXPainter.HorizontalLine(const Y, X0, X1: Single);
begin
  ICanvas.DrawLine(TPointF.Create(X0,Y),TPointF.Create(X1,Y),FOpacity);
end;

procedure TFMXPainter.Line(const X0, Y0, X1, Y1: Single);
begin
  ICanvas.DrawLine(TPointF.Create(X0,Y0),TPointF.Create(X1,Y1),FOpacity);
end;

// Wish: TFMXCanvas.DrawLines
procedure TFMXPainter.Lines(const P: TPointsF);
var tmp : TPathData;
    t : Integer;
begin
  tmp:=TPathData.Create;
  try
    tmp.MoveTo(P[0]);

    for t:=1 to High(P) do
        tmp.LineTo(P[t]);

    ICanvas.DrawPath(tmp,FOpacity);
  finally
    tmp.Free;
  end;
end;

type
  {$IF CompilerVersion>25}
  FMXGradient=FMX.Graphics.TGradient;
  FMXJoin=FMX.Graphics.TStrokeJoin;
  {$ELSE}
  FMXGradient=FMX.Types.TGradient;
  FMXJoin=FMX.Types.TStrokeJoin;
  {$IFEND}

  TBrushAccess=class(TBrush);

procedure TFMXPainter.SetBrush(const ABrush: TBrush);

  function ColorFrom(const AIndex:Integer):TAlphaColor;
  begin
    result:=TColorHelper.SwapCheck(ABrush.Gradient.Colors[AIndex].Color);
  end;

  function Color0:TAlphaColor;
  begin
    if ABrush.Gradient.Inverted then
       result:=ColorFrom(1)
    else
       result:=ColorFrom(0);
  end;

  function Color1:TAlphaColor;
  begin
    if ABrush.Gradient.Inverted then
       result:=ColorFrom(0)
    else
       result:=ColorFrom(1);
  end;

  procedure PrepareGradient(const AGradient:FMXGradient);

    procedure TransformRadial(const ATransform:TTransform);
    begin
      {
      ATransform.RotationCenter.X:=0.5+AGradient.Center.X*0.01;
      ATransform.RotationCenter.Y:=0.5+AGradient.Center.Y*0.01;

      ATransform.RotationAngle:=AGradient.Rotation;

      ATransform.Position.X:=0;
      ATransform.Position.Y:=0;
      }
    end;

    procedure TransformLinear(const Start,Stop:TPosition);
    begin
      case ABrush.Gradient.Direction of
        TGradientDirection.Vertical:
                     begin
                       Start.X:=0.5;
                       Stop.X:=0.5;
                       Start.Y:=0;
                       Stop.Y:=1;
                     end;
        TGradientDirection.Horizontal:
                     begin
                       Start.X:=0;
                       Stop.X:=1;
                       Start.Y:=0.5;
                       Stop.Y:=0.5;
                     end;
      TGradientDirection.Diagonal:
                     begin
                       Start.X:=0;
                       Stop.X:=1;
                       Start.Y:=0;
                       Stop.Y:=1;
                     end;
    TGradientDirection.BackDiagonal:
                     begin
                       Start.X:=0;
                       Stop.X:=1;
                       Start.Y:=1;
                       Stop.Y:=0;
                     end;
      else
        Start.X:=0;
        Start.Y:=0;
        Stop.X:=0;
        Stop.Y:=1;
      end;
    end;

  begin
    AGradient.Color:=Color0;
    AGradient.Color1:=Color1;

    if ABrush.Gradient.Direction=TGradientDirection.Radial then
    begin
      TransformRadial(AGradient.RadialTransform);
      AGradient.Style:=TGradientStyle.{$IF CompilerVersion>26}Radial{$ELSE}gsRadial{$IFEND}
    end
    else
    begin
      TransformLinear(AGradient.StartPosition,AGradient.StopPosition);
      AGradient.Style:=TGradientStyle.{$IF CompilerVersion>26}Linear{$ELSE}gsLinear{$IFEND};
    end;
  end;

begin
  if ABrush.Visible then
  begin
    if TBrushAccess(ABrush).HasGradient then
    begin
      ICanvas.Fill.Kind:=BrushKindGradient;
      PrepareGradient(ICanvas.Fill.Gradient);
    end
    else
    begin
      ICanvas.Fill.Kind:=BrushKindSolid;
      ICanvas.Fill.Color:=TColorHelper.SwapCheck(ABrush.Color);
    end;
  end
  else
    ICanvas.Fill.Kind:=BrushKindNone;
end;

procedure TFMXPainter.SetCanvas(const ACanvas: TCanvas);
begin
  ICanvas:=ACanvas;
end;

procedure TFMXPainter.SetFont(const AFont: TFont);
begin
  {$IFDEF USELAYOUT}
  ILayout.BeginUpdate;
  ApplyFont(AFont,ILayout.Font);
  ILayout.EndUpdate;
  {$ELSE}
  ApplyFont(AFont,ICanvas.Font);
  {$ENDIF}

  FFontColor:=TColorHelper.SwapCheck(AFont.Color);
end;

procedure TFMXPainter.SetHorizontalAlign(const Align: THorizontalAlign);
begin
  FHorizAlign:=Align;
end;

function DashOf(const AStyle:TStrokeStyle):TStrokeDash;
begin
  case AStyle of
     TStrokeStyle.Solid: result:=TStrokeDash.{$IF CompilerVersion>26}Solid{$ELSE}sdSolid{$IFEND};
      TStrokeStyle.Dash: result:=TStrokeDash.{$IF CompilerVersion>26}Dash{$ELSE}sdDash{$IFEND};
       TStrokeStyle.Dot: result:=TStrokeDash.{$IF CompilerVersion>26}Dot{$ELSE}sdDot{$IFEND};
   TStrokeStyle.DashDot: result:=TStrokeDash.{$IF CompilerVersion>26}DashDot{$ELSE}sdDashDot{$IFEND};
TStrokeStyle.DashDotDot: result:=TStrokeDash.{$IF CompilerVersion>26}DashDotDot{$ELSE}sdDashDotDot{$IFEND};
  else
    result:=TStrokeDash.{$IF CompilerVersion>26}Custom{$ELSE}sdCustom{$IFEND};
  end;
end;

procedure TFMXPainter.SetStroke(const AStroke: TStroke);

  function CalcJoin:FMXJoin;
  begin
    case AStroke.JoinStyle of
       TStrokeJoin.Round : result:=FMXJoin.{$IFDEF D20}Round{$ELSE}sjRound{$ENDIF};
       TStrokeJoin.Bevel : result:=FMXJoin.{$IFDEF D20}Bevel{$ELSE}sjBevel{$ENDIF};
    else
       result:=FMXJoin.{$IFDEF D20}Miter{$ELSE}sjMiter{$ENDIF};
    end;
  end;

  function CalcCap:TStrokeCap;
  begin
    if AStroke.EndStyle=TStrokeEnd.Flat then
       result:=TStrokeCap.{$IFDEF D20}Flat{$ELSE}scFlat{$ENDIF}
    else
       result:=TStrokeCap.{$IFDEF D20}Round{$ELSE}scRound{$ENDIF};
  end;

begin
  if AStroke.Visible then
  begin
    ICanvas.Stroke.Kind:=BrushKindSolid;
    ICanvas.Stroke.Color:=TColorHelper.SwapCheck(AStroke.Color);
    ICanvas.{$IFDEF D20}Stroke.Thickness{$ELSE}StrokeThickness{$ENDIF}:=AStroke.Size;
    ICanvas.{$IFDEF D20}Stroke.Dash{$ELSE}StrokeDash{$ENDIF}:=DashOf(AStroke.Style);

    ICanvas.{$IFDEF D20}Stroke.Join{$ELSE}StrokeJoin{$ENDIF}:=CalcJoin;
    ICanvas.{$IFDEF D20}Stroke.Cap{$ELSE}StrokeCap{$ENDIF}:=CalcCap;
  end
  else
    ICanvas.Stroke.Kind:=BrushKindNone;
end;

procedure TFMXPainter.SetVerticalAlign(const Align: TVerticalAlign);
begin
  FVertAlign:=Align;
end;

procedure TFMXPainter.Paint(const AFormat: TFormat; const R: TRectF);
begin
  if AFormat.Brush.Visible then
  begin
    SetBrush(AFormat.Brush);
    Fill(R);
  end;

  if AFormat.Stroke.Visible then
  begin
    SetStroke(AFormat.Stroke);
    Draw(R);
  end;
end;

procedure TFMXPainter.Paint(const AFormat: TFormat; const P: TPointsF);
begin
  if AFormat.Brush.Visible then
  begin
    SetBrush(AFormat.Brush);
    Fill(P);
  end;

  if AFormat.Stroke.Visible then
  begin
    SetStroke(AFormat.Stroke);
    Draw(P);
  end;
end;

{$IFDEF USELAYOUT}
procedure TFMXPainter.PrepareLayout(const R:TRectF; const AText:String);
begin
  ILayout.TopLeft:=R.TopLeft;
  ILayout.MaxSize:=PointF(R.Width,R.Height);
  ILayout.Text:=AText;
end;
{$ENDIF}

procedure TFMXPainter.TextSize(const AText:String; out AWidth,AHeight:Single);
{$IFDEF USELAYOUT}
var tmp : TRectF;
{$ENDIF}
begin
  {$IFDEF USELAYOUT}
  ILayout.BeginUpdate;
  PrepareLayout(RectF(0,0,10000,10000),AText);
  ILayout.EndUpdate;

  tmp:=ILayout.TextRect;

  AWidth:=tmp.Width;
  AHeight:=tmp.Height;
  {$ELSE}
  AWidth:=ICanvas.TextWidth(AText);
  AHeight:=ICanvas.TextHeight(AText);
  {$ENDIF}
end;

function TFMXPainter.TextHeight(const AText: String): Single;
var tmp : Single;
begin
  TextSize(AText,tmp,result);
//  result:=ICanvas.TextHeight(AText);
end;

procedure TFMXPainter.TextOut(const X, Y: Single; const AText: String);

  {$IFNDEF USELAYOUT}
  function CalcHorizAlign:TTextAlign;
  begin
    case FHorizAlign of
        THorizontalAlign.Left: result:=TTextAlign.{$IFDEF D20}Leading{$ELSE}taLeading{$ENDIF};
      THorizontalAlign.Center: result:=TTextAlign.{$IFDEF D20}Center{$ELSE}taCenter{$ENDIF};
    else
       result:=TTextAlign.{$IFDEF D20}Trailing{$ELSE}taTrailing{$ENDIF};
    end;
  end;
  {$ENDIF}

  function CalcRect:TRectF;
  var tmpW,
      tmpH : Single;
  begin
    TextSize(AText,tmpW,tmpH);

    if FHorizAlign=THorizontalAlign.Left then
       result:=TRectF.Create(X,Y,X+tmpW,Y+tmpH)
    else
    begin
      if FHorizAlign=THorizontalAlign.Center then
         result:=TRectF.Create(X-(0.5*tmpW),Y,X+(0.5*tmpW),Y+tmpH)
      else
         result:=TRectF.Create(X-tmpW,Y,X,Y+tmpH);
    end;
  end;

var R : TRectF;
begin
  R:=CalcRect;

  {$IFDEF USELAYOUT}
  ILayout.BeginUpdate;
  PrepareLayout(R,AText);
  ILayout.Color:=TColorHelper.SwapCheck(FFontColor);
  ILayout.EndUpdate;

  ILayout.RenderLayout(ICanvas);

  {$ELSE}
  ICanvas.Fill.Color:=TColorHelper.SwapCheck(FFontColor);
  ICanvas.Fill.Kind:=BrushKindSolid;
  ICanvas.FillText(R,AText,False,FOpacity,[],CalcHorizAlign);
  {$ENDIF}
end;

function TFMXPainter.TextWidth(const AText: String): Single;
var tmp : Single;
begin
  TextSize(AText,result,tmp);
//  result:=ICanvas.TextWidth(AText);
end;

procedure TFMXPainter.UnClip;
var L : Integer;
begin
  L:=Length(IClipped);

  if L>0 then
  begin
    ICanvas.RestoreState(IClipped[L-1]);
    SetLength(IClipped,L-1);
  end;
end;

procedure TFMXPainter.VerticalLine(const X, Y0, Y1: Single);
begin
  ICanvas.DrawLine(TPointF.Create(X,Y0),TPointF.Create(X,Y1),FOpacity);
end;

procedure TFMXPainter.Draw(const P: TPointsF);
begin
  ICanvas.DrawPolygon(TPolygon(P),FOpacity);
end;

procedure TFMXPainter.Fill(const P: TPointsF);
begin
  ICanvas.FillPolygon(TPolygon(P),FOpacity);
end;

procedure TFMXPainter.Fill(const R: TRectF; const AColor: TColor);
begin
  ISolidBrush.Color:=TColorHelper.SwapCheck(AColor);

  {$IF CompilerVersion>26}
  ICanvas.FillRect(R,0,0,AllCorners,FOpacity,ISolidBrush);
  {$ELSE}
  ICanvas.Fill:=ISolidBrush;
  ICanvas.FillRect(R,0,0,AllCorners,FOpacity);
  {$IFEND}
end;

procedure TFMXPainter.FillEllipse(const R: TRectF);
begin
  ICanvas.FillEllipse(R,FOpacity);
end;

procedure SetDefaultFontNameAndSize;

  procedure SetDefaultFont(const AName:String; const ASize:Single);
  begin
    Tee.Format.TFont.DefaultName:=AName;
    Tee.Format.TFont.DefaultSize:=ASize;
  end;

{$IFDEF D20}
var FontSvc : IFMXSystemFontService;
{$ENDIF}
begin
  {$IFDEF D20}
  if TPlatformServices.Current.SupportsPlatformService(IFMXSystemFontService, IInterface(FontSvc)) then
     SetDefaultFont(FontSvc.GetDefaultFontFamilyName,
                    {$IF CompilerVersion>26}FontSvc.GetDefaultFontSize{$ELSE}12{$IFEND})
  else
  {$ENDIF}
     SetDefaultFont('Segoe UI',12);
end;

type
  TPictureAccess=class(TPicture);

function TFMXPainter.GraphicOf(const APicture: TPicture):TBitmap;
begin
  if TPictureAccess(APicture).TagObject is TBitmap then
     result:=TBitmap(TPictureAccess(APicture).TagObject)
  else
     result:=nil;
end;

function BoundsOf(const ABitmap:TBitmap):TRectF;
begin
  {$IF CompilerVersion>30}
  result:=ABitmap.BoundsF;
  {$ELSE}
  result:=TRectF.Create(0,0,ABitmap.Width,ABitmap.Height);
  {$IFEND}
end;

procedure TFMXPainter.Draw(const APicture: TPicture; const X, Y: Single);
var tmp : TBitmap;
begin
  tmp:=GraphicOf(APicture);

  if tmp<>nil then
     ICanvas.DrawBitmap(tmp,BoundsOf(tmp),TRectF.Create(X,Y,X+tmp.Width,Y+tmp.Height),FOpacity);
end;

procedure TFMXPainter.Draw(const APicture: TPicture; const R: TRectF);
var tmp : TBitmap;
begin
  tmp:=GraphicOf(APicture);

  if tmp<>nil then
     ICanvas.DrawBitmap(tmp,BoundsOf(tmp),R,FOpacity);
end;

procedure TFMXPainter.DrawEllipse(const R: TRectF);
begin
  ICanvas.DrawEllipse(R,FOpacity);
end;

initialization
  SetDefaultFontNameAndSize;
end.

