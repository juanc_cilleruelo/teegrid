{*********************************************}
{  TeeGrid Software Library                   }
{  TStringGrid emulation data class           }
{  Copyright (c) 2016 by Steema Software      }
{  All Rights Reserved                        }
{*********************************************}
unit Tee.Grid.Data.Strings;
{$I Tee.inc}

// Force range-checking in Debug mode
{$IFOPT D+}
{$R+}
{$ENDIF}

interface

{
  "TStringGrid" emulator.

  A virtual data class for custom "Column x Row" grid of Cells[col,row].

  Usage:

    var Data : TStringsData;
    Data:= TStringsData.Create;

    // Initial size

    Data.Columns:= 5;
    Data.Rows:= 4;

    // Set data to grid

    TeeGrid1.Data:= Data;

    // Set header texts

    Data.Headers[1]:='Company';

    // Set cell values

    Data[1,1]:= 'Steema';

    // Optional events
    Data.OnGetValue:=...
    Data.OnSetValue:=...
}

uses
  {System.}Classes,
  Tee.Grid.Columns, Tee.Grid.Data, Tee.Painter;

type
  TOnGetValue=procedure(Sender:TObject; const AColumn,ARow:Integer; var AValue:String) of object;
  TOnSetValue=procedure(Sender:TObject; const AColumn,ARow:Integer; const AValue:String) of object;

  { TStringsData }

  TStringArray=Array of String;

  TStringsData=class(TVirtualData)
  private
    FColumns: Integer;
    FRows: Integer;

    IColumns : TColumns;
    IHeaders : TStringArray;
    IData : Array of TStringArray;

    FOnSetValue: TOnSetValue;
    FOnGetValue: TOnGetValue;

    {$IFOPT R+}
    procedure RangeCheck(const Column,Row: Integer);
    {$ENDIF}

    function GetCell(const Column,Row: Integer): String;
    function GetHeader(const Column: Integer): String;
    function IndexOf(const AColumn: TColumn):Integer; inline;
    procedure SetCell(const Column,Row: Integer; const Value: String);
    procedure SetColumns(const Value: Integer);
    procedure SetHeader(const Column: Integer; const Value: String);
    procedure SetRows(const Value: Integer);
  public
    Constructor Create(const AColumns:Integer=0; const ARows:Integer=0);

    procedure AddColumns(const AColumns:TColumns); override;
    function AsString(const AColumn:TColumn; const ARow:Integer):String; override;
    function AutoWidth(const APainter:TPainter; const AColumn:TColumn):Single; override;
    function Count:Integer; override;
    procedure Load; override;
    procedure Resize(const AColumns,ARows:Integer);
    procedure SetValue(const AColumn:TColumn; const ARow:Integer; const AText:String); override;

    property Cells[const Column,Row:Integer]:String read GetCell write SetCell; default;
    property ColumnList:TColumns read IColumns;
    property Headers[const Column:Integer]:String read GetHeader write SetHeader;

  //published
    property Columns:Integer read FColumns write SetColumns default 0;
    property Rows:Integer read FRows write SetRows default 0;

    property OnGetValue:TOnGetValue read FOnGetValue write FOnGetValue;
    property OnSetValue:TOnSetValue read FOnSetValue write FOnSetValue;
  end;

implementation

uses
  {System.}SysUtils;

{ TStringsData }

Constructor TStringsData.Create(const AColumns, ARows: Integer);
begin
  inherited Create;
  Resize(AColumns,ARows);
end;

procedure TStringsData.AddColumns(const AColumns: TColumns);
var t : Integer;
begin
  IColumns:=AColumns;

  AColumns.Clear;

  for t:=0 to FColumns-1 do
      AColumns.Add(IHeaders[t]).TagObject:=TObject(t);
end;

function TStringsData.IndexOf(const AColumn: TColumn):Integer;
begin
  result:=Integer(AColumn.TagObject);
end;

function TStringsData.AsString(const AColumn: TColumn;
  const ARow: Integer): String;
begin
  result:=Cells[IndexOf(AColumn),ARow];
end;

function TStringsData.AutoWidth(const APainter:TPainter; const AColumn: TColumn):Single;
var t,
    tmpIndex : Integer;
    tmpMax,
    tmp : Integer;
    tmpData : TStringArray;
begin
  tmpMax:=0;

  tmpIndex:=IndexOf(AColumn);

  if (tmpIndex>=0) and (tmpIndex<Length(IData)) then
  begin
    tmpData:=IData[tmpIndex];

    for t:=0 to Count-1 do
    begin
      tmp:=Length(tmpData[t]);

      if tmp>tmpMax then
         tmpMax:=tmp;
    end;
  end;

  result:=APainter.TextWidth('0')*tmpMax;
end;

function TStringsData.Count: Integer;
begin
  result:=FRows;
end;

procedure TStringsData.Load;
begin
end;

function TStringsData.GetCell(const Column,Row: Integer): String;
begin
  {$IFOPT R+}
  RangeCheck(Column,Row);
  {$ENDIF}

  result:=IData[Column,Row];

  if Assigned(FOnGetValue) then
     FOnGetValue(Self,Column,Row,result);
end;

function TStringsData.GetHeader(const Column: Integer): String;
begin
  result:=IHeaders[Column];
end;

{$IFOPT R+}
procedure TStringsData.RangeCheck(const Column,Row: Integer);

  procedure DoRaise;
  begin
    raise Exception.Create('Invalid Range at SetCell: '+IntToStr(Column)+' '+IntToStr(Row));
  end;

begin
  if (Column<0) or (Column>=Length(IData)) or
     (Row<0) or (Row>=Length(IData[Column])) then
     DoRaise
end;
{$ENDIF}

procedure TStringsData.SetCell(const Column,Row: Integer;
  const Value: String);
begin
  {$IFOPT R+}
  RangeCheck(Column,Row);
  {$ENDIF}

  IData[Column,Row]:=Value;

  if Assigned(FOnSetValue) then
     FOnSetValue(Self,Column,Row,Value);

  Repaint;
end;

procedure TStringsData.Resize(const AColumns,ARows:Integer);

  procedure SyncColumns;
  begin
    while IColumns.Count<FColumns do
          IColumns.Add;

    while IColumns.Count>FColumns do
          IColumns[IColumns.Count-1].{$IFDEF AUTOREFCOUNT}DisposeOf{$ELSE}Free{$ENDIF};
  end;

begin
  if (AColumns<>FColumns) or (ARows<>FRows) then
  begin
    FColumns:=AColumns;
    FRows:=ARows;

    if IColumns<>nil then
       SyncColumns;

    SetLength(IData,FColumns,FRows);
    SetLength(IHeaders,FColumns);

    Repaint;
  end;
end;

procedure TStringsData.SetColumns(const Value: Integer);
begin
  Resize(Value,FRows);
end;

procedure TStringsData.SetHeader(const Column: Integer;
  const Value: String);
begin
  IHeaders[Column]:=Value;

  if IColumns<>nil then
     IColumns[Column].Header.Text:=Value;
end;

procedure TStringsData.SetRows(const Value: Integer);
begin
  Resize(FColumns,Value);
end;

procedure TStringsData.SetValue(const AColumn: TColumn;
  const ARow: Integer; const AText: String);
begin
  Cells[IndexOf(AColumn),ARow]:=AText;
end;

end.

