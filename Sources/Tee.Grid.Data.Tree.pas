{*********************************************}
{  TeeGrid Software Library                   }
{  Tree Emulation Virtual Data class          }
{  Copyright (c) 2016 by Steema Software      }
{  All Rights Reserved                        }
{*********************************************}
unit Tee.Grid.Data.Tree;

interface

uses
  Tee.Grid, Tee.Grid.Columns, Tee.Painter, Tee.Renders, Tee.Grid.Data;

type
  PNode=^TNode;

  TNodes=Array of PNode;

  TNode=record
  public
    Data : TVirtualData;
    Text : String;
    Items : TNodes;

    function Add(const AText:String):PNode;
    function Count:Integer; inline;
  end;

  TTreeGridData=class(TVirtualData)
  private
    FItems : TNodes;

    class function NodeOf(const AColumn:TColumn):PNode; inline; static;
  protected
  public
    Constructor Create;
    Destructor Destroy; override;

    procedure AddColumns(const AColumns:TColumns); override;
    function AsFloat(const AColumn:TColumn; const ARow:Integer):TFloat; override;
    function AsString(const AColumn:TColumn; const ARow:Integer):String; override;
    function AutoWidth(const APainter:TPainter; const AColumn:TColumn):Single; override;

    function CanExpand(const Sender:TRender; const ARow:Integer):Boolean; override;
    function CanSortBy(const AColumn:TColumn):Boolean; override;

    function Count:Integer; override;

    function GetDetail(const ARow:Integer; const AColumns:TColumns; out AParent:TColumn): TVirtualData; override;

    function IsSorted(const AColumn:TColumn; out Ascending:Boolean):Boolean; override;

    procedure Load; override;
    procedure SetValue(const AColumn:TColumn; const ARow:Integer; const AText:String); override;
    procedure SortBy(const AColumn:TColumn); override;
  end;

implementation

uses
  System.Classes, System.SysUtils;

{ TTreeGridData }

Constructor TTreeGridData.Create;
begin
  inherited;
end;

function TTreeGridData.AutoWidth(const APainter:TPainter; const AColumn:TColumn):Single;

  function MaxItemLength(const AItems:TNodes):Integer;
  var t,
      tmp : Integer;
  begin
    result:=0;

    for t:=0 to Count-1 do
    begin
      tmp:=Length(FItems[t].Text);

      if tmp>result then
         result:=tmp;
    end;
  end;

  function AverageCharWidth:Single;
  begin
    result:=APainter.TextWidth('0');
  end;

begin
  result:=AverageCharWidth*MaxItemLength(FItems);
end;

procedure TTreeGridData.AddColumns(const AColumns: TColumns);
begin
  AColumns.Add('Items').TagObject:=Self;
end;

class function TTreeGridData.NodeOf(const AColumn:TColumn):PNode;
begin
  result:=PNode(AColumn.TagObject);
end;

Destructor TTreeGridData.Destroy;
begin
  inherited;
end;

function TTreeGridData.GetDetail(const ARow:Integer; const AColumns:TColumns;
                                 out AParent:TColumn): TVirtualData;

begin
  result:=TTreeGridData.Create;
  TTreeGridData(result).FItems:=FItems[ARow].Items;
end;

function TTreeGridData.CanExpand(const Sender:TRender; const ARow:Integer):Boolean;
begin
  result:=Count>0;
end;

procedure TTreeGridData.Load;
begin
end;

procedure TTreeGridData.SetValue(const AColumn: TColumn; const ARow: Integer;
  const AText: String);
begin
//  tmp:=NodeOf(AColumn);

//  tmpRow:=RowOf(ARow);
end;

function TTreeGridData.AsFloat(const AColumn: TColumn;
  const ARow: Integer): TFloat;
begin
  result:=StrToFloat(AsString(AColumn,ARow));
end;

function TTreeGridData.AsString(const AColumn:TColumn; const ARow:Integer):String;
begin
  result:=FItems[ARow].Text;
end;

function TTreeGridData.CanSortBy(const AColumn: TColumn): Boolean;
begin
  result:=False;
end;

function TTreeGridData.IsSorted(const AColumn:TColumn; out Ascending:Boolean):Boolean;
var tmp : PNode;
begin
  tmp:=NodeOf(AColumn);
  result:=(tmp<>nil) and tmp.Data.IsSorted(AColumn,Ascending);
end;

procedure TTreeGridData.SortBy(const AColumn:TColumn);
var tmpAsc : Boolean;
    tmp : PNode;
begin
  if IsSorted(AColumn,tmpAsc) then
     tmpAsc:=not tmpAsc
  else
     tmpAsc:=True;

  tmp:=NodeOf(AColumn);
  tmp.Data.SortBy(AColumn);
end;

function TTreeGridData.Count: Integer;
begin
  result:=Length(FItems);
end;

{ TNode }

function TNode.Add(const AText: String): PNode;
var L : Integer;
begin
  L:=Count;
  SetLength(Items,L+1);
  Items[L].Text:=AText;

  result:=@Items[L];
end;

function TNode.Count: Integer;
begin
  result:=Length(Items);
end;

end.
